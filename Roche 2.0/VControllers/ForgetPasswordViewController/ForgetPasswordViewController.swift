//
//  ForgetPasswordViewController.swift
//  RocheApp
//
//  Created by  Traffic MacBook Pro on 02/08/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation
class ForgetPasswordViewController: HeaderViewController,UITextFieldDelegate, AFWrapperDelegate {

    @IBOutlet var btnSend: buttonFontCustomClass!
    
    
    @IBOutlet var txtEmail: BaseUITextField!
    @IBOutlet var topConstraint : NSLayoutConstraint?

    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == txtEmail{
            txtEmail.resignFirstResponder()
        }
             return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        _headerView.heading = "Forgot Password"
        
        
        
        txtEmail.keyboardType = .EmailAddress
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON

        
        btnSend.addTarget(self, action: #selector(ForgetPasswordViewController.btnSendAction), forControlEvents: .TouchUpInside)
        
        
      //  _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }

    override func viewDidAppear(animated: Bool) {
        self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view .endEditing(true)
    }
    
   func btnSendAction(){
    print("Send Email")
    
    self.view .endEditing(true)
    
    if GlobalStaticMethods.isValidEmail(txtEmail.text!) == true {
    AFWrapper.delegate = self
    AFWrapper.serviceCall(AFUrlRequestHamza.ForgetPassword(email: txtEmail.text!), success: { (res) in
        
        printLog(res)
        
        
        let strMessage = res["statusMessage"].stringValue
        
        Alert.showAlertMsgWithTitle("Success", msg: strMessage, btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
         self.navigationController?.popViewControllerAnimated(true)
        })
        
        
        
        
        }) { (NSERROR) in
            printLog(NSERROR)
            
    }
    
    }
    else{
        
        Alert.showAlertMsgWithTitle("Error", msg: "Email not valid", btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
            print("")
            self.txtEmail.becomeFirstResponder()

        })
        

    }
    
    }
    
    func didTapOnOKayOnAFWrapperAlert(errorCode: Int) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
