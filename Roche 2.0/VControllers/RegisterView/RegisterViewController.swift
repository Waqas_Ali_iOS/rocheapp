    //
    //  RegisterViewController.swift
    //  RocheDesignsOnly
    //
    //  Created by  Traffic MacBook Pro on 26/07/2016.
    //  Copyright © 2016 My Macbook Pro. All rights reserved.
    //
    
    import UIKit
    import IQKeyboardManager
    import SwiftyJSON
    
    //import CoreActionSheetPicker
    class RegisterViewController: HeaderViewController,ProfileSettingsTableViewCellDelegate,STCustomDelegate,UITableViewDelegate,UITableViewDataSource {
        
        @IBOutlet var tblViewProfile : UITableView!
        @IBOutlet var topConstraint : NSLayoutConstraint?
        @IBOutlet var btnUpdateProfile : UIButton?
        var  suggestionTblView : UITableView?
        var  arrSugesstionData = Array<AnyObject>()
        
        var hasManager = false
        
        var isHospitalSelected = false
        
        var predictiveEmail = false
        var CSTable = STable()
        
        weak var timer: NSTimer?
        
        let arrProfile = ["firstName","lastName","email","password","confirmPassword","workNumber","mobileNumber","hospital","labManager","secondaryEmail","updateProfileButton"]
        
        let single = Singleton.sharedInstance
        
        var arrForSalesEmails = [AnyObject]()
        
        var arrForLabManagerEmails = Array<AnyObject>()
        var arrRegisterArr = Array<registerPropertyModelStruct>()
        
        
        override func viewDidLoad() {
            
            super.viewDidLoad()
            
            
            tblViewProfile.bounces = false
            _headerView.heading = "Register"
            
            _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
            
            btnUpdateProfile?.addTarget(self, action: #selector(RegisterViewController.goToHome), forControlEvents: .TouchUpInside)
            // Do any additional setup after loading the view.
            
            
            populateModelData()
            
        }
        
        override func viewDidAppear(animated: Bool) {
            
            self.mm_drawerController.openDrawerGestureModeMask = .None
            
            //
            //
            
            
        }
        
        
        func populateModelData(){
            
            arrRegisterArr = [
                
                registerPropertyModelStruct(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: "", placeholder: "First Name", leftImage: "gen-name-txtfield", rightImage: "", txtfieldValue: "", descriptionLabel : ""),
                registerPropertyModelStruct(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: "", placeholder: "Last Name", leftImage: "gen-name-txtfield", rightImage: "", txtfieldValue: "", descriptionLabel : ""),
                
                registerPropertyModelStruct(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.EmailAddress, validationType: txtFieldTypeEnum.email, value: "", placeholder: "Email", leftImage: "gen-email-txtfield", rightImage: "", txtfieldValue: "", descriptionLabel : "e.g. example@example.com"),
                
                registerPropertyModelStruct(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: "", placeholder: "Password", leftImage: "gen-password-txtfield", rightImage: "", txtfieldValue: "", descriptionLabel : "Tip: Choose a password with at least 8 characters (1 Uppercase & 1 Number) "),
                
                registerPropertyModelStruct(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: "", placeholder: "Confirm Password", leftImage: "gen-password-txtfield", rightImage: "", txtfieldValue: "", descriptionLabel : ""),
                
                registerPropertyModelStruct(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.PhonePad, validationType: txtFieldTypeEnum.phone, value: "", placeholder: "Work Number", leftImage: "gen-work-txtfield", rightImage: "", txtfieldValue: "", descriptionLabel : "e.g. +97123456789 OR 0097123456789"),
                
                registerPropertyModelStruct(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.PhonePad, validationType: txtFieldTypeEnum.phone, value: "", placeholder: "Mobile Number", leftImage: "gen-mobile-txtfield", rightImage: "", txtfieldValue: "", descriptionLabel : "e.g. +97123456789 OR 0097123456789"),
                
                registerPropertyModelStruct(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: "", placeholder: "Type Hospital Name", leftImage: "gen-hospital-txtfield", rightImage: "", txtfieldValue: "", descriptionLabel : ""),
                
                registerPropertyModelStruct(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: "", placeholder: "Select Job Title", leftImage: "gen-lab-txtfield", rightImage: "gen-downArrow", txtfieldValue: "", descriptionLabel : ""),
                
                
                registerPropertyModelStruct(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: "", placeholder: "Roche Representative Email", leftImage: "gen-email-txtfield", rightImage: "", txtfieldValue: "", descriptionLabel : "e.g. example@example.com"),
                
                registerPropertyModelStruct(cellType : registerPropertyFieldTypeEnum.kButton, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: "", placeholder: "Register", leftImage: "", rightImage: "", txtfieldValue: "", descriptionLabel : "")
                
                
            ]
            
            
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        //MARK: UITableViewDelegate
        
        func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            
            
            if tableView == suggestionTblView {
                
                let selectedCell = tableView.cellForRowAtIndexPath(indexPath)!
                let str = arrSugesstionData[indexPath.row] as! String
                if str == "No Record Found" {
                    suggestionTblView?.hidden = true
                    
                    if !predictiveEmail {
                        isHospitalSelected = false
                    }
                    
                    
                }
                else {
                    var row = 0
                    
                    if predictiveEmail == true{
                        row = 9
                    }
                    else{
                        row = 7
                    }
                    
                    let indexPathRow = NSIndexPath(forRow: row, inSection: 0)
                    
                    
                    
                    
                    
                    let originalCell = tblViewProfile.cellForRowAtIndexPath(indexPathRow)! as! ProfileSettingsTableViewCell
                    originalCell.txtFieldProfile.text = str
                    
                    
                    if row == 7 {
                        
                        
                        isHospitalSelected = true
                        
                        let      arrHospital = single.arrHospitalsName as! [String]
                        
                        let     idHospital : Int = arrHospital.indexOf(str)!
                        print(idHospital)
                        
                        let tempDic : NSDictionary = single.arrHospitals[idHospital] as! NSDictionary
                        
    
                        
                        hasManager = tempDic["hasManager"] as! Bool
                        arrForLabManagerEmails = [tempDic["salesrepresentativeemail"] as! String]
                        
                        
                        var indexPath = NSIndexPath.init(forRow: 8, inSection: 0)
                        var celloBj = tblViewProfile.cellForRowAtIndexPath(indexPath)! as! ProfileSettingsTableViewCell
                        celloBj.txtFieldProfile.text = ""
                        
                        indexPath = NSIndexPath.init(forRow: 9, inSection: 0)
                        
                        if tblViewProfile.cellForRowAtIndexPath(indexPath) != nil
                        {
                            celloBj = tblViewProfile.cellForRowAtIndexPath(indexPath)! as! ProfileSettingsTableViewCell
                            celloBj.txtFieldProfile.text = ""
                            
                            if hasManager {
                                celloBj.lblDescription!.text = ""
                                celloBj.txtFieldProfile.userInteractionEnabled = false
                            }
                            else
                            {
                                celloBj.lblDescription!.text = "e.g. example@roche.com"
                                celloBj.contentView.alpha = 1
                                celloBj.txtFieldProfile.userInteractionEnabled = true
                            }
                        }
                        
                        
                        
                        
                        
                        arrRegisterArr[8].txtfieldValue = ""
                        arrRegisterArr[8].value = ""
                        
                        arrRegisterArr[9].txtfieldValue = ""
                        arrRegisterArr[9].value = ""
                        
                        
                        
                        
                        let value = Int(idHospital) + 1
                        //                        arrRegisterArr[7].value = String(value)
                        
                        originalCell.arrMainDataReference.value = String(value)
                    }
                    else{
                        //                        arrRegisterArr[7].value = str
                        
                        originalCell.arrMainDataReference.value = str
                        
                    }
                    
                    suggestionTblView?.hidden = true
                }
            }
            else if tableView == tblViewProfile {
                
                suggestionTblView?.hidden = true
                self.view.endEditing(true)
                
            }
        }
        
        func scrollViewDidScroll(scrollView: UIScrollView) {
           
            if scrollView == self.tblViewProfile
            {
                self.suggestionTblView?.hidden = true
            }
            
            
            //self.suggestionTblView?.hidden = true
            //            self.view.endEditing(true)
            
        }
        
        func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
            
            if arrRegisterArr[8].txtfieldValue == "" {
                if indexPath.row == 9{
                    return 0
                }
                else{
                    if GlobalStaticMethods.isPadPro() {
                        if indexPath.row > 1 && indexPath.row < 7 {
                            if indexPath.row == 4 {
                                return 90
                            }
                            return   110 //DeviceUtil.size.width * 0.18841
                            
                        }else if indexPath.row == 9{
                            
                            return    110  //DeviceUtil.size.width * 0.18841
                            
                        }else if indexPath.row == 10 {
                            
                            return  160// DeviceUtil.size.width * 0.24155 // 100
                            
                        }
                        
                        
                        
                        return 90 //DeviceUtil.size.width * 0.13286
                        
                        
                        
                        
                        
                    }else if GlobalStaticMethods.isPad(){
                        
                        
                        if indexPath.row > 1 && indexPath.row < 7 {
                            if indexPath.row == 4 {
                                return 70
                            }
                            return   90 //DeviceUtil.size.width * 0.18841
                            
                        }else if indexPath.row == 9{
                            
                            return    90  //DeviceUtil.size.width * 0.18841
                            
                        }else if indexPath.row == 10 {
                            
                            return   120 //DeviceUtil.size.width * 0.24155 // 100
                            
                        }
                        
                        
                        
                        return 80 //DeviceUtil.size.width * 0.13286
                        
                        
                        
                        
                    }else {
                        
                        if indexPath.row > 1 && indexPath.row < 7 {
                            if indexPath.row == 4 {
                                return 58
                            }
                            else if indexPath.row == 3 {
                                return 90
                            }
                            return   78 //DeviceUtil.size.width * 0.18841
                            
                        }else if indexPath.row == 9{
                            
                            return    78  //DeviceUtil.size.width * 0.18841
                            
                        }else if indexPath.row == 10 {
                            
                            return   DeviceUtil.size.width * 0.24155 // 100
                            
                        }
                        
                        
                        
                        return 55 //DeviceUtil.size.width * 0.13286
                        
                    }
                    
                }
            }
            else{
                if GlobalStaticMethods.isPadPro() {
                    if indexPath.row > 1 && indexPath.row < 7 {
                        if indexPath.row == 4 {
                            return 90
                        }
                        return   110 //DeviceUtil.size.width * 0.18841
                        
                    }else if indexPath.row == 9{
                        
                        return    110  //DeviceUtil.size.width * 0.18841
                        
                    }else if indexPath.row == 10 {
                        
                        return  160// DeviceUtil.size.width * 0.24155 // 100
                        
                    }
                    
                    
                    
                    return 80 //DeviceUtil.size.width * 0.13286
                    
                    
                    
                    
                    
                }
                    
                else if GlobalStaticMethods.isPad(){
                    
                    
                    if indexPath.row > 1 && indexPath.row < 7 {
                        if indexPath.row == 4 {
                            return 70
                        }
                        return   90 //DeviceUtil.size.width * 0.18841
                        
                    }else if indexPath.row == 9{
                        
                        return    90  //DeviceUtil.size.width * 0.18841
                        
                    }else if indexPath.row == 10 {
                        
                        return   120 //DeviceUtil.size.width * 0.24155 // 100
                        
                    }
                    
                    
                    
                    return 80 //DeviceUtil.size.width * 0.13286
                    
                    
                    
                    
                }else {
                    
                    if indexPath.row > 1 && indexPath.row < 7 {
                        
                        if indexPath.row == 4  || indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 4 || indexPath.row == 7 || indexPath.row == 8 {
                            return 50
                        }
                        return   78 //DeviceUtil.size.width * 0.18841
                        
                    }else if indexPath.row == 9{
                        
                        return    78  //DeviceUtil.size.width * 0.18841
                        
                    }else if indexPath.row == 10 {
                        
                        return   DeviceUtil.size.width * 0.24155 // 100
                        
                    }
                    
                    
                    
                    return 55 //DeviceUtil.size.width * 0.13286
                    
                }
                
            }
            
        }
        
        //MARK: UITableViewDataSource
        
        
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if tableView == tblViewProfile{
                return arrRegisterArr.count
            }
            else{
                return arrSugesstionData.count
            }
        }
        
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            
            if tableView == tblViewProfile {
                
                
                          let count = arrRegisterArr.count
                let row = indexPath.row
                
                let cellData = arrRegisterArr[row]
                
                if indexPath.row < count-1 {
                    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! ProfileSettingsTableViewCell
//                    if indexPath.row % 2 == 0 {
//                        cell.backgroundColor = UIColor.redColor()
//                    }
//                    else{
//                        cell.backgroundColor = UIColor.brownColor()
//                    }

                    cell.txtFieldProfile.tag = indexPath.row
                    cell.selectionStyle = .None
                    cell.contentView.backgroundColor = UIColor.clearColor()
                    cell.configureCell(cell, forRowAtIndexPath: indexPath, Data:  cellData)
                    
                    
                    
                    cell.txtFieldProfile.tag = indexPath.row
                    
                    if arrForLabManagerEmails.count > 0 && hasManager && cell.txtFieldProfile.text == arrForLabManagerEmails[0] as! String {
                        
                        cell.txtFieldProfile.userInteractionEnabled = false
                        cell.contentView.alpha = 0.5
                    }
                    else
                    {
                        cell.contentView.alpha = 1
                    }
                    
                    
                    if indexPath.row == 3
                    {
                        cell.textFieldContentHeightConstrint?.constant = (cell.textFieldContentHeightConstrint?.constant)! + 10
                    }
                    
                    
                    
                    
                    cell.delegate = self
                    
                    return cell
                    
                }
                else{
                    let cell = tableView.dequeueReusableCellWithIdentifier("buttonCell", forIndexPath: indexPath) as! ProfileSettingsTableViewCell
                    cell.selectionStyle = .None
                    
                    cell.contentView.backgroundColor = UIColor.clearColor()
                    cell.configureCell(cell, forRowAtIndexPath: indexPath, Data:  cellData)
                    cell.delegate = self
                    return cell
                }
            }
            else{
                
                let cell = UITableViewCell()
                cell.backgroundColor = UIColor.clearColor()
                cell.layoutMargins = UIEdgeInsetsZero
                cell.textLabel?.textColor = UIColor(hexString: "#204C9B")
                var lblSize = 0.041063 * DeviceUtil.size.width
                
                if GlobalStaticMethods.isPad()
                {
                    lblSize = 0.01464128 * DeviceUtil.size.width
                }

                
                if arrSugesstionData.count > 0
                {
                    cell.textLabel?.text = arrSugesstionData[indexPath.row] as! String
                }
                else
                {
                    cell.textLabel?.text = "Please try latter no data found yet"
                }
                
                // cell.textLabel!.font = UIFont.init(name: "MarselisPro", size: 20)!
                
                return cell
            }
        }
        
        func btnPressedAction(buttonClass: ProfileSettingsTableViewCell) {
            print("Press me nah")
            
            goToHome()
        }
        
        
        
        override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
            
            
            super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
            
            
            coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
                // print("LOOOOOL")
                
                self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
                
            }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
                //  print("End")
                
            }
        }
        
        
        func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
            topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
            
            if GlobalStaticMethods.isPhone() {
                print("iphone changed orientation")
            }
            else{
                if GlobalStaticMethods.isPad() {
                    if GlobalStaticMethods.isPadPro() {
                        print("ipad pro changed orientation")
                        
                        
                        
                        if orientation.isLandscape {
                            //   self.viewSupplementHeightConstraint.constant = 45
                        }
                        else{
                            //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                            
                        }
                        
                    }
                    else{
                        if orientation.isLandscape {
                            //    self.viewSupplementHeightConstraint.constant = 50
                        }
                        else{
                            //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                            
                        }
                        
                        print("ipad changed orientation")
                    }
                }
            }
            
        }
        
        override func viewWillAppear(animated: Bool) {
            updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }
        
        
        func goToHome(){
            self.suggestionTblView?.hidden = true
            
            self.view.endEditing(true)
            validateFields()
            //  goToViewController(constants.viewControllers.attendenceVC)
            
            
            //  single.currentUser = userTypes.labTechnician
            
            
            
            //  NSNotificationCenter.defaultCenter().postNotificationName("reloadLeftMenu", object: nil)
            
        }
        
        
        func goToViewController(identifier : String){
            let sb = navigation.getStoryBoardForController(identifier)!.storyboardObject
            let homeVC = sb.instantiateViewControllerWithIdentifier(identifier)
            self.navigationController!.pushViewController(homeVC, animated: true)
            
        }
        
        
        func didTapOnPickerBTN(buttonClass: ProfileSettingsTableViewCell) {
            
            if buttonClass.txtFieldProfile.tag == 8{
                
                if isHospitalSelected {
                    var arr = ["Lab Manager", "Lab Supervisor", "Lab Technician"]
                    
                    if hasManager {
                        arr.removeAtIndex(0)
                    }
                    else
                    {
                        arr.removeLast(2)
                    }
                    
                    
                    
                    buttonClass.showPicker(arr , arrForID:  [])
                }
                else
                {
                    Alert.showAlertMsgWithTitle("Message", msg: "Please select hospital first", btnActionTitle: "OK", viewController: self, completionAction: { (VOID) in
                        
                    })
                }
                
                
            }
            
        }
        
        
        func didTypeOnPredictiveHospital(buttonClass: ProfileSettingsTableViewCell) {
            
            
            arrSugesstionData.removeAll()
            
            let strkeyword = buttonClass.txtFieldProfile.text!
            var diction = Dictionary<String,AnyObject>()
            
            buttonClass.arrMainDataReference.value = ""
            
            diction["strKeyword"] = strkeyword
            diction["indexPath"] = 7
            
            // ShowHideSuggestionTable()
            
            
            if strkeyword != "" {
                
                suggestionTblView?.hidden = true
                
                if (timer != nil)
                {
                    timer?.invalidate()
                }
                timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(RegisterViewController.validateTextField(_:)), userInfo: diction, repeats: false)
            }else{
                timer?.invalidate()
                suggestionTblView?.hidden = true
                
            }
            
            
            
        }
        
        func didTypeOnPredictiveEmail(buttonClass: ProfileSettingsTableViewCell) {
            
            
            arrSugesstionData.removeAll()
            
            
            let userType =   arrRegisterArr[8].value
            let strkeyword = buttonClass.txtFieldProfile.text!
            
            
            var diction = Dictionary<String,AnyObject>()
            
            diction["strKeyword"] = strkeyword
            diction["indexPath"] = 9
            
            //ShowHideSuggestionTable()
            
            if userType != "" {
                
                
                if strkeyword != "" {
                    if (timer != nil)
                    {
                        timer?.invalidate()
                    }
                    //    if searchResultModleObject?.arrData.count > 0 {
                    suggestionTblView?.hidden = true
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        self.timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(RegisterViewController.validateTextField(_:)), userInfo: diction, repeats: false)
                    }
                }else{
                    timer?.invalidate()
                    suggestionTblView?.hidden = true
                    //        getPredictiveEmail((Int(userType)! , keyword: strkeyword)
                    
                }
                
            }
            else {
                
                
                //Select User Type
                
                Alert.showAlertMsgWithTitle("Error", msg: "Select User Type", btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
                    printLog("Success")
                    self.suggestionTblView?.hidden = true
                    
                })
                
            }
            
            
        }
        
        func validateTextField(time : NSTimer){
            self.suggestionTblView?.hidden = true
            
            if single.arrEmails.count > 0{
                let arr = single.arrEmails[0]
                
               // arrForLabManagerEmails = arr["labManagerEmails"]!
                arrForSalesEmails = arr["salesRepEmails"]!
                
                
                //   arrForLabManagerEmails = ["labManagerEmails"]! as! [AnyObject]
                // arrForSalesEmails = single.arrEmails[0]["salesRepEmails"]! as! [AnyObject]
                
                //        var arrForSearch = Array<JSON>()
                
                
                
                var arrForSearch = Array<AnyObject>()
                let dict = time.userInfo as! Dictionary<String,AnyObject>
                
                
                let indexPath = dict["indexPath"] as! Int
                
                if indexPath == 7 {
                    
                    let dict = single.arrHospitalsName
                    
                    arrForSearch = dict
                    
                    predictiveEmail = false
                    
                }
                else if indexPath == 9 {
                    predictiveEmail = true
                    
                    
                    if Int(arrRegisterArr[8].value) == managerEnum.labTechnician.rawValue {
                        
                        arrForSearch =   arrForLabManagerEmails
                        
                    }
                    else if Int(arrRegisterArr[8].value) == managerEnum.labSupervisor.rawValue {
                        
                        arrForSearch =   arrForLabManagerEmails
                        
                    }
                    else if Int(arrRegisterArr[8].value) == managerEnum.labManager.rawValue {
                        
                        arrForSearch =   arrForSalesEmails
                        
                    }
                }
                
                
                arrSugesstionData.removeAll();
                
                
                
                let str = dict["strKeyword"] as! String
                
                let namePredicate = NSPredicate(format: "SELF CONTAINS[c] %@", str)
                
                arrSugesstionData =  (arrForSearch.filter { namePredicate.evaluateWithObject($0) });
                
                
                
                if arrSugesstionData.count == 0
                {
                    arrSugesstionData.append("No Record Found")
                    
                    //    suggestionTblView?.hidden = true
                    
                }
                 CreatTableForSuggestion(indexPath)
                
            }
            else{
                
                arrSugesstionData.removeAll()
                
                
            }
            
            
        }
        
        
        func CreatTableForSuggestion(row : Int)
        {
            let indexPath = NSIndexPath(forRow: row, inSection: 0)
            
            let cell = tblViewProfile.cellForRowAtIndexPath(indexPath) as! ProfileSettingsTableViewCell
            
            
            cell.tag = row
            if suggestionTblView == nil
            {
                
                let txtFieldframe : CGRect = cell.txtFieldProfile.frame;
                
                let yPosition = getCellHeight(row) - (44 * 4)
                
                var ratio : CGFloat = 1.1
                
                if GlobalStaticMethods.isPad() {
                    ratio = 1.05
                }
                
                let tblView = UITableView(frame: CGRectMake(txtFieldframe.origin.x, yPosition , txtFieldframe.size.width * ratio , 44 * 4), style: .Plain)
                
                if #available(iOS 9, *) {
                    tblView.cellLayoutMarginsFollowReadableWidth = false
                }
                
                
                //            tblView.backgroundColor = UIColor.lightGrayColor()
                
                tblView.center = CGPointMake(self.view  .center.x, tblView.center.y)
                
                
                tblView.layer.borderColor = UIColor.lightGrayColor().CGColor
                tblView.layer.borderWidth = 1.0
                
                tblView.layer.cornerRadius = 10
                
                tblView.dataSource = self
                tblView.delegate = self
                
                tblView.layoutMargins = UIEdgeInsetsZero
                tblView.separatorInset = UIEdgeInsetsZero
                
                
                tblView.backgroundColor = UIColor.whiteColor()
                
                tblView.bounces = false
                
                
                
                suggestionTblView = tblView;
                
                suggestionTblView?.hidden = true
                
                self.view.addSubview(suggestionTblView!)
            }
            
            if suggestionTblView?.hidden == true
            {
                
                let yPosition = getCellHeight(row) - (44 * 4)
                
                //                let tblView = UITableView(frame: CGRectMake(txtFieldframe.origin.x, yPosition , txtFieldframe.size.width * ratio , 44 * 4), style: .Plain)
                
                suggestionTblView?.frame.origin.y = yPosition
                
                
                suggestionTblView?.hidden = false
            }
            suggestionTblView?.reloadData()
            
            
            
            
            
            //  cell.txtField.activityIndicator.stopAnimating()
            
        }
        
        func getCellHeight(row : Int) -> CGFloat
        {
            
            
            let indexPath = NSIndexPath(forRow: row, inSection: 0)
            let myRect : CGRect = tblViewProfile.rectForRowAtIndexPath(indexPath)
            
            
            let myrecTtwo : CGRect = tblViewProfile.convertRect(myRect, toView:  tblViewProfile.superview)
            
            return myrecTtwo.origin.y
            
        }
        
        
        
        func didTapOnPickerDoneBTN(cell: ProfileSettingsTableViewCell) {
            let userType =  cell.arrMainDataReference.value
            
            let str = Int(cell.arrMainDataReference.value)
            if str == managerEnum.labTechnician.rawValue {
                
                arrRegisterArr[9].placeholder = "Roche Representative Email"
                arrRegisterArr[9].descriptionLabel = "               "
                
            }
            else if str == managerEnum.labSupervisor.rawValue {
                arrRegisterArr[9].placeholder = "Roche Representative Email"
                arrRegisterArr[9].descriptionLabel = "                "
                
                
            }
                
            else if str == managerEnum.labManager.rawValue {
                arrRegisterArr[9].placeholder = "Roche Representative Email"
                arrRegisterArr[9].descriptionLabel = "e.g. example@roche.com"
                
            }
            
            let indexPath = NSIndexPath(forRow: 9, inSection: 0)
            
            if hasManager {
                arrRegisterArr[9].txtfieldValue = arrForLabManagerEmails[0] as! String
                arrRegisterArr[9].value = arrForLabManagerEmails[0] as! String
            }

            self.tblViewProfile.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
 
        }
        
        
        func ShowHideSuggestionTable()
        {
            if suggestionTblView?.hidden == true {
                suggestionTblView?.hidden = false
            }
            else
            {
                suggestionTblView?.hidden = true
            }
            
        }
        
        
        
        func numberOfItemsInSelectionList(cSlider: STable, selectedObject: AnyObject) {
            let indexPath = NSIndexPath(forRow: 9, inSection: 0)
            let cell = self.tblViewProfile .cellForRowAtIndexPath(indexPath) as! ProfileSettingsTableViewCell// [self.tableView cellForRowAtIndexPath:nowIndex];
            
            cell.txtFieldProfile.text = selectedObject as? String
            cell.arrMainDataReference.value = selectedObject as! String
            cell.arrMainDataReference.txtfieldValue = selectedObject as! String
            
            
            tblViewProfile.reloadData()
        }
        
        
        
        func validateFields()->Bool{
            
            let strFirstName = arrRegisterArr[cellList.firstName.rawValue].txtfieldValue
            let strLastName = arrRegisterArr[cellList.lastName.rawValue].txtfieldValue
            let strEmail = arrRegisterArr[cellList.email.rawValue].txtfieldValue
            let strPassword = arrRegisterArr[cellList.password.rawValue].txtfieldValue
            let strConfirmPassword = arrRegisterArr[cellList.confirmPassword.rawValue].txtfieldValue
            let strWorkNumber  = arrRegisterArr[cellList.workNumber.rawValue].txtfieldValue
            let strMobileNumber = arrRegisterArr[cellList.mobileNumber.rawValue].txtfieldValue
            let strHospital = arrRegisterArr[cellList.hospital.rawValue].value
            //        let strLabManager = arrRegisterArr[cellList.labManager.rawValue].value
            //        let strSecondaryEmail = arrRegisterArr[cellList.secondaryEmail.rawValue].txtfieldValue
            
            
            var strMessage = ""
            var index = 0
            
            
            
            if strFirstName == ""{
                
                index = 0
                strMessage = "First Name can not be empty"
                
                registerUser(false, index: cellList.firstName.rawValue, errorMessage: strMessage)
                return false
                
                
            }
            
            if GlobalStaticMethods.isLettersANDSpacesOnly(strFirstName) == false {
                index = 0
                strMessage = "Letters only."
                registerUser(false, index: cellList.firstName.rawValue, errorMessage: strMessage)
                return false
                
            }
            if strLastName == ""{
                
                index = cellList.lastName.rawValue
                strMessage = "Last Name can not be empty"
                registerUser(false, index: cellList.lastName.rawValue, errorMessage: strMessage)
                return false
                
                
            }
            
            if GlobalStaticMethods.isLettersANDSpacesOnly(strLastName) == false {
                index = cellList.lastName.rawValue
                strMessage = "Letters only."
                registerUser(false, index: cellList.lastName.rawValue, errorMessage: strMessage)
                return false
                
            }
            if strEmail == ""{
                
                index = cellList.email.rawValue
                strMessage = "Email can not be empty"
                registerUser(false, index: cellList.email.rawValue, errorMessage: strMessage)
                return false
                
            }
            
            if GlobalStaticMethods.isValidEmail(strEmail) == false {
                index = cellList.email.rawValue
                strMessage = "Invalid Email"
                registerUser(false, index: cellList.firstName.rawValue, errorMessage: strMessage)
                return false
                
            }
            if strPassword == ""{
                
                index = cellList.password.rawValue
                strMessage = "Password can not be empty. Choose a password with at least 8 characters including 1 number , 1 special characterr and 1 Uppercase character"
                registerUser(false, index: cellList.password.rawValue, errorMessage: strMessage)
                return false
                
                
            }
            if strPassword.characters.count < 8{
                
                index = cellList.password.rawValue
                strMessage = "Choose a password with at least 8 characters (1 number & 1 Uppercase)"
                registerUser(false, index: cellList.password.rawValue, errorMessage: strMessage)
                return false
                
                
            }
            
            if GlobalStaticMethods.isValidPassword(strPassword) != true{
                index = cellList.password.rawValue
                strMessage = "Choose a password with at least 8 characters (1 number & 1 Uppercase)"
                registerUser(false, index: cellList.password.rawValue, errorMessage: strMessage)
                return false
            }
            
            if strConfirmPassword == ""{
                
                index = cellList.confirmPassword.rawValue
                strMessage = "Confirm your password"
                
                
                
                registerUser(false, index: cellList.confirmPassword.rawValue, errorMessage: strMessage)
                return false
                
                
                
            }
            if strPassword != strConfirmPassword{
                
                index = cellList.confirmPassword.rawValue
                strMessage = "Passwords does not match"
                registerUser(false, index: cellList.confirmPassword.rawValue, errorMessage: strMessage)
                return false
                
                
                
            }
            
            if strWorkNumber == ""{
                
                index = cellList.workNumber.rawValue
                strMessage = "Enter work number"
                registerUser(false, index: cellList.workNumber.rawValue, errorMessage: strMessage)
                return false
                
                
                
            }
            
            if GlobalStaticMethods.isPhoneNumber(strWorkNumber) == false {
                index = cellList.workNumber.rawValue
                strMessage = "Enter valid work number"
                registerUser(false, index: cellList.workNumber.rawValue, errorMessage: strMessage)
                return false
                
                
            }
            if strMobileNumber == ""{
                
                index = cellList.mobileNumber.rawValue
                strMessage = "Enter mobile number"
                registerUser(false, index: cellList.mobileNumber.rawValue, errorMessage: strMessage)
                return false
                
            }
            
            if GlobalStaticMethods.isPhoneNumber(strMobileNumber) == false {
                index = cellList.mobileNumber.rawValue
                strMessage = "Enter valid mobile number"
                registerUser(false, index: cellList.mobileNumber.rawValue, errorMessage: strMessage)
                return false
                
                
            }
            
            if arrRegisterArr[cellList.hospital.rawValue].value == "" {
                index = cellList.hospital.rawValue
                strMessage = "Select hospital from list"
                registerUser(false, index: cellList.hospital.rawValue, errorMessage: strMessage)
                return false
                
            }
            
            if arrRegisterArr[cellList.labManager.rawValue].txtfieldValue == "" {
                index = cellList.labManager.rawValue
                strMessage = "Select Job Title"
                registerUser(false, index: cellList.labManager.rawValue, errorMessage: strMessage)
                return false
                
            }
            
            if arrRegisterArr[cellList.secondaryEmail.rawValue].txtfieldValue == "" {
                index = cellList.secondaryEmail.rawValue
                strMessage = "Select secondaryEmail"
                registerUser(false, index: cellList.secondaryEmail.rawValue, errorMessage: strMessage)
                return false
                
            }
            
            if hasManager == false {
                
                let temsalesMangerEmail = arrForSalesEmails as! [String]
                
                if !temsalesMangerEmail.contains(arrRegisterArr[cellList.secondaryEmail.rawValue].txtfieldValue)
                {
                    index = cellList.secondaryEmail.rawValue
                    strMessage = "Select email from list"
                    registerUser(false, index: cellList.secondaryEmail.rawValue, errorMessage: strMessage)
                    return false
                }
            }
            
            //        if GlobalStaticMethods.isValidEmail(arrRegisterArr[cellList.secondaryEmail.rawValue].txtfieldValue) {
            //            index = cellList.secondaryEmail.rawValue
            //            strMessage = "Invalid Email"
            //            registerUser(false, index: cellList.secondaryEmail.rawValue, errorMessage: strMessage)
            //            return false
            //
            //        }
            
            registerUser(true, index: 0, errorMessage: "")
            
            
            return true
            
        }
        
        func registerUser(valid : Bool , index : Int , errorMessage : String){
            
            
            if valid == false{
                Alert.showAlertMsgWithTitle("Error", msg: errorMessage, btnActionTitle: "Ok", viewController: self) { (success) in
                    printLog("")
                }
                suggestionTblView?.hidden = true
            }
                
            else {
                
                
                let strFirstName = arrRegisterArr[cellList.firstName.rawValue].txtfieldValue
                let strLastName = arrRegisterArr[cellList.lastName.rawValue].txtfieldValue
                let strEmail = arrRegisterArr[cellList.email.rawValue].txtfieldValue
                let strPassword = arrRegisterArr[cellList.password.rawValue].txtfieldValue
                let strConfirmPassword = arrRegisterArr[cellList.confirmPassword.rawValue].txtfieldValue
                let strWorkNumber  = arrRegisterArr[cellList.workNumber.rawValue].txtfieldValue
                let strMobileNumber = arrRegisterArr[cellList.mobileNumber.rawValue].txtfieldValue
                let strHospital = arrRegisterArr[cellList.hospital.rawValue].value
                let strLabManager = arrRegisterArr[cellList.labManager.rawValue].value
                let strSecondaryEmail = arrRegisterArr[cellList.secondaryEmail.rawValue].txtfieldValue
        
                var dict = Dictionary<String,AnyObject>()
                
                if Int(strLabManager) == managerEnum.labManager.rawValue{
                    
                    dict = ["firstName":strFirstName, "lastName":strLastName, "email":strEmail, "password":strPassword, "confirmPassword":strConfirmPassword, "workNumber":strWorkNumber, "mobileNumber":strMobileNumber, "hospitalId":strHospital, "jobCourseTitle":strLabManager, "salesRepEmail":strSecondaryEmail,"labManagerEmail":""]
                }
                else {
                    dict = ["firstName":strFirstName, "lastName":strLastName, "email":strEmail, "password":strPassword, "confirmPassword":strConfirmPassword, "workNumber":strWorkNumber, "mobileNumber":strMobileNumber, "hospitalId":strHospital, "jobCourseTitle":strLabManager, "labManagerEmail":strSecondaryEmail ,"salesRepEmail":strSecondaryEmail]
                }
                
                AFWrapper.serviceCall(AFUrlRequestHamza.Register(dict: dict), success: { (res) in
                    printLog(res)
                    
                    if res["statusCode"].intValue == statusCode.success.rawValue {
                        
                        
                        Alert.showAlertMsgWithTitle("Success", msg: "Your account has been created . You will soon receive a confirmation email.", btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
                            self.navigationController?.popViewControllerAnimated(true)
                        })
                    }
                    
                    
                    
                    
                    }, failure: { (NSError) in
                        printLog(NSError)
                })
            }
        }
        
        enum cellList : Int {
            case firstName = 0
            case lastName
            case email
            case password
            case confirmPassword
            case workNumber
            case mobileNumber
            case hospital
            case labManager
            case secondaryEmail
            
        }
        
        enum managerEnum : Int {
            
            case  labManager = 1
            case labSupervisor = 2
            case  labTechnician = 3
        }
        
        
    }
    
    
