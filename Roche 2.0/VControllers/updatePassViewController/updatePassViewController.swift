//
//  updatePassViewController.swift
//  RocheApp
//
//  Created by  Traffic MacBook Pro on 02/08/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class updatePassViewController: HeaderViewController,UITextFieldDelegate {

    @IBOutlet var txtConfirmPass: BaseUITextField!
    @IBOutlet var txtPassword: BaseUITextField!
    @IBOutlet var btnSubmit: buttonFontCustomClass!
    
    @IBOutlet var topConstraint : NSLayoutConstraint?
    let single = Singleton.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        _headerView.heading = "Update Password"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON

        
        btnSubmit.addTarget(self, action: #selector(updatePassViewController.submit), forControlEvents: .TouchUpInside)
        // Do any additional setup after loading the view.
        
        txtPassword.secureTextEntry = true
        txtConfirmPass.secureTextEntry = true
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func submit()
    {
        self.view .endEditing(true)
        
        if validateForm()
        {
            AFWrapper.serviceCall(AFUrlRequestSharjeel.ResetPassword(strPassword: txtPassword.text!), success: { (res) in
                
                let result = res["data"].dictionary!
                
                let user = UserModel.sharedInstance
                
                user.sessionID = result["sessionId"]!.stringValue
                user.userID =  result["userId"]!.intValue
                user.userName = result["name"]!.stringValue
                user.userEmail = result["email"]!.stringValue
                user.userType = userTypes(rawValue: result["userType"]!.intValue)
                
                // Create Url from string
                let url = NSURL(string: result["qrImage"]!.stringValue)!
                
                // Download task:
                // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
                let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (responseData, responseUrl, error) -> Void in
                    // if responseData is not null...
                    if let data = responseData{
                        
                        // execute in UI thread
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            user.userQRimage = UIImage(data: data)
                            
                            AFWrapper.stopActivityAnimatingNew()
                          //  if res["isForceUpdate"].intValue == 0 {
                                if UserModel.sharedInstance.userType == userTypes.Instructor{
                                    self.goToAttendence()
                               
                                  
                                    
                                    
                                }else{
                                    self.goToHome()
                                
                                   
                                }
                            //}
                        })
                    }else{
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            
                            AFWrapper.stopActivityAnimatingNew()
                          //  if res["isForceUpdate"].intValue == 0 {
                                if UserModel.sharedInstance.userType == userTypes.Instructor{
                                    self.goToAttendence()
                                    
                                    
                                }else{
                                    self.goToHome()
                                }
                            //}
                        })
                    }
                }
                
                // Run task
                task.resume()
                
                user.unreadMessageCount = result["unreadMessageCount"]!.intValue
                
                
                }, failure: { (NSError) in
                    
                    Alert.showAlertMsgWithTitle("Error", msg:"Failed to update password, please try agin later", btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
                    })
            })
        }
        
    }
    
    func validateForm() -> Bool {
        
        if txtPassword.text?.characters.count == 0 {
            
            Alert.showAlertMsgWithTitle("Error", msg:"Password required", btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
            })
            
            return false
        }
        else if GlobalStaticMethods.isValidPassword(txtPassword.text!) != true{
                
               let  strMessage = "Choose a password with at least 8 characters (1 number & 1 Uppercase)"
            
            Alert.showAlertMsgWithTitle("Error", msg:strMessage, btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
            })
                return false
                
            }
        if txtPassword.text != txtConfirmPass.text {
        
            Alert.showAlertMsgWithTitle("Error", msg:"Password and confirm password should be same", btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
            })
            return false
        }
        
        
        return true
    }

    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view .endEditing(true)
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == txtPassword {
            txtConfirmPass .becomeFirstResponder()
        }
        else{
            txtConfirmPass .resignFirstResponder()
        }
        
        return true
        
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    

    func goToHome(){
        
        goToViewController(constants.viewControllers.homeContainerViewController)
        single.currentUser = UserModel.sharedInstance.userType
        NSNotificationCenter.defaultCenter().postNotificationName("reloadLeftMenu", object: nil)
        
    }
    
    
    func goToAttendence(){
        
        goToViewController(constants.viewControllers.attendenceVC)
        single.currentUser = UserModel.sharedInstance.userType
        NSNotificationCenter.defaultCenter().postNotificationName("reloadLeftMenu", object: nil)
        
        
        
    }
    
    func goToViewController(identifier : String){
        let sb = navigation.getStoryBoardForController(identifier)!.storyboardObject
        let homeVC = sb.instantiateViewControllerWithIdentifier(identifier)
        self.navigationController!.pushViewController(homeVC, animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
