//
//  tutorialViewController.swift
//  RocheDesignsOnly
//
//  Created by  Traffic MacBook Pro on 26/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class tutorialViewController: UIViewController {
    @IBOutlet   var kevView : kevTutorialView!

    @IBOutlet var btnSkip : UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        kevView.arrTutorialImage = ["tutorial-image-one","tutorial-image-two","tutorial-image-three","tutorial-image-four"]
//        kevView.arrTutorialImage = ["tutorial-image-one-pro","tutorial-image-one-pro","tutorial-image-one-pro","tutorial-image-one-pro"]

        
        btnSkip.addTarget(self, action: #selector(tutorialViewController.skip), forControlEvents: .TouchUpInside)
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
    //    NSNotificationCenter.defaultCenter().addObserver(kevView, selector: #selector(kevTutorialView.setConstraintsWithWidth), name: UIApplicationDidChangeStatusBarOrientationNotification, object: nil)
        self.mm_drawerController.openDrawerGestureModeMask = .None

        
    }
    override func viewDidDisappear(animated: Bool) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func skip(){
        let navCon = leftMenu.getMainViewController(self.mm_drawerController)
        
        navigation.setViewControllerArray(constants.viewControllers.loginViewController, navCon: navCon)
// self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
