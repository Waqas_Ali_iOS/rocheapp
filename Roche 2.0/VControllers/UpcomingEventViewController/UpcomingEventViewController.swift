 
//
//  UpcomingEventViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/11/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit
import SwiftyJSON

 
class UpcomingEventViewController: HeaderViewController, upcommingTableViewCellDelegate,AFWrapperDelegate,UITableViewDelegate {

    @IBOutlet weak var tableview : UITableView!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    
    var  arrData = [AnyObject]()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableview.hidden = true
        
        tableview.bounces = false
      
        
        callService()
        
        
        _headerView.heading = "Upcoming Events"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        if GlobalStaticMethods.isPhone() {
            self.tableview.rowHeight = UITableViewAutomaticDimension
            self.tableview.estimatedRowHeight = 140
        }
        
        
        
        
        // Do any additional setup after loading the view.
    }

 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.mm_drawerController.openDrawerGestureModeMask = .None

        self.tableview .reloadData()
        
        
        self.performSelector(#selector(setUI), withObject: nil, afterDelay: 0.2)

    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }
    
    func setUI(){
        
        self.view.layoutIfNeeded()
    
    }

    
   
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
       
        return arrData.count
        
    }
    
    
    
//    btnDetail
//    lblLocation
//    btnSetLocation
//     lblDescription
//    lblTitle
//    lblHeading
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! UpCommingTableViewCell
        
        
        let cornerRadius: CGFloat = 10
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = UIBezierPath(
            roundedRect: cell.mapBtn.bounds,
            byRoundingCorners: [.BottomLeft, .BottomRight],
            cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)
            ).CGPath
        cell.mapBtn.layer.mask = maskLayer
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.upperView.layer.cornerRadius = 10;
        cell.upperView.layer.masksToBounds = true;
        cell.upperView.layer.borderWidth = 1
        cell.upperView.layer.borderColor =   UIColor (red: 201.0/255.0, green:208.0/255.0, blue: 220/255.0, alpha: 1.0).CGColor
        
        cell.lowerView.layer.cornerRadius = 10;
        cell.lowerView.layer.masksToBounds = true;
        cell.lowerView.layer.borderWidth = 1
        cell.lowerView.layer.borderColor =   UIColor (red: 201.0/255.0, green:208.0/255.0, blue: 220/255.0, alpha: 1.0).CGColor
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UpCommingTableViewCell {
        [self.tableview.separatorColor = UIColor .clearColor()]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! UpCommingTableViewCell
        
        
        let cornerRadius: CGFloat = 10
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = UIBezierPath(
            roundedRect: cell.mapBtn.bounds,
            byRoundingCorners: [.BottomLeft, .BottomRight],
            cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)
            ).CGPath
        cell.mapBtn.layer.mask = maskLayer
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.upperView.layer.cornerRadius = 10;
        cell.upperView.layer.masksToBounds = true;
        cell.upperView.layer.borderWidth = 1
        cell.upperView.layer.borderColor =   UIColor (red: 201.0/255.0, green:208.0/255.0, blue: 220/255.0, alpha: 1.0).CGColor
        
        cell.lowerView.layer.cornerRadius = 10;
        cell.lowerView.layer.masksToBounds = true;
        cell.lowerView.layer.borderWidth = 1
        cell.lowerView.layer.borderColor =   UIColor (red: 201.0/255.0, green:208.0/255.0, blue: 220/255.0, alpha: 1.0).CGColor
        
//        let maskPAth1 = UIBezierPath(roundedRect: cell.mapBtn.bounds,
//                                     byRoundingCorners: [.BottomLeft,.BottomRight],
//                                     cornerRadii:CGSizeMake(10.0, 10.0))
//        let maskLayer1 = CAShapeLayer()
//        maskLayer1.frame = cell.mapBtn.bounds
//        maskLayer1.path = maskPAth1.CGPath
//        cell.mapBtn.layer.mask = maskLayer1
//        cell.mapBtn.clipsToBounds = true

        
       

        
       
        
        if(indexPath.row % 2 == 0){
            cell.backgroundColor = UIColor.whiteColor()

           
            
            
        } else{
             cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
        
        cell.delegate = self
        
        
        // there is no setup cell method so, cell items values set here
        
        
        //    btnDetail
        //    lblLocation
        //    btnSetLocation
        //     lblDescription
        //    lblTitle
        //    lblHeading
        
        cell.lblHeading?.text = ""
        
        
        if (arrData[indexPath.row]["type"] as! Int) == AFUrlRequestSharjeel.courseType.Online.rawValue
        {
            cell.lblTitleImage!.image = UIImage.init(imageName: "certificate-online-img")
            
            cell.lblHeading?.text = "Online Course"
            
        }
        else
        {
            cell.lblTitleImage!.image = UIImage.init(imageName: "certificate-inClass-img")
            
            cell.lblHeading?.text = "In-Class Course"
        }
        
        
        
        cell.lblTitle?.text = arrData[indexPath.row]["title"] as? String
        cell.lblDescription?.text = arrData[indexPath.row]["shortDescription"] as? String
        cell.lblLocation?.text = arrData[indexPath.row]["location"] as? String
        cell.lblTimeDate?.text = arrData[indexPath.row]["timeDate"] as? String
        

        cell.btnDetail?.tag = indexPath.row
        cell.btnSetLocation?.tag = (arrData[indexPath.row]["id"] as? Int)!
        
    
        cell.mapBtn.tag = indexPath.row
        
        cell.mapBtn!.addTarget(self, action: #selector(self.gotoMap), forControlEvents: .TouchUpInside)

        
    
        return cell
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        let cellSize: CGFloat
        
        let deviceHeight = CGRectGetHeight(self.view.frame);
        
        var tempHeight : CGFloat = 0.0

        if  constants.deviceType.IS_IPHONE {
            
            
            if constants.deviceType.IS_IPHONE_5 || constants.deviceType.IS_IPHONE_4
            {
                cellSize = CGFloat (deviceHeight * 0.63) + tempHeight
                return cellSize
                
            }
            else{
                cellSize = CGFloat (deviceHeight * 0.6) + tempHeight
                
                print (cellSize)
                return cellSize
            }
        }
            
        else if GlobalStaticMethods.isPad() {
            
            
            return 0.68402343 * CGRectGetHeight(self.view.frame)
            if GlobalStaticMethods.isPadPro() {
                print("ipad pro changed orientation")
                
                
                
                //                if UIInterfaceOrientationIsPortrait(UIApplication .sharedApplication() .statusBarOrientation) {
                //
                //
                //                    cellSize = CGFloat (deviceHeight * 0.25)
                //                    return cellSize
                //
                //                }
                //                else{
                //
                //                    cellSize = CGFloat (deviceHeight * 0.32)
                //                    return cellSize                }
                //
                //            }
                //            else{
                //                if UIInterfaceOrientationIsPortrait(UIApplication .sharedApplication() .statusBarOrientation) {
                //                    cellSize = CGFloat (deviceHeight * 0.27)
                //                    return cellSize                }
                //                else{
                //                    cellSize = CGFloat (deviceHeight * 0.36)
                //                    return cellSize
                //
                //                }
                //
            }
            
        }
        cellSize = CGFloat (deviceHeight * 0.35) + tempHeight
        return cellSize
        
    }
    
     func upCommingBtnTappedDelegate(detailBTN: UpCommingTableViewCell) {
       // navigation.goToViewController(constants.viewControllers.seminarVC, sideDrawer: self.mm_drawerController)
        
        
        let cell = detailBTN
        
        
       
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseDetailVC)!.storyboardObject
        let vc = sb.instantiateViewControllerWithIdentifier(constants.viewControllers.courseDetailVC) as! courseDetailViewController
        
        
        let temp =  arrData[(cell.btnDetail?.tag)!] as! Dictionary <String,AnyObject>
        
        vc.strCourseID = "\(temp["id"]!)"
        vc.isCurriculum = false
        vc.courseType = 2
        
        self.navigationController!.pushViewController(vc, animated: true)
        

    }
    
    func goToViewController(identifier : String){
        
    }

    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    // MARK: - Call Service
    
    func callService()
    {
        AFWrapper.delegate = self
        AFWrapper.serviceCall(AFUrlRequestSharjeel.GetUpComingEvents, success: { (res) in
            
            self.parseResponce(res)
            
        }) { (NSError) in
            printLog(NSError)
        }
        
    }
    func didTapOnOKayOnAFWrapperAlert(errorCode: Int) {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    func parseResponce(res : JSON)
    {
        
        
        

    
    
        if res["data"] != nil
        {
            if res["data"]["items"] != nil
            {
    
                arrData = res["data"]["items"].arrayObject!
                self.tableview.reloadData()
                
                tableview.hidden = false
                
            }
        }
    }
    
    
    func gotoMap(sender: UIButton!) -> Void
    {
       
       let temp = arrData[sender.tag]
        
        printLog(temp)
        print("")
        let sb = navigation.getStoryBoardForController(constants.viewControllers.locationViewController)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.locationViewController) as! locationViewController
        vc.lat =  arrData[sender.tag]["lat"] as! Double
        vc.long =  arrData[sender.tag]["lng"] as! Double
        vc.locationTitle = arrData[sender.tag]["title"] as! String

        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        var numOfSections: Int = 0
        
        if arrData.count > 0{
            self.tableview.separatorStyle = .None
            numOfSections                = 1
            self.tableview.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRectMake(0, 0, self.tableview.bounds.size.width, self.tableview.bounds.size.height))
            noDataLabel.text             = "No data available"
            noDataLabel.textColor        = UIColor.lightGrayColor()
            noDataLabel.textAlignment    = .Center
            self.tableview.backgroundView = noDataLabel
            self.tableview.separatorStyle = .None
        }
        
        return numOfSections
    }
    
    func getTextheight(withConstrainedWidth width: CGFloat, font: UIFont, str : String) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .max)
        
        let boundingBox = str.boundingRectWithSize(constraintRect, options: .UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
