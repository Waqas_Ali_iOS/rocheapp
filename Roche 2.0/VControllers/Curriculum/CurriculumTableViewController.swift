//
//  CurriculumTableViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 8/15/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit

class CurriculumTableViewController: UIViewController ,UIScrollViewDelegate, CurriculumTableViewCellDelegate {

    
    @IBOutlet weak var tableview : UITableView!
    @IBOutlet weak var viewCalender : UIView!
    @IBOutlet weak var viewResult : UIView!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    
    
    
    var previousOffset : CFloat = 0.0
    
    
    var  type : Int = 0
    
    
    
    var arrData = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
               
        
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 140
        
        self.tableview.hidden = true
        
       
        self.tableview.scrollEnabled = false
        
        
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CurriculumTableViewController.reload), name: "reloadData", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CurriculumTableViewController.setTableState), name:"MyScrollUpdatedNotification", object: nil)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CurriculumTableViewController.disableTableState), name:"MyScrollUpdatedNotificationWhenNotOnTop", object: nil)
        
        
        
        // self.viewCalender .removeFromSuperview()
        //self.viewResult .removeFromSuperview()
        
        // Do any additional setup after loading the view.
        if #available(iOS 9.0, *) {
          topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        }
    }
    
    
    func disableTableState() {
        
        if self.tableview.scrollEnabled == true {
             self.tableview.scrollEnabled = false
        }
    }
    
    func setTableState() {
         self.tableview.scrollEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        var numOfSections: Int = 0
        
        if arrData.count > 0{
            self.tableview.separatorStyle = .None
            numOfSections                = 1
            self.tableview.backgroundView = nil
        }
        else
        {
            let view : UIView   = UIView(frame: CGRectMake(0, 0, self.tableview.bounds.size.width, self.tableview.bounds.size.height))
            
            var YPosition : CGFloat = 0.0
            if GlobalStaticMethods.isPad() {
                YPosition = (0.1953125 * DeviceUtil.size.height)
            }
            else
            {
                YPosition = (0.135869 * DeviceUtil.size.height)
            }
            
            let noDataLabel: UILabel     = UILabel(frame: CGRectMake(0, 0, self.tableview.bounds.size.width, YPosition))
            
            if type == 1 {
                noDataLabel.text             = "No Pre-Requisites Set"
            }
            else
            {
                noDataLabel.text             = "No Elective Set"
            }
            
            
            noDataLabel.textColor        = UIColor.lightGrayColor()
            noDataLabel.textAlignment    = .Center
            
            view.addSubview(noDataLabel)
            
            self.tableview.backgroundView = view
            self.tableview.separatorStyle = .None
        }
        
        return numOfSections
    }
    
    
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
        
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> CurriculumTableViewCell {
       //--ww [self.tableview.separatorColor = UIColor .clearColor()]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! CurriculumTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        
        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor.whiteColor()
            
            
        } else{
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
        
        
        cell.delegate = self
        
     //   cell.setupC
        
        cell.setupCell(arrData[indexPath.row] as! CurriculumModel, Index: indexPath.row)
        
        
        //--ww cell.textLabel?.text = "teee"

        return cell
        
    }
    
    
    
    func detailBtnTappedDelegate(detailBTN: CurriculumTableViewCell) {
        print("lol")
        

        
        print(detailBTN.btnCourseDetail?.tag)
        

        
        let delegate = UIApplication.sharedApplication().delegate as? AppDelegate

        let temp = arrData[(detailBTN.btnCourseDetail?.tag)!] as! CurriculumModel

        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseDetailVC)!.storyboardObject
        let vc = sb.instantiateViewControllerWithIdentifier(constants.viewControllers.courseDetailVC) as! courseDetailViewController
        let navCon = delegate!.drawerController!.centerViewController as! MMNavigationController
        vc.strCourseID = "\(temp.id)"
        
//        if (temp.courseStatus == CurriculumTableViewCell.CourseStatus.Enrolled.rawValue) || (temp.courseStatus == CurriculumTableViewCell.CourseStatus.Passed.rawValue)
//        {
//            vc.isCurriculum = true
//        }
//        else
//        {
//            vc.isCurriculum = false
//        }
        
        
        vc.courseType =  temp.courseType

        navCon.pushViewController(vc, animated: true)
        

        
    }
    
    
    
    func bookMarkBtnTappedDelegate(detailBTN: CurriculumTableViewCell)
    {
       
        
        
        let temp =  arrData[(detailBTN.btnCourseDetail?.tag)!] as! CurriculumModel
        
        
        
        
        let user = UserModel.sharedInstance
        
        AFWrapper.serviceCall(AFUrlRequestNabeel.BookMarkCourse(sessionId: user.sessionID, courseId: temp.id), success: { (result) in
            
            
            
            if (result.dictionary!["statusCode"])! == 1000            {
                
                
                
                
                if GlobalStaticMethods.isPad(){
                    
                    
                    if detailBTN.btnBookMark!.currentBackgroundImage!.isEqual(UIImage(imageName :"Gen-bookmark-img-sel")) {
                        //do something here
                        
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.UNBOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        
                        detailBTN.btnBookMark?.setBackgroundImage(UIImage(imageName :"gen-bookmark" ), forState: .Normal)
                        
                        temp.bookmarked = 0
                        
                       
                        
                    }
                    else
                    {
                        
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.BOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        
                        detailBTN.btnBookMark?.setBackgroundImage(UIImage(imageName :"Gen-bookmark-img-sel" ), forState: .Normal)
                          temp.bookmarked = 1
                        
                    }
                    
                    
                    
                    
                    //self.arrData["bookmark"].Int = 1
                }
                    
                else{
                    
                    
                    if detailBTN.btnBookMark!.currentImage!.isEqual(UIImage(imageName :"Gen-bookmark-img-sel")) {
                        //do something here
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.UNBOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        
                        detailBTN.btnBookMark?.setImage(UIImage(imageName :"gen-bookmark" ), forState: .Normal)
                       temp.bookmarked = 0
                        
                    }
                    else{
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.BOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        
                        detailBTN.btnBookMark?.setImage(UIImage(imageName :"Gen-bookmark-img-sel" ), forState: .Normal)
                        
                        temp.bookmarked = 1
                        
                    }
                }
    
                
                let indexPath = NSIndexPath(forRow: (detailBTN.btnBookMark?.tag)!, inSection: 0)
                
                self.tableview.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
                
            }
                
            else{
                
                Alert.showAlertMsgWithTitle("Message", msg: "Some Error", btnActionTitle: "ok", viewController: self, completionAction: { (Void) in

                    
                })
                
            }
            
        }) { (NSError) in
            
            
            
        }
        
        
        //["bookmarked"]
        
       
        
    }
    
    func SuggestCourseTappedDelegate(detailBTN: CurriculumTableViewCell)
    {
        let delegate = UIApplication.sharedApplication().delegate as? AppDelegate
        
        let temp = arrData[(detailBTN.btnShare?.tag)!] as! CurriculumModel
        
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseSuggestVC)!.storyboardObject
        let vc = sb.instantiateViewControllerWithIdentifier(constants.viewControllers.courseSuggestVC) as! CourseSuggestionViewController
        let navCon = delegate!.drawerController!.centerViewController as! MMNavigationController
        
        
        vc.courseID = temp.id
        
        navCon.pushViewController(vc, animated: true)
        
    }
    
   
 
    /*
     func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
         var lastContentOffset: CGFloat = 0
        

//            if (lastContentOffset > scrollView.contentOffset.y) {
//                // move up
//            }
//            else if (lastContentOffset < scrollView.contentOffset.y) {
//                // move down
//            }
        
            // update the new position acquired
            lastContentOffset = scrollView.contentOffset.y
        
        if(velocity.y>0){
            NSLog("dragging Up");
            
            lastContentOffset = scrollView.contentOffset.y
            
            
        }else{
            lastContentOffset = scrollView.contentOffset.y
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let deviceHeight = CGRectGetHeight(self.view.frame);
        
        let dynamicHeight = deviceHeight - 56
        
       
        
      //  let mVC = MyCurriculumViewController()
     //   mVC.heightConstraintsSwiftPages?.constant = dynamicHeight
        
       
        
        
        
    }
 */
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
       /// topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }

    
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }

    func reloadtblViewData(arrData: [AnyObject])
    {
        self.arrData = arrData
        NSNotificationCenter.defaultCenter().postNotificationName("reloadData", object: nil, userInfo:nil)
    }
    
    
    func reload(notification: NSNotification) {
        
        
        self.arrData = notification.userInfo?["data"] as! [AnyObject]
        
        type = notification.userInfo?["type"] as! Int
        
        
        tableview.reloadData()
        self.tableview.hidden = false
        
        if arrData.count == 0 {
            tableview.scrollEnabled = false
        }
        
    }
    
    
//    func scrollViewDidScroll(scrollView: UIScrollView)
//    {
//        
//        
//        let obj = self.parentViewController as! MyCurriculumViewController
//        
//        var rect = obj.scrollView
//        
//        var newrect = CGRectMake(rect.frame.origin.x, rect.frame.origin.y + CGFloat(previousOffset) - scrollView.contentOffset.y, rect.frame.size.width, rect.frame.size.height)
//        
//       // previousOffset = scrollView.contentOffset.y
//        
//    }
    
    
//    - (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    static CGFloat previousOffset;
//    CGRect rect = self.view.frame;
//    rect.origin.y += previousOffset - scrollView.contentOffset.y;
//    previousOffset = scrollView.contentOffset.y;
//    self.view.frame = rect;
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        
        


        
        //  //NSLog(@"bottomEdge = %f scrollView.contentSize.height = %f", bottomEdge,  scrollView.contentSize.height);
        if bottomEdge >= scrollView.contentSize.height{
        // we are at the end
        printLog("we are at the end scroll helper")
            
        }
        
        
        NSLog("%f",scrollView.contentOffset.y)
        
        if (scrollView.contentOffset.y < 1) { // TOP
            self.tableview.scrollEnabled = false
          //  [[NSNotificationCenter defaultCenter] postNotificationName:@"MyScrollUpdatedNotification" object:self];
            
            NSNotificationCenter.defaultCenter().postNotificationName("EnableBaseScroll", object: self)
            

        }
        else
        {
             NSNotificationCenter.defaultCenter().postNotificationName("DisableBaseScroll", object: self)
        }

        

    
    
    
    }
      
}
