//
//  MyCurriculumViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 8/15/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyCurriculumViewController: HeaderScrollViewController,SwiftPagesDelegate {
    

   
    var showCourse : AFUrlRequestSharjeel.CurriculumType = AFUrlRequestSharjeel.CurriculumType.PreRequisites

    @IBOutlet weak var viewTop: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var lblred: UILabel! = UILabel()
    @IBOutlet var btnCircularView : UIButton!

    
    @IBOutlet var swiftPagesView: SwiftPages!
    @IBOutlet var topConstraintsSwiftPages : NSLayoutConstraint!
    @IBOutlet var heightConstraintsSwiftPages : NSLayoutConstraint!

    @IBOutlet var topConstraint : NSLayoutConstraint?
    
    @IBOutlet weak var imgCurriculamPass: UIImageView!
    
    @IBOutlet weak var btnCurriculamPass: UIButton!
    
    @IBOutlet weak var crRatingView: circularRatingView!
    @IBOutlet weak var lblPercentage: BaseUILabel?
    @IBOutlet weak var preRequistesCourseText: BaseUILabel?
    @IBOutlet weak var elctiveCourseText: BaseUILabel?
    
    @IBOutlet weak var SwiftPagesHeightConstraint: NSLayoutConstraint?
    
    var arr = [AnyObject]()
    
    
     var arrElec = [CurriculumModel]()
     var arrPre = [CurriculumModel]()
    

    var dicData = Dictionary <String,AnyObject>()

    override func viewDidLoad() {
        super.viewDidLoad()
        
         topView = viewTop
        baseScrollView = scrollView
        scrollView.bounces = false
        
        lblPercentage!.text =  ""
        
        elctiveCourseText!.text = ""
        preRequistesCourseText!.text = ""
        
        crRatingView?.rating = 0
        
        self.btnCircularView .addTarget(self, action: #selector(self.goTocourseInProgress), forControlEvents: .TouchUpInside)

        
        
        
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        _headerView.heading = "My Curriculum"

     
        imgCurriculamPass.hidden = true
        btnCurriculamPass.hidden = true
        
        self.navigationController?.navigationBarHidden = true
        //   self.viewProfileHeightConstraint.constant = 0
        automaticallyAdjustsScrollViewInsets = false
       
    
        // Initiation
        let VCIDs = ["CurriculumTableViewController", "CurriculumTableViewController"]
        // let buttonTitles = ["Latest Updates", "Pending Approvals"]
        let buttonImages = [
            UIImage(named:"curriculum-prerequisite-img")!,
            UIImage(named:"curriculum-electivecourse-img")!
        ]
        
        let imageSel0 : UIImage = UIImage(named:"curriculum-prerequisite-img")!.maskWithColor(UIColor(hexString :"#113c8b"))!
        
        let imageSel1: UIImage =  UIImage(named:"curriculum-electivecourse-img")!.maskWithColor(UIColor(hexString :"#eceff4"))!
        
        let buttonHeighlightdImages = [
            imageSel0,
          imageSel1
           
        ]
        
        
         let bgColor = [ "#eceff4", "#3e8ada"]
        //--ww swiftPagesView.disableTopBar()
        
        
        // Sample customization
        swiftPagesView.setOriginY(0.0)
        swiftPagesView.setTopBarHeight(GlobalStaticMethods.getTabsHeight())
       
        
        swiftPagesView.setTopBarBackground(UIColor(hexString: "#eceff4"))
        swiftPagesView.setAnimatedBarColor(UIColor(hexString:"#113c8b"))
        swiftPagesView.enableBarShadow(false)
        
        
        
        swiftPagesView.currentIndex = 0
        
        
        var stbName = "sharjeel-iphone-en"
        
        if GlobalStaticMethods.isPad() {
        stbName = "sharjeel-ipad-en"
        }
        
        
         swiftPagesView.initializeWithVCIDsArrayAndButtonImagesArray (VCIDs, buttonImagesArray: buttonImages, storyBoardName: stbName , buttonHighlightedImagesArray: buttonHeighlightdImages, buttonBgColorArray : bgColor)
        
        self.swiftPagesView.hidden = true
        
        
        self.swiftPagesView.delegate = self
        
        
        
        
        showCourse == AFUrlRequestSharjeel.CurriculumType.PreRequisites
        
        
       // callService()
        
   
        //  self.imgEclipse.transform = CGAffineTransformMakeScale(0.95, 0.95)
        
        
        // Do any additional setup after loading the view.
        let deviceHeight = CGRectGetHeight(self.view.frame);
        
        let dynamicHeight = deviceHeight - (GlobalStaticMethods.getHeaderFooterHeight())
        
        
        heightConstraintsSwiftPages.constant = dynamicHeight
        
        self.swiftPagesView.layoutIfNeeded()
        
    }
    

    override func viewDidAppear(animated: Bool) {
        

        callService()
        self.mm_drawerController.openDrawerGestureModeMask = .None

        self.view.layoutIfNeeded()
        self.swiftPagesView.hidden = false
   
        //--ww self.performSelector(#selector(ContactRocheViewController.animateProfileView), withObject:nil, afterDelay: 2)
        
        //    self.performSelector(Selector("animateProfileView"), withObject:nil, afterDelay: 2)
        let deviceHeight = CGRectGetHeight(self.view.frame);
        
        let dynamicHeight = deviceHeight - (GlobalStaticMethods.getHeaderFooterHeight())
        
        

        
        let devicetype =  DeviceUtil.deviceType
        
//        
//        if devicetype == .iPhone5 {
//             heightConstraintsSwiftPages.constant = dynamicHeight + 10
//        }
        
        
//        switch devicetype {
//        case .iPhone5:
//            heightConstraintsSwiftPages.constant = dynamicHeight + 10
//            break
//        case .iPad:
//            heightConstraintsSwiftPages.constant = dynamicHeight
//            break
//        default:
//        //    heightConstraintsSwiftPages.constant = dynamicHeight
//            break
//        }
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.setTableState), name:"DisableBaseScroll", object: nil)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.disableTableState), name:"EnableBaseScroll", object: nil)
        
        
       
         self.swiftPagesView.layoutIfNeeded()
    
    }
    
    
    func goTocourseInProgress(){
        
        navigation.goToViewController(constants.viewControllers.CourseInProgressVC, sideDrawer: self.mm_drawerController)
    }
  
    func setTableState()  {
        baseScrollView.scrollEnabled = false
    }
    func disableTableState()  {
         baseScrollView.scrollEnabled = true
    }
    
    
    func animateProfileView(){
        UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseOut, animations: {
            //  self.viewProfileHeightConstraint.constant = 0
            self.view.layoutIfNeeded()
            }, completion: nil)
        
    }

    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        
     topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }

    // MARK: - Web service call methods
    
    func callService()
    {
        
        var cType : Int = 0
        
        if showCourse == AFUrlRequestSharjeel.CurriculumType.PreRequisites {
            cType = 1
        }
        else
        {
            cType = 2
        }
        
        AFWrapper.serviceCall(AFUrlRequestSharjeel.GetMyCurriculum(showCourse: cType, offset: 0, limit: 0), success: { (res) in
            
            self.parseResponce(res)
            
        }) { (NSError) in
            printLog(NSError)
        }
        
    }
    
    
    func parseResponce(res : JSON)
    {
        
        arrPre = [CurriculumModel]()
        arrElec = [CurriculumModel]()
        
        if res["data"] != nil
        {
            if res["data"] != nil
            {
                dicData = res["data"].dictionaryObject!
                
                
            printLog(dicData["currPercentage"])
                
                
                if dicData["currPercentage"]! as! Double > 0 {
                    
                    lblPercentage!.text =  String.init(format:"%@%%", "\(dicData["currPercentage"]! as! Double)")
                }
                else
                {
                    lblPercentage!.text =  String.init(format:"0%%")
                }
                
                
                elctiveCourseText!.text = "\(dicData["passElectiveCourses"]!)/\(dicData["totalElectiveCourses"]!) Completed"
                preRequistesCourseText!.text = "\(dicData["passPrerequisiteCourses"]!)/\(dicData["totalPrerequisiteCourses"]!) Completed"
                
                crRatingView?.rating = (dicData["currPercentage"] as! Double) / 100
                
                
                if (dicData["currPercentage"] as! Double) >= 100
                {
                    imgCurriculamPass.hidden = false
                    btnCurriculamPass.hidden = false
                    
                    self.btnCurriculamPass.addTarget(self, action: #selector(self.gotoMyCertificates), forControlEvents: .TouchUpInside)
                }
                
                
                
                var tempArr = dicData["prerequisite"]!["items"] as! [AnyObject]
                var count = tempArr.count
                
                
                for  i in 0..<count
                {
                    let variable : CurriculumModel = CurriculumModel.init(bookmarked: tempArr[i]["bookmarked"] as! Int , courseStatus: tempArr[i]["courseStatus"] as! Int, courseType: tempArr[i]["courseType"] as! Int, id: tempArr[i]["id"] as! Int, shortDescription: tempArr[i]["shortDescription"] as! String, title: tempArr[i]["title"] as! String)
                    
                    arrPre.append(variable)
                }
                
                
                
                tempArr = dicData["elective"]!["items"] as! [AnyObject]
                count = tempArr.count
                
                
                for  i in 0..<count
                {
                    let variable : CurriculumModel = CurriculumModel.init(bookmarked: tempArr[i]["bookmarked"] as! Int , courseStatus: tempArr[i]["courseStatus"] as! Int, courseType: tempArr[i]["courseType"] as! Int, id: tempArr[i]["id"] as! Int, shortDescription: tempArr[i]["shortDescription"] as! String, title: tempArr[i]["title"] as! String)
                    
                    arrElec.append(variable)
                }
                
                loadDataAndCallService()
            }
        }
    }
    
    
    // Mark: - SwiftPages delegates
    
    func SwiftPagesCurrentPageNumber(currentIndex: Int)
    {
        if currentIndex == 0
        {
            showCourse = AFUrlRequestSharjeel.CurriculumType.PreRequisites
        }
        else
        {
            showCourse = AFUrlRequestSharjeel.CurriculumType.Electives
        }
        loadDataAndCallService()
        
        
    }
    
    func loadDataAndCallService()
    {
        
        
        NSNotificationCenter.defaultCenter().postNotificationName("EnableBaseScroll", object: self)
        if dicData.count > 0 {
            
        var data : Dictionary <String,AnyObject>
            
        if showCourse == AFUrlRequestSharjeel.CurriculumType.PreRequisites
        {
           data = ["data":arrPre,"type":1]
        
            

        
        }
        else
        {
            data = ["data":arrElec,"type":2]
            
            
             //978
            //345
            
           // 235 - 563
            
//            if arrElec.count == 0 {
//                baseScrollView.scrollEnabled = false
//            }
//            else
//            {
//                baseScrollView.scrollEnabled = true
//            }
            
            
            
            
        }

            
             NSNotificationCenter.defaultCenter().postNotificationName("reloadData", object: nil, userInfo:data)
            
        }
        

       
    }
    
    
    func gotoMyCertificates()
    {
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.myCertificateVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.myCertificateVC) as! MyCertificateViewController

        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



class CurriculumModel
{
    
    var bookmarked : Int!
    var courseStatus : Int!
    var courseType : Int!
    var id : Int!
    var shortDescription : String!
    var title : String!
    
    
    init(bookmarked : Int , courseStatus : Int,courseType : Int,id : Int,shortDescription : String, title : String)
    {
        self.bookmarked = bookmarked
        self.courseStatus = courseStatus
        self.courseType = courseType
        self.id = id
        self.shortDescription = shortDescription
        self.title = title
        
    }

}
