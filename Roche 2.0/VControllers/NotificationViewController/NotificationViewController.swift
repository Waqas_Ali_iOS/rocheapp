//
//  NotificationViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/13/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationViewController: HeaderViewController , CurriculumTableViewCellDelegate , AFWrapperDelegate{
    @IBOutlet weak var tableview : UITableView!
    @IBOutlet var topConstraint : NSLayoutConstraint?

    var arrData : [JSON] = []
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        refreshControl = UIRefreshControl()
        //  refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        tableview.addSubview(refreshControl) // not required when using UITableViewController

        
        
        _headerView.heading = "Notifications"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 200
        
    self.serviceCall()
        
      UserModel.sharedInstance.unreadMessageCount = 0
        // Do any additional setup after loading the view.
    }
    
    func refresh(sender:AnyObject) {
        
        self.serviceCall()
        // self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
    
        super.viewDidAppear(animated)
        self.mm_drawerController.openDrawerGestureModeMask = .None

        self.tableview .reloadData()
        
    }
    
    
    func parseResponce(res : JSON)
    {
        
        if res["data"] != nil
        {
            if res["data"]["items"] != nil
            {
                
                arrData = res["data"]["items"].arrayValue
                self.tableview.reloadData()
                
            }
        }
    }
    func serviceCall()  {
        
        AFWrapper.delegate = self
        
        AFWrapper.serviceCall(AFUrlRequestNabeel.Notification(), success: { (res) in
            
            printLog(res)
            self.parseResponce(res)

        }) { (NSError) in
            printLog(NSError)
            
        }
        
    }
    
    
    
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        if  arrData.count != 0 {
            
            self.tableview.hidden = false
            
            return ( arrData.count)
        }else{
            self.tableview.hidden = true
            return 0
        }
    }
    
//    @IBOutlet var lblTime: UILabel!
//    @IBOutlet var lblDescription: UILabel!
//    @IBOutlet var lblName: UILabel!
//    @IBOutlet weak var viewSuggest : UIView!
//    @IBOutlet weak var viewHeading : UIView!
//    @IBOutlet weak var imgIdentity : UIImageView!
//    
//    // -- Reminder Cell
//    
//    @IBOutlet var remLblTitle: UILabel!
//    @IBOutlet var remLblDescription: UILabel!
//    @IBOutlet var remLblTime: UILabel!
//    @IBOutlet weak var reImgIdentity : UIImageView!
//    
//    //--- Message Cell
//    
//    @IBOutlet var mesLblTitle: UILabel!
//    @IBOutlet var mesLblDescription: UILabel!
//    @IBOutlet var mesLblTime: UILabel!
//    @IBOutlet var mesLblMessage: UILabel!
//    @IBOutlet weak var mesImgIdentity : UIImageView!

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        [self.tableview.separatorColor = UIColor .clearColor()]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! NotificationTableViewCell
        
        let messageCell = tableView.dequeueReusableCellWithIdentifier("MessageCell") as! NotificationTableViewCell
        
        let reminderCell = tableView.dequeueReusableCellWithIdentifier("ReminderCell") as! NotificationTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        messageCell.selectionStyle = UITableViewCellSelectionStyle.None
        reminderCell.selectionStyle = UITableViewCellSelectionStyle.None

    

        let shareImg = UIImage(named: "Notification-share-btn")
        let msgImg = UIImage(named: "Notification-message-btn")
        let timeimg = UIImage(named: "Gen-clock-img")
        
        self.CornerImage(cell.imgIdentity)
        self.CornerImage(reminderCell.reImgIdentity)
        self.CornerImage(messageCell.mesImgIdentity)
        
        
        
        
        
        
        
        
        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
            reminderCell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
            messageCell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
            
            
            cell.imgIdentity.backgroundColor =  UIColor .whiteColor()
            reminderCell.reImgIdentity.backgroundColor = UIColor .whiteColor()
            messageCell.mesImgIdentity.backgroundColor = UIColor .whiteColor()
            
        }
            
        else{
            cell.backgroundColor = UIColor.whiteColor()
            reminderCell.backgroundColor = UIColor.whiteColor()

            messageCell.backgroundColor = UIColor.whiteColor()
            
            cell.imgIdentity.backgroundColor =  UIColor (hexString: "#f5f5f5")
            reminderCell.reImgIdentity.backgroundColor = UIColor (hexString: "#f5f5f5")
            messageCell.mesImgIdentity.backgroundColor = UIColor (hexString: "#f5f5f5")
            
            
        }
        
        if arrData[indexPath.row]["type"] == 4{
            
            messageCell.mesLblTitle.text = arrData[indexPath.row]["title"].stringValue
            messageCell.mesLblDescription.text = arrData[indexPath.row]["description"].stringValue
            messageCell.mesLblTime.text = arrData[indexPath.row]["timestamp"].stringValue
            messageCell.mesImgIdentity.image = timeimg
            messageCell.mesLblMessage.text = "Reminder"
            
            
            return messageCell
            
            
        }
        
        else if arrData[indexPath.row]["type"] == 3{
            
            
            messageCell.mesLblTitle.text = arrData[indexPath.row]["title"].stringValue
            messageCell.mesLblDescription.text = arrData[indexPath.row]["description"].stringValue
            messageCell.mesLblTime.text = arrData[indexPath.row]["timestamp"].stringValue
            messageCell.mesImgIdentity.image = timeimg
            messageCell.mesLblMessage.text = "Enrollment"
            
            
            return messageCell

            
            
            
//            reminderCell.remLblTitle.text = arrData[indexPath.row]["title"].stringValue
//
//            
//            reminderCell.remLblDescription.text = arrData[indexPath.row]["description"].stringValue
//            reminderCell.remLblTime.text = arrData[indexPath.row]["timestamp"].stringValue
//            reminderCell.reImgIdentity.image = timeimg
//            
//            
//            return reminderCell

            
        }
       else if arrData[indexPath.row]["type"] == 2{
            
            cell.lblName.text = arrData[indexPath.row]["title"].stringValue
            cell.lblDescription.text = arrData[indexPath.row]["description"].stringValue
            cell.lblTime.text = arrData[indexPath.row]["timestamp"].stringValue
            cell.imgIdentity.image = shareImg
            return cell

            
        }
       else{
            
            messageCell.mesLblTitle.text = arrData[indexPath.row]["title"].stringValue
            messageCell.mesLblDescription.text = arrData[indexPath.row]["description"].stringValue
            messageCell.mesLblTime.text = arrData[indexPath.row]["timestamp"].stringValue
            messageCell.mesImgIdentity.image = msgImg
            messageCell.mesLblMessage.text = "Message"
            
            
            return messageCell

            
        }
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.clearColor()
        
        if arrData[indexPath.row]["type"] == 1{
            
            //Message
            
            printLog("Message")
         
            let tID = arrData[indexPath.row]["messageId"].intValue
            
            
            let sb = navigation.getStoryBoardForController(constants.viewControllers.replyMsgVC)?.storyboardObject
            let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.replyMsgVC) as! ReplyMessageViewController
            vc.tID = tID
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
       
        else{
            // courseDetail
            
            printLog("courseDetail")
            var courseID = arrData[indexPath.row]["courseId"]
            var courseType = arrData[indexPath.row]["courseType"].intValue
            
            
            let sb = navigation.getStoryBoardForController(constants.viewControllers.courseDetailVC)?.storyboardObject
            let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.courseDetailVC) as! courseDetailViewController
            vc.strCourseID = "\(courseID)"
            vc.courseType = courseType
            printLog(vc.courseType)
            printLog(vc.strCourseID)

            
            self.navigationController?.pushViewController(vc, animated: true)
         
            
        }

        
        
    }
    
    func CornerImage(img : UIImageView) -> Void {
        
        
        let maskPAth1 = UIBezierPath(roundedRect: img.bounds,
                                     byRoundingCorners: [.TopLeft , .BottomLeft, .TopRight, .BottomRight],
                                     cornerRadii:CGSizeMake(25.0, 25.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = img.bounds
        maskLayer1.path = maskPAth1.CGPath
        img.layer.mask = maskLayer1
        
        
    }
    
    func didTapOnOKayOnAFWrapperAlert(errorCode: Int) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    
    func roundImage() {
        
    }
    
    
    
    
    func detailBtnTappedDelegate(detailBTN: CurriculumTableViewCell) {
        print("LOL")
    }
    

    

    

}
