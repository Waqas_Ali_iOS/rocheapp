//
//  ReplyMessageViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 8/15/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ReplyMessageViewController: HeaderViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate, MessageTableViewCellDelegate  {

    
     let imagePicker = UIImagePickerController()
    @IBOutlet weak var tblView : UITableView!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    
    @IBOutlet weak var lblTitleMsg: BaseUILabel!
    
    var dataDict : Dictionary<String, AnyObject>!
    var dataArray = [JSON]()
    var imageData : NSData!
    var imageName : String!
    var tID : Int?
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        refreshControl = UIRefreshControl()
        //  refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        tblView.addSubview(refreshControl) // not required when using UITableViewController

        
        
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        _headerView.heading = "Messages"
        
        if GlobalStaticMethods.isPhone() {
            self.tblView.rowHeight = UITableViewAutomaticDimension
            self.tblView.estimatedRowHeight = 340
            
        }
        
        
//        dataArray = dataDict["items"] as! [AnyObject]
//        dataArray.append("replyMessage")
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ReplyMessageViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        tblView.addGestureRecognizer(tapGesture)
        
        imagePicker.delegate = self
        
       imageData  = nil
       imageName = ""
        
        serviceCall()
        
    }
    
    func refresh(sender:AnyObject) {
        
        self.serviceCall()
        // self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    func serviceCall()  {
        
        
        AFWrapper.serviceCall(AFUrlRequestWaqas.GetMessageDetail(tid: tID!), success: { (res) in
            
            printLog(res)
            

           self.dataArray = res["data"]["items"].arrayValue
           self.dataArray.append("replyMessage")
            
           self.dataDict = res["data"].dictionaryObject
            
            
            self.lblTitleMsg.text = self.dataDict["threadTitle"] as? String
            self.tblView.reloadData()
            
            
        }) { (NSError) in
            printLog(NSError)
            
        }
        
        

        
        
    }
    
    
    func hideKeyboard() {
        tblView.endEditing(true)
    }
    
    override func viewDidAppear(animated: Bool) {
        self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        
        if  dataArray.count != 0 {
            
            self.tblView.hidden = false
            
            return ( dataArray.count)
        }else{
            self.tblView.hidden = true
            return 0
        }

        
      // return dataArray.count
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        
        if GlobalStaticMethods.isPad()
        {
            if(indexPath.row  == 5)
            {
                return 0.253613 * self.view.frame.size.height;
            }
            return 0.203613 * self.view.frame.size.height;
        }
        return UITableViewAutomaticDimension
        
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        [self.tblView.separatorColor = UIColor .clearColor()]
        
        if(indexPath.row  == dataArray.count - 1)
        {
            
            let ReplyCell = tableView.dequeueReusableCellWithIdentifier("ReplyCell") as! ReplyMessageTableViewCell
            ReplyCell.selectionStyle = UITableViewCellSelectionStyle.None
           //--ww ReplyCell.txtView.delegate = self;
            
            ReplyCell.btnAssignment .addTarget(self, action: #selector(ReplyMessageViewController.selectAttachment), forControlEvents: .TouchUpInside)
             ReplyCell.btnAssignment .setTitle("Choose Image", forState: .Normal)
            
            ReplyCell.txtView.text = "Type your message here!"
            return ReplyCell
            
        }else{
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! MessageTableViewCell
           cell.selectionStyle = UITableViewCellSelectionStyle.None
       
        
        
      
        
        
        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor.whiteColor()
            //  cell.ViewAssignment.removeFromSuperview()
            
        } else{
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
        
        
        //   cell.setDataReplyMessage(dataArray[indexPath.row] as! Dictionary<String, AnyObject>)
            
            cell.setDataReplyMessage(dataArray[indexPath.row])
            cell.indexPathNo = indexPath.row
            cell.delegate = self
      
        return cell
            
        }
        
    }
    
    
    func btnAssignmentDidClick(cell: MessageTableViewCell, indexNo: Int) {
        
        printLog(dataArray[indexNo]["attachLink"]["ios"].stringValue)
        
        if dataArray[indexNo]["attachLink"]["ios"] != nil{
            
        
            
            
            
            Alert.showAlertMsgWithTitle("Confirmation", msg: "Do you want to open this attachment?", otherBtnTitle: "Yes", otherBtnAction: { (success) in
                let urlString = self.dataArray[indexNo]["attachLink"]["ios"].stringValue
                UIApplication.sharedApplication().openURL(NSURL(string: urlString)!)
                }, cancelBtnTitle: "No", cancelBtnAction: { (success) in
                   
                }, viewController: self)
        }
        
        
    }
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
    }
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }
    

 
    
    

    
    
    
    func selectAttachment() {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
  //    imagePicker.modalPresentationStyle = .CurrentContext
   //      presentViewController(imagePicker, animated: true, completion: nil)
        
        imagePicker.modalPresentationStyle = .Popover
        let presentationController = imagePicker.popoverPresentationController
        
        // You must set a sourceView or barButtonItem so that it can
        // draw a "bubble" with an arrow pointing at the right thing.
        
        let indexPath = NSIndexPath(forRow: dataArray.count - 1, inSection: 0)
        let cell =  tblView.cellForRowAtIndexPath(indexPath) as! ReplyMessageTableViewCell
        
       
        presentationController?.sourceView =  cell.btnAssignment
        
        self.presentViewController(imagePicker, animated: true) {}
        
    }
    
     // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        imageData = nil
        imageName = ""
        let indexPath = NSIndexPath(forRow: dataArray.count - 1, inSection: 0)
        let cell =  tblView.cellForRowAtIndexPath(indexPath) as! ReplyMessageTableViewCell
        cell.btnAssignment .setTitle("Choose Image", forState: .Normal)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
 
        var newImage: UIImage
        
        if let possibleImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            newImage = possibleImage
        } else if let possibleImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            newImage = possibleImage
        } else {
            return
        }
        
        let imageUrl          = info[UIImagePickerControllerReferenceURL] as! NSURL
        imageName         = imageUrl.lastPathComponent
     //--ww   let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first as String!
        //--wwlet photoURL          = NSURL(fileURLWithPath: documentDirectory)
       //--ww let localPath         = photoURL.URLByAppendingPathComponent(imageName!)
     //--ww   let image             = info[UIImagePickerControllerOriginalImage]as! UIImage
       imageData      = UIImageJPEGRepresentation(newImage, 0.6)
        
        // do something interesting here!
        print(newImage.size)
        printLog(imageName)
        imageName = "Attachment"
       //--ww  printLog(data)
        
        let indexPath = NSIndexPath(forRow: dataArray.count - 1, inSection: 0)
        let cell =  tblView.cellForRowAtIndexPath(indexPath) as! ReplyMessageTableViewCell
        
        cell.btnAssignment .setTitle(imageName, forState: .Normal)
      
        
        
       dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func doPostMsg(sender: buttonFontCustomClass) {
       
       
        let indexPath = NSIndexPath(forRow: dataArray.count - 1, inSection: 0)
        let myArray =  tblView.indexPathsForVisibleRows
        if myArray?.contains(indexPath) == false{
        return
        
        }
        let cell =  tblView.cellForRowAtIndexPath(indexPath) as! ReplyMessageTableViewCell
        
        if stringsClass.isEmptyString(cell.txtView.text!) == "" || cell.txtView.text! == "Type your message here!" {
            
            Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: "Message can not be blank!", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                
            })
        
        }else{
           let idThread =  self.dataDict["threadId"] as! Int
            let idString = "\(idThread)"
     var parameters = [String:AnyObject]()
        parameters = ["sessionId":UserModel.sharedInstance.sessionID,
             "threadId": idString,
             "comment":cell.txtView.text!,
             "attachTitle":imageName,
             "attachType":"jpg"
             ]
            printLog(parameters)
            
         
            
        
        AFWrapper.UploadImageWithParamteres(parameters, imgData: imageData, URL: "postMessage", success: { (res) in
            printLog(res)
            
            self.dataArray.removeLast()
            self.dataArray = self.dataArray + res["data"].arrayValue
            
          //--NS  self.dataArray.append(res["data"].dictionaryObject!)
            self.dataArray.append("forReply")
            self.tblView.reloadData()
            }, failure: { (NSError) in
           printLog(NSError)
        })
        
        
        }
        
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if GlobalStaticMethods.isPhone() {
            return .Portrait
        }else{
            
            return .Landscape
        }
    }

}
extension UIImagePickerController
{
    public override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if GlobalStaticMethods.isPhone() {
        return .Portrait
        }else{
        
        return .Landscape
        }
    }
    
    public override func shouldAutorotate() -> Bool {
        return false
    }
}




