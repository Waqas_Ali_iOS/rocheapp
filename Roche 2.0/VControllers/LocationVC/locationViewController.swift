//
//  locationViewController.swift
//  Roche
//
//  Created by Syed Sharjeel Ali on 10/14/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import GoogleMaps

class locationViewController: HeaderViewController,GMSMapViewDelegate {
    
    @IBOutlet weak var mapView1: GMSMapView!
    var locationTitle = ""
    
    @IBOutlet weak var mapView: GMSMapView!
    
    
    
    
    var lat : Double = 25.0805422
    var long : Double = 55.1403426
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        _headerView.heading = "Location"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        let location = CLLocationCoordinate2DMake(lat, long)
        mapView1.camera = GMSCameraPosition(target: location, zoom: 15, bearing: 0, viewingAngle: 0)
        let marker = GMSMarker()
        
        //     cllocationdeg
        
        marker.position = location //CLLocationCoordinate2DMake(strLat, strLong)//CLLocationCoordinate2DMake(strLat, strLong)
        marker.icon = UIImage(named: "mapIcon")
        //        marker.title = locationTitle
        //            marker.title = "Sydney"
        marker.snippet = locationTitle
        
        marker.map = mapView1
        mapView1.delegate = self
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
        self.mm_drawerController.openDrawerGestureModeMask = .None
    }
    
    func mapView(mapView: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {
        
        let url = NSURL(string: "comgooglemaps://")
        let isGoogleMap = UIApplication.sharedApplication().canOpenURL(url!) as Bool
        ////        BOOL isGoogleMap = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
        //        UIAlertView *alert;
        
        
        
        if(isGoogleMap)
        {
            
            UIApplication.sharedApplication().openURL(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(lat),\(long)&directionsmode=driving")!)
            
            
            
            print("Google Maps is")
        }
        else{
            let alert = UIAlertController(title: "Message", message: "In order to navigate, Kindly install Google Maps.", preferredStyle: .Alert)
            
            let action = UIAlertAction(title: "Ok", style: .Default, handler: nil)
            
            alert.addAction(action)
            
            
            self.presentViewController(alert, animated: true, completion: nil)
            
            
        }
        //        else {
        //            print("Google Maps Not")
        //        
        //            marker.showInfoWindow();
        //        }
        
        
        return true
    }
    
}




/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */


