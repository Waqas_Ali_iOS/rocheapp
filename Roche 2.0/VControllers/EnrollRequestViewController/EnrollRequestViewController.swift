//
//  EnrollRequestViewController.swift
//  Roche
//
//  Created by TRA-MBP02 on 10/27/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import SwiftyJSON

class EnrollRequestViewController: HeaderViewController, EnrollRequestTableViewCellDelegate, AFWrapperDelegate {
    
    var refreshControl: UIRefreshControl!
    
    @IBOutlet weak var tableview : UITableView!
    @IBOutlet weak var viewCalender : UIView!
    @IBOutlet weak var viewResult : UIView!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    @IBOutlet var topViewConstraint : NSLayoutConstraint?
    
    var  arrData = [JSON]()
    var strBookMark = 0
    var courseId : Int!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        //  refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(EnrollRequestViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        tableview.addSubview(refreshControl) // not required when using UITableViewController
        
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 140
        _headerView.heading = "Enroll Requests"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        if GlobalStaticMethods.isPad() {
            if  GlobalStaticMethods.isPadPro() {
                self.topViewConstraint?.constant = 51
                
            }else{
                self.topViewConstraint?.constant = 40
            }
        }
        
        self.serviceCall()
        
    }
    
    func refresh(sender:AnyObject) {
        
        self.serviceCall()
        // self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.mm_drawerController.openDrawerGestureModeMask = .None
        
    }
    
    
    func parseResponce(res : JSON)
    {
        
        if res["data"] != nil
        {
            if res["data"]["items"] != nil
            {
                
                arrData = res["data"]["items"].arrayValue
                self.tableview.reloadData()
                
            }
        }
    }
    func serviceCall()  {
        
        let user = UserModel.sharedInstance
        AFWrapper.delegate = self
        
        AFWrapper.serviceCall(AFUrlRequestNabeel.EnrollRequest(), success: { (res) in
            
           printLog(res)
            self.parseResponce(res)
            
            
        }) { (NSError) in
            printLog(NSError)
        
            
        }
        
    }
    
    func ApproveOrReject(sender: UIButton, forIndex:Int) {
        

        
        
   //     let cell = EnrollRequestTableViewCell()
        
        
        let userID = arrData[forIndex]["id"].stringValue
        var status = ""
        var msg = ""
        
        if sender.tag == 1{
            
            msg = "Are you sure, you want to approve the request?"
            status = "1"
            
        }
        else{
            
            msg = "Are you sure, you want to reject the request?"
            status = "2"
            
        }
        
        Alert.showAlertMsgWithTitle("Confirmation", msg: msg , otherBtnTitle: "Yes", otherBtnAction:{ (Void) in
            
            AFWrapper.serviceCall(AFUrlRequestNabeel.EnrollrequestStatus(userID:userID , status: status), success: { (res) in
                
                printLog(res)
                
                if status == "2"{
                    
                
                    if res["statusCode"] == 1000{
                    
                        msg = "Successfully rejected"

                        
                        let buttonPosition = sender.convertPoint(CGPointZero, toView: self.tableview)
                        let indexPath = self.tableview.indexPathForRowAtPoint(buttonPosition)
                        
                        self.arrData.removeAtIndex(indexPath!.row)
                        self.tableview.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Automatic)
                        
                        
                        _ = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(EnrollRequestViewController.loadData), userInfo: nil, repeats: false)
                        
                        
                       // self.tableview.reloadData()
                }
                
                }
                else{
                    if res["statusCode"] == 1000{
                        
                        msg = "Successfully approved"
                        
                        let buttonPosition = sender.convertPoint(CGPointZero, toView: self.tableview)
                        let indexPath = self.tableview.indexPathForRowAtPoint(buttonPosition)
                        
                        self.arrData.removeAtIndex(indexPath!.row)
                        self.tableview.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Automatic)
                       // self.tableview.reloadData()
                        
                         _ = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(EnrollRequestViewController.loadData), userInfo: nil, repeats: false)

                    }
                    
                }
                
                
                
                
               // self.parseResponce(res)
                
                
            }) { (NSError) in
                printLog(NSError)
                
              
            }

            
            }, cancelBtnTitle: "NO", cancelBtnAction: { (Void) in
                
            }, viewController: self)
        
        
        
        
        
        
    }
    
    
    func didTapOnOKayOnAFWrapperAlert(errorCode : Int) -> Void {
        
        printLog("Do your awesome work here....")
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    func goToCalendar(){
        
        leftMenu.pushOrPop("customCalendarViewController", sideDrawer: self.mm_drawerController)
        
    }
    func filter(){
        
        leftMenu.pushOrPop(constants.viewControllers.filterVC, sideDrawer: self.mm_drawerController)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        if  arrData.count != 0 {
            
            self.tableview.hidden = false
            
            return ( arrData.count)
        }else{
            self.tableview.hidden = true
            return 0
        }
    }
    
   
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> EnrollRequestTableViewCell {
        [self.tableview.separatorColor = UIColor .clearColor()]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! EnrollRequestTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.delegate = self
    
        cell.tag = indexPath.row
        
        
        cell.btnAccept?.tag = 1
        cell.btnReject?.tag = 2
        cell.row = indexPath.row
        
        var name = arrData[indexPath.row]["name"].stringValue
        var designation = arrData[indexPath.row]["designation"].stringValue

        
        cell.lblNameDesignation?.text = "\(name)\n\(designation)"
        cell.lblCourseTitle?.text = arrData[indexPath.row]["courseTitle"].stringValue
     //   cell.lblDescription?.text = arrData[indexPath.row]["title"].stringValue

        
        
        //cell.lblDescription?.text = arrData[indexPath.row]["shortDescription"].stringValue
//        cell.btnBookMark?.tag = arrData[indexPath.row]["id"].int!
//        cell.btnDetail?.tag = arrData[indexPath.row]["id"].int!
//        cell.btnSuggest?.tag = arrData[indexPath.row]["id"].int!
        
//
//        
//        if arrData[indexPath.row]["courseType"].int == 1{
//            cell.lblTitle?.text = Constants.ONLINE_COURSE
//            cell.imgCourse?.image = UIImage (named: Constants.ONLINE_COURSE_IMAGE)
//            
//        }
//        else{
//            cell.lblTitle?.text = Constants.INCLASS_COURSE
//            cell.imgCourse?.image = UIImage (named: Constants.INCLASS_COURSE_IMAGE)
//            
//        }
//        
        
        
        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor.whiteColor()
            
            
        } else{
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
        
//        if GlobalStaticMethods.isPad() {
//            if  GlobalStaticMethods.isPadPro() {
//                cell.btnHeightConstraint?.constant = 51
//                
//            }else{
//                cell.btnHeightConstraint?.constant = 40
//            }
//        }
        
        //print(cell.btnDetail!.tag)
        cell.tag = indexPath.row
        
        return cell
        
        
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if  constants.deviceType.IS_IPHONE {
            
            return UITableViewAutomaticDimension
            
        }
            
        else if GlobalStaticMethods.isPad() {
            if GlobalStaticMethods.isPadPro() {
                print("ipad pro changed orientation")
                
            }
            
        }
        return UITableViewAutomaticDimension
        
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    
    
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }
    
    
    func loadData()
    {
        if arrData.count == 0
        {
             self.navigationController?.popViewControllerAnimated(true)
        }
        else
        {
            self.tableview.reloadData()
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
