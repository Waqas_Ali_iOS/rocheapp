//
//  DetailViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/10/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit
import SwiftyJSON

class DetailViewController: UIViewController,DetailTableViewCellDelegate {

    
    @IBOutlet weak var tableview : UITableView!
    @IBOutlet var topConstraint : NSLayoutConstraint?

    var dataDict = [String:JSON]()
    var dataArray = []
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if #available(iOS 9.0, *) {
            topConstraint?.constant = GlobalStaticMethods.getTabsHeight()
        }
        
        self.tableview.bounces = false
        

        
        let user = UserModel.sharedInstance
        
        
        AFWrapper.serviceCall(AFUrlRequestNabeel.ContactDetail(sessionId:    user.sessionID), success: { (res) in
            
            
            
            if (res.dictionary!["statusCode"]?.int)! == 1000
            {

            self.dataDict = (res.dictionary!["data"]?.dictionary)!
            print("dataDict" , self.dataDict)
           // dataDict = dataDict["items"]
        
           printLog(self.dataDict["items"]![0]["title"].stringValue) 
            self.tableview .reloadData()
            }
            else if res["statusCode"].intValue == statusCode.noRecordFound.rawValue{

              Alert.showAlertMsgWithTitle("Message", msg: "No record found", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                
              })
                
                
            }
            else{
                
            }
            
         //   print(res)
            }) { (NSError) in
                printLog(NSError)

        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        
        if self.dataDict["items"] != nil {
        return (self.dataDict["items"]?.count)!
        }else{
        return 0
        }
        //
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       [self.tableview.separatorColor = UIColor .clearColor()]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! DetailTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
       cell.lblTitle!.text = self.dataDict["items"]![indexPath.row]["title"].stringValue
        cell.btnEmail!.setTitle(self.dataDict["items"]![indexPath.row]["email"].stringValue, forState: UIControlState.Normal)
        
        cell.btnNumber!.setTitle(self.dataDict["items"]![indexPath.row]["phone"].stringValue, forState: UIControlState.Normal)
        
        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor.whiteColor()
            
            
        } else{
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
        cell.delegate = self
        return cell
        
    }

    
    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        
        let deviceWidth = CGRectGetWidth(self.view.frame);
        let deviceHeight = CGRectGetHeight(self.view.frame);
        let cellSize:CGFloat

        if GlobalStaticMethods.isPhone() {
            
            
            if UIInterfaceOrientationIsPortrait(UIApplication .sharedApplication() .statusBarOrientation) {
                cellSize = deviceHeight * 0.25
                return cellSize
                
            }
            
            print("iphone changed orientation")
        }        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
            
            
            
                    if UIInterfaceOrientationIsLandscape(UIApplication .sharedApplication() .statusBarOrientation) {
                        cellSize = deviceHeight * 0.2
                        return cellSize
            
                    }
            
                }
                else{
                    if UIInterfaceOrientationIsLandscape(UIApplication .sharedApplication() .statusBarOrientation) {
                        cellSize = deviceHeight * 0.25
                        return cellSize
           // self.viewSupplementHeightConstraint.constant = 50
        }
            
            
                    print("ipad changed orientation")
                }
            }
        }

        
        
        return 87
            
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
      //--ww  topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        
        let deviceWidth = CGRectGetWidth(self.view.frame);
        let deviceHeight = CGRectGetHeight(self.view.frame);

        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                
                
                
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }

    
    
    func PhoneNoTappedDelegate(cell : DetailTableViewCell) {
        
     if cell.seletedItem == .Phone
     {
        if let phoneCallURL:NSURL = NSURL(string: "telprompt://\(cell.btnNumber.titleLabel!.text!)") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
    else
    {
        if let emailURL:NSURL = NSURL(string: "mailto:\(cell.btnEmail.titleLabel!.text!)") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(emailURL)) {
                application.openURL(emailURL);
            }
        }
    }
        
       
        print("Succes")
        
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
