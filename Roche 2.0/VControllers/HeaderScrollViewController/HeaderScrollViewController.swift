//
//  HeaderScrollViewController.swift
//  Roche
//
//  Created by Syed Sharjeel Ali on 10/26/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class HeaderScrollViewController: ScrollHelperViewController,HeaderViewDelegate {

    var _headerView:HeaderView!
    var _footerView:HeaderView!
    
    internal var viewsDictionary = Dictionary<String,AnyObject>()
    internal var constraints = [NSLayoutConstraint]()
    var headerViewHeight : CGFloat = 56
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        //  self.loadWithNib("HeaderView", viewIndex: 0, owner: self)
        
        
        headerViewHeight = GlobalStaticMethods.getHeaderFooterHeight()
        
        self._headerView =  HeaderView(frame: CGRectMake(0, 0, self.view.frame.size.width, headerViewHeight))
        self.view.addSubview(self._headerView!)
        
        
        
        self._footerView = HeaderView(frame: CGRectMake(0, (self.view.frame.size.height - headerViewHeight), self.view.frame.size.width, headerViewHeight))
        
        self.view.addSubview(self._footerView!)
        
        
        
        
        _headerView.delegate = self
        _footerView.delegate = self
        
        configureHeaderViewConstraints()
        
        self .updateHeaderWithHeadingText("", rightBtnImageName: "", leftBtnImageName:"", backgroundImageName: "", bgColor: "#113c8b")
        
        self .updateFooterWithHeadingText("", rightBtnImageName: "", leftBtnImageName:"", backgroundImageName: "", bgColor: "#113c8b")
        
        _footerView.hidden = true
        
        
        //        self._footerView.btnFooter = UIButton.init(type: .Custom)
        //
        //        self._footerView.btnFooter  .frame = CGRectMake(0, 0,  self._footerView.frame.size.height , self._footerView.frame.size.height)
        //
        //        self._footerView.btnFooter.backgroundColor = UIColor.clearColor()
        //        self._footerView.addSubview(self._footerView.btnFooter!)
        
        self._footerView.btnFooter  .addTarget(self, action: #selector(HeaderViewController.didFooterBtnPressed), forControlEvents: .TouchUpInside)
        
        
        self._footerView.btnFooter  .addTarget(self, action: #selector(HeaderViewController.heighlightFooter), forControlEvents: .TouchDown)
        
        
        self._footerView.btnFooter  .addTarget(self, action: #selector(HeaderViewController.unHeighlightFooter), forControlEvents: .TouchCancel)
        
        self._footerView.btnFooter  .addTarget(self, action: #selector(HeaderViewController.unHeighlightFooter), forControlEvents: .TouchDragOutside)
        
        
        
        
        
        
    }
    
    public override func viewDidAppear(animated: Bool) {
        
        
    }
    
    
    
    
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadWithNib(nibName:String, viewIndex:Int, owner: AnyObject) -> Void {
        let headerNib : UIView = (NSBundle.mainBundle().loadNibNamed(nibName, owner: owner, options: nil) as NSArray).objectAtIndex(viewIndex) as! UIView;
        
        
        headerNib.frame = CGRectMake( 0, 0, self.view.frame.width, 56)
        
        self.view.addSubview(headerNib)
        
        
    }
    
    
    
    
    public func updateHeaderWithHeadingText(hText: String, rightBtnImageName: String, leftBtnImageName: String, backgroundImageName: String, bgColor: String  ) -> Void {
        
        
        _headerView.updateHeaderWithHeadingText(hText, rightBtnImageName: rightBtnImageName, leftBtnImageName: leftBtnImageName, backgroundImageName:backgroundImageName, bgColor: bgColor);
        
        
    }
    
    
    
    public func updateFooterWithHeadingText(hText: String, rightBtnImageName: String, leftBtnImageName: String, backgroundImageName: String, bgColor: String  ) -> Void {
        
        
        _footerView.updateHeaderWithHeadingText(hText, rightBtnImageName: rightBtnImageName, leftBtnImageName: leftBtnImageName, backgroundImageName:backgroundImageName, bgColor: bgColor);
        
        
    }
    
    
    
    
    func headerViewLeftBtnDidClick(headerView: HeaderView) {
        
        
        
        
        if headerView == self._headerView {
            print("leftButtonClicked")
            if(self._headerView.leftButtonImage != constants.imagesNames.LEFT_MENU_BACK_ICON){
                
                
                mm_drawerController .toggleDrawerSide(.Left, animated: true) { (success) in
                    print("Allhamdulillah")
                    
                }
                
            }
            else{
                self.navigationController!.popViewControllerAnimated(true)
            }
        }else{
            
            print("leftButtonClicked footer")
            
        }
    }
    
    
    func headerViewRightBtnDidClick(headerView: HeaderView) {
        print("rightButtonClicked")
        // self._headerView.lblHeading.text = "waqas ali"
        
        self._headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
    }
    
    
    func didFooterBtnPressed (sender : AnyObject) {
        
        
        self._footerView.viewContainer.backgroundColor =  UIColor(hexString: self._footerView.bgColorVar!)
        
    }
    
    
    func heighlightFooter(){
        
        
        self._footerView.viewContainer.backgroundColor =  UIColor(hexString: self._footerView.bgColorHeighlightedFooter!)
        
        
    }
    
    func unHeighlightFooter(){
        
        self._footerView.viewContainer.backgroundColor =  UIColor(hexString: self._footerView.bgColorVar!)
    }
    
    
    func configureHeaderViewConstraints(){
        
        
        viewsDictionary["headerView"] = _headerView
        
        _headerView.translatesAutoresizingMaskIntoConstraints = false
        _footerView.translatesAutoresizingMaskIntoConstraints = false
        
        let strVConstraints = "V:|-0-[headerView(\(headerViewHeight))]"
        
        
        let strHConstraints = "H:|-0-[headerView]-0-|"
        
        
        viewsDictionary["footerView"] = _footerView
        
        _headerView.translatesAutoresizingMaskIntoConstraints = false
        
        let strVConstraintsfooterView = "V:[footerView(\(headerViewHeight))]-0-|"
        
        
        let strHConstraintsfooterView = "H:|-0-[footerView]-0-|"
        
        
        setConstraintsForHeader(strVConstraints)
        setConstraintsForHeader(strHConstraints)
        setConstraintsForHeader(strVConstraintsfooterView)
        setConstraintsForHeader(strHConstraintsfooterView)
        
        
        NSLayoutConstraint.activateConstraints(constraints)
        
    }
    
    func setConstraintsForHeader(format : String){
        
        let newConstraint = NSLayoutConstraint.constraintsWithVisualFormat(format, options:[], metrics: nil, views: viewsDictionary)
        
        constraints += newConstraint
        
    }
    
    
    public override func prefersStatusBarHidden() -> Bool {
        return true
    }

}
