//
//  InviteNewMemberViewController.swift
//  RocheApp
//
//  Created by  Traffic MacBook Pro on 02/08/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import SwiftyJSON
class InviteNewMemberViewController: HeaderViewController, UITextFieldDelegate {

    
    @IBOutlet var btnInvite: buttonFontCustomClass!
    @IBOutlet var txtDesignation: BaseUITextField!
    @IBOutlet var txtEmail: BaseUITextField!
    @IBOutlet var txtName: BaseUITextField!
    @IBOutlet var topConstraint : NSLayoutConstraint?

        override func viewDidLoad() {
        super.viewDidLoad()

        _headerView.heading = "Invite New Member"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
            
            self.btnInvite .addTarget(self, action: #selector(InviteNewMemberViewController.invite), forControlEvents: .TouchUpInside)
            
           
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view .endEditing(true)
        
    }
    func invite(){
        print("Invite them")
        
        self.view.endEditing(true)
        
        if stringsClass.isEmptyString(txtEmail.text!) == "" {
        
        Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: "Email field can not be blank!", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
            
        })
        return
        }
        
        
        
        if GlobalStaticMethods.isValidEmail(txtEmail.text!) == true{
        
        AFWrapper.serviceCall(AFUrlRequestHamza.InviteNewMembers(userEmail: txtEmail.text!), success: { (res) in
            
            printLog(res)
            
            
            var statusCodeVal = res["statusCode"].intValue
         
            
            let strMessage = res["statusMessage"].stringValue
            
            
            if statusCodeVal ==  statusCode.success.rawValue{
            Alert.showAlertMsgWithTitle("Success", msg: strMessage, btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
                print("")
            })
            
            }
            else{
                Alert.showAlertMsgWithTitle("Error", msg: strMessage, btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
                    print("")
                })

            }
            
            

            
            
            }) { (NSERROR) in
                printLog(NSERROR)
        }
        }
        else{
            
            Alert.showAlertMsgWithTitle("Error", msg: "Email not valid", btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
                print("")
                self.txtEmail.becomeFirstResponder()
                
            })
            
            
        }

    }
    

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == txtName{
            txtEmail.becomeFirstResponder()
        }
        else if textField == txtEmail{
            txtDesignation.becomeFirstResponder()
        }
        else {
            txtDesignation.resignFirstResponder()
        }
        return true
        
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
