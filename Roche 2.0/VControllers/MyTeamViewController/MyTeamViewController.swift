//
//  MyTeamViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/10/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyTeamViewController: HeaderViewController ,AFWrapperDelegate{

    @IBOutlet weak var tableview : UITableView!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    
    
    var offset : Int = 0
    var limit : Int = 0
    
    var arrData = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.tableview.rowHeight = UITableViewAutomaticDimension
//        self.tableview.estimatedRowHeight = 140
        self.tableview.separatorColor = UIColor .clearColor()
        self.tableview.separatorStyle = .None

        _headerView.heading = "My Team"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        // Do any additional setup after loading the view.
       
        self.tableview.bounces = false
        
        callService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }

    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
        
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //[self.tableview.separatorColor = UIColor .clearColor()]
        self.tableview.separatorColor = UIColor .clearColor()

        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! MyTeamTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        
        // Code for Shadow color of the cell
        
//        cell.layer.borderColor = UIColor.lightGrayColor().CGColor
//        cell.layer.borderWidth = 0.5
//        cell.layer.shadowOffset = CGSizeMake(0, 1)
//        cell.layer.shadowColor = UIColor.lightGrayColor().CGColor
//        cell.layer.shadowRadius = 1.0
//        cell.layer.shadowOpacity = 0.8
//        cell.layer.masksToBounds = false
        
        
        cell.setupCell(arrData[indexPath.row] as! Dictionary<String, AnyObject>)
        
        return cell
        
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    

//    func tableView(tableView: UITableView,
//                            heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        
//        
//        
//        return UITableViewAutomaticDimension
//        
//    }
    
    
    // MARK: - Web service call methods
    
    func callService()
    {
        AFWrapper.delegate = self
        AFWrapper.serviceCall(AFUrlRequestSharjeel.GetMyTeam(offset: offset, limit: limit), success: { (res) in
            
            self.parseResponce(res)
            
        }) { (NSError) in
            printLog(NSError)
        }
        
    }
    
    
    func parseResponce(res : JSON)
    {
        
        if res["data"] != nil
        {
            if res["data"]["items"] != nil
            {
                
                arrData = res["data"]["items"].arrayObject!
                self.tableview.reloadData()
                
            }
        }
    }
    
    
    
    func didTapOnOKayOnAFWrapperAlert(errorCode : Int) -> Void {
        
        printLog("Do your awesome work here....")
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
//
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
