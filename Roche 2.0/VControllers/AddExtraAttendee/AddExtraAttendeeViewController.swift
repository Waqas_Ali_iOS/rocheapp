//
//  AddExtraAttendeeViewController.swift
//  RocheApp
//
//  Created by  Traffic MacBook Pro on 02/08/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class AddExtraAttendeeViewController: HeaderViewController,STCustomDelegate, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate {

    @IBOutlet var btnAdd: buttonFontCustomClass!
    @IBOutlet var txtHospitalName: BaseUITextField!
    @IBOutlet var txtDesignation: BaseUITextField!
    @IBOutlet var txtEmail: BaseUITextField!
    @IBOutlet var txtName: BaseUITextField!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    var isTypingDesignation  = false
    var courseID = 0
    var locationID = 0
    
    
    var  suggestionTblView : UITableView?
    var  arrSugesstionData = Array<AnyObject>()
    var CSTable = STable()
    weak var timer: NSTimer?
    let single = Singleton.sharedInstance


    
    
    override func viewDidLoad() {

        super.viewDidLoad()
        _headerView.heading = "Add Extra Attendee"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        btnAdd.addTarget(self, action: #selector(AddExtraAttendeeViewController.addAttendees), forControlEvents: .TouchUpInside)
        txtName.delegate = self
        txtHospitalName.delegate = self
        txtEmail.delegate = self
        txtDesignation.delegate = self
        
        txtEmail.keyboardType = .EmailAddress
        
        
        txtName.tag = 0
        txtHospitalName.tag = 0
        txtEmail.tag = 0
        txtDesignation.tag = 0
        
        
        txtHospitalName.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        txtDesignation.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)



        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func addAttendees(){
        
        
        self.view.endEditing(true)
        let name = txtName.text
        let hospitalName = txtHospitalName.text
        let email = txtEmail.text
        let designation = txtDesignation.text
        
        var strMessageError = ""
        var validated = false
        if name == "" {
            
            strMessageError = "Name cannot be empty"
            validated = false
        }
        else if name?.characters.count < 3 {
      
            strMessageError = "Name must be of 3 characters atleast"
            validated = false
        }
        else if email == "" {
            strMessageError = "Email  cannot be empty"
            validated = false
        }
        else if GlobalStaticMethods.isValidEmail(email!) == false{
            strMessageError = "Invalid Email"
            validated = false
        }
        
        else if designation == "" {
            strMessageError = "Designation cannot be empty"
            validated = false
        }
        else if hospitalName == "" {
            strMessageError = "Hospital name cannot be empty"
            validated = false
        }
    
        else{
            strMessageError = ""
            
            validated = true
        }
        
        webserviceCall(name!, hospitalName: hospitalName!, email: email!, designation: designation! , validated:  validated , strErrorMessage:  strMessageError)
        
    
    }
    
    
    
    func webserviceCall(name : String , hospitalName : String , email : String , designation : String , validated : Bool , strErrorMessage : String){
        
        
        if validated {
            
                    AFWrapper.serviceCall(AFUrlRequestHamza.AddExtraAttendees(courseId: courseID, locId: locationID, name: name, email: email, designation: designation, hospName: hospitalName), success: { (res) in
                        printLog(res)
                        
                        if res["statusCode"].intValue == statusCode.success.rawValue {
                            
                            Alert.showAlertMsgWithTitle("Success", msg: "Extra attendee added successfully", btnActionTitle: "Ok", viewController: self, completionAction: { (success) in

                            self.navigationController?.popViewControllerAnimated(true)
                            
                            })

                            
                        }
                        
                    }) { (NSERROR) in
                        printLog(NSERROR)
                    }
            
                    }
                    else {
                        Alert.showAlertMsgWithTitle("Error", msg: strErrorMessage, btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
                            printLog("Completed button action")
                        })
                    }

//        AFWrapper.serviceCall(AFUrlRequestHamza.AddExtraAttendees(courseId: courseID, locId: locationID, name: name, email: email, designation: designation, hospName: hospitalName), success: { (res) in
//            printLog(res)
//        }) { (NSERROR) in
//            printLog(NSERROR)
//        }
//            
//        }
//        else {
//            Alert.showAlertMsgWithTitle("Error", msg: strErrorMessage, btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
//                printLog("Completed button action")
//            })
//        }

    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == txtName{
            txtEmail.becomeFirstResponder()
        }
        else if textField == txtEmail{
            txtDesignation.becomeFirstResponder()
        }
        else if textField == txtDesignation{
            txtHospitalName.becomeFirstResponder()
        }
        else{
            txtHospitalName.resignFirstResponder()
        }
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view .endEditing(true)
        
        self.suggestionTblView?.hidden = true
    }

    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    // MARK: - TextField Delegates
    
    func textFieldDidBeginEditing(textField: UITextField) {
        //        delegate?.didTypeOnPredictiveEmail!(self)
        self.suggestionTblView?.hidden = true
	
        
    }
    
    
    func textFieldDidChange(textField: UITextField) {
        //your code
        
        if textField == txtHospitalName {
            isTypingDesignation = false
            didTypeOnHospitalTextfield()
        }
        else if textField == txtDesignation{
            isTypingDesignation = true
            didTypeOnDesignationTextfield()
        }
        else{
            suggestionTblView?.hidden = true
        }
        
    }
    
    
  
    
//    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
//        
//        
//        return true
//    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        
    }
    
    
    
    // MARK: - Predictive Hospitals
    
    func didTypeOnDesignationTextfield() {
        arrSugesstionData.removeAll()
        
        let strkeyword = txtDesignation.text
        var diction = Dictionary<String,AnyObject>()
        
        
        diction["strKeyword"] = strkeyword
        //diction["indexPath"] = 7
        
        //    ShowHideSuggestionTable()
        
        
        if strkeyword != "" {
            //     suggestionTblView?.hidden = true
            
            if (timer != nil)
            {
                timer?.invalidate()
            }
            timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(self.validateTextField(_:)), userInfo: diction, repeats: false)
        }else{
            timer?.invalidate()
            suggestionTblView?.hidden = true
            
        }
        
        
    }
    
    func didTypeOnHospitalTextfield() {
        arrSugesstionData.removeAll()

        let strkeyword = txtHospitalName.text
        var diction = Dictionary<String,AnyObject>()
        
        
        diction["strKeyword"] = strkeyword
        //diction["indexPath"] = 7
        
    //    ShowHideSuggestionTable()
        
        
        if strkeyword != "" {
       //     suggestionTblView?.hidden = true

            if (timer != nil)
            {
                timer?.invalidate()
            }
            timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(self.validateTextField(_:)), userInfo: diction, repeats: false)
        }else{
            timer?.invalidate()
            suggestionTblView?.hidden = true
            
        }
        
    
    }
    
    func validateTextField(time : NSTimer){
      //  self.suggestionTblView?.hidden = true

//        self.view.endEditing(true)
        var arrForSearch = Array<AnyObject>()
        let dict = time.userInfo as! Dictionary<String,AnyObject>

        if isTypingDesignation == true{
            let arr = ["Lab Manager", "Lab Supervisor", "Lab Technician"]

            let designationArr = arr
            arrForSearch = designationArr

        }
        else{
            if single.arrHospitals.count > 0 {
            let hospitalArr = single.arrHospitalsName
            arrForSearch = hospitalArr
            }
            
        }
        
        if arrForSearch.count > 0 {
            
            arrSugesstionData.removeAll();
            
            
            
            
            let str = dict["strKeyword"] as! String
            
            let namePredicate = NSPredicate(format: "SELF CONTAINS [c] %@", str)
            
            arrSugesstionData =  (arrForSearch.filter { namePredicate.evaluateWithObject($0) });
            
            
            
            
            if arrSugesstionData.count == 0
            {
                arrSugesstionData.append("No Record Found")
                
                //    suggestionTblView?.hidden = true
                
            }
            CreatTableForSuggestion()
        }
        else{
            suggestionTblView?.hidden = true

        }
    
        
    }
    
    
    func CreatTableForSuggestion()
    {
        
        if arrSugesstionData.count > 0 {
            
        
        if suggestionTblView == nil
        {
            
             let txtFieldframe : CGRect?
            if isTypingDesignation == true{
                txtFieldframe = txtDesignation.frame
            }
            else{
                txtFieldframe =  txtHospitalName.frame

            }
            
            let yPosition = getCellHeight() - (44 * 4)
            
            var ratio : CGFloat = 1.1
            
            if GlobalStaticMethods.isPad() {
                ratio = 1.05
            }
            
            let tblView = UITableView(frame: CGRectMake(txtFieldframe!.origin.x, yPosition , txtFieldframe!.size.width * ratio , 44 * 4), style: .Plain)
            
            if #available(iOS 9, *) {
                tblView.cellLayoutMarginsFollowReadableWidth = false
            }
            
            
            //            tblView.backgroundColor = UIColor.lightGrayColor()
            
            
            if isTypingDesignation == true{
            tblView.center = CGPointMake(txtDesignation.center.x, tblView.center.y)
            }
            else{
                tblView.center = CGPointMake(txtHospitalName.center.x, tblView.center.y)

            }
            
            tblView.layer.borderColor = UIColor.lightGrayColor().CGColor
            tblView.layer.borderWidth = 1.0
            
            tblView.layer.cornerRadius = 10
            
            tblView.dataSource = self
            tblView.delegate = self
            
            tblView.layoutMargins = UIEdgeInsetsZero
            tblView.separatorInset = UIEdgeInsetsZero
            
            
            tblView.backgroundColor = UIColor.whiteColor()
            
            tblView.bounces = false
            
            
            
            suggestionTblView = tblView;
            
            suggestionTblView?.hidden = true
            
            self.view.addSubview(suggestionTblView!)
        }
        
        if suggestionTblView?.hidden == true
        {
            
            let yPosition = getCellHeight() - (44 * 4)
            
            //                let tblView = UITableView(frame: CGRectMake(txtFieldframe.origin.x, yPosition , txtFieldframe.size.width * ratio , 44 * 4), style: .Plain)
            
            suggestionTblView?.frame.origin.y = yPosition
            
            
            suggestionTblView?.hidden = false
        }
        suggestionTblView?.reloadData()
        
        
        
        }
        
        //  cell.txtField.activityIndicator.stopAnimating()
        
    }
    
    func getCellHeight() -> CGFloat
    {
        
        
        if isTypingDesignation != true{
            
        
        
        let myRect : CGRect = txtHospitalName.frame
        
        
        let myrecTtwo : CGRect = txtHospitalName.frame//tblViewProfile.convertRect(myRect, toView:  txtHospitalName)
        
        return myrecTtwo.origin.y
        }
        else{
            let myRect : CGRect = txtDesignation.frame
            
            
            let myrecTtwo : CGRect = txtDesignation.frame//tblViewProfile.convertRect(myRect, toView:  txtHospitalName)
            
            return myrecTtwo.origin.y

        }
    }
    
    func ShowHideSuggestionTable()
    {
        if suggestionTblView?.hidden == true {
            suggestionTblView?.hidden = false
        }
        else
        {
            suggestionTblView?.hidden = true
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        if tableView == suggestionTblView {
            
            let selectedCell = tableView.cellForRowAtIndexPath(indexPath)!
            let str = arrSugesstionData[indexPath.row] as! String
            if str == "No Record Found" {
                
                suggestionTblView?.hidden = true
            }
            else {
          
                if isTypingDesignation == true{
                    txtDesignation.text = arrSugesstionData[indexPath.row] as! String

                }
                else{
              txtHospitalName.text = arrSugesstionData[indexPath.row] as! String
//                
//                
                }
                suggestionTblView?.hidden = true

            }
            
        }
    
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
            if GlobalStaticMethods.isPadPro() {
                
                    return 110 //DeviceUtil.size.width * 0.13286
                    
            }
            
            else if GlobalStaticMethods.isPad(){
                    
                    
                return 90 //DeviceUtil.size.width * 0.13286
                
                    
            }
            
            else {
                    
                    return 78 //DeviceUtil.size.width * 0.13286
                    
                }
                
        
        
    }
    
    //MARK: UITableViewDataSource
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
                 return arrSugesstionData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
            
            let cell = UITableViewCell()
            
            cell.backgroundColor = UIColor.clearColor()
            
            cell.layoutMargins = UIEdgeInsetsZero
            
            cell.textLabel?.text = arrSugesstionData[indexPath.row] as! String
            
            cell.textLabel?.textColor = UIColor.blueColor()
            
            
            var lblSize = 0.041063 * DeviceUtil.size.width
            
            if GlobalStaticMethods.isPad()
            {
                lblSize = 0.01464128 * DeviceUtil.size.width
            }

            return cell
        
    }
    
    func numberOfItemsInSelectionList(cSlider: STable, selectedObject: AnyObject) {
        
        if isTypingDesignation == true{
            txtDesignation.text = selectedObject as? String
        }
        else{
        txtHospitalName.text = selectedObject as? String
        }
    }


    

    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
