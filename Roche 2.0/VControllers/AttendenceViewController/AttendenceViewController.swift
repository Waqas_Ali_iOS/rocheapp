//
//  AttendenceViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/11/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit

class AttendenceViewController: HeaderViewController , attendenceTableViewCellDelegate{

   
    @IBOutlet weak var tableview : UITableView!
   
    @IBOutlet var topConstraint : NSLayoutConstraint?

    var arrDataTableView = Array<AnyObject>()
    var refreshControl: UIRefreshControl!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        //  refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        tableview.addSubview(refreshControl) // not required when using UITableViewController
        

        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 140
        _headerView.heading = "Attendance"
        _headerView.rightButtonImage = constants.imagesNames.HEADER_LOGOUT_RIGHT_IMAGE
//        _headerView.btnRight.setTitle("Log out", forState: .Normal)
//        _headerView.btnRight.hidden = false
        
       // _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        

        
        serviceCall()
       
        
       // self.viewCalender .removeFromSuperview()
        //self.viewResult .removeFromSuperview()

        // Do any additional setup after loading the view.
    }
    
    func refresh(sender:AnyObject) {
        
        self.serviceCall()
        // self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    func logMeOut() {
        
            
            AFWrapper.serviceCall(AFUrlRequestHamza.Logout, success : { (res) in
                
                
                
                let userDefaults = NSUserDefaults.standardUserDefaults()
                userDefaults.setBool(false, forKey: Constants.AUTO_LOGIN)
                userDefaults.synchronize()
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.drawerController!.closeDrawerAnimated(true, completion: nil)
                navigation.PopToVC(constants.viewControllers.loginViewController, sideDrawer: appDelegate.drawerController!)
                
                } )
            { (NSError) in
                printLog(NSError)
            }
            
            
     
              
        }

    
    override func headerViewRightBtnDidClick(headerView: HeaderView) {
        
        Alert.showAlertMsgWithTitle("Confirmation", msg: "Do you really want to log out?", otherBtnTitle: "YES", otherBtnAction: { (Void) in
            self.logMeOut()
            }, cancelBtnTitle: "NO", cancelBtnAction: { (Void) in
                
            }, viewController: self)    }

    
    
    
    func serviceCall(){
        
        AFWrapper.serviceCall(AFUrlRequestNabeel.Attendance(offset: 0, limit: 100), success: { (res) in
            printLog(res)
            
             let data =  res["data"].dictionaryValue
            let totalCount = data["totalCount"]?.intValue
            
            
            if totalCount > 0 {
                self.arrDataTableView.removeAll()

                for i in 0 ..< totalCount! {
                    
                    let dict = data["items"]?.arrayObject
                    
                    let courseType =  dict![i]["courseType"] as! Int
                    let title = dict![i]["title"] as! String
                    let courseId = dict![i]["courseId"] as! Int
                    let shortDescription = dict![i]["shortDescription"] as! String

                 let attendanceModel =   attendanceModelClass.init(courseType: courseType, title: title, courseID: courseId, shortDescription: shortDescription)
                   
                    
                    
                    self.arrDataTableView.append(attendanceModel)
                    

                }
                
                
                self.tableview.reloadData()
                // res["data"]arr
                
            }
            
            }) { (NSError) in
                printLog(NSError)
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        if arrDataTableView.count > 0  {
            return arrDataTableView.count
            
        }
        else{
            
            self.tableview.separatorColor = UIColor.clearColor()
            
            return 0
        }    }
    
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        [self.tableview.separatorColor = UIColor .clearColor()]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! AttendenceTableViewCell
        cell.btnSetLocation .setTitle("Select Location", forState: .Normal)
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        
        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor.whiteColor()

            
        } else{
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }

        let cellData = arrDataTableView[indexPath.row] as! attendanceModelClass

        cell.configCell(indexPath.row, data: cellData)
        
        if GlobalStaticMethods.isPad() {
            if  GlobalStaticMethods.isPadPro() {
                cell.btnHeightConstraint?.constant = 51
                
            }else{
                cell.btnHeightConstraint?.constant = 40
            }
        }
        
        cell.delegate = self
        return cell
        
        
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        let cellSize: CGFloat
        
        let deviceHeight = CGRectGetHeight(self.view.frame);
        
        
        if  constants.deviceType.IS_IPHONE {
            
            
//            if constants.deviceType.IS_IPHONE_5 || constants.deviceType.IS_IPHONE_4
//            {
//                cellSize = CGFloat (deviceHeight * 0.35)
//                return cellSize
//                
//            }
//            else{
//                cellSize = CGFloat (deviceHeight * 0.3
//                )
//                
//                print (cellSize)
//                return cellSize
//            }
            
            return UITableViewAutomaticDimension
        }
            
        else if GlobalStaticMethods.isPad() {
            if GlobalStaticMethods.isPadPro() {
                print("ipad pro changed orientation")
                
                
                
//                if UIInterfaceOrientationIsPortrait(UIApplication .sharedApplication() .statusBarOrientation) {
//                    
//                    
//                    cellSize = CGFloat (deviceHeight * 0.25)
//                    return cellSize
//                    
//                }
//                else{
//                    
//                    cellSize = CGFloat (deviceHeight * 0.32)
//                    return cellSize                }
//                
//            }
//            else{
//                if UIInterfaceOrientationIsPortrait(UIApplication .sharedApplication() .statusBarOrientation) {
//                    cellSize = CGFloat (deviceHeight * 0.27)
//                    return cellSize                }
//                else{
//                    cellSize = CGFloat (deviceHeight * 0.36)
//                    return cellSize
//                    
//                }
//                
            }
            
        }
        cellSize = CGFloat (deviceHeight * 0.35)
        return UITableViewAutomaticDimension
        
    }
    func detailBtnTappedDelegate(cell: AttendenceTableViewCell) {
        
        
        let data = cell.cellData
        
        let courseID = data.courseID
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.attendenceListVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.attendenceListVC) as! AttendenceListViewController
        
        vc.courseID = courseID
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
//    func detailBtnTappedDelegate(detailBTN: AttendenceTableViewCell) {
//        
//    }
    func goToCalendar(){
     goToViewController("customCalendarViewController")
        
       
    }
    func filter(){
        goToViewController(constants.viewControllers.filterVC)

    }
    
    
    func goToViewController(identifier : String){
        let sb = navigation.getStoryBoardForController(identifier)!.storyboardObject
        let homeVC = sb.instantiateViewControllerWithIdentifier(identifier)
        self.navigationController!.pushViewController(homeVC, animated: true)
        
    }

//    func tableView(tableView: UITableView,
//                   heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        
//        return 158 //Whatever fits your need for that cell
//    }

    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }

    override func headerViewLeftBtnDidClick(headerView: HeaderView) {
        //
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
