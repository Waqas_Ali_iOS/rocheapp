//
//  ScanQRCodeViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 10/25/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftyJSON


class ScanQRCodeViewController: HeaderViewController,QRCodeReaderViewControllerDelegate , AFWrapperDelegate {

   // @IBOutlet var cameraView : ReaderOverlayView!
    @IBOutlet var cView : UIView!

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var viewCheckAnother: UIView!
    @IBOutlet weak var viewDone: UIView!

    @IBOutlet weak var btnMarkAttendence: UIButton!
    @IBOutlet weak var BtnCheckAnother: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    
    
    
    var locID : Int! = 10
    var courseID : Int! = 10
    var arrUserID : JSON!
    var strUserID : String!
    var arrData  :JSON!
    var filteredString = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        _headerView.heading = "Scan QR Code"
        
        viewDone.hidden = true
        viewCheckAnother.hidden = true
        self.showCamera()
        
        self.btnMarkAttendence.setTitle("Mark Attendance", forState: .Normal)
        
        self.btnDone!.addTarget(self, action: #selector(ScanQRCodeViewController.pressDone), forControlEvents: .TouchUpInside)

        
        self.BtnCheckAnother!.addTarget(self, action: #selector(ScanQRCodeViewController.checkAnotherFunc), forControlEvents: .TouchUpInside)

        self.btnMarkAttendence!.addTarget(self, action: #selector(ScanQRCodeViewController.markAttendence), forControlEvents: .TouchUpInside)

        
        
    }

    func checkAnotherFunc()   {
        
        self.viewDone.hidden = true
        self.viewCheckAnother.hidden = true
        
        
        self.showCamera()
    }
    
    
    func showCamera() {
        
                if QRCodeReader.supportsMetadataObjectTypes() {
                    let reader = createReader()
                    reader.modalPresentationStyle = .FullScreen
                    reader.delegate               = self
        
                    reader.completionBlock = { (result: QRCodeReaderResult?) in
                        if let result = result {
                            // print("Completion with result: \(result.value) of type \(result.metadataType)")
                            
                            let numericSet = "0123456789"
                            let filteredCharacters = result.value.characters.filter {
                                return numericSet.containsString(String($0))
                            }
                            self.filteredString = String(filteredCharacters)
                            print("Completion with result: \(self.filteredString)")
                            
                            printLog(self.filteredString)
                          //  self.arrUserID = result.value
                           
                            self.viewDone.hidden = false
                            
                            
                            
                            
                        }
                    }
        
                    
                    self.addChildViewController(reader)
                    
                    reader.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
                    
                    self.containerView.addSubview(reader.view)
                    reader.didMoveToParentViewController(self)
                    
                    
                    
                }
                else {
                    let alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
                    
                   // presentViewController(alert, animated: true, completion: nil)
                }
        
        
    }
    
    
    
    
//    func parseResponce(res : JSON)
//    {
//        
//        if res["data"] != nil
//        {
//            if res["data"]["items"] != nil
//            {
//                
//                arrData = res["data"]["items"].arrayValue
//                self.tableview.reloadData()
//                
//            }
//        }
//    }
    
    
    func pressDone()  {
        
        
        self.navigationController?.popViewControllerAnimated(true)

    }
    
    
    
    func markAttendence()  {
        
        AFWrapper.delegate = self
        AFWrapper.serviceCall(AFUrlRequestNabeel.AttendanceScan(locId: self.locID, courseId: courseID, userId: self.filteredString ), success: { (res) in
            
            self.arrData = res
            
            if self.arrData["statusCode"].int! == 1000{
                
                printLog("")
                
               Alert.showAlertMsgWithTitle("Message", msg: "Attendance marked successfully", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                
                
                self.viewCheckAnother.hidden = false
                self.viewDone.hidden = true
                
               })
                
                
               
                
                
                
            }
            
            
            
            //   self.parseResponce(res)
            
        }) { (NSError) in
            printLog(NSError)
            
          
            self.showCamera()
            
            
            
        }
        
        
    }
    
    
    func didTapOnOKayOnAFWrapperAlert(errorCode: Int) {
        
        if errorCode == 1000{
            
          printLog("")
            
            
            
            
            viewCheckAnother.hidden = false
            viewDone.hidden = true

            
            
        }
        else if errorCode == 1002{
            
          
            self.showCamera()
            
            viewDone.hidden = true
            viewCheckAnother.hidden = true

            
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
        // Dispose of any resources that can be recreated.
    }
    
   

    
    @IBAction func scanAction(sender: AnyObject) {
        
        
        
    }
    
    // MARK: - QRCodeReader Delegate Methods
    
    func reader(reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        self.dismissViewControllerAnimated(true) { [weak self] in
            
            
            let alert = UIAlertController(
                title: "QRCodeReader",
                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                preferredStyle: .Alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            
            
            // self!.openGuestInfo()
            
            
            // self?.presentViewController(, animated: true, completion: nil)
        }
    }
    
    func readerDidCancel(reader: QRCodeReaderViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    private func createReader() -> QRCodeReaderViewController {
        let builder = QRCodeViewControllerBuilder { builder in
            builder.reader          = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode])
            //builder.showTorchButton = true
            //  builder.showCancelButton = self.showCancelButtonSwitch.on
        }
        

        return QRCodeReaderViewController(builder: builder)
    }
    
    

    
    
    
    
    
    /*
     MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
