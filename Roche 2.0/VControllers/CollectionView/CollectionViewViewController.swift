//
//  CollectionViewViewController.swift
//  Roche
//
//  Created by Syed Sharjeel Ali on 8/17/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class CollectionViewViewController: UIViewController {

    
    let  dataDic : NSMutableDictionary = ["title":"A","text":"1","image":"test"]

    // Custom Cell object
    @IBOutlet weak var customCView: customColView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       // arrCellTypes = [dataDic,dataDic,dataDic,dataDic]
        
        
        customCView?.arrCellData = [dataDic,dataDic,dataDic,dataDic]
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return (customCView?.arrCellData.count)!
    }
    
    // make a cell for each cell index path
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        var cell = CustomCollectionViewCell()
        cell = collectionView.dequeueReusableCellWithReuseIdentifier("CustomCell", forIndexPath: indexPath) as! CustomCollectionViewCell
        
        let dic : NSMutableDictionary = customCView?.arrCellData[indexPath.row] as! NSMutableDictionary
        cell.backgroundColor = UIColor.whiteColor()
        
        
        
        cell.SetupCell(dic, indexpath: indexPath.row)
        
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    //Use for size
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        let  width = (self.view.frame.size.width - 40)  * 0.50;
        
        
        return CGSizeMake(width, self.view.frame.size.width * 0.50 * 0.862637);
        
    }
    //Use for interspacing
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    
    
}
