//
//  ContactRocheViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/10/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit




class ContactRocheViewController: HeaderViewController,SwiftPagesDelegate,GeneralEnquiryDelegate {

    @IBOutlet var swiftPagesView: SwiftPages!
    @IBOutlet var topConstraint : NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()

        
        _headerView.heading = "Contact Roche"
        
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.GeneralEnquiryViewController)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.GeneralEnquiryViewController) as! GeneralEnquiryViewController
        vc.delegate = self

        
        self.navigationController?.navigationBarHidden = true
     //   self.viewProfileHeightConstraint.constant = 0
        automaticallyAdjustsScrollViewInsets = false
        
        // Initiation
        let VCIDs = ["DetailViewController", "GeneralEnquiryViewController"]
         // let buttonTitles = ["Latest Updates", "Pending Approvals"]
        let buttonImages = [
            UIImage(named:"Contact-Detail-img-sel")!,
            UIImage(named:"Contact-GeneralEnquiry-img")!
]
        
        let buttonHeighlightdImages = [
            UIImage(named:"Contact-Detail-img")!,
            UIImage(named:"Contact-GeneralEnquiry-img-sel")!
        ]
        
     
        
        
        let bgColor = [ "#eceff4", "#3e8ada"]
        //--ww swiftPagesView.disableTopBar()
        
        
        // Sample customization
        swiftPagesView.setOriginY(0.0)
        swiftPagesView.setTopBarHeight(GlobalStaticMethods.getTabsHeight())
        
        
        swiftPagesView.setTopBarBackground(UIColor(hexString: "#eceff4"))
        swiftPagesView.setAnimatedBarColor(UIColor(hexString:"#113c8b"))
        swiftPagesView.enableBarShadow(false)
        swiftPagesView.delegate = self
        
        
        
        if constants.deviceType.IS_IPHONE{
            swiftPagesView.initializeWithVCIDsArrayAndButtonImagesArray (VCIDs, buttonImagesArray: buttonImages, storyBoardName: "nabeel-iphone-en", buttonHighlightedImagesArray: buttonHeighlightdImages, buttonBgColorArray : bgColor)
        }
        else{
        swiftPagesView.initializeWithVCIDsArrayAndButtonImagesArray (VCIDs, buttonImagesArray: buttonImages, storyBoardName: "nabeel-ipad-en", buttonHighlightedImagesArray: buttonHeighlightdImages, buttonBgColorArray : bgColor)
        }
        self.swiftPagesView.hidden = true
        
        
      //  self.imgEclipse.transform = CGAffineTransformMakeScale(0.95, 0.95)
        
        
        // Do any additional setup after loading the view.
        
    }

   
    
    override func viewDidAppear(animated: Bool) {
       // self.viewProfileHeightConstraint.constant = 110

        self.mm_drawerController.openDrawerGestureModeMask = .None

        
        
        self.view.layoutIfNeeded()
        self.swiftPagesView.hidden = false
       // swiftPagesView.barButtons[1]!.badgeString = "General Enquiry"
      //  swiftPagesView.barButtons[1]!.badgeTextColor = UIColor(hexString: "#ffffff")
        swiftPagesView.barButtons[1]!.badgeTextColor = UIColor.whiteColor();swiftPagesView.barButtons[1]!.badgeBackgroundColor = UIColor(hexString:"eceff4")
        
        //20, 0 , 0 , 25
        swiftPagesView.barButtons[1]!.badgeEdgeInsets = UIEdgeInsetsMake( swiftPagesView.barButtons[1]!.bounds.height / 2 , 0, 0, ( swiftPagesView.barButtons[1]!.bounds.width -  (swiftPagesView.barButtons[1]!.imageView?.bounds.width)!) * 0.3 )
        
        self.performSelector(#selector(ContactRocheViewController.animateProfileView), withObject:nil, afterDelay: 2)
        
       //    self.performSelector(Selector("animateProfileView"), withObject:nil, afterDelay: 2)
        
    }

    
    
    func animateProfileView(){
        UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseOut, animations: {
          //  self.viewProfileHeightConstraint.constant = 0
            self.view.layoutIfNeeded()
            }, completion: nil)
        
    }
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        self.view .bringSubviewToFront(_footerView)
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    

    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }
    
     func SwiftPagesCurrentPageNumber(currentIndex: Int)
     {
        self.view.endEditing(true)
    }
    
    
    
    func viewEditingBegan() {
        swiftPagesView.disableScrollView()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
