//
//  BookMarkViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 8/22/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import SwiftyJSON


class BookMarkViewController:HeaderViewController, bookMarkTableViewCellDelegate, AFWrapperDelegate{
    
    @IBOutlet weak var tableview : UITableView!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    
    var  arrData = [JSON]()
    var courseId : String!
    var row = NSIndexPath()

    override func viewDidLoad() {
        super.viewDidLoad()
    
        
       // dataTag = ["Tags": 1, "Tags": 2, "Tags": 3, "Tags": 4,"Tags": 5]
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 140
        _headerView.heading = "Bookmarked Courses"
    
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
    
        self.serviceCall()
  

        }

    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        self.mm_drawerController.openDrawerGestureModeMask = .None
        
    }
    
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
        }
    
    func serviceCall()  {
        
        AFWrapper.delegate = self
        
        let user = UserModel.sharedInstance

        AFWrapper.serviceCall(AFUrlRequestNabeel.GetBookmarkCourses(sessionId : user.sessionID), success: { (res) in
            
            
           self.parseResponce(res)
            
        }) { (NSError) in
            printLog(NSError)
            
            
            
            
            
            
        }
        
    }
    
    func didTapOnOKayOnAFWrapperAlert(errorCode: Int) {
        self.navigationController?.popViewControllerAnimated(true)

    }
    
//    func didTapOnOKayOnAFWrapperAlert() {
//    }
    
    
    func parseResponce(res : JSON)
    {
        
        if res["data"] != nil
        {
            if res["data"]["items"] != nil
            {
                
                arrData = res["data"]["items"].arrayValue
                self.tableview.reloadData()
                
            }
        }
    }
    
        func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) ->   Int {
        if arrData.count != 0 {
        
            self.tableview.hidden = false

            return (arrData.count)
        }else{
            self.tableview.hidden = true
            return 0
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> BookMarkTableViewCell {
        [self.tableview.separatorColor = UIColor .clearColor()]
    
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! BookMarkTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        cell.BtnUnbook?.tag = arrData[indexPath.row]["id"].int!
        cell.btnDetail?.tag = arrData[indexPath.row]["id"].int!
        cell.tag = self.arrData[indexPath.row]["courseType"].int!
        
        
        if self.arrData[indexPath.row]["courseType"].int! == 1{
            cell.lblTitle?.text = Constants.ONLINE_COURSE
            cell.imgCourse?.image = UIImage (imageName: Constants.ONLINE_COURSE_IMAGE)
            
        }
            
            
        else{
            cell.lblTitle?.text = Constants.INCLASS_COURSE
            cell.imgCourse?.image = UIImage (imageName: Constants.INCLASS_COURSE_IMAGE)
            
        }
    
        cell.lblHeading?.text = arrData[indexPath.row]["title"].stringValue
        cell.lblDescription?.text = arrData[indexPath.row]["shortDescription"].stringValue

        
        if(indexPath.row % 2 == 0){
        
            cell.backgroundColor = UIColor.whiteColor()
        
        
        } else{
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
    
    
        if GlobalStaticMethods.isPad() {
            if  GlobalStaticMethods.isPadPro() {
                cell.btnHeightConstraint?.constant = 51
            
            }else{
                cell.btnHeightConstraint?.constant = 40
            }
        }
    
        cell.delegate = self
        return cell
    
    
    
}
func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    
    
    let cellSize: CGFloat
    
    let deviceHeight = CGRectGetHeight(self.view.frame);
    
    
    if  constants.deviceType.IS_IPHONE {
        
//
//        if constants.deviceType.IS_IPHONE_5 || constants.deviceType.IS_IPHONE_4
//        {
//            cellSize = CGFloat (deviceHeight * 0.35)
//            return cellSize
//            
//        }
//        else{
//            cellSize = CGFloat (deviceHeight * 0.3
//            )
//            
//            print (cellSize)
//            return cellSize
//        }
        
        return UITableViewAutomaticDimension

    }
        
        
    else if GlobalStaticMethods.isPad() {
        if GlobalStaticMethods.isPadPro() {
            print("ipad pro changed orientation")
            
           
        }
        
    }
    cellSize = CGFloat (deviceHeight * 0.35)
    return UITableViewAutomaticDimension
    
}



    func bookMarkBtnTappedDelegate(detailBTN: BookMarkTableViewCell) {
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseDetailVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.courseDetailVC) as! courseDetailViewController
        vc.strCourseID = String(detailBTN.btnDetail!.tag)
        vc.courseType = detailBTN.tag
        
        
        
        print(detailBTN.btnDetail!.tag)
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        

        
        
     //   navigation.goToViewController(constants.viewControllers.courseDetailVC, sideDrawer: self.mm_drawerController)
        
      
        
    }
   
    func UnbookMarkBtnTappedDelegate(unBookBTN: BookMarkTableViewCell) {
        
        let user = UserModel.sharedInstance

        self.tableview.deselectRowAtIndexPath(row, animated: true)

        let courseId = unBookBTN.BtnUnbook!.tag
         print(courseId,"courseId")
        
       

            
                Alert.showAlertMsgWithTitle("Confirmation", msg: "Are you sure you want to remove bookmark?", otherBtnTitle: "Yes", otherBtnAction: { (Void) in
                    
                    AFWrapper.serviceCall(AFUrlRequestNabeel.BookMarkCourse(sessionId: user.sessionID, courseId: courseId), success: { (result) in
                        
                        if (result.dictionary!["statusCode"])! == 1000            {
                    
                    
                    let buttonPosition = unBookBTN.convertPoint(CGPointZero, toView: self.tableview)
                    let indexPath = self.tableview.indexPathForRowAtPoint(buttonPosition)
                    
                    self.arrData.removeAtIndex(indexPath!.row)
                    self.tableview.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Automatic)
                    
                        }
                        
                    }) { (NSError) in
                        
                        
                    }

                            
                            
                    }, cancelBtnTitle: "No", cancelBtnAction: { (Void) in
                        
                    }, viewController: self)
                
            
                  
        
    }
    
  
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
    
    
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    
    
        coordinator.animateAlongsideTransition({ (context :     UIViewControllerTransitionCoordinatorContext!) -> Void in
        // print("LOOOOOL")
        
        self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
        //  print("End")
        
        }
    }


    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
    
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                
                
                    if orientation.isLandscape {
                    //   self.viewSupplementHeightConstraint.constant = 45
                    }
            else{
                    //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                    
                }
                
            }
            else{
                if orientation.isLandscape {
                    //    self.viewSupplementHeightConstraint.constant = 50
                }
                else{
                    //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                    
                }
                
                print("ipad changed orientation")
            }
        }
    }
    
}

override func viewWillAppear(animated: Bool) {
    updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
    
}


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
