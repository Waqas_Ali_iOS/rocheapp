//
//  CourseSuggestionViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 10/13/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation

class CourseSuggestionViewController: HeaderViewController,UITextFieldDelegate ,AFWrapperDelegate {

    @IBOutlet var btnSend: buttonFontCustomClass!
    @IBOutlet var txtEmail: BaseUITextField!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    var courseID = 9
    
    
    
//    func textFieldShouldReturn(textField: UITextField) -> Bool {
//        if textField == txtEmail{
//            txtEmail.resignFirstResponder()
//        }
//        return true
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _headerView.heading = "Suggest Course"
        print(courseID)
        txtEmail.text = "supervisorone@wewanttraffic.pk"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        self.txtEmail.text = ""
        
       // btnSend.addTarget(self, action: #selector(CourseSuggestionViewController.btnSendAction), forControlEvents: .TouchUpInside)
        
        
        //  _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view .endEditing(true)
    }
    
    @IBAction func btnSendAction(sender : UIButton){
        print("Send Email")
        let user = UserModel.sharedInstance
        var strEmail = self.txtEmail.text
        self.view .endEditing(true)

        if  strEmail == ""{
            Alert.showAlertMsgWithTitle("Message", msg: "Please enter email", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
            self.txtEmail.becomeFirstResponder()
                
            })
        }
        
        
        else if GlobalStaticMethods.isValidEmail(strEmail!) == false {
            
            
            Alert.showAlertMsgWithTitle("Message", msg: "Please enter a valid email", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                self.txtEmail.becomeFirstResponder()
                
            })
            
        }
        else{
            AFWrapper.delegate = self
        AFWrapper.serviceCall(AFUrlRequestNabeel.CourseSuggestion(sessionID: user.sessionID, courseID: courseID, email: strEmail!), success: { (res) in
                
                printLog(res)
            
                
                let strMessage = res["statusMessage"].stringValue
                
                Alert.showAlertMsgWithTitle("Message", msg: strMessage, btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
                    print("")
                    self.navigationController?.popViewControllerAnimated(true)
 
                })
                
                
            }) { (NSERROR) in
                printLog(NSERROR)
                
            }
            
        }
        
        
    }
    
    func didTapOnOKayOnAFWrapperAlert(errorCode: Int) {
            self.navigationController?.popViewControllerAnimated(true)
        
    }
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    


}
