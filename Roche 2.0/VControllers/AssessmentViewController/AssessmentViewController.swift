//
//  AssessmentViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/4/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit
import SwiftyJSON

class AssessmentViewController: HeaderViewController {

    @IBOutlet weak var QuesView: UIView!
    @IBOutlet weak var CountLabel: UILabel!
    
    @IBOutlet weak var lblQuestion: BaseUILabel!
    @IBOutlet weak var option1Img: UIImageView!
    @IBOutlet weak var option2Img: UIImageView!
    @IBOutlet weak var option3Img: UIImageView!
    @IBOutlet weak var option4Img: UIImageView!
    @IBOutlet weak var option5Img: UIImageView!
    @IBOutlet var topConstraint : NSLayoutConstraint?

    @IBOutlet weak var lblOptionOne: BaseUILabel!
    @IBOutlet weak var lblOptionTwo: UILabel!
    @IBOutlet weak var lblOptionThree: BaseUILabel!

    @IBOutlet weak var lblOptionFour: BaseUILabel!
    @IBOutlet weak var lblOptionFive: BaseUILabel!
    @IBOutlet weak var btnNextQuestion: buttonFontCustomClass!
    @IBOutlet weak var viewFive: UIView!
    @IBOutlet weak var scrollView: UIScrollView?
    @IBOutlet var topScrollViewConstraint : NSLayoutConstraint?
    @IBOutlet var optionConstraint : NSLayoutConstraint?

    
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var viewTwo: UIView!
    @IBOutlet weak var viewThree: UIView!
    @IBOutlet weak var viewFour: UIView!
    
    @IBOutlet weak var ProgressView: UIProgressView!
    var isQuestionSelect : Bool = false
    
    var methodType = 0
    var methodTypeEnum : AFUrlRequestSharjeel.ListServiceType = AFUrlRequestSharjeel.ListServiceType.Assesment
    
    
    var arrQuestion = [AnyObject]()
    
    var totalCount : Int = 0
    var currentCourseID : Int = 0
    
    var currentQuestionNumber : Int = 0
    
    var arrAnsID = [AnyObject]()
    
    
    var strQID = ""
    
    var strAnsID = ""
    
    var screenTitle = NSString()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewFive.removeFromSuperview()
        ProgressView.progress = 0;
        
        self.lblOptionFive.hidden = true
        self.option5Img.hidden = true
        
        [self .AttributedLabelForSizeAndColor()]
        [self .CornersRoundOfViews()]

        switch methodType {
        case AFUrlRequestSharjeel.ListServiceType.Assesment.rawValue:
            _headerView.heading = "Assessment"
        case AFUrlRequestSharjeel.ListServiceType.preCourseSurvay.rawValue:
            _headerView.heading = "Pre Attend Survey"
        default:
            _headerView.heading = "Post Attend Survey"
            
        }
        
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        
        self.btnNextQuestion.addTarget(self, action: #selector(self.nextQuestion), forControlEvents: .TouchUpInside)
        // MARK: - view button methods
        
        
        viewOne.hidden  = true
        viewTwo.hidden  = true
        viewThree.hidden  = true
        viewFour.hidden  = true
        

        
        QuesView.hidden = true
        CountLabel.hidden = true
        lblQuestion.hidden = true
        option1Img.hidden = true
        option2Img.hidden = true
        option3Img.hidden = true
        option4Img.hidden = true
        option5Img.hidden = true
        lblOptionOne.hidden = true
        lblOptionTwo.hidden = true
        lblOptionThree.hidden = true
        lblOptionFour.hidden = true
        lblOptionFive.hidden = true
        btnNextQuestion.hidden = true
        ProgressView.hidden = true

        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
         callService(methodType) // move this service into view did load
        self.mm_drawerController.openDrawerGestureModeMask = .None
        
     //   scrollView!.showsHorizontalScrollIndicator = true;

      //  scrollView! .flashScrollIndicators()

        
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func AttributedLabelForSizeAndColor()
    {
        
        let longString = "01/10"
        let longestWord = "/10"
        
        let longestWordRange = (longString as NSString).rangeOfString(longestWord)
        
        let attributedString = NSMutableAttributedString(string: longString, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(20)])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.systemFontOfSize(15), NSForegroundColorAttributeName :  UIColor (red: 123.0/255.0, green:123.0/255.0, blue: 123/255.0, alpha: 1.0)], range: longestWordRange)
        
        
        self.CountLabel.attributedText = attributedString
        
    }
    
    
    func CornersRoundOfViews(){
        QuesView.layer.cornerRadius = 10;
        QuesView.layer.masksToBounds = true;
    }
    
   @IBAction func optionBtnClicked(sender:UIButton)
    {
        if currentQuestionNumber < totalCount {
            isQuestionSelect = true
        }
        
        
        
        switch (sender.tag){
        case 1:
            self.option1Img.image = UIImage (named: "Gen-RoundBlue-img")
            self.option2Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option3Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option4Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option5Img.image = UIImage  (named: "Gen-RoundBlueCircle-img")
            
            

            
           break
        case 2:
            self.option2Img.image = UIImage (named: "Gen-RoundBlue-img")
            
            self.option1Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option3Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option4Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option5Img.image = UIImage  (named: "Gen-RoundBlueCircle-img")

            break
        case 3:
            self.option3Img.image = UIImage (named: "Gen-RoundBlue-img")
            
            self.option1Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option2Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option4Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option5Img.image = UIImage  (named: "Gen-RoundBlueCircle-img")
            break
        case 4:
            self.option4Img.image = UIImage (named: "Gen-RoundBlue-img")
            
            self.option1Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option2Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option3Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option5Img.image = UIImage  (named: "Gen-RoundBlueCircle-img")
            break
        case 5:
            self.option5Img.image = UIImage (named: "Gen-RoundBlue-img")
            
            self.option1Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option2Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option3Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
            self.option4Img.image = UIImage (named: "Gen-RoundBlueCircle-img")

            break
        default:
           break
        }
        
        
        let temDict = arrQuestion[currentQuestionNumber] as! Dictionary<String, AnyObject> as Dictionary
        
        let isIndexValid = arrAnsID.indices.contains(currentQuestionNumber)
        
        if isIndexValid == true
        {
             arrAnsID[currentQuestionNumber] = String(format: "%d",temDict["ansList"]![sender.tag-1]["id"] as! Int)
        }
        else
        {
            arrAnsID.append(String(format: "%d",temDict["ansList"]![sender.tag-1]["id"] as! Int))
            
        }
        

      
//        if currentQuestionNumber == 0
//        {
//            strAnsID = String(format: "%d",temDict["ansList"]![sender.tag-1]["id"] as! Int)
//        }
//        else
//        {
//            strAnsID = String(format: "%@|%d",strAnsID, temDict["ansList"]![sender.tag-1]["id"] as! Int)
//        }
        
        
       // sender.selected = !sender.selected;
    }
    
    
    //        let isIndexValid = arrAnsID.indices.contains(currentQuestionNumber)
    //
    //        if isIndexValid == true
    //        {
    //             arrAnsID[currentQuestionNumber] = String(format: "%d",temDict["ansList"]![sender.tag-1]["id"] as! Int)
    //        }
    //        else
    //        {
    //            arrAnsID.append(String(format: "%d",temDict["ansList"]![sender.tag-1]["id"] as! Int))
    //            
    //        }

    
    func convertArrIntoStr() -> String
    {
        
        for (index, element) in arrAnsID.enumerate()
        {
            print("Item \(index): \(element)")
            
            
            let str =  arrAnsID[index] as! String
            
            if index == 0
            {
                strAnsID = str
            }
            else
            {
                strAnsID = String.init(format: "%@|%@", strAnsID,str)
            }
        }
        
        return strAnsID
    }
    
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    
                        topScrollViewConstraint?.constant = 0
                        optionConstraint!.constant = 50
                    
                    print("ipad changed orientation")
                }
            }
        }
        
        
    }
    
    //MARK: - btn Methods
    
    func nextQuestion(){
        
        // temp load pop on click
        
//        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AssesmentThankViewController") as! AssesmentThankViewController
//        self.presentViewController(vc, animated: true, completion: nil)
        
        
        if isQuestionSelect == true
        {
            
            currentQuestionNumber += 1
            if currentQuestionNumber < totalCount
            {

                CountLabel.text = String.init(format: "%d/%d", currentQuestionNumber+1,totalCount)
            
                isQuestionSelect = false
                refreshOptionBtnImages()
                loadQuestionWithIndex(currentQuestionNumber)
                
                if currentQuestionNumber == totalCount - 1
                {
                    self.btnNextQuestion?.setTitle("Submit", forState: .Normal)
                }
            }
            else
            {
                
                let sb = navigation.getStoryBoardForController(constants.viewControllers.assesmentThankViewController)!.storyboardObject
                let objVC : AssesmentThankViewController = sb.instantiateViewControllerWithIdentifier(constants.viewControllers.assesmentThankViewController) as! AssesmentThankViewController
                //            strQuestionID
                //            strAnswerID
                
                
                strAnsID = convertArrIntoStr()
                
                objVC.strQuestionID = strQID
                objVC.strAnswerID = strAnsID
                objVC.courseId = currentCourseID
                
                objVC.methodTypeVariable = methodType
                
                self.navigationController!.pushViewController(objVC, animated: true)
            }
        }
        else
        {
            
            
            Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: "Please select an option", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                
                
                
            })
        }
    
       
        
    }
    
    func goToViewController(identifier : String){
        let sb = navigation.getStoryBoardForController(identifier)!.storyboardObject
        let homeVC = sb.instantiateViewControllerWithIdentifier(identifier)
        self.navigationController!.pushViewController(homeVC, animated: true)
        
    }
    
    //MARK: -  Web service Call and Responce parser
    func callService(mType : AFUrlRequestSharjeel.ListServiceType.RawValue)
    {
        
        AFWrapper.serviceCall(AFUrlRequestSharjeel.GetAssessmentList(courseId: currentCourseID, methodType: mType), success: { (res) in
            
            
            self.parseResponce(res)
            
        }) { (NSError) in
            printLog(NSError)
        }
        
    }
    
    
    func parseResponce(res : JSON)
    {
        if res["data"] != nil
        {
            totalCount = res["data"]["totalCount"].int!
            currentCourseID = res["data"]["courseId"].int!
            
            arrQuestion = res["data"]["items"].arrayObject!
            
            
           
            
            
            
            CountLabel.text = String.init(format: "%d/%d", currentQuestionNumber+1,totalCount)
            
            if arrQuestion.count > 0 {
               loadQuestionWithIndex(currentQuestionNumber) // load question with first index 
                
                ProgressView.hidden = false
                QuesView.hidden = false
                CountLabel.hidden = false
                lblQuestion.hidden = false
                option1Img.hidden = false
                option2Img.hidden = false
                option3Img.hidden = false
                option4Img.hidden = false
                // option5Img.hidden = false
                lblOptionOne.hidden = false
                lblOptionTwo.hidden = false
                lblOptionThree.hidden = false
                lblOptionFour.hidden = false
                // lblOptionFive.hidden = false
                btnNextQuestion.hidden = false
                
            }
            else
            {
                Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: "No Questions Available,Please try latter", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                    
                      navigation.PopToVC(constants.viewControllers.courseDetailVC, sideDrawer: self.mm_drawerController)
                    
                })
            }
            
            
          
        }
    }
    
    func  loadQuestionWithIndex(index : Int)
    {
        ProgressView.progress =  Float(currentQuestionNumber+1)/Float(totalCount)
        
        let temDict = arrQuestion[index] as! Dictionary<String, AnyObject> as Dictionary
        
        
        lblQuestion.text = temDict["title"] as? String
        
        
        if index == 0 {   
            strQID = String(format: "%d",temDict["qId"] as! Int)
        }
        else
        {
            strQID = String(format: "%@|%d",strQID,temDict["qId"] as! Int)
        }
        
        var items = [AnyObject]()
        
        
        
        items = temDict["ansList"] as! [AnyObject]
        
        for i in 0 ..< items.count {
            
            switch i {
            case 0:
                lblOptionOne.text = temDict["ansList"]![0]!["title"] as? String
                viewOne.hidden  = false
                break
            case 1:
                lblOptionTwo.text = temDict["ansList"]![1]!["title"] as? String
                viewTwo.hidden  = false
                break
            case 2:
                lblOptionThree.text =  temDict["ansList"]![2]!["title"] as? String
                viewThree.hidden  = false
                break
            case 3:
                lblOptionFour.text = temDict["ansList"]![3]!["title"] as? String
                viewFour.hidden  = false
                break
            default:
                break
            }
        }

       // lblOptionFive.text = temDict["ansList"]![4]!["title"] as? String
        
    }
    
    
    func refreshOptionBtnImages()
    {
        self.option1Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
        self.option2Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
        self.option3Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
        self.option4Img.image = UIImage (named: "Gen-RoundBlueCircle-img")
        self.option5Img.image = UIImage  (named: "Gen-RoundBlueCircle-img")
    }
    
    
    
//    
//    {
//    ansList =                 (
//    {
//    id = 21;
//    title = Everything;
//    },
//    {
//    id = 22;
//    title = Alphabets;
//    },
//    {
//    id = 23;
//    title = Information;
//    },
//    {
//    id = 24;
//    title = "None of the above";
//    }
//    );
//    qId = 6;
//    qNo = 1;
//    title = "What is a data?";
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
