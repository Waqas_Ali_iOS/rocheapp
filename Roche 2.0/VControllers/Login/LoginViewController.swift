//
//  LoginViewController.swift
//  BaseProject
//
//  Created by  Traffic MacBook Pro on 15/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import SwiftyJSON
import NVActivityIndicatorView

class LoginViewController: UIViewController,UITextFieldDelegate, AFWrapperDelegate{
    
    @IBOutlet weak var travelPolicyBtnXConstraints: NSLayoutConstraint!
    
    @IBOutlet var btnLogin : UIButton!
    @IBOutlet var btnForgetPassword : UIButton!
    @IBOutlet var btnRegister : UIButton!
    @IBOutlet var btnTerms : UIButton!
    @IBOutlet var btnPrivacy : UIButton!
    @IBOutlet var btnTravelPolicy : UIButton!
    @IBOutlet var txtPassword : UITextField!
    @IBOutlet var txtEmail : UITextField!
    
    let single = Singleton.sharedInstance
    let keyChain = KeychainSwift()
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view .endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == txtEmail {
            txtPassword.becomeFirstResponder()
        }
        else{
            txtPassword.resignFirstResponder()
        }
        return true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnRegister.exclusiveTouch = true
        self.btnLogin.exclusiveTouch = true
        
        self.mm_drawerController.openDrawerGestureModeMask = .None
   
        txtEmail.delegate = self
        txtPassword.delegate = self
        //  UIStoryboard *sb = [Singleton, getStoryBoard];
        if GlobalStaticMethods.isPhone(){
        btnTerms.titleLabel?.numberOfLines = 2
        btnPrivacy.titleLabel?.numberOfLines = 2
        btnTravelPolicy.titleLabel?.numberOfLines = 2
        
        self.travelPolicyCenterX()
        }
        
        btnForgetPassword.exclusiveTouch = true
        btnTerms.exclusiveTouch = true
        btnPrivacy.exclusiveTouch = true
        btnTravelPolicy.exclusiveTouch = true
        
        btnLogin.addTarget(self, action: #selector(LoginViewController.login), forControlEvents: .TouchUpInside)
        btnForgetPassword.addTarget(self, action: #selector(LoginViewController.openForgetPass), forControlEvents: .TouchUpInside)
        btnTerms.addTarget(self, action: #selector(LoginViewController.openTerms), forControlEvents: .TouchUpInside)
        btnRegister.addTarget(self, action: #selector(LoginViewController.openRegister), forControlEvents: .TouchUpInside)
        btnTravelPolicy.addTarget(self, action: #selector(LoginViewController.openTravelPolicy), forControlEvents: .TouchUpInside)
        btnPrivacy.addTarget(self, action: #selector(LoginViewController.openPrivacy), forControlEvents: .TouchUpInside)
        
        //   openTutorial()
        //  goToHome()
        // Do any additional setup after loading the view.
        
        
        txtEmail.text = "" //a@a.com
        txtPassword.text = "" //Traffic123raffic123
        txtEmail.keyboardType = .EmailAddress
        
        txtPassword.secureTextEntry = true
        
        
        
        
        
        /*
         var portraitConstraints = [NSLayoutConstraint]()
         // var con : NSLayoutConstraint!
         
         let arr = self.view.constraints as [NSLayoutConstraint]
         
         for con in arr {
         
         
         if (con.firstItem as! NSObject == self.view || con.secondItem as! NSObject  == self.view) {
         portraitConstraints.append(con)
         }
         }
         
         printLog(portraitConstraints)
         
         
         
         var list = [NSLayoutConstraint]()
         let constraints = self.view.constraints
         for c in constraints {
         if c.firstItem as? UIView == self.view || c.secondItem as? UIView == self.view {
         list.append(c)
         }
         }
         
         printLog(list)
         */
        
        
//        getHospitals()
        getEmails()
      
        if userDefaults.boolForKey(Constants.AUTO_LOGIN) == true{
        
       let email = self.keyChain.get("email")
       let psd = self.keyChain.get("password")
            
        getLoginDataFromWeb(email!, pwd: psd!)
        }
        
    }
    
    
    override func viewDidAppear(animated: Bool) {

         self.mm_drawerController.openDrawerGestureModeMask = .None
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func login(){
        
        //--ww goToHome()
        
        self.view.endEditing(true)
        
        if validateForm() == true {
            
           
            getLoginDataFromWeb(txtEmail.text!, pwd: txtPassword.text!)
        }
        
    }
    
    
    func getLoginDataFromWeb(email : String, pwd: String) {
        
      
        AFWrapper.delegate = self
        
        AFWrapper.serviceCall(AFUrlRequestWaqas.LoginUser(userName: email, password: pwd), success: { (res) in
            
            
            
            let size = CGSize(width: 50, height:50)
            
            AFWrapper.startActivityAnimatingNew(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
           
            self.userDefaults.setBool(true, forKey: Constants.AUTO_LOGIN)
             self.userDefaults.synchronize()
            
            self.keyChain.set(email, forKey: "email")
            self.keyChain.set(pwd, forKey: "password")
            
            
            let result = res["data"].dictionary!
            
            let user = UserModel.sharedInstance
            
            user.sessionID = result["sessionId"]!.stringValue
            user.userID = result["userId"]!.intValue
            user.userName = result["name"]!.stringValue
            user.userEmail = result["email"]!.stringValue
            user.userType = userTypes(rawValue: result["userType"]!.intValue)
            user.unreadMessageCount = result["unreadMessageCount"]!.intValue
            /*
             let str = "iVBORw0KGgoAAAANSUhEUgAAADoAAAA6AQMAAADbddhrAAAABlBMVEUjTZr+/v45oR6dAAAA2UlEQVQokXXPsQrCMBCA4QuFZhGztiD2FdLNoaSvUnBwLXQt6ObiIwi+yk262VeIFOwDuFQonilKm9q6hG847r8AUahxSwgdkM+DVcosFFW4c7PnALCu/0HmagCzJz9/F35gWsfzN/oDgLhsXwsBa1TmY4/HTgWu84IJiP01USnHHpFXnHCmoYc4weLStDMjyCRu2ME0O0QbUb5mJtpBenEJYFpjRJz0hWoLACEp526BiGm3/cUYyKWjHHNhh6ISOqblABBA6uMkRMkOTwvImSa6WWhbCvwJvAFKFNfp4PfTLgAAAABJRU5ErkJggg=="
             let imageData = NSData(base64EncodedString: str, options: [])
             
             user.userQRimage =  UIImage(data: imageData!)
             
 
            user.userQRimage = imageClass.convertBase64ToImage((result["qrImage"]?.stringValue)!)
            
            */
            // Create Url from string
            let url = NSURL(string: result["qrImage"]!.stringValue)!
            
            // Download task:
            // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
            let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (responseData, responseUrl, error) -> Void in
                // if responseData is not null...
                if let data = responseData{
                    
                    // execute in UI thread
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        user.userQRimage = UIImage(data: data)
                        
                       AFWrapper.stopActivityAnimatingNew()
                        if result["isForceUpdate"]!.intValue == 0 {
                            if UserModel.sharedInstance.userType == userTypes.Instructor{
                                self.goToAttendence()
                               self.txtEmail.text = ""
                               self.txtPassword.text = ""
                                
                                
                            }else{
                                self.goToHome()
                             self.txtEmail.text = ""
                             self.txtPassword.text = ""
                            }
                        }else{
                            self.goToViewController("updatePassViewController")
                            self.txtEmail.text = ""
                            self.txtPassword.text = ""
                        }
                    })
                }else{
                
                   dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                     AFWrapper.stopActivityAnimatingNew()
                    if result["isForceUpdate"]!.intValue == 0 {
                        if UserModel.sharedInstance.userType == userTypes.Instructor{
                            
                            self.goToAttendence()
                            self.txtEmail.text = ""
                            self.txtPassword.text = ""
                            
                            
                        }else{
                            self.goToHome()
                            self.txtEmail.text = ""
                            self.txtPassword.text = ""
                        }
                    }else{
                        self.goToViewController("updatePassViewController")
                        self.txtEmail.text = ""
                        self.txtPassword.text = ""
                    }
                     })
                }
            }
            
            // Run task
            task.resume()
            
           
            
            
            print(result["qrImage"]!.stringValue)
            
         
            
            
        }) { (NSError) in
            printLog(NSError)
        }
    }
    
   
    func didTapOnOKayOnAFWrapperAlert(errorCode: Int) {
        printLog("do nothing here")
    }
    
    func validateForm() -> Bool {
        
        var res = true
        var msg = ""
        
        if stringsClass.isEmptyString(txtEmail.text!) == ""{
            
            res = false
            msg = "Email field cannot be blank!"
        }else if stringsClass.isEmptyString(txtPassword.text!) == ""{
            res = false
            msg = "Password field cannot be blank!"
            
        }else if GlobalStaticMethods.isValidEmail(txtEmail.text!) == false{
            
            res = false
            msg = "Email is not valid!"
            
        }
        
        if msg != ""{
            
            Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: msg, btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                
            })
            
        }
        
        
        
        return res
        
    }
    
    
    
    func openForgetPass(){
        goToViewController(constants.viewControllers.forgetPasswordVC)
        
        
        
        
        
        
    }
    
    
    func openTutorial(){
        
        //       goToViewController(constants.viewControllers.tutorialScreenVC)
        let sb = navigation.getStoryBoardForController(constants.viewControllers.tutorialScreenVC)!.storyboardObject
        let homeVC = sb.instantiateViewControllerWithIdentifier(constants.viewControllers.tutorialScreenVC)
        
        self.presentViewController(homeVC, animated: true, completion: nil)
        
    }
    
    func openTerms(){
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.PrivacyPolicyViewController)?.storyboardObject
        let newsVC = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.PrivacyPolicyViewController) as! PrivcayViewController
        newsVC.strType = "terms-conditions"
        
        print("news.strtype",newsVC.strType)
        
        self.navigationController!.pushViewController(newsVC, animated: true)
        
        //  goToViewController(constants.viewControllers.TermsConditionViewController)
        
        
    }
    
    func openPrivacy(){
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.PrivacyPolicyViewController)?.storyboardObject
        let newsVC = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.PrivacyPolicyViewController) as! PrivcayViewController
        newsVC.strType = "privacy-policy"
        
        print("news.strtype",newsVC.strType)
        self.navigationController!.pushViewController(newsVC, animated: true)
        
        
        
        
        //   goToViewController(constants.viewControllers.PrivacyPolicyViewController)
        
        
    }
    
    
    
    func openTravelPolicy(){
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.PrivacyPolicyViewController)?.storyboardObject
        let newsVC = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.PrivacyPolicyViewController) as! PrivcayViewController
        newsVC.strType = "travel-policy"
        self.navigationController!.pushViewController(newsVC, animated: true)
        
        
        // goToViewController(constants.viewControllers.travelPolicyVC)
        
        
    }
    
    
    func openRegister (){
        
        if single.arrEmails.count == 0 {
            getEmails()
        }
        if single.arrHospitals.count == 0 {
            getHospitals()
        }
        goToViewController(constants.viewControllers.registerVC)
        
    }
    
    
    func goToHome(){
        
        goToViewController(constants.viewControllers.homeContainerViewController)
        single.currentUser = UserModel.sharedInstance.userType
        NSNotificationCenter.defaultCenter().postNotificationName("reloadLeftMenu", object: nil)
        
    }
    
    
    func goToAttendence(){
        
        goToViewController(constants.viewControllers.attendenceVC)
        single.currentUser = UserModel.sharedInstance.userType
        NSNotificationCenter.defaultCenter().postNotificationName("reloadLeftMenu", object: nil)
        
        
        
    }
    func travelPolicyCenterX()  {
        
        
        
        let deviceWidth = CGRectGetWidth(self.view.frame);
        
        
        
        
        if  !constants.deviceType.IS_IPHONE_6P{
            travelPolicyBtnXConstraints.constant = deviceWidth * 0.04;
        }
    }
    
    
    func goToViewController(identifier : String){
        let sb = navigation.getStoryBoardForController(identifier)!.storyboardObject
        let homeVC = sb.instantiateViewControllerWithIdentifier(identifier)
        self.navigationController!.pushViewController(homeVC, animated: true)
        
    }
    
    
    func getEmails(){
        AFWrapper.serviceCallWithoutLoader(AFUrlRequestHamza.PredictiveEmail(userType: "" , keyword : ""), success: { (res) in
            printLog(res)
            
            
            if res["statusCode"].intValue == statusCode.success.rawValue{
            let data = res["data"].dictionary
                
                if data?.count > 0{
           // let countLabManagerEmails = data["countLabManagerEmails"].intValue
            let labManagerEmails = data!["labManagerEmails"]!.arrayObject
        //    let countSalesRepEmails = data["countSalesRepEmails"].intValue
            let salesRepEmails = data!["salesRepEmails"]!.arrayObject
            
            
            
            var dict = Dictionary<String,[AnyObject]>()
            
            //dict["countLabManagerEmails"] = countLabManagerEmails
            dict["labManagerEmails"] = labManagerEmails
     //       dict["countSalesRepEmails"] = countLabManagerEmails
            dict["salesRepEmails"] = salesRepEmails
            
            self.single.arrEmails.append(dict)
                }

            }
        }) { (NSError) in
            printLog(NSError)
        }

    }
    func getHospitals(){
        AFWrapper.serviceCallWithoutLoader(AFUrlRequestHamza.PredictiveHosp, success: { (res) in
            printLog(res)
            
            
            self.single.arrHospitalsName = []
            self.single.arrHospitals = []
            
//            var arrDataName = [String]()
//            var arrDataID = [Int]()
            
            if res["statusCode"].intValue == statusCode.success.rawValue{
            let data = res["data"].dictionary
            if data?.count > 0{
                
            self.single.arrHospitals = res["data"]["items"].arrayObject!
                
//            self.single.arrHospitals = res["data"]["items"]
                
//-- SW
            for i in 0 ..< res["data"]["items"].count{
                
                
                
                let strName = res["data"]["items"][i]["name"].stringValue
                let strID = res["data"]["items"][i]["id"].intValue
                var dict = Dictionary<Int,AnyObject>()

                dict[i] = ["id": strID , "name" : strName]
                //dict[strID] = strName
                
                self.single.arrHospitalsName.append(strName)

                

            }
                }
            
            }
//            cell.showPicker(arrDataName as! [String], arrForID:  arrDataID)
            
        }) { (NSError) in
            printLog(NSError)
        }

    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     
     
     static func loadImageFromUrl(url: String, view: UIImageView){
     
     // Create Url from string
     let url = NSURL(string: url)!
     
     // Download task:
     // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
     let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (responseData, responseUrl, error) -> Void in
     // if responseData is not null...
     if let data = responseData{
     
     // execute in UI thread
     dispatch_async(dispatch_get_main_queue(), { () -> Void in
     view.image = UIImage(data: data)
     })
     }
     }
     
     // Run task
     task.resume()
     }
     */
    
}
