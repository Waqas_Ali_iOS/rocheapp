//
//  MessageViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/12/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit

class MessageViewController: HeaderViewController,UITableViewDelegate, AFWrapperDelegate {

    var refreshControl: UIRefreshControl!

    @IBOutlet weak var tblView : UITableView!
    
    @IBOutlet var topConstraint : NSLayoutConstraint?
    
    var msgArray  = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        tblView.addSubview(refreshControl) // not required when using UITableViewController
        
        
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        _headerView.heading = "Messages"
        _headerView.rightButtonImage = constants.imagesNames.RIGHT_NEW_MESSAGE
        
        
        self.tblView.hidden = true
        
        if GlobalStaticMethods.isPhone() {
            self.tblView.rowHeight = UITableViewAutomaticDimension
            self.tblView.estimatedRowHeight = 340
        }
          msgArray = []
        
        
        
       
      
    }
    
    func refresh(sender:AnyObject) {
        
        self.serviceCall()
        // self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    override func viewDidAppear(animated: Bool) {
        
        self.mm_drawerController.openDrawerGestureModeMask = .None
        self.serviceCall()
       
    }
    
    func serviceCall()  {
        
        self.tblView.hidden = false
        
        AFWrapper.delegate = self
        AFWrapper.serviceCall(AFUrlRequestWaqas.GetMessages, success: { (res) in
            
            
            self.msgArray = res["data"]["items"].arrayObject!
            self.tblView.reloadData()
            
           
            
        }) { (NSError) in
            printLog(NSError)
            self.navigationController?.popViewControllerAnimated(true)
        }
        
        
    }
    
    
    func didTapOnOKayOnAFWrapperAlert(errorCode : Int) -> Void {
      //  self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        
        var numOfSections: Int = 0
        
        if msgArray.count > 0{
            self.tblView.separatorStyle = .None
            numOfSections                = msgArray.count
           // self.tblView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRectMake(0, 0, self.tblView.bounds.size.width, self.tblView.bounds.size.height))
            noDataLabel.text             = "No message available"
            noDataLabel.textColor        = UIColor.blackColor()
            noDataLabel.textAlignment    = .Center
           // self.tblView.backgroundView = noDataLabel
            self.tblView.separatorStyle = .None
        }
        
        return numOfSections

        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        /* ww ---- who did this? and why?
         if GlobalStaticMethods.isPhone() {
            return 340
        }*/
 
        return 0.2446093 * CGRectGetHeight(self.view.frame)
        
    }
   
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //self.tblView.separatorColor = UIColor .clearColor()
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! MessageTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        
      
        /*
        if(indexPath.row % 2 == 0){
            
         //   cell.backgroundColor = UIColor.whiteColor()
            
        }
        else if indexPath.row == 5 {
           
            cell.backgroundColor = UIColor (hexString: "#eceff4")
            cell.lblHeading.textColor = UIColor (hexString: "#7b7b7b")
            cell.lblDescription.textColor = UIColor (hexString: "#7b7b7b")
            cell.lblTimeDate.textColor = UIColor (hexString: "#a9acb2")
            cell.viewSeperator.backgroundColor = UIColor (hexString: "#7b7b7b")

            
            cell.imgTimeDate.image = cell.imgTimeDate.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            cell.imgTimeDate.tintColor = UIColor (hexString: "#a9acb2")
            cell.btnAssignment.setTitleColor(UIColor (hexString: "#a9acb2"), forState: UIControlState.Normal)
            cell.btnAssignment.enabled = false
            
            
            let image = UIImage(named: "Message-Attachment-icon")?.imageWithRenderingMode(.AlwaysTemplate)
            cell.btnAssignment.setImage(image, forState: .Normal)
            cell.btnAssignment.tintColor = UIColor (hexString: "#a9acb2")

          //  cell.btnAssignment.titleLabel?.textColor = UIColor (hexString: "#a9acb2")

            
        }
        
        else{
            cell.ViewAssignment.removeFromSuperview()

          //  cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
        */
        
        cell.setData(msgArray[indexPath.row] as! Dictionary<String, AnyObject>)
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        let tID = msgArray[indexPath.row]["threadID"] as? Int
        
//        AFWrapper.serviceCall(AFUrlRequestWaqas.GetMessageDetail(tid: tID!), success: { (res) in
//            
//            printLog(res)
        
            
            let sb = navigation.getStoryBoardForController(constants.viewControllers.replyMsgVC)?.storyboardObject
            
            
            let desiredVC = sb!.instantiateViewControllerWithIdentifier(constants.viewControllers.replyMsgVC) as! ReplyMessageViewController
            
        //    desiredVC.dataDict = res["data"].dictionaryObject
        
            desiredVC.tID = tID
        
                leftMenu.pushOrPopObj(desiredVC, sideDrawer:self.mm_drawerController)
            
            
//            
//        }) { (NSError) in
//            printLog(NSError)
//        
//        }
        
        
     
         
      
        
        
        //--ww leftMenu.pushOrPop(constants.viewControllers.replyMsgVC, sideDrawer:  self.mm_drawerController)

    }
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }

    
    override func headerViewRightBtnDidClick(headerView: HeaderView) {
        let sb = navigation.getStoryBoardForController(constants.viewControllers.PostNewMessageVC)?.storyboardObject
        let newMessageVC = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.PostNewMessageVC)
        self.navigationController?.pushViewController(newMessageVC!, animated: true)
        
        
    }


}
