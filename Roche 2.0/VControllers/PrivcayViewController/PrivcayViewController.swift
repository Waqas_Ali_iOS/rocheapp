//
//  PrivcayViewController.swift
//  RocheApp
//
//  Created by  Traffic MacBook Pro on 02/08/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import SwiftyJSON

class PrivcayViewController: HeaderViewController {
    @IBOutlet var topConstraint : NSLayoutConstraint?
    @IBOutlet var lblTitle : UILabel?
    @IBOutlet var txtView : UITextView?

    var strType : String = ""
    
    
    var dataDict = [String:JSON]()
    override func viewDidLoad() {
        super.viewDidLoad()
        print("travel-policy" ,self.strType)
     
        
        txtView?.bounces = false
        
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        
        self.ServiceCallLocal()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    
    func ServiceCallLocal()  {
        
        if self.strType == "terms-conditions"{
            self.lblTitle?.text = "Terms & Conditions"
            _headerView.heading = "Terms & Conditions"
        }
        else if self.strType == "privacy-policy"{
            self.lblTitle?.text = "Privacy Policy"
            _headerView.heading = "Privacy Policy"
        }
        else{
            self.lblTitle?.text = "Travel Policy"
            _headerView.heading =  "Travel Policy"
        }
        
    AFWrapper.serviceCall(AFUrlRequestNabeel.PrivacyPolicy(type : strType), success: { (res) in
        //  printLog(AnyObject)
        self.dataDict = (res.dictionary!["data"]?.dictionary)!
        
        print("dataDict" , self.dataDict)
        
        
       self.lblTitle?.text = self.dataDict["title"]?.stringValue
        let fontSize:Int = Int((self.txtView!.font?.pointSize)!) 

        let attributedString = stringsClass.getAttributedStringForHTMLWithFont((self.dataDict["description"]?.stringValue )!, textSize: fontSize  , fontName: "Tahoma")
        
        self.txtView!.attributedText = attributedString
        
        
        
        
        }) { (NSError) in
            printLog(NSError)

        }
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
           txtView?.selectable = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {

                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
