//
//  AttendenceListViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/11/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit

class AttendenceListViewController: HeaderViewController , attendenceListTableViewCellDelegate,AFWrapperDelegate{

    @IBOutlet weak var tableview : UITableView!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    var courseID = 0
    @IBOutlet var courseTypeImg : UIImageView!

    @IBOutlet var lblCourseType : UILabel!
    @IBOutlet var lblHeading : UILabel!
    @IBOutlet  var upperView: UIView?

    var arrDataTableView = Array<AnyObject>()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        _headerView.heading = "Attendance"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON

        lblHeading.text = ""
        lblCourseType.text = ""
        self.courseTypeImg.hidden = true
        
        self.whiteView()
    serviceCall()
        // Do any additional setup after loading the view.
    }
    
    func whiteView()  {
        
        upperView!.backgroundColor = UIColor.whiteColor()
        upperView!.hidden = false
    }
    
    func serviceCall(){
        AFWrapper.delegate = self
        
        AFWrapper.serviceCall(AFUrlRequestNabeel.AttendanceSelectLocation(courseId: courseID), success: { (res) in
            printLog(res)
            
            let data =  res["data"].dictionaryValue
            let totalCount = data["totalCount"]?.intValue
            let courseType =  data["courseType"]!.intValue
            let title = data["title"]!.stringValue
            let courseId = data["courseId"]!.intValue

            self.lblHeading.text = title
            if courseType == courseTypeEnum.online.rawValue {
                self.lblCourseType!.text = "Online Course"
                self.courseTypeImg.image = UIImage (imageName: Constants.ONLINE_COURSE_IMAGE)

                
            }
            else if courseType == courseTypeEnum.inClass.rawValue{
                self.lblCourseType!.text = "In-Class Course"
                self.courseTypeImg.image = UIImage (imageName: Constants.INCLASS_COURSE_IMAGE)

            }
            self.courseTypeImg.hidden = false

            
            if totalCount > 0 {
                
                for i in 0 ..< totalCount! {
                    
                    let dict = data["items"]?.arrayObject
                    
                    let location = dict![i]["location"] as! String
                    let locationID = dict![i]["locId"] as! Int
                    let timeDate = dict![i]["timeDate"] as! String

                    let attendanceModel =   attendanceModelClass.init(courseType: courseType, title: title, courseID: courseId, location: location, locationID: locationID, timeDate: timeDate)
                    
                    
                    self.arrDataTableView.append(attendanceModel)
                    
                    
                }
                
                self.upperView!.hidden = true
                self.tableview.reloadData()
                // res["data"]arr
                
            }
            
        }) { (NSError) in
            printLog(NSError)
            
            
            self.navigationController?.popViewControllerAnimated(true)
            
        }
        
        

        
        
    }

    
    func didTapOnOKayOnAFWrapperAlert(errorCode: Int) {
        
    
        self.navigationController?.popViewControllerAnimated(true)

    
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)

        self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        if arrDataTableView.count > 0  {
            return arrDataTableView.count

        }
        else{
            
            self.tableview.separatorColor = UIColor.clearColor()
            
            return 0
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        [self.tableview.separatorColor = UIColor .clearColor()]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! AttendenceListTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.upperView.layer.cornerRadius = 10;
        cell.upperView.layer.masksToBounds = true;
        cell.upperView.layer.borderWidth = 1
        cell.upperView.layer.borderColor =   UIColor (red: 201.0/255.0, green:208.0/255.0, blue: 220/255.0, alpha: 1.0).CGColor
        
        cell.lowerView.layer.cornerRadius = 10;
        cell.lowerView.layer.masksToBounds = true;
        cell.lowerView.layer.borderWidth = 1
        cell.lowerView.layer.borderColor =   UIColor (red: 201.0/255.0, green:208.0/255.0, blue: 220/255.0, alpha: 1.0).CGColor
        
        let cellData = arrDataTableView[indexPath.row] as! attendanceModelClass

        cell.configCell(indexPath.row, data: cellData)

        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
            
            
        } else{
            cell.backgroundColor = UIColor.whiteColor()

        }
        
        
        cell.delegate = self
        
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        var cellSize: CGFloat
        
        let deviceHeight = CGRectGetHeight(self.view.frame);
         cellSize = CGFloat (deviceHeight * 0.35)
        
        
        if  constants.deviceType.IS_IPHONE {
            
            
            if constants.deviceType.IS_IPHONE_5 || constants.deviceType.IS_IPHONE_4
            {
                cellSize = CGFloat (deviceHeight * 0.43)
                return cellSize
                
            }
            else{
                cellSize = CGFloat (deviceHeight * 0.35
                )
                
                print (cellSize)
                return cellSize
            }
        }
            
        else if GlobalStaticMethods.isPad() {
          
            if GlobalStaticMethods.isPadPro() {
                print("ipad pro changed orientation")
                
                  cellSize = CGFloat (deviceHeight * 0.3)
                
                //                if UIInterfaceOrientationIsPortrait(UIApplication .sharedApplication() .statusBarOrientation) {
                //
                //
                //                    cellSize = CGFloat (deviceHeight * 0.25)
                //                    return cellSize
                //
                //                }
                //                else{
                //
                //                    cellSize = CGFloat (deviceHeight * 0.32)
                //                    return cellSize                }
                //
                //            }
                //            else{
                //                if UIInterfaceOrientationIsPortrait(UIApplication .sharedApplication() .statusBarOrientation) {
                //                    cellSize = CGFloat (deviceHeight * 0.27)
                //                    return cellSize                }
                //                else{
                //                    cellSize = CGFloat (deviceHeight * 0.36)
                //                    return cellSize
                //                    
                //                }
                //                
            }
            
        }
       
        return cellSize
        
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    
    

    func detailBtnTappedDelegate(detailBTN: AttendenceListTableViewCell) {
//      goToViewController(constants.viewControllers.attendenceDetailVC)

        
        let data = detailBTN.cellData
        
        let courseID = data.courseID
        let locationID = data.locationID
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.attendenceDetailVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.attendenceDetailVC) as! AttendenceDetailViewController
        
        vc.courseID = courseID
        vc.locationID = locationID!
        
        self.navigationController?.pushViewController(vc, animated: true)

        
      //--ww  mm_drawerController .toggleDrawerSide(.Left, animated: true) { (success) in
          //  print("Allhamdulillah")
            
        //}
    }
    
//    func detailBtnTappedDelegate(detailBTN: AttendenceTableViewCell) {
//     
//        
//        goToViewController(constants.viewControllers.attendenceDetailVC)
//            
//    
//        
//    }
    
           func goToViewController(identifier : String){
            let sb = navigation.getStoryBoardForController(identifier)!.storyboardObject
            let homeVC = sb.instantiateViewControllerWithIdentifier(identifier)
            self.navigationController!.pushViewController(homeVC, animated: true)
            
        }
    
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
