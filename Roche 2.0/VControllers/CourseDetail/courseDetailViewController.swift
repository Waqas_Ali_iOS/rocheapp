//
//  courseDetailViewController.swift
//  RocheDesignsOnly
//
//  Created by  Traffic MacBook Pro on 31/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Photos
class courseDetailViewController: HeaderViewController,NSURLSessionDownloadDelegate {
    
    
    var reloadView = true
    // Custom Cell object
    @IBOutlet var imgCourseType : UIImageView!
    @IBOutlet var mainScrollView : UIScrollView!
    @IBOutlet weak var customCView: customColView?
    @IBOutlet var btnBookMark : UIButton!
    @IBOutlet var btnSuggestCourse : UIButton!
    var  surveyFlow = 0
    @IBOutlet var lblEventStatus: BaseUILabel!
    @IBOutlet var btnFeedback: buttonFontCustomClass!
    @IBOutlet var btnTakeAssesment: buttonFontCustomClass!
    @IBOutlet var firstView : UIView!
    @IBOutlet var secondView : UIView!
    @IBOutlet var thirdView : UIView!
    @IBOutlet var fourthView : UIView!
    @IBOutlet var fifthView : UIView!
    @IBOutlet var sixView : UIView!
    @IBOutlet var sevenView : UIView!
    @IBOutlet var eightView : UIView!
    @IBOutlet var lblHeader: UILabel!
    
    @IBOutlet var btnTakeAssesments: buttonFontCustomClass!
    
    @IBOutlet var btnFirstLastView : buttonFontCustomClass?
    @IBOutlet var btnSecondLastView : buttonFontCustomClass?
    
    @IBOutlet var txtViewCourseOverView: BaseUITextView!
    @IBOutlet var btnPostMaterial: buttonFontCustomClass!
    @IBOutlet var btnDownloadMaterial: buttonFontCustomClass!
    @IBOutlet var videoView: UIView!
    @IBOutlet var lblLocationDetail: UILabel!
    @IBOutlet var tblViewCourse: UITableView!
    @IBOutlet var btnEnrolled: buttonFontCustomClass!
    @IBOutlet var btnMap: buttonFontCustomClass!
    @IBOutlet var lblCourseLocation: BaseUILabel!
    @IBOutlet var lblCourseTimeDate: BaseUILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var btnLocationDropDown: UIButton!
    @IBOutlet var txtViewDescription: UITextView!
    @IBOutlet var lblDescriptionHeading: UILabel!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    @IBOutlet weak var enrollBtnSeparateView: UIView?
    
    @IBOutlet var heightConstraintSixthView: NSLayoutConstraint!
    
    @IBOutlet var heightConstraintFifthView: NSLayoutConstraint!
    
    @IBOutlet var heightConstraintFourthView: NSLayoutConstraint!
    
    @IBOutlet var heightConstraintSevenView: NSLayoutConstraint!
    
    @IBOutlet var proportionalHeightConstraintFourthView: NSLayoutConstraint!
    @IBOutlet var proportionalHeightConstraintBtnFirstLastView: NSLayoutConstraint?
    @IBOutlet var proportionalHeightConstraintBtnSecondLasView: NSLayoutConstraint?
    @IBOutlet var bottomSpaceConstraintBtnSecondLasView: NSLayoutConstraint?

    @IBOutlet var btnVideo: UIButton!
    
    @IBOutlet var detailViewFifthView: UIView!
    @IBOutlet var colViewFifithView: UIView!
    @IBOutlet var trailingConstraintTakeAssessment : NSLayoutConstraint?
    @IBOutlet var leadingConstraintTakeAssessment : NSLayoutConstraint?
    
    @IBOutlet var trailingConstraintDownloadVideo : NSLayoutConstraint?
    @IBOutlet var leadingConstraintDownloadVideo : NSLayoutConstraint?
    
    @IBOutlet var heightConstraintFirstViewButton : NSLayoutConstraint?
    @IBOutlet var heightConstraintSecondViewButton : NSLayoutConstraint?
    
    @IBOutlet weak var txtViewTest: UITextView?
    
    var heightSecondViewButton : CGFloat = 0
    
    var aIndicaror = UIActivityIndicatorView()
    
    var isVideoDownloading = false
    
    
    
    var arrLocationNames = [String]()
    
    // MARK: - Public variables
    var strViewControllerName = NSString()
    
    // MARK: -  View outlet
    
    
    @IBOutlet weak var viewTimeAndDate: UIView?
    @IBOutlet weak var viewLocation: UIView?
    var strCourseID : String?
    // MARK: - temp methods and outlets
    @IBOutlet weak var btnBAMCClick: UIButton?
    
    @IBOutlet weak var SViewBottomConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var footerView: UIView?
    
    let single = Singleton.sharedInstance
    
    
    var eventArr : AnyObject?
    var dictData = Dictionary<String,AnyObject>()
    
    var courseType : Int = 0
    
    var isCurriculum = false
    let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    
    // VideoPlayerViewController * vd;
    
    var vd =  VideoPlayerViewController()
    
    var task : NSURLSessionTask!
    
    var percentageWritten:Float = 0.0
    var taskTotalBytesWritten = 0
    var taskTotalBytesExpectedToWrite = 0
    
    
    var isLoadFromDocumentDirecorty = false
    
    var remainingHours = 0
    var remainingDays = 0
    
    var urlStringForVideo = ""
    //    @IBOutlet weak var btnSave: UIButton!
    //    @IBOutlet weak var txtEnterName: UITextField!
    //    @IBOutlet weak var btnDelete: UIButton!
    //    
    
    lazy var session : NSURLSession = {
        let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        config.allowsCellularAccess = false
        let session = NSURLSession(configuration: config, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        return session
    }()
    
    
    
    
    //let  dataDic : NSMutableDictionary = ["title":"A","text":"1","image":"gen-work-txtfield"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        self.deleteVideo()
        
        
        self.txtViewDescription.textContainerInset = UIEdgeInsetsZero;
        self.automaticallyAdjustsScrollViewInsets = false
        
        
        if heightConstraintSecondViewButton != nil {
            heightSecondViewButton = heightConstraintSecondViewButton!.constant
        }
        self.mm_drawerController.openDrawerGestureModeMask = .None
        
        mainScrollView.hidden = true
        mainScrollView.bounces = false
       // enrollBtnSeparateView?.hidden = true
        
        if courseType == courseTypeEnum.inClass.rawValue {
            lblHeader.text = "In-Class Course"
        }
        else if courseType == courseTypeEnum.online.rawValue {
            lblHeader.text = "Online Course"
        }
        
        
        _headerView.heading = "Course Detail"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        
        if isCurriculum == false{
            _footerView.lblHeading.iPhoneFontSize = 50
            _footerView.leftButtonImage = "education"
            _footerView.rightButtonImage = "rightWhiteArrow"
            _footerView.bgColorVar = "#3e8ada"
            _footerView.bgColorHeighlightedFooter = "#113c8b"
            _footerView.heading = "Browse All Academy Courses"
            
        }
        else{
            _footerView.lblHeading.iPhoneFontSize = 50
            _footerView.leftButtonImage = ""
            _footerView.rightButtonImage = ""
            _footerView.bgColorVar = "#3e8ada"
            _footerView.bgColorHeighlightedFooter = "#113c8b"
            _footerView.heading = "Browse My Curriculum"
            
            
        }
        _footerView.btnFooter.hidden = false
        
        
        self.viewTimeAndDate?.layer.cornerRadius = 10
        self.viewTimeAndDate?.layer.borderWidth = 1
        self.viewTimeAndDate?.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        self.viewLocation?.layer.cornerRadius = 10
        self.viewLocation?.layer.borderWidth = 1
        self.viewLocation?.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.viewLocation?.layer.masksToBounds = true
        
        
        
        enrollBtnSeparateView?.hidden = true
        
        btnSetups()
        
      
        
        
    }
    
    
    
//    func populateDataFromNotification()  {
//        
//        self.viewDidLoad()
//        self.courseDetail(Int(strCourseID!)!)
//        
//    }
    
    func btnSetups(){
        btnEnrolled.addTarget(self, action: #selector(courseDetailViewController.btnEnrollButtonPressed), forControlEvents: .TouchUpInside)
        btnLocationDropDown.addTarget(self, action: #selector(courseDetailViewController.didTapOnLocation), forControlEvents: .TouchUpInside)
        btnDownloadMaterial.addTarget(self, action: #selector(courseDetailViewController.downloadVideo), forControlEvents: .TouchUpInside)
        btnMap.addTarget(self, action: #selector(courseDetailViewController.gotoMap), forControlEvents: .TouchUpInside)
        
        btnVideo.addTarget(self, action: #selector(courseDetailViewController.playVideo), forControlEvents: .TouchUpInside)
        self.btnFeedback.addTarget(self, action: #selector(courseDetailViewController.goToCourseSurvey), forControlEvents: .TouchUpInside)
        self.btnTakeAssesment.addTarget(self, action: #selector(courseDetailViewController.goToAssessment), forControlEvents: .TouchUpInside)
        
        self.btnBAMCClick!.addTarget(self, action: #selector(self.goToDetail), forControlEvents: .TouchUpInside)
        btnBookMark.addTarget(self, action: #selector(courseDetailViewController.bookMarkCourse), forControlEvents: .TouchUpInside)
        btnSuggestCourse.addTarget(self, action: #selector(courseDetailViewController.suggestCourse), forControlEvents: .TouchUpInside)
        
        btnFirstLastView?.addTarget(self, action: #selector(courseDetailViewController.openPreMaterial), forControlEvents: .TouchUpInside)
        
        btnSecondLastView?.addTarget(self, action: #selector(courseDetailViewController.openPostMaterial), forControlEvents: .TouchUpInside)
    }
    
    func gotoMap(){
        
        
        
        let sb = navigation.getStoryBoardForController("locationViewController")?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier("locationViewController") as! locationViewController
        
        let enroll =  self.dictData[keys.kEnrolled.rawValue]!.integerValue
        
        var lat = ""
        var long = ""
        
        if enroll == enrolledEnum.RequestEnrolled.rawValue  || enroll ==  enrolledEnum.Enrolled.rawValue {
            
            lat = self.dictData[keys.kLocationLatitutde.rawValue] as! String
            long = self.dictData[keys.kLocationLongitude.rawValue] as! String
        }
        else{
            lat = self.eventArr![keys.kLocationLatitutde.rawValue] as! String
            long = self.eventArr![keys.kLocationLongitude.rawValue] as! String
            
        }
        
        
        
        
        vc.locationTitle = self.dictData[keys.kTitle.rawValue] as! String
        
        vc.lat = Double(lat)!
        vc.long = Double(long)!
        reloadView = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func openPreMaterial(){
        
        let link = self.dictData[keys.kPreMaterial.rawValue]! as! String
        
        if link != "" {
            let sb = navigation.getStoryBoardForController("VideoViewController")?.storyboardObject
            let vc = sb?.instantiateViewControllerWithIdentifier("VideoViewController") as! VideoViewController
            
            vc.strLink = link
            vc.titleHeader = "Pre Material"
            reloadView = true

            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            Alert.showAlertMsgWithTitle("Message", msg: "No pre-course material uploaded yet", btnActionTitle: "OK", viewController: self, completionAction: { (sucess) in
                printLog("")
            })
        }
        
    }
    func openPostMaterial(){
        
        let link = self.dictData[keys.kPostMaterial.rawValue]! as! String
        
        if link != "" {
            let sb = navigation.getStoryBoardForController("VideoViewController")?.storyboardObject
            let vc = sb?.instantiateViewControllerWithIdentifier("VideoViewController") as! VideoViewController
            
            vc.strLink = link
            vc.titleHeader = "Post Material"
            reloadView = true

            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else{
            Alert.showAlertMsgWithTitle("Message", msg: "No post-course material uploaded yet", btnActionTitle: "OK", viewController: self, completionAction: { (sucess) in
                printLog("")
            })
        }
        
    }
    
    
    //    func playVideo(){
    //        
    ////        let sb = navigation.getStoryBoardForController("VideoViewController")?.storyboardObject
    ////        let vc = sb?.instantiateViewControllerWithIdentifier("VideoViewController") as! VideoViewController
    ////        vc.strLink = self.dictData[keys.kVideoLink.rawValue]! as! String
    ////        vc.titleHeader = "Video"
    ////        self.navigationController?.pushViewController(vc, animated: true)
    //        
    //        
    //    }
    
    
    
    func btnEnrollButtonPressed(){
        
        let courseID = self.dictData[keys.kCourseID.rawValue] as! String
        
        
        if courseType == courseTypeEnum.inClass.rawValue {
            
            
            
            let eventID = eventArr!["locId"] as! Int
            
            enrollForCourseInClass(courseID, strEventID: String(eventID))
            
        }
        else if courseType == courseTypeEnum.online.rawValue {
            
            enrollForCourseOnline(courseID)
        }
    }
    
    func suggestCourse(){
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseSuggestVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.courseSuggestVC) as! CourseSuggestionViewController
        vc.courseID = Int(strCourseID!)!
        reloadView = true

        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    override func didFooterBtnPressed(sender: AnyObject) {
        super.didFooterBtnPressed(sender)
        
        
        if isCurriculum == false{
            reloadView = true

            leftMenu.pushOrPop(constants.viewControllers.academyCourseVC, sideDrawer: appdelegate.drawerController!)
            //            navigation.goToViewController(constants.viewControllers.academyCourseVC, sideDrawer: appdelegate.drawerController!)
            
        }
        else {
            reloadView = true

            leftMenu.pushOrPop(constants.viewControllers.curriculumVC, sideDrawer: appdelegate.drawerController!)
            
            
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        
        if reloadView == true{
            courseDetail(Int(strCourseID!)!)

        }
        
        
        
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
        
        
        self.view.layoutIfNeeded()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    override func viewDidLayoutSubviews() {
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    
    
    func bookMarkCourse(){
        
        
        let user = UserModel.sharedInstance
        
        AFWrapper.serviceCall(AFUrlRequestNabeel.BookMarkCourse(sessionId: user.sessionID, courseId: Int(strCourseID!)!), success: { (result) in
            
            
            
            if (result.dictionary!["statusCode"])! == 1000            {
                
                
                let isBookmarked = result["data"]["bookmarked"].intValue
                
                var statusMessage = ""
                
                if isBookmarked == 1 {
                    self.btnBookMark.selected = true
                    statusMessage = Constants.BOOKMARK_MSG
                }
                else {
                    self.btnBookMark.selected = false
                    statusMessage = Constants.UNBOOKMARK_MSG
                    
                    
                }
                
                
                
                
                Alert.showAlertMsgWithTitle("Success", msg: statusMessage, btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
                    
                })
                
                self.bookmarkSelection()
            }
            
        }) { (NSError) in
            
        }
        
    }
    
    func bookmarkSelection(){
        //Gen-bookmark-img-sel
        
        //Gen-BookMark-Btn
        
        var imageName = ""
        if self.btnBookMark.selected == true{
            imageName = "Gen-bookmark-img-sel"
            
            let image = UIImage(imageName: imageName)
            
            self.btnBookMark.setImage(image, forState: .Selected)
            
        }
        else {
            imageName = "Gen-BookMark-Btn"
            
            let image = UIImage(imageName: imageName)
            
            self.btnBookMark.setImage(image, forState: .Normal)
            
        }
        
        
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return (customCView?.arrCellData.count)!
    }
    
    // make a cell for each cell index path
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        var cell = CustomCollectionViewCell()
        cell = collectionView.dequeueReusableCellWithReuseIdentifier("CustomCell", forIndexPath: indexPath) as! CustomCollectionViewCell
        
        let dic : NSDictionary = customCView?.arrCellData[indexPath.row] as! NSDictionary
        
        
        
        cell.SetupCell(dic, indexpath: indexPath.row)
        //--ww   cell.backgroundColor = UIColor.redColor()
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    //Use for size
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        
        if GlobalStaticMethods.isPhone(){
            let  width =  CGRectGetWidth((self.customCView?.frame)!) * 0.50
            let height  = CGRectGetHeight((self.customCView?.frame)!) / 3
            
            return CGSizeMake(width, height);
            
        }
        else{
            
            
            let  width = Int ( CGRectGetWidth((self.customCView?.frame)! ) * 0.33 )
            let height  = CGRectGetHeight((self.customCView?.frame)!) / 2
            return CGSizeMake(CGFloat(width), height);
            
            
        }
        
        
    }
    //Use for interspacing
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    
    
    
    // MARK: - view button methods
    
    func downloadVideo(){
        
        
        var videoImageUrl =  self.dictData[keys.kVideoLink.rawValue] as! String
        
        
        var fileName: String?
        var finalPath: NSURL?
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            //All stuff here
            
            
            videoImageUrl = videoImageUrl.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
            
            
            let url=NSURL(string: videoImageUrl)
            
            if (url != nil)
            {
                let urlData=NSData(contentsOfURL: url!)
                
                if((urlData) != nil)
                {
                    let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
                    
                    let fileName = url!.lastPathComponent //.stringByDeletingPathExtension
                    
                    let filePath="\(documentsPath)/\(fileName)"
                    
                    //saving is done on main thread
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        urlData?.writeToFile(filePath, atomically: true)
                    })
                    
                }
            }
            
            
        })
        //        guard let videoURL = URL(string: videoImageUrl) else { return }
        //        Alert.showAlertMsgWithTitle("Error 691", msg: "Internal error occured while downloading. Please upgrade your OS", btnActionTitle: "Return", viewController: self) { (success) in
        //            printLog("OK")
        //        }
    }
    
    //    func goToAssesments(){
    //        
    //        let sb = navigation.getStoryBoardForController("AssessmentViewController")!.storyboardObject
    //        let homeVC : AssessmentViewController = sb.instantiateViewControllerWithIdentifier("AssessmentViewController") as! AssessmentViewController
    //        
    //        homeVC.currentCourseID = Int(strCourseID!)!
    //        
    //        
    //        let navCon = self.mm_drawerController.centerViewController as! MMNavigationController
    //        
    //        navCon.pushViewController(homeVC, animated: true)
    //        
    //    }
    func filter(){
        
        
        
        leftMenu.pushOrPop(constants.viewControllers.filterVC, sideDrawer: self.mm_drawerController)
        
        
    }
    
    func goToDetail() {
        navigation.goToViewController(constants.viewControllers.curriculumVC, sideDrawer: appdelegate.drawerController!)
    }
    
    // MARK: - Custom method for navigation
    
    func goToCourseSurvey()
    {
        let sb = navigation.getStoryBoardForController("AssessmentViewController")!.storyboardObject
        let homeVC : AssessmentViewController = sb.instantiateViewControllerWithIdentifier("AssessmentViewController") as! AssessmentViewController
        
        homeVC.screenTitle = "CourseSurvey"
        homeVC.currentCourseID = Int(strCourseID!)!
        let tag = btnFeedback.tag
        
        
        homeVC.methodType = tag
        
        let navCon = self.mm_drawerController.centerViewController as! MMNavigationController
        reloadView = true
        navCon.pushViewController(homeVC, animated: true)
    }
    func goToAssessment()
    {
        let sb = navigation.getStoryBoardForController("AssessmentViewController")!.storyboardObject
        let homeVC : AssessmentViewController = sb.instantiateViewControllerWithIdentifier("AssessmentViewController") as! AssessmentViewController
        
        homeVC.screenTitle = "Course Survey"
        let tag = btnTakeAssesment.tag
        
        
        homeVC.methodType = tag
        homeVC.currentCourseID = Int(strCourseID!)!
        
        
        let navCon = self.mm_drawerController.centerViewController as! MMNavigationController
        reloadView = true

        navCon.pushViewController(homeVC, animated: true)
    }
    
    
    
    
    func didTapOnLocation(){
        ActionSheetStringPicker.showPickerWithTitle("Choose Location", rows: arrLocationNames, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            
            
            self.heightConstraintFifthView.active = false
            
            self.lblLocation.text = self.arrLocationNames[value]
            //            
            //            let dict = self.dictData[keys.kEvents.rawValue]! as! Array<AnyObject>
            //            
            //             self.eventArr = dict[value]
            
            let myArr = self.dictData[keys.kEvents.rawValue]! as! [AnyObject]
            self.eventArr = myArr[value]
            
            
            self.lblCourseTimeDate.text = self.eventArr![keys.kLocationTimeDate.rawValue] as! String
            let address = self.eventArr![keys.kLocationAddress.rawValue] as! String
            if address != "" {
                self.lblCourseLocation.text = address
            }
            else{
                self.lblCourseLocation.text = self.eventArr![keys.kLocationText.rawValue] as! String
            }
            let attending = self.eventArr![keys.kLocationAttending.rawValue] as! Int
            let availability = self.eventArr![keys.kLocationAvailability.rawValue] as! Int
            let trainer = self.eventArr![keys.kLocationInstructor.rawValue] as! String
            let classSize = self.eventArr![keys.kLocationClassSize.rawValue] as! Int
            let courseType = self.eventArr![keys.kLocationCourseType.rawValue] as! Int
            var strCourseType = ""
            var hospRep = self.eventArr![keys.kLocationHospitalRep.rawValue] as! String
            let expired = self.eventArr![keys.kLocationExpired.rawValue] as! Int
            
            
            
            if courseType == courseTypeEnum.inClass.rawValue{
                strCourseType = "In-Class"
                self.courseSessionExpiration(expired)
                // self.courseSessionExpiration(
            }
            else{
                strCourseType = "Online"
                //courseSessionExpiration()
                
            }
            let courseDuration = self.eventArr![keys.kLocationCourseDuration.rawValue] as! String
            
            
            if hospRep == "" {
                hospRep = "N/A"
                
            }
            
            
            self.customCView?.arrCellData = [["title":"Instructor","text": trainer ,"image":"gen-Instructor"],["title":"Class Size","text":"\(classSize)","image":"gen-attendies"],["title":"Availability","text":"\(availability) Seats","image":"gen-Availability"],["title":"Hospitals Rep.","text":hospRep,"image":"gen-HospitalsRep"],["title":"Course Type","text": strCourseType ,"image":"gen-courseType"],["title":"Class Duration","text":courseDuration,"image":"gen-ClassDuration"]]
          
            self.performSelector(#selector(self.callMethodTOReloadCell), withObject: nil, afterDelay: 0.1)
            
           // self.customCView?.reloadData()
            
            
            //            self.lblCourseTimeDate.text = eventArr[valu]
            self.screenVariationsForInClass(courseDetailVariations.inClassLocationSelected.rawValue)
            
            }, cancelBlock: { ActionStringCancelBlock in
                
                
                return }, origin: self.btnLocationDropDown)
        
        
    }
    
    func callMethodTOReloadCell() {
        
        self.customCView?.reloadData()
    }
    
    
    func variationSetup(){
        
        
       // enrollBtnSeparateView?.hidden = true
        
        var footerRightImage = ""
        var footerLeftImage = ""
        var footerCurriculumImage = ""
        
        
        footerLeftImage = constants.imagesNames.FOOTER_ACADEMY_LEFT_IMAGE
        footerRightImage = constants.imagesNames.FOOTER_FORWARD_RIGHT_IMAGE
        footerCurriculumImage = constants.imagesNames.FOOTER_CURRICULUM_LEFT_IMAGE
        
        
        if isCurriculum == true{
            _footerView.leftButtonImage = footerCurriculumImage
            
        }
        else{
            _footerView.leftButtonImage = footerLeftImage
            
        }
        //        _footerView.leftButtonImage = footerLeftImage
        
        _footerView.rightButtonImage = footerRightImage
        
        //        let enroll =  self.dictData[keys.kEnrolled.rawValue]!.integerValue
        //        
        //        
        //        courseType = self.dictData[keys.kCourseType.rawValue]!.integerValue
        
        //        if courseType == courseTypeEnum.online.rawValue {
        //            
        //            if enroll == enrolledEnum.NotEnrolled.rawValue {
        //                screenVariationsForOnline(courseDetailVariations.onlineClassBasicNotEnrolled.rawValue)
        //            }
        //            else if enroll == enrolledEnum.Enrolled.rawValue || enroll == enrolledEnum.RequestEnrolled.rawValue {
        //                screenVariationsForOnline(courseDetailVariations.onlineClassEnrolled.rawValue)
        //                
        //            }
        //            
        //        }
        //        else if courseType == courseTypeEnum.inClass.rawValue{
        //            
        //            if enroll == enrolledEnum.NotEnrolled.rawValue {
        //                screenVariationsForInClass(courseDetailVariations.inClassBasicNotEnrolled.rawValue)
        //            }
        //            else if enroll == enrolledEnum.RequestEnrolled.rawValue{
        //                screenVariationsForInClass(courseDetailVariations.inClassRequestForEnrolled.rawValue)
        //                
        //            }
        //            else if enroll == enrolledEnum.Enrolled.rawValue{
        //                screenVariationsForInClass(courseDetailVariations.inClassEnrolledApproved.rawValue)
        //                
        //            }
        
        
        // }
        
        
    }
    
    func screenVariationsForInClass(variation : Int){
        
        
        
        
        if variation == courseDetailVariations.inClassBasicNotEnrolled.rawValue {
            proportionalHeightConstraintFourthView.active = false
            heightConstraintFourthView.active = true
            heightConstraintFourthView.constant = 0
            btnEnrolled.userInteractionEnabled = true
            btnEnrolled.backgroundColor =  UIColor(hexString: "#3e8ada")
            
            self.fourthView.clipsToBounds = true
            heightConstraintFifthView.active = true
            heightConstraintFifthView.constant = 0
            self.fifthView.clipsToBounds = true
            
            
            //            btnLocationDropDown.userInteractionEnabled = true
            sevenView.removeFromSuperview()
            proportionalHeightConstraintBtnFirstLastView?.active = false
            proportionalHeightConstraintBtnSecondLasView?.active = false
            heightConstraintFirstViewButton?.active = true
            heightConstraintSecondViewButton?.active = true
            heightConstraintSixthView.active = false
            
            //       else{
            heightConstraintFirstViewButton?.constant = 0
            heightConstraintSecondViewButton?.constant = 0
            //       self.view.layoutIfNeeded()
            
            
            self.view.layoutSubviews()
        }
        else if  variation == courseDetailVariations.inClassLocationSelected.rawValue{
            // sixView.removeFromSuperview()
            sevenView.removeFromSuperview()
            heightConstraintSixthView.active = true
            heightConstraintSixthView.constant = 0
            
            
            
            btnLocationDropDown.userInteractionEnabled = true
            
            // SW--
            
            let expired = self.eventArr![keys.kLocationExpired.rawValue] as! Int
            
            if expired == expiredEnum.notExpired.rawValue {
                btnEnrolled.userInteractionEnabled = true
                btnEnrolled.backgroundColor =  UIColor(hexString: "#3e8ada")
            }
            // SW--
            
            //            btnEnrolled.userInteractionEnabled = true
            //            btnEnrolled.backgroundColor =  UIColor(hexString: "#3e8ada")
            
            btnFirstLastView!.hidden = true
            btnFirstLastView?.userInteractionEnabled = true
            btnFirstLastView?.setTitle("Pre Material", forState: .Normal)
            btnFirstLastView?.backgroundColor = UIColor(hexString: "#3e8ada")
            
            btnSecondLastView!.hidden = true
            btnSecondLastView?.setTitle("Post Material", forState: .Normal)
            btnSecondLastView?.userInteractionEnabled = false
            btnSecondLastView?.backgroundColor = UIColor.lightGrayColor()
            
            btnFirstLastView?.addTarget(self, action: #selector(courseDetailViewController.openPreMaterial), forControlEvents: .TouchUpInside)
            proportionalHeightConstraintBtnFirstLastView?.active = false
            proportionalHeightConstraintBtnSecondLasView?.active = false
            heightConstraintFirstViewButton?.active = true
            heightConstraintSecondViewButton?.active = true
            
            //       else{
            heightConstraintFirstViewButton?.constant = 0
            heightConstraintSecondViewButton?.constant = 0
            
            
            // SW -- In inClassLocationSelected status PreMaterial and PostMaterial Should not be visible.
            //            proportionalHeightConstraintBtnSecondLasView?.active = true
            //            heightConstraintSecondViewButton?.active = false
            
            //            btnFirstLastView?.hidden = true
            //            btnSecondLastView?.hidden = true
            
            
            
        }
            
        else if variation == courseDetailVariations.inClassRequestForEnrolled.rawValue{
            
            // SW -- In InClassRequestForEnrolled status PreMaterial and PostMaterial Should not be visible.
            proportionalHeightConstraintBtnSecondLasView?.active = true
            heightConstraintSecondViewButton?.active = false
            
            btnFirstLastView?.hidden = true
            btnSecondLastView?.hidden = true
            btnLocationDropDown.userInteractionEnabled = false
            
            
            btnEnrolled.userInteractionEnabled = false
            btnEnrolled.selected = true
            btnEnrolled.setTitle("Request for Enrolled", forState: .Selected)
            btnEnrolled.backgroundColor = UIColor.lightGrayColor()
            
            sixView.removeFromSuperview()
            fourthView.removeFromSuperview()
            sevenView.removeFromSuperview()
            
            
            btnSecondLastView?.addTarget(self, action: #selector(courseDetailViewController.openPostMaterial), forControlEvents: .TouchUpInside)
            
            
        }
            
        else if variation == courseDetailVariations.inClassEnrolledApproved.rawValue{
            
            heightConstraintFourthView.active = false
            
            
            btnFirstLastView?.hidden = false
            //            btnFirstLastView?.setTitle("Pre Material", forState: .Normal)
            //            btnFirstLastView?.userInteractionEnabled = false
            //            btnFirstLastView?.backgroundColor = UIColor.lightGrayColor()
            //   
            btnSecondLastView?.hidden = false
            //            btnSecondLastView?.userInteractionEnabled = true
            //            btnSecondLastView?.backgroundColor = UIColor(hexString: "#3e8ada")
            //            btnSecondLastView?.setTitle("Post Material", forState: .Normal)
            
            
            
            btnEnrolled.userInteractionEnabled = false
            btnEnrolled.selected = true
            btnEnrolled.backgroundColor = UIColor.lightGrayColor()
            
            if String(surveyFlow) == prePostButtonsEnable.allDisable.rawValue {
                
                
              /*  if GlobalStaticMethods.isPad(){
                    proportionalHeightConstraintBtnFirstLastView?.active = false
                    proportionalHeightConstraintBtnSecondLasView?.active = false
                    heightConstraintFirstViewButton?.active = true
                    heightConstraintSecondViewButton?.active = true
                }
                //       else{
                heightConstraintFirstViewButton?.constant = 0
                heightConstraintSecondViewButton?.constant = 0
                
                //      }
                btnSecondLastView?.setTitle("", forState: .Normal)
                btnFirstLastView?.setTitle("", forState: .Normal)
                
                */
                
                
            }
            else{
                
                btnEnrolled.userInteractionEnabled = false
                btnEnrolled.selected = true
                btnEnrolled.setTitle("Enrolled", forState: .Selected)
                btnEnrolled.backgroundColor = UIColor.lightGrayColor()
                proportionalHeightConstraintFourthView.active = true
                proportionalHeightConstraintFourthView.constant = 0
                
            }
            
            thirdView.removeFromSuperview()
            sixView.removeFromSuperview()
            sevenView.removeFromSuperview()
            
            btnSecondLastView?.addTarget(self, action: #selector(courseDetailViewController.openPostMaterial), forControlEvents: .TouchUpInside)
            
            
            _footerView.lblHeading.iPhoneFontSize = 50
            _footerView.leftButtonImage = "footer-curriculum"
            _footerView.rightButtonImage = "rightWhiteArrow"
            _footerView.bgColorVar = "#3e8ada"
            _footerView.bgColorHeighlightedFooter = "#113c8b"
            _footerView.heading = "Browse My Curriculum"
            
            isCurriculum = true
            
        }
        
    }
    
    //    override func updateViewConstraints() {
    ////        self.fifthView .addConstraints(fifthViewConstraints)
    //    }
    
    func screenVariationsForOnline(variation : Int)
    {
        
        
        
        

        if self.checkFileExsists(strCourseID!)
        {
           
            isVideoDownloading = false
            btnDownloadMaterial.userInteractionEnabled = false
            btnDownloadMaterial.setTitle("Downloaded", forState: .Normal)
            btnDownloadMaterial.backgroundColor = UIColor.lightGrayColor()
            
            
            let documentsDirectory = self.getDocumentDirectory()
            
            let fileName = strCourseID
            
            let filePath = String.init(format: "%@/%@.mp4", documentsDirectory,fileName!)
            
            urlStringForVideo = filePath
            
            isLoadFromDocumentDirecorty = true
        }
        else
        {
            isLoadFromDocumentDirecorty = false
          btnDownloadMaterial.setTitle("Download Video", forState: .Normal)
        }
        
        if variation == courseDetailVariations.onlineClassBasicNotEnrolled.rawValue{
            
            enrollBtnSeparateView?.hidden = false
            
            colViewFifithView.removeFromSuperview()
            detailViewFifthView.removeFromSuperview()
            thirdView.removeFromSuperview()
            fourthView.removeFromSuperview()
            heightConstraintSevenView.active = true
            heightConstraintSevenView.constant = 0
            self.sevenView.clipsToBounds = true
            sixView.removeFromSuperview()
            btnFirstLastView!.hidden = true
            btnSecondLastView!.hidden = true
            
            proportionalHeightConstraintBtnFirstLastView?.active = false
            proportionalHeightConstraintBtnSecondLasView?.active = false
            heightConstraintFirstViewButton?.active = true
            heightConstraintSecondViewButton?.active = true
            
            //       else{
            heightConstraintFirstViewButton?.constant = 0
            heightConstraintSecondViewButton?.constant = 0
            
            btnEnrolled.userInteractionEnabled = true
            btnEnrolled.backgroundColor = UIColor(hexString: "#3e8ada")
            
            
            
            
        }
            
        else if variation == courseDetailVariations.onlineClassEnrolled.rawValue{
            
            
            enrollBtnSeparateView?.hidden = true
            
            if GlobalStaticMethods.isPad(){
                
                let spacing : CGFloat =  267 //0.1953125 * self.view.bounds.height
                trailingConstraintTakeAssessment!.constant = spacing
                leadingConstraintTakeAssessment!.constant = spacing
                trailingConstraintDownloadVideo!.constant = spacing
                leadingConstraintDownloadVideo!.constant = spacing
                
            }
            //--YAHAN MASLA MILA 
            btnPostMaterial.removeFromSuperview()
            
            thirdView.removeFromSuperview()
            colViewFifithView.removeFromSuperview()
            detailViewFifthView.removeFromSuperview()
            heightConstraintSevenView.active = false
            fourthView.removeFromSuperview()
            sixView.removeFromSuperview()
            btnSecondLastView?.setTitle("Take Assessment", forState: .Normal)
            
            btnSecondLastView?.setImage(UIImage.init(named: "gen-takeAssesment"), forState: .Normal)
            btnSecondLastView?.setImage(UIImage.init(named: "gen-takeAssesment"), forState: .Highlighted)
            
            heightConstraintSecondViewButton?.active = true
            
            
            if self.dictData[keys.kCourseStatus.rawValue]!.integerValue == 3 {
                heightConstraintSecondViewButton?.constant = 0
                btnEnrolled.setTitle("Passed", forState: .Normal)
                let image = UIImage(named: "curriculum-pass-img")
                btnEnrolled.setImage(image!, forState: .Normal)
                btnSecondLastView!.hidden = true
                if GlobalStaticMethods.isPadPro(){
                bottomSpaceConstraintBtnSecondLasView?.constant = -10
                }
                else if GlobalStaticMethods.isPad(){
                    bottomSpaceConstraintBtnSecondLasView?.constant = -20

                }
                else {
                    bottomSpaceConstraintBtnSecondLasView?.constant = -10

                }
            }
            else {
                //            proportionalHeightConstraintBtnSecondLasView?.active = true
                heightConstraintSecondViewButton?.constant = heightSecondViewButton
                btnSecondLastView!.hidden = false
                
                //       else{
            }
            
            
            
            self.btnSecondLastView?.removeTarget(self, action: #selector(self.openPostMaterial), forControlEvents: .TouchUpInside)
            self.btnSecondLastView!.addTarget(self, action: #selector(self.goToAssessment), forControlEvents: .TouchUpInside)
            self.btnTakeAssesment?.tag = 1
            btnFirstLastView?.removeFromSuperview()
            
            btnEnrolled.userInteractionEnabled = false
            btnEnrolled.backgroundColor = UIColor.lightGrayColor()
            
            _footerView.lblHeading.iPhoneFontSize = 50
            _footerView.leftButtonImage = "footer-curriculum"
            _footerView.rightButtonImage = "rightWhiteArrow"
            _footerView.bgColorVar = "#3e8ada"
            _footerView.bgColorHeighlightedFooter = "#113c8b"
            _footerView.heading = "Browse My Curriculum"
            
            isCurriculum = true
        }
        
        
    }
    
    
    func enrollForCourseOnline(strCourseID : String){
        
        
        
        
        AFWrapper.serviceCall(AFUrlRequestHamza.EnrollCourseOnline(courseID: strCourseID), success: { (res) in
            printLog(res)
            
            let statusMessage = res["statusMessage"].string
            
            let enrolledStatus = res["data"]["enrolled"].int
            
            let data = res["data"].dictionaryValue
            
            if statusMessage == "success"
            {
                
                self.btnEnrolled.userInteractionEnabled = false
                self.btnEnrolled.backgroundColor = UIColor.lightGrayColor()
                
                if enrolledStatus == 2 {
                    self.btnEnrolled.setTitle("Request for Enrolled", forState: .Normal)
                }
                else if enrolledStatus == 3 {
                    self.btnEnrolled.setTitle("Enrolled", forState: .Normal)
                }
                
                if self.courseType == courseTypeEnum.online.rawValue {
                    
                    
                    let showAssessment = data[keys.kShowAssessment.rawValue]!.stringValue
                    if Int(showAssessment) == 0{
                        self.btnSecondLastView?.userInteractionEnabled = false
                        self.btnSecondLastView?.backgroundColor = UIColor.lightGrayColor()
                        
                    }
                    else {
                        self.btnSecondLastView?.userInteractionEnabled = true
                        self.btnSecondLastView?.backgroundColor = UIColor(hexString: "#3e8ada")
                        
                        
                    }
                    self.dictData[keys.kShowAssessment.rawValue] = data[keys.kShowAssessment.rawValue]!.stringValue
                    
                    
                    
                    if enrolledStatus == enrolledEnum.NotEnrolled.rawValue {
                        self.btnEnrolled.setTitle("Enroll", forState: .Normal)
                        
                        self.screenVariationsForOnline(courseDetailVariations.onlineClassBasicNotEnrolled.rawValue)
                    }
                    else if enrolledStatus == enrolledEnum.Enrolled.rawValue || enrolledStatus == enrolledEnum.RequestEnrolled.rawValue{
                        
                        self.btnEnrolled.setTitle("Enrolled", forState: .Normal)
                        
                        self.screenVariationsForOnline(courseDetailVariations.onlineClassEnrolled.rawValue)
                        
                    }
                }
                
                
                
                
                
            }
            
        }) { (NSERROR) in
            print("")
        }
        
        
        
        
    }
    
    
    func enrollForCourseInClass(strCourseID : String , strEventID : String){
        
        
        
        
        AFWrapper.serviceCall(AFUrlRequestHamza.EnrollCourseInClass(courseID: strCourseID, eventID: strEventID), success: { (res) in
            printLog(res)
            
            let statusMessage = res["statusMessage"].string
            
            let enrolledStatus = res["data"]["enrolled"].int
            
            let data = res["data"].dictionaryValue
            
            if statusMessage == "success"
            {
                
                self.btnEnrolled.userInteractionEnabled = false
                self.btnEnrolled.backgroundColor = UIColor.lightGrayColor()
                
                if enrolledStatus == 2 {
                    self.btnEnrolled.setTitle("Request for Enrolled", forState: .Normal)
                }
                else if enrolledStatus == 3 {
                    self.btnEnrolled.setTitle("Enrolled", forState: .Normal)
                }
                
                if self.courseType == courseTypeEnum.inClass.rawValue{
                    
                    
                    
                    
                    if enrolledStatus == enrolledEnum.NotEnrolled.rawValue {
                        self.screenVariationsForInClass(courseDetailVariations.inClassBasicNotEnrolled.rawValue)
                    }
                    else if enrolledStatus == enrolledEnum.RequestEnrolled.rawValue{
                        self.screenVariationsForInClass(courseDetailVariations.inClassRequestForEnrolled.rawValue)
                        
                    }
                    else if enrolledStatus == enrolledEnum.Enrolled.rawValue{
                        self.screenVariationsForInClass(courseDetailVariations.inClassEnrolledApproved.rawValue)
                        
                    }
                }
                
                
                
                
                
                
                
            }
            
        }) { (NSERROR) in
            print("")
        }
        
        
        
        
    }
    
    
    func courseDetail(courseID : Int){
        
        AFWrapper.serviceCall(AFUrlRequestHamza.CourseDetail(courseID: courseID), success: { (res) in
            printLog(res)
            
            let data = res["data"].dictionaryValue
            
            self.getDataDictionary(data)
            self.variationSetup()
            self._footerView.hidden = false
            self.mainScrollView.hidden = false
            
            
            
        }) { (NSERROR) in
            
            
            print("")
            
            
            
        }
        
        
        
    }
    
    
    func getDataDictionary(data : [String : JSON]){
        
        
        self.dictData[keys.kTitle.rawValue] = data[keys.kTitle.rawValue]!.stringValue
        self.dictData[keys.kCourseStatus.rawValue] = data[keys.kCourseStatus.rawValue]!.stringValue
        let enrolled = data[keys.kEnrolled.rawValue]!.stringValue
        self.dictData[keys.kEnrolled.rawValue] = enrolled
        self.dictData[keys.kCourseID.rawValue] = data[keys.kCourseID.rawValue]!.stringValue
        self.dictData[keys.kShortDescription.rawValue] = data[keys.kShortDescription.rawValue]!.stringValue
        self.dictData[keys.kDescription.rawValue] = data[keys.kDescription.rawValue]!.stringValue
        
        
        self.lblDescriptionHeading.text = self.dictData[keys.kTitle.rawValue] as! String
        self.txtViewDescription.attributedText = stringsClass.getAttributedStringForHTML( self.dictData[keys.kShortDescription.rawValue]! as! String, textSize:Int( (self.txtViewCourseOverView.font?.pointSize)!))
        //        //
        //        
        //        self.lblDescription.sizeToFit()
        //        self.lblDescription.layoutIfNeeded()
        
        self.txtViewCourseOverView.editable = false
        self.txtViewCourseOverView.attributedText = stringsClass.getAttributedStringForHTML(self.dictData[keys.kDescription.rawValue]! as! String, textSize:Int( (self.txtViewCourseOverView.font?.pointSize)!))
        
        
        
        if courseType == courseTypeEnum.inClass.rawValue {
            
            
            imgCourseType.image    = UIImage (named: Constants.INCLASS_COURSE_IMAGE)
            
            let bookmark = data[keys.kBookmark.rawValue]?.intValue
            if bookmark == 0{
                btnBookMark.selected = false
                
            }
            else {
                btnBookMark.selected = true
            }
            bookmarkSelection()
            
            let preMaterial = data[keys.kPreMaterial.rawValue]!["ios"].stringValue
            self.dictData[keys.kPreMaterial.rawValue] = preMaterial.stringByRemovingPercentEncoding
            let postMaterial = data[keys.kPostMaterial.rawValue]!["ios"].stringValue
            self.dictData[keys.kPostMaterial.rawValue] =   postMaterial.stringByRemovingPercentEncoding
            self.dictData[keys.kCertificate.rawValue] = data[keys.kCertificate.rawValue]!.stringValue
            //     self.dictData[keys.kShowAssessment.rawValue] = data[keys.kShowAssessment.rawValue]!.stringValue
            
            
            if Int(enrolled) == enrolledEnum.NotEnrolled.rawValue {
                
                btnEnrolled.setTitle("Enroll", forState: .Normal)
                btnLocationDropDown.userInteractionEnabled = true
                
                self.dictData[keys.kEvents.rawValue] = data[keys.kEvents.rawValue]?.arrayObject
                self.lblLocation.text = "Choose Location"
                if data[keys.kEvents.rawValue]?.arrayObject != nil {
                    
                    getLocationTitleArray(data[keys.kEvents.rawValue]!.arrayObject!)
                 let midArray = self.dictData[keys.kEvents.rawValue] as! [AnyObject]
                let count = midArray.count
                  
                    self.lblLocationDetail.text = "This course is available on \(count) location(s)"
                    
                    if count == 0 {
                        btnLocationDropDown.userInteractionEnabled = false
                        self.lblLocation.text = "No location available"
                        
                    }
                    
                }
                
            }
                
            else if Int(enrolled) == enrolledEnum.RequestEnrolled.rawValue {
                
                
                let address = data[keys.kLocationText.rawValue]!.stringValue
               // if address != "" {
                    self.lblCourseLocation.text = data[keys.kLocationAddress.rawValue]!.stringValue
            //    }
                

                btnEnrolled.setTitle("Request for Enrolled", forState: .Normal)
                btnLocationDropDown.userInteractionEnabled = false
                self.lblLocation.text = data[keys.kLocationText.rawValue]!.stringValue
                self.lblCourseTimeDate.text = data[keys.kLocationTimeDate.rawValue]!.stringValue
        //        self.lblCourseLocation.text = data[keys.kLocationText.rawValue]!.stringValue
                let attending = data[keys.kLocationAttending.rawValue]!.intValue
                let expired = data[keys.kLocationExpired.rawValue]!.intValue
                let availability = data[keys.kLocationAvailability.rawValue]!.intValue
                let trainer = data[keys.kLocationInstructor.rawValue]!.stringValue
                let classSize = data[keys.kLocationClassSize.rawValue]!.int
                let courseType = data[keys.kLocationCourseType.rawValue]?.intValue
                var strCourseType = ""
                
                if courseType == courseTypeEnum.inClass.rawValue {
                    strCourseType = "In-Class"
                }
                else{
                    strCourseType = "Online"
                    
                }
                var hospRep = data[keys.kLocationHospitalRep.rawValue]!.string
                
                if hospRep == "" {
                    hospRep = "N/A"
                }
                let expiration = data[keys.kLocationExpired.rawValue]!.intValue
                self.dictData[keys.kLocationLongitude.rawValue] = data[keys.kLocationLongitude.rawValue]!.stringValue
                self.dictData[keys.kLocationLatitutde.rawValue] = data[keys.kLocationLatitutde.rawValue]!.stringValue
                let courseDuration = data[keys.kLocationCourseDuration.rawValue]!.stringValue
                
                
                
                
                courseSessionExpiration(expired)
                
                self.customCView?.arrCellData = [["title":"Instructor","text": trainer ,"image":"gen-Instructor"],["title":"Class Size","text":"\(classSize!)","image":"gen-attendies"],["title":"Availability","text":"\(availability) Seats","image":"gen-Availability"],["title":"Hospitals Rep.","text":hospRep! ,"image":"gen-HospitalsRep"],["title":"Course Type","text": strCourseType ,"image":"gen-courseType"],["title":"Class Duration","text":courseDuration,"image":"gen-ClassDuration"]]
            
                
                // self.customCView?.reloadData()
                
                self.performSelector(#selector(self.callMethodTOReloadCell), withObject: nil, afterDelay: 0.1)
                
                
                
            }
            else if Int(enrolled) == enrolledEnum.Enrolled.rawValue {
               
                
                let address = data[keys.kLocationText.rawValue]!.stringValue
                // if address != "" {
                self.lblCourseLocation.text = data[keys.kLocationAddress.rawValue]!.stringValue
                btnEnrolled.setTitle("Enrolled", forState: .Normal)
                btnLocationDropDown.userInteractionEnabled = false
                surveyFlow = data[keys.kSurveyFlow.rawValue]!.intValue
                self.dictData[keys.kSurveyFlow.rawValue] = surveyFlow
                
                self.enableDisableCourseSurvey(String(surveyFlow))
                
                let expired = data[keys.kLocationExpired.rawValue]!.intValue
                self.lblCourseTimeDate.text = data[keys.kLocationTimeDate.rawValue]!.stringValue
                
                let availability = data[keys.kLocationAvailability.rawValue]!.intValue
                let trainer = data[keys.kLocationInstructor.rawValue]!.stringValue
                let classSize = data[keys.kLocationClassSize.rawValue]!.int
                let courseType = data[keys.kLocationCourseType.rawValue]?.intValue
                var strCourseType = ""
                
                if courseType == courseTypeEnum.inClass.rawValue {
                    strCourseType = "In-Class"
                }
                else{
                    strCourseType = "Online"
                    
                }
                
                var hospRep = data[keys.kLocationHospitalRep.rawValue]!.string
                
                if hospRep == "" {
                    hospRep = "N/A"
                }
                
                self.dictData[keys.kLocationLongitude.rawValue] = data[keys.kLocationLongitude.rawValue]!.stringValue
                self.dictData[keys.kLocationLatitutde.rawValue] = data[keys.kLocationLatitutde.rawValue]!.stringValue
                let courseDuration = data[keys.kLocationCourseDuration.rawValue]!.stringValue
                
                
                courseSessionExpiration(expired)
                
                
                self.customCView?.arrCellData = [["title":"Instructor","text": trainer ,"image":"gen-Instructor"],["title":"Class Size","text":"\(classSize!)","image":"gen-attendies"],["title":"Availability","text":"\(availability) Seats","image":"gen-Availability"],["title":"Hospitals Rep.","text":hospRep! ,"image":"gen-HospitalsRep"],["title":"Course Type","text": strCourseType ,"image":"gen-courseType"],["title":"Class Duration","text":courseDuration,"image":"gen-ClassDuration"]]
                
                
                //self.customCView?.reloadData()
                self.performSelector(#selector(self.callMethodTOReloadCell), withObject: nil, afterDelay: 0.1)
                
            }
            self.screenVariationsForInClass(Int(enrolled)!)
            
        }
        else if courseType == courseTypeEnum.online.rawValue {
            
            imgCourseType.image    = UIImage (named: Constants.ONLINE_COURSE_IMAGE)
            let bookmark = data[keys.kBookmark.rawValue]?.intValue
            if bookmark == 0{
                btnBookMark.selected = false
                
            }
            else {
                btnBookMark.selected = true
            }
            bookmarkSelection()

            
            
            
            let videoLink = data[keys.kVideoLink.rawValue]!.stringValue
            self.dictData[keys.kVideoLink.rawValue] = videoLink.stringByRemovingPercentEncoding
            var thumbnail = data[keys.kVideoThumbnail.rawValue]!["ios"].stringValue
            self.dictData[keys.kShowAssessment.rawValue] = thumbnail.stringByRemovingPercentEncoding
            
            
            thumbnail = thumbnail.stringByRemovingPercentEncoding!
            
            // Create Url from string
            let url = NSURL(string: thumbnail)!
            
            // Download task:
            // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
            let taskFORIMAGE = NSURLSession.sharedSession().dataTaskWithURL(url) { (responseData, responseUrl, error) -> Void in
                // if responseData is not null...
                if let data = responseData{
                    
                    // execute in UI thread
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        let videoimage = UIImage(data: data)
                        
                        
                        self.btnVideo.setBackgroundImage(videoimage, forState: .Normal)
                        
                        
                        
                    })
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                    })
                }
            }
            
            // Run task
            taskFORIMAGE.resume()
            
            
            
            
            
            
            
            
            urlStringForVideo =  self.dictData[keys.kVideoLink.rawValue]! as! String
            
            
            if urlStringForVideo != ""
            {
                btnDownloadMaterial.addTarget(self, action: #selector(courseDetailViewController.DownloadVideoWithProgressBar), forControlEvents: .TouchUpInside)
            }
            
            
            
            
            
            
            if Int(enrolled) == enrolledEnum.NotEnrolled.rawValue {
                btnEnrolled.setTitle("Enroll", forState: .Normal)
                
                self.screenVariationsForOnline(courseDetailVariations.onlineClassBasicNotEnrolled.rawValue)
                
                
            }
            else if Int(enrolled) == enrolledEnum.RequestEnrolled.rawValue || Int(enrolled) == enrolledEnum.Enrolled.rawValue {
                btnEnrolled.setTitle("Enrolled", forState: .Normal)
                
                self.screenVariationsForOnline(courseDetailVariations.onlineClassEnrolled.rawValue)
                
            }
            
            
            
            
            
        }
        
        
        //
        //
        //
        //        if courseType != courseTypeEnum.online.rawValue{
        //            
        //            
        //            if Int(enrolled) == enrolledEnum.RequestEnrolled.rawValue {
        //              
        //                self.btnLocationDropDown.userInteractionEnabled = false
        //
        //                if courseType == courseTypeEnum.inClass.rawValue{
        //                    strCourseType = "In-Class"
        //                    
        //                    self.courseSessionExpiration(expiration)
        //                }
        //                else{
        //                    strCourseType = "Online"
        //                    //courseSessionExpiration()
        //                    
        //                }
        //
        //                
        //                self.customCView?.reloadData()
        //
        //            }
        //            
        //            self.dictData[keys.kTotalCount.rawValue] = data[keys.kTotalCount.rawValue]?.stringValue
        //            self.dictData[keys.kdisablePostMaterial.rawValue] = data[keys.kdisablePostMaterial.rawValue]!.stringValue
        //            
        //            self.dictData[keys.kdisablePostMaterial.rawValue] = data[keys.kdisablePostMaterial.rawValue]!.stringValue
        //            self.dictData[keys.kShowFeedback.rawValue] = data[keys.kShowFeedback.rawValue]!.stringValue
        //
        //
        //            
        //        }
        //            
        //        else {
        //        //
        //        }
        //        
        
        //        self.dictData[keys.kLocationLongitude.rawValue] = data[keys.kEvents.rawValue]![keys.kLocationLongitude.rawValue].arrayObject
        //        self.dictData[keys.kLocationAttending.rawValue] = data[keys.kEvents.rawValue]![keys.kLocationAttending.rawValue].arrayObject
        //        self.dictData[keys.kLocationExpired.rawValue] = data[keys.kEvents.rawValue]![keys.kLocationExpired.rawValue].arrayObject
        //        self.dictData[keys.kLocationHospitalRep.rawValue] = data[keys.kEvents.rawValue]![keys.kLocationHospitalRep.rawValue].arrayObject
        //        self.dictData[keys.kLocationID.rawValue] = data[keys.kEvents.rawValue]![keys.kLocationID.rawValue].arrayObject
        //        self.dictData[keys.kLocationCourseDuration.rawValue] = data[keys.kEvents.rawValue]![keys.kLocationCourseDuration.rawValue].arrayObject
        //            self.dictData["eventsDetailLocCourseDuration"] = data[keys.kEvents.rawValue]!["locCourseDuration"].arrayObject
        
        
        
    }
    
    
    
    
    
    func getLocationTitleArray(EventsArray : [AnyObject]){
        arrLocationNames.removeAll()
        for i in 0 ..< EventsArray.count{
            
            let strLocationName = EventsArray[i][keys.kLocationText.rawValue] as! String
            
            arrLocationNames.append(strLocationName)
            
        }
        
        printLog(arrLocationNames)
        
    }
    
    func courseSessionExpiration(expired : Int){
        
        //let expired = self.eventArr![keys.kLocationExpired.rawValue] as! Int
        
        if expired == expiredEnum.expired.rawValue || expired == expiredEnum.disapproved.rawValue {
            
            if expired == expiredEnum.disapproved.rawValue
            {
                self.lblEventStatus.text = "(Disapproved)"
            }
            else
            {
                self.lblEventStatus.text = "(Expired)"
            }
            
            
            
            
            // SW -- If course is expired then user not able to enroll in course
            
            /////// My code start Here ////////
            viewTimeAndDate?.backgroundColor = UIColor(hexString: "#F9F1F2")
            
            self.btnFirstLastView?.hidden = true;
            self.btnSecondLastView?.hidden = true;
            self.btnEnrolled.userInteractionEnabled = false
            self.btnEnrolled.backgroundColor = UIColor.lightGrayColor()
            
            
            /////// My code End Here ////////
            
            //            self.btnTakeAssesment.userInteractionEnabled = true
            //            self.btnTakeAssesment.backgroundColor = UIColor(hexString: "#3e8ada")
            //            self.btnFeedback.userInteractionEnabled = true
            //            self.btnFeedback.backgroundColor = UIColor(hexString: "#3e8ada")
            
        }
        else{
            self.lblEventStatus.text = ""
            viewTimeAndDate?.backgroundColor = UIColor.whiteColor()
            
            //SW-- self.lblEventStatus.text = "Not expired"
            //            
            //            self.btnTakeAssesment.userInteractionEnabled = false
            //            self.btnTakeAssesment.backgroundColor = UIColor.lightGrayColor()
            //            self.btnFeedback.userInteractionEnabled = false
            //            self.btnFeedback.backgroundColor = UIColor.lightGrayColor()
            
            
            
        }
        
        
    }
    
    func enableDisableCourseSurvey(survey : String){
        
        if survey == prePostButtonsEnable.preCourseEnable.rawValue {
            
            btnFeedback.enabled = true
            btnFeedback.setTitle("Pre Survey", forState: .Normal)
            btnFeedback.backgroundColor =  UIColor(hexString: "#3e8ada")
            btnFeedback.tag = btnFeedbackEnum.openPreSurvey.rawValue
            
            
            btnTakeAssesment.enabled = false
            btnTakeAssesment.backgroundColor = UIColor.lightGrayColor()
            
            
            btnFirstLastView?.setTitle("Pre Material", forState: .Normal)
            btnFirstLastView?.userInteractionEnabled = true
            btnFirstLastView?.backgroundColor = UIColor(hexString: "#3e8ada")//.backgroundColor = UIColor.lightGrayColor()
            //
            btnSecondLastView?.userInteractionEnabled = false
            btnSecondLastView?.backgroundColor = UIColor.lightGrayColor()
            btnSecondLastView?.setTitle("Post Material", forState: .Normal)
            
        }
        else if survey == prePostButtonsEnable.postCourseDisable.rawValue {
            
            btnFeedback.enabled = false
            btnFeedback.setTitle("Attend Class", forState: .Normal)
            btnFeedback.backgroundColor = UIColor.lightGrayColor()
            btnFeedback.tag = btnFeedbackEnum.openPostSurvery.rawValue
            
            btnTakeAssesment.enabled = false
            btnTakeAssesment.backgroundColor = UIColor.lightGrayColor()
            
            
            btnFirstLastView?.setTitle("Pre Material", forState: .Normal)
            btnFirstLastView?.userInteractionEnabled = true
            btnFirstLastView?.backgroundColor = UIColor(hexString: "#3e8ada")//.backgroundColor = UIColor.lightGrayColor()
            //
            btnSecondLastView?.userInteractionEnabled = false
            btnSecondLastView?.backgroundColor = UIColor.lightGrayColor()
            btnSecondLastView?.setTitle("Post Material", forState: .Normal)
            
            
            
        }
        else if survey == prePostButtonsEnable.postCourseEnable.rawValue {
            
            btnFeedback.enabled = true
            btnFeedback.setTitle("Post Survey", forState: .Normal)
            btnFeedback.backgroundColor =  UIColor(hexString: "#3e8ada")
            
            btnFeedback.tag = btnFeedbackEnum.openPostSurvery.rawValue
            
            btnTakeAssesment.enabled = false
            btnTakeAssesment.backgroundColor = UIColor.lightGrayColor()
            
            
            btnFirstLastView?.setTitle("Pre Material", forState: .Normal)
            btnFirstLastView?.userInteractionEnabled = true
            btnFirstLastView?.backgroundColor = UIColor(hexString: "#3e8ada")
            //
            btnSecondLastView?.userInteractionEnabled = true
            btnSecondLastView?.backgroundColor = UIColor(hexString: "#3e8ada")
            btnSecondLastView?.setTitle("Post Material", forState: .Normal)
            
            
        }
        else if survey == prePostButtonsEnable.assesmentEnable.rawValue {
            
            btnFeedback.enabled = false
            btnFeedback.setTitle("Post Survey", forState: .Normal)
            btnFeedback.backgroundColor = UIColor.lightGrayColor()
            btnFeedback.tag = 2
            
            
            btnTakeAssesment.enabled = true
            btnTakeAssesment.backgroundColor = UIColor(hexString: "#3e8ada")
            btnTakeAssesment.tag = 1
            
            
            btnFirstLastView?.setTitle("Pre Material", forState: .Normal)
            btnFirstLastView?.userInteractionEnabled = true
            btnFirstLastView?.backgroundColor = UIColor(hexString: "#3e8ada")
            //
            btnSecondLastView?.userInteractionEnabled = true
            btnSecondLastView?.backgroundColor = UIColor(hexString: "#3e8ada")
            btnSecondLastView?.setTitle("Post Material", forState: .Normal)
            
            
            
        }
        else if survey == prePostButtonsEnable.allDisable.rawValue {
            
            btnTakeAssesment.enabled = false
            btnTakeAssesment.backgroundColor = UIColor.lightGrayColor()
            
            
            btnEnrolled.setTitle("Passed", forState: .Selected)
            let image = UIImage(named: "curriculum-pass-img")
            btnEnrolled.setImage(image!, forState: .Normal)
            btnFeedback.setTitle("Post Survey", forState: .Normal)
            btnFeedback.backgroundColor = UIColor.lightGrayColor()
            btnFeedback.tag = 2
            
            fourthView.removeFromSuperview()
            
            btnFirstLastView?.setTitle("Pre Material", forState: .Normal)
            btnFirstLastView?.userInteractionEnabled = true
            btnFirstLastView?.backgroundColor = UIColor(hexString: "#3e8ada")
            //
            btnSecondLastView?.userInteractionEnabled = true
            btnSecondLastView?.backgroundColor = UIColor(hexString: "#3e8ada")
            btnSecondLastView?.setTitle("Post Material", forState: .Normal)
            
            proportionalHeightConstraintBtnFirstLastView?.active = false
            proportionalHeightConstraintBtnSecondLasView?.active = false
            
            heightConstraintFirstViewButton?.active = true
            heightConstraintSecondViewButton?.active = true
            
            heightConstraintFirstViewButton?.constant = btnEnrolled.frame.size.height
            heightConstraintSecondViewButton?.constant = btnEnrolled.frame.size.height
           
            
        
            
            self.view.layoutIfNeeded()
            
            
            
        }
    }
    
    
    enum keys : String {
        case kBookmark = "bookmarked"
        case kTitle = "title"
        case kTotalCount = "totalCount"
        case kdisablePostMaterial = "disablePostMaterial"
        case kCourseStatus = "courseStatus"
        case kShowFeedback = "showFeedback"
        case kDescription = "description"
        case kPreMaterial = "preMaterial"
        case kEnrolled = "enrolled"
        case kCourseID = "courseId"
        case kCourseType = "courseType"
        case kCertificate = "certificate"
        case kEvents = "events"
        case kSurveyFlow = "surveyFlow"
        case kLocationLongitude = "locLong"
        case kLocationAttending = "locAttending"
        case kLocationExpired = "expired"
        case kLocationHospitalRep = "locHospRep"
        case kLocationID = "locId"
        case kLocationCourseDuration = "locCourseDuration"
        case kLocationText = "locText"
        case kLocationAddress = "locAddress"
        
        case kLocationClassSize = "locClassSize"
        case kLocationTimeDate = "locTimeDate"
        case kLocationInstructor = "locInstructor"
        case kLocationAvailability = "locAvailability"
        case kLocationCourseType = "locCourseType"
        case kLocationLatitutde = "locLat"
        case kShowAssessment = "showAssessment"
        case kShortDescription = "shortDescription"
        case kPostMaterial = "postMaterial"
        case kDisablePreMaterial = "disablePreMaterial"
        case kVideoLink = "videoLink"
        case kVideoThumbnail = "videoThumbnail"
        
    }
    
    enum disbaleYesOrNo : Int{
        case no = 0
        case yes
        
    }
    
    enum courseTypeEnum : Int{
        case online = 1
        case inClass = 2
        
    }
    
    enum enrolledEnum : Int{
        case NotEnrolled = 1
        case RequestEnrolled = 2
        case Enrolled = 3
    }
    enum expiredEnum : Int{
        case notExpired = 0
        case expired
        case disapproved
    }
    
    enum btnFeedbackEnum : Int {
        
        case openPreSurvey = 2
        case openPostSurvery = 3
        case openNothing = 4
    }
    
    enum prePostButtonsEnable : String{
        
        case preCourseEnable = "0"
        case postCourseDisable = "1000"
        case postCourseEnable = "1100"
        case assesmentEnable = "1110"
        case allDisable = "1111"
        
    }
    
    
    //MARK: - Load Video
    func saveFileNameInPlist(strName : String )
    {
        let currentDate = NSDate()
        let fileManager = NSFileManager.defaultManager()
        let plistPath = self.getPlistPath()
        
        if(!fileManager.fileExistsAtPath(plistPath))
        {
            let data : [String: String] = [
                strName : toShortTimeString(currentDate),
                ]
            let someData = NSDictionary(dictionary: data)
            let isWritten = someData.writeToFile(plistPath, atomically: true)
            print("is the file created: \(isWritten)")
        }else{
            var myDict: NSDictionary?
            myDict = NSDictionary(contentsOfFile: plistPath)
            if let dict = myDict {
                dict.setValue(toShortTimeString(currentDate), forKey: strName)
                dict.writeToFile(plistPath, atomically: true)
            }
            myDict = NSDictionary(contentsOfFile: plistPath)
            print(myDict)
        }
    }
    
    func getFilePathName()
    {
        let documentsDirectory = self.getDocumentDirectory()
        var contentOfDirectory : Array<String>
        
        do {
            contentOfDirectory = try NSFileManager.defaultManager().contentsOfDirectoryAtPath(documentsDirectory)
            
            print(contentOfDirectory)
        } catch {
            print(error)
        }
    }
    
    
        func deleteVideo()
        {
    
            let fileName = strCourseID
    
            let filePath = String.init(format: "%@/%@", self.getDocumentDirectory(),fileName!)
    
            let fileManager = NSFileManager.defaultManager()
            if fileManager.fileExistsAtPath(filePath)
            {
                var myDict: NSDictionary?
                myDict = NSDictionary(contentsOfFile: getPlistPath())
    
                if myDict![strCourseID!] != nil
                {
                    let fileDate = convertDateFromString(myDict![strCourseID!] as! String)
    
                    //                let calendar = NSCalendar.currentCalendar()
    
                    let currentdate = NSDate()
    
    
                    let component = getDiffernceInDate(fileDate, secondDate: currentdate)
    
    
                    if component.hour >= 72
                    {
    
                        try! fileManager.removeItemAtPath(filePath)
    
                        isLoadFromDocumentDirecorty = false
                    }
                    else
                    {
                        isLoadFromDocumentDirecorty = true
                        
                        
                        remainingHours = 72 - component.hour
                        
                        
                        remainingDays = remainingHours/24
                        
                        remainingHours = (component.hour - (remainingDays * 24))
                        
                        
                        print(String.init(format: "number of time left %d , %d", 72 - component.hour  ,component.minute))
                    }
                }
            }
            else
            {
                isLoadFromDocumentDirecorty = false
                print("No such file")
            }
    
        }
    
    
    func getPlistPath() -> String
    {
        
        let documentDirectory = self.getDocumentDirectory()
        return  documentDirectory.stringByAppendingString("/courseVideosName.plist")
    }
    
    
    func getDocumentDirectory() -> String {
        return NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
    }
    
    func convertDateFromString(strDate : String) -> NSDate
    {
        //Get Short Time String
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ";
        let date = formatter.dateFromString(strDate)
        
        return date!
    }
    
    
    func getDiffernceInDate(firstDate : NSDate ,secondDate : NSDate  ) -> NSDateComponents {
        let calendar = NSCalendar.currentCalendar()
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = firstDate
        let date2 = secondDate
        
        let flags = NSCalendarUnit.Hour
        let components = calendar.components(flags, fromDate: date1, toDate: date2, options: [])
        
        return components
    }
    
    func toShortTimeString(date: NSDate) -> String
    {
        //Get Short Time String
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ";
        let timeString = formatter.stringFromDate(date)
        
        //Return Short Time String
        return timeString
    }
   
    func checkFileExsists(courseID : String) -> Bool
    {
        let documentsDirectory = self.getDocumentDirectory()
        
        let fileName = strCourseID
        
        let filePath = String.init(format: "%@/%@", documentsDirectory,fileName!)
        
        let fileManager = NSFileManager.defaultManager()
        if !fileManager.fileExistsAtPath(filePath) {
            
            return false
        }
        return true
    }
    
   
    
    
    func DownloadVideoWithProgressBar() {
        
        if !self.checkFileExsists(strCourseID!) {
            
            if isVideoDownloading == false
            {
                if self.task != nil {
                    
                }
                
                isVideoDownloading = true
                
                aIndicaror = UIActivityIndicatorView.init(activityIndicatorStyle: .White)
                aIndicaror.hidesWhenStopped = true
                let halfButtonHeight : CGFloat = btnDownloadMaterial.bounds.size.height / 2;
                let buttonWidth : CGFloat = btnDownloadMaterial.bounds.size.width;
                aIndicaror.center = CGPointMake(buttonWidth - halfButtonHeight , halfButtonHeight)
                //            [btnLogin addSubview:indicator];
                
                btnDownloadMaterial.addSubview(aIndicaror)
                
                //            aIndicaror.center = btnDownloadMaterial.center
                
                
                //            Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: "Video start downloading", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                //                
                //            })
                
                btnDownloadMaterial.setTitle("Downloading", forState: .Normal)
                
                
                aIndicaror.startAnimating()
                
                var s = urlStringForVideo
                
                s = s.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
                
                let url = NSURL(string:s)!
                let req = NSMutableURLRequest(URL:url)
                let task = self.session.downloadTaskWithRequest(req)
                self.task = task
                task.resume()
                
                btnDownloadMaterial.userInteractionEnabled = false
            }
        }
        else{
            
            Alert.showAlertMsgWithTitle("Error", msg: "Video Downloaded Already", btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
                print("")
            })
        }
        
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten writ: Int64, totalBytesExpectedToWrite exp: Int64)
    {
        print("downloaded \(100*writ/exp)")
        taskTotalBytesWritten = Int(writ)
        taskTotalBytesExpectedToWrite = Int(exp)
        percentageWritten = Float(taskTotalBytesWritten) / Float(taskTotalBytesExpectedToWrite)
        
        print(String(format: "%.01f", percentageWritten*100) + "%")
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
        // unused in this example
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        print("completed: error: \(error)")
        
        if error != nil
        {
                aIndicaror.stopAnimating()
                isVideoDownloading = false
                btnDownloadMaterial.userInteractionEnabled = false
                btnDownloadMaterial.setTitle("Downloaded", forState: .Normal)
                btnDownloadMaterial.backgroundColor = UIColor.lightGrayColor()
        }
//        

    }
    
    // this is the only required NSURLSessionDownloadDelegate method
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL location: NSURL) {
        
        let documentsDirectory = self.getDocumentDirectory()
        
        let fileName = strCourseID
        
        let filePath = String.init(format: "%@/%@", documentsDirectory,fileName!)
        
        let fileManager = NSFileManager.defaultManager()
        if !self.checkFileExsists(strCourseID!) {
            
            let data = NSData.init(contentsOfURL: location)!
            data.writeToFile(filePath, atomically: true)
            self.saveFileNameInPlist(fileName!)
            aIndicaror.stopAnimating()
            isVideoDownloading = false
            btnDownloadMaterial.userInteractionEnabled = true
            btnDownloadMaterial.setTitle("Downloaded", forState: .Normal)
            btnDownloadMaterial.backgroundColor = UIColor.lightGrayColor()
    
            Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: "Video download completely", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                
            })
            
            
        }
    }
    
    
    
    
    func playVideo() {
        
        
        
        if urlStringForVideo != ""
        {
            if isLoadFromDocumentDirecorty == true
            {
                
                var tempMessage = "Video will be deleted in "
                
                if remainingDays > 0
                {
                    tempMessage = String.init(format: "%d days ", remainingDays)
                }
                if remainingHours > 0
                {
                    tempMessage = String.init(format: "%d hours ", remainingHours)
                }
                
                Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: String.init(format: "%@ remaining", tempMessage), btnActionTitle: "Ok" , viewController: self, completionAction: { (Void) in
                    
                })
            }
            
            let str = urlStringForVideo
            
            vd = VideoPlayerViewController.init()
            
            vd.videoUrlString = str
            vd.view.frame = CGRectMake(0, 0, videoView.frame.size.width, videoView.frame.size.height)
            
            videoView.addSubview(vd.view)
            videoView.clipsToBounds = true
            
            self.addChildViewController(vd)
            
        }

        
    }
    
  
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
