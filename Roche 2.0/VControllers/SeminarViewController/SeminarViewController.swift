//
//  SeminarViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/28/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit

class SeminarViewController: HeaderViewController {

    
    @IBOutlet weak var upperView : UIView!
    @IBOutlet weak var lowerView : UIView!
    @IBOutlet weak var mapBtn : UIButton!
    @IBOutlet var topConstraint : NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()

        _headerView.heading = "Seminar Details"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        self.upperView.layer.cornerRadius = 10;
        self.upperView.layer.masksToBounds = true;
        self.upperView.layer.borderWidth = 1
        self.upperView.layer.borderColor =   UIColor (red: 201.0/255.0, green:208.0/255.0, blue: 220/255.0, alpha: 1.0).CGColor
        
        self.lowerView.layer.cornerRadius = 10;
        self.lowerView.layer.masksToBounds = true;
        self.lowerView.layer.borderWidth = 1
        self.lowerView.layer.borderColor =   UIColor (red: 201.0/255.0, green:208.0/255.0, blue: 220/255.0, alpha: 1.0).CGColor
        
        //        let maskPAth1 = UIBezierPath(roundedRect: cell.mapBtn.bounds,
        //                                     byRoundingCorners: [.BottomLeft,.BottomRight],
        //                                     cornerRadii:CGSizeMake(10.0, 10.0))
        //        let maskLayer1 = CAShapeLayer()
        //        maskLayer1.frame = cell.mapBtn.bounds
        //        maskLayer1.path = maskPAth1.CGPath
        //        cell.mapBtn.layer.mask = maskLayer1
        //        cell.mapBtn.clipsToBounds = true
        
        /*
        let cornerRadius: CGFloat = 10
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = UIBezierPath(
            roundedRect: self.mapBtn.bounds,
            byRoundingCorners: [.BottomLeft, .BottomRight],
            cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)
            ).CGPath
        self.mapBtn.layer.mask = maskLayer
        
        */
        
        
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
