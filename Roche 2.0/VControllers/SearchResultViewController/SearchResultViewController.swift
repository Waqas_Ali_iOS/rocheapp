//
//  SearchResultViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 8/22/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import SwiftyJSON

class SearchResultViewController: HeaderViewController , SearchTableViewCellDelegate,AFWrapperDelegate{
    
    var refreshControl: UIRefreshControl!
    @IBOutlet weak var viewCalendearHeightConstraint: NSLayoutConstraint!
    @IBOutlet var btnFilter: buttonFontCustomClass!
    @IBOutlet var btnCalendar: buttonFontCustomClass!
    @IBOutlet weak var tableview : UITableView!
    @IBOutlet weak var viewCalender : UIView!
    @IBOutlet weak var viewResult : UIView!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    @IBOutlet var topViewConstraint : NSLayoutConstraint?
    @IBOutlet var lblTotalCount : UILabel?

    var arrData = [JSON]()
    var courseId : Int!
    var TotaCount = 0
    var month = ""
    var year = ""
    var day = ""
    var strKeyword : String!
    var sortBy : Int!
    var courseType : String!


    override func viewDidLoad() {
    super.viewDidLoad()
        lblTotalCount?.text = ""
        print(month ?? 0, "month")
        print(day ?? 0, "month")
        print(year ?? 0, "month")
        print(courseType ?? 0 ,"courseTypeInclass")
        print(sortBy ?? 0,"sortBy")
        print(strKeyword ?? 0,"strKeyword")

        refreshControl = UIRefreshControl()
        //  refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        tableview.addSubview(refreshControl) // not required when using UITableViewController
        self.viewCalender.removeFromSuperview()
        
        
         self.serviceCall()
        
            self.tableview.rowHeight = UITableViewAutomaticDimension
            self.tableview.estimatedRowHeight = 140
    _headerView.heading = "Search Results"
     _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
    
        
        
  //  self.viewCalenderHeight()
    
    
    self.btnFilter.addTarget(self, action: #selector(SearchResultViewController.filter), forControlEvents: .TouchUpInside)
    self.btnCalendar.addTarget(self, action: #selector(SearchResultViewController.goToCalendar), forControlEvents: .TouchUpInside)
    
        if GlobalStaticMethods.isPad() {
            if  GlobalStaticMethods.isPadPro() {
                self.topViewConstraint?.constant = 51
                
            }else{
                self.topViewConstraint?.constant = 40
            }
        }
        
       
    
    }
    func refresh(sender:AnyObject) {
        
        self.serviceCall()
        // self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    override func viewDidAppear(animated: Bool) {
        
        self.mm_drawerController.openDrawerGestureModeMask = .None
        
        self.btnFilter.exclusiveTouch = false
        self.btnCalendar.exclusiveTouch = false
        
        
    }
    
    
    func goToCalendar(){
     //   navigation.goToViewController("customCalendarViewController", sideDrawer: self.mm_drawerController)
        leftMenu.pushOrPop("customCalendarViewController", sideDrawer: self.mm_drawerController)

    }
    func filter(){
     //   navigation.goToViewController(constants.viewControllers.filterVC, sideDrawer: self.mm_drawerController)
        leftMenu.pushOrPop(constants.viewControllers.filterVC, sideDrawer: self.mm_drawerController)

    }

    func serviceCall()  {
        
        
        
        
        
        let user = UserModel.sharedInstance
        AFWrapper.delegate = self
        AFWrapper.serviceCall(AFUrlRequestNabeel.GetSearchAcademyCourse(sessionId: user.sessionID, keyword: strKeyword ?? "", sortBy:"\(sortBy)"  ?? "", courseType:courseType ?? "" , offset: 0, limit:10 , year: "\(year)" ?? "" , month: "\(month)" ?? "", date:"\(day)" ?? "") , success: { (res) in
            
            
            self.parseResponce(res)

            
        }) { (NSError) in
            printLog(NSError)
            
        
            
        }
        
    }
    
   
    func didTapOnOKayOnAFWrapperAlert(errorCode : Int) -> Void {
        
        printLog("Do your awesome work here....")
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    func parseResponce(res : JSON)
    {
        
        if res["data"] != nil
        {
            
            if res["data"]["items"] != nil
            {
                
                arrData = res["data"]["items"].arrayValue
                self.tableview.reloadData()
                self.TotaCount = res["data"] ["totalCount"].int!
                
                self.lblTotalCount?.text = "Your search returned \(self.TotaCount) result(s)"
 
                
            }
            
            
            
        }
        
        
    }
    

    override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        if  arrData.count != 0 {
            
            self.tableview.hidden = false
            
            return ( arrData.count)
        }else{
            self.tableview.hidden = true
            return 0
        }
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> SearchTableViewCell {
        [self.tableview.separatorColor = UIColor .clearColor()]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! SearchTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.delegate = self
        cell.tag = indexPath.row
        cell.lblHeading?.text = arrData[indexPath.row]["title"].stringValue
        cell.lblDescription?.text = arrData[indexPath.row]["shortDescription"].stringValue
        cell.btnBookMark?.tag = arrData[indexPath.row]["id"].int!
        cell.btnDetail?.tag = arrData[indexPath.row]["id"].int!
        cell.btnSuggest?.tag = arrData[indexPath.row]["id"].int!

        if arrData[indexPath.row]["courseType"].int == 1{
            cell.lblTitle?.text = "Online Course"
            cell.imgCourse?.image = UIImage (named: Constants.ONLINE_COURSE_IMAGE)

        }
        else{
            cell.lblTitle?.text = "In-Class Course"
            cell.imgCourse?.image = UIImage (named: Constants.INCLASS_COURSE_IMAGE)

        }
        
        if arrData[indexPath.row]["bookmarked"].int! == 1
            
        {
            if GlobalStaticMethods.isPad(){
                cell.btnBookMark!.setBackgroundImage(UIImage(imageName :"Gen-bookmark-img-sel" ), forState: .Normal)
            }
            else{
                cell.btnBookMark!.setImage(UIImage(named: "Gen-bookmark-img-sel"), forState: UIControlState.Normal)
            }
            
        }else{
            //  cell.btnBookMark!.selected == true
            if GlobalStaticMethods.isPad(){
                cell.btnBookMark!.setBackgroundImage(UIImage(imageName :"Gen-BookMark-Btn" ), forState: .Normal)
            }
            else{
                cell.btnBookMark!.setImage(UIImage(named: "gen-bookmark"), forState: UIControlState.Normal)
            }
            
        }
        
       
        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor.whiteColor()
            
            
        } else{
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
        
        if GlobalStaticMethods.isPad() {
            if  GlobalStaticMethods.isPadPro() {
                cell.btnHeightConstraint?.constant = 51
                
            }else{
                cell.btnHeightConstraint?.constant = 40
            }
        }
        
        print(cell.btnDetail!.tag)
        cell.tag = indexPath.row
        
        return cell
        
        
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    
    
    let cellSize: CGFloat
    
    let deviceHeight = CGRectGetHeight(self.view.frame);
    
    
    if  constants.deviceType.IS_IPHONE {
    
    
//    if constants.deviceType.IS_IPHONE_5 || constants.deviceType.IS_IPHONE_4
//    {
//    cellSize = CGFloat (deviceHeight * 0.35)
//    return cellSize
//    
//    }
//    else{
//    cellSize = CGFloat (deviceHeight * 0.3
//    )
//    
//    print (cellSize)
//    return cellSize
//    }
        return UITableViewAutomaticDimension
    }
    
    else if GlobalStaticMethods.isPad() {
    if GlobalStaticMethods.isPadPro() {
    print("ipad pro changed orientation")
    
        }
    
    }
    cellSize = CGFloat (deviceHeight * 0.35)
    return UITableViewAutomaticDimension
    
    }
    
    func viewCalenderHeight(){
    
    let deviceWidth = CGRectGetWidth(self.view.frame);
    
    viewCalendearHeightConstraint.constant = deviceWidth * 0.17
    
    }
    
   
   
  
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
    
    
    super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    
    
    coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
    // print("LOOOOOL")
    
    self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
    
    }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
    //  print("End")
    
    }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()

    if GlobalStaticMethods.isPhone() {
        

    print("iphone changed orientation")
    }
    else{
    if GlobalStaticMethods.isPad() {

        
    if GlobalStaticMethods.isPadPro() {
        
        
    print("ipad pro changed orientation")
    
    
    
    if orientation.isLandscape {
    //   self.viewSupplementHeightConstraint.constant = 45
    }
    else{
    //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
    
    }
    
    }
    else{
    if orientation.isLandscape {
    //    self.viewSupplementHeightConstraint.constant = 50
    }
    else{
    //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
    
    }
    
    print("ipad changed orientation")
    }
    }
    }
    
    }
    
    override func viewWillAppear(animated: Bool) {
    updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
    
    }
    
    
    func UnbookMarkBtnTappedDelegate(unBookBTN: SearchTableViewCell) {
        
        courseId = unBookBTN.btnBookMark?.tag
        
        var user = UserModel.sharedInstance

        
        AFWrapper.serviceCall(AFUrlRequestNabeel.BookMarkCourse(sessionId:user.sessionID , courseId: courseId!), success: { (result) in
            
            
            
            if (result.dictionary!["statusCode"])! == 1000            {
                
                
                
                
                if GlobalStaticMethods.isPad(){
                    
                    
                    if unBookBTN.btnBookMark!.currentBackgroundImage!.isEqual(UIImage(imageName :"Gen-bookmark-img-sel")) {
                        //do something here
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.UNBOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        
                        unBookBTN.btnBookMark?.setBackgroundImage(UIImage(imageName :"gen-bookmark" ), forState: .Normal)
                        
                        self.arrData[unBookBTN.tag]["bookmarked"] = 0
                        
                    }
                    else
                    {
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.BOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        
                        unBookBTN.btnBookMark?.setBackgroundImage(UIImage(imageName :"Gen-bookmark-img-sel" ), forState: .Normal)
                        self.arrData[unBookBTN.tag]["bookmarked"] = 1
                        
                    }
                    
                    
                    
                    
                    //self.arrData["bookmark"].Int = 1
                }
                    
                else{
                    
                    
                    if unBookBTN.btnBookMark!.currentImage!.isEqual(UIImage(imageName :"Gen-bookmark-img-sel")) {
                        //do something here
                        
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.UNBOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        unBookBTN.btnBookMark?.setImage(UIImage(imageName :"gen-bookmark" ), forState: .Normal)
                        
                        self.arrData[unBookBTN.tag]["bookmarked"] = 0
                        
                    }
                    else{
                        
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.BOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        unBookBTN.btnBookMark?.setImage(UIImage(imageName :"Gen-bookmark-img-sel" ), forState: .Normal)
                        
                        self.arrData[unBookBTN.tag]["bookmarked"] = 1
                        
                    }
                }
                
                
                
            }
                
            else{
                
                Alert.showAlertMsgWithTitle("Message", msg: "Some Error", btnActionTitle: "ok", viewController: self, completionAction: { (Void) in
                    
                    
                    
                    
                })
                
            }
            
        }) { (NSError) in
            
            
            
        }
        
        
        
    }
    
    func goToSuggestCourse(Cell: SearchTableViewCell) {
        
        courseId = Cell.btnSuggest?.tag
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseSuggestVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.courseSuggestVC) as! CourseSuggestionViewController
        vc.courseID = courseId
        
       // print(cell.btnSuggest!.tag)
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    func SearchBtnTappedDelegate(detailBTN: SearchTableViewCell) {
   
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseDetailVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.courseDetailVC) as! courseDetailViewController
        vc.strCourseID = String(detailBTN.btnDetail!.tag)
        let courseTypeVal = arrData[detailBTN.tag]["courseType"].int
        
        print(detailBTN.btnDetail!.tag)
        vc.courseType = courseTypeVal!
        printLog(vc.courseType)

        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
