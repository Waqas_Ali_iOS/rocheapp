//
//  customCalendarViewController.swift
//  BaseProject
//
//  Created by  Traffic MacBook Pro on 17/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import FSCalendar
class customCalendarViewController: HeaderViewController,FSCalendarDataSource, FSCalendarDelegate,FSCalendarDelegateAppearance
{
    @IBOutlet weak var lblMonthTitle: UILabel!
    @IBOutlet weak var btnScrollBackCalendar: UIButton!
    @IBOutlet weak var btnScrollForwardCalendar: UIButton!
    @IBOutlet weak var viewtitleHeaderForCalendar: UIView!
    @IBOutlet weak var viewCalendarConainer: UIView!
    @IBOutlet weak var viewCalendarIconConainer: UIView!

    @IBOutlet var topConstraint : NSLayoutConstraint?

   @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    let currentDATE = NSDate()
    let calendarDate = NSCalendar.currentCalendar()
     //= calendarDate.components([.Day , .Month , .Year], fromDate: current)
    var dictCalendar = Dictionary<String, AnyObject>()
    var arrDictCalendar = Array<Dictionary<String,AnyObject>>()
    var arrEventDates = Array<String>()

    
    var currentDay : Int = 1
    var currentMonth : Int = 1
    var currentYear : Int = 2016
    let datesWithCat : NSArray = ["2015-05-05","2015-06-05","2015-07-05","2015-08-05","2015-09-05","2015-10-05","2015-11-05","2015-12-05","2016-01-06",
                        "2016-02-07","2016-03-08","2016-04-06","2016-05-06","2016-06-06","2016-07-06"]

    override func viewDidLoad() {
        super.viewDidLoad()

        
        calendar.dataSource = self
        calendar.delegate = self

        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
//        _headerView.heading = "Course Calendar"
        
        _headerView.bgColorVar = "#FFFFFF"
        _headerView.lblHeading.textColor = UIColor(hexString: "#113c8b")

        _headerView.leftButtonImage = "header-blue-arrow"
        _headerView._bgImage = "calendarHeaderImage"//UIImage(imageName: "calendarHeaderImage")
        viewtitleHeaderForCalendar.backgroundColor  = UIColor(hexString: "#113c8c")
//
    let components = calendarDate.components([.Day , .Month , .Year], fromDate: currentDATE)
        
        currentDay = components.day
        currentMonth = components.month
        currentYear = components.year
        
        calendar.appearance.selectionColor = UIColor(hexString: "#113c8c")
        calendar.scrollDirection = .Horizontal
        ///calendar.backgroundColor = UIColor(hexString: "#3e8ada")
        calendar.appearance.caseOptions = [.WeekdayUsesUpperCase]
        calendar.appearance.cellShape = .Rectangle
        lblMonthTitle.text =   calendar.stringFromDate(currentDATE, format: "MMMM, YYYY")
        
        calendar.scrollEnabled = false
        viewCalendarConainer.layer.cornerRadius = 15;
        viewCalendarConainer.layer.masksToBounds = true
        
        viewCalendarIconConainer.layer.cornerRadius = 5;
        viewCalendarIconConainer.layer.masksToBounds = true
     //   calendar. = UIColor.darkGrayColor()
        
        viewtitleHeaderForCalendar.backgroundColor = UIColor(hexString: "#113c8c")
        
        customizeBtns()
      //SW--  calendar.selectDate(calendar.dateWithYear(currentYear, month: currentMonth, day: currentDay))

        
        courseCalendarData(currentYear, month: currentMonth)


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    
    override func headerViewLeftBtnDidClick(headerView: HeaderView) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    
    
    
    func customizeBtns(){
        btnScrollBackCalendar .addTarget(self, action: #selector(customCalendarViewController.scrollCalendarToLeft), forControlEvents: .TouchUpInside)
        btnScrollForwardCalendar .addTarget(self, action: #selector(customCalendarViewController.scrollCalendarToRight), forControlEvents: .TouchUpInside)
    }
    
    func minimumDateForCalendar(calendar: FSCalendar) -> NSDate {
        return calendar.dateWithYear(2015, month: 1, day: 1)
    }
    
//    
//    func calendar(calendar: FSCalendar, numberOfEventsForDate date: NSDate) -> Int {
//        return 5
//    }
//
    
    
//    func calendar(calendar: FSCalendar, hasEventForDate date: NSDate) -> Bool {
////        let dateString: String = calendar.stringFromDate(date, format: "yyyy-MM-dd")
////
////        return arrEventDates.contains(dateString)
//
//    }
    func maximumDateForCalendar(calendar: FSCalendar) -> NSDate {
        return calendar.dateWithYear(2020, month: 12, day: 31)
    }
    
    func calendarCurrentPageDidChange(calendar: FSCalendar) {
        NSLog("change page to \(calendar.stringFromDate(calendar.currentPage))")
    }
    
    func calendar(calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorForDate date: NSDate) -> UIColor? {
        let dateString: String = calendar.stringFromDate(date, format: "yyyy-MM-dd")
        
        
        
        if arrEventDates.contains(dateString) {

            return UIColor(hexString: "#0860bd")
        }
        else {
            return UIColor.clearColor()
        }
        
    }
    
//    func calendar(calendar: FSCalendar, numberOfEventsForDate date: NSDate) -> Int {
//        let day = calendar.dayOfDate(date)
//        return day % 5 == 0 ? day/5 : 0;
//    }

//    func calendar(calendar: FSCalendar, didSelectDate date: NSDate) {
//        NSLog("calendar did select date \(calendar.stringFromDate(date))")
//    }
    
    func calendar(calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        calendarHeightConstraint.constant = bounds.height
        view.layoutIfNeeded()
    }
    
    
    
    
    
    
    func calendar(calendar: FSCalendar, didSelectDate date: NSDate) {
      
        let dateString: String = calendar.stringFromDate(date, format: "yyyy-MM-dd")

        
        
        if arrEventDates.contains(dateString) {
            
            let index = arrEventDates.indexOf(dateString)
            
            
            print(dateString)
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let eventDate = dateFormatter.dateFromString(dateString)
            let components = calendarDate.components([.Day , .Month , .Year], fromDate: eventDate!)
            let day = components.day
            let month = components.month
            let year = components.year



            
            let sb = navigation.getStoryBoardForController(constants.viewControllers.searchResultVC)?.storyboardObject
            let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.searchResultVC) as! SearchResultViewController
            vc.day = String(day)
            vc.month = String(month)
            vc.year = String(year)
            vc.courseType = String(courseTypeEnum.inClass.rawValue)
     
            self.navigationController?.pushViewController(vc, animated: true)
        
        }
        else{
            print(date)
        }
    }


    func scrollCalendarToLeft(){
        
        let minYear = currentYear - 1
        let minDate = calendar.dateWithYear(minYear, month: 1, day: 1)
        
        if calendar.currentPage .compare(minDate) == NSComparisonResult.OrderedAscending {
            print("NO")
        }
        else{
        let previousMonth = calendar.dateBySubstractingMonths(1, fromDate: calendar.currentPage)
        self.lblMonthTitle.text = calendar.stringFromDate(previousMonth, format: "MMMM, YYYY")
        calendar .setCurrentPage(previousMonth, animated: true)
            let components = calendarDate.components([.Day , .Month , .Year], fromDate: previousMonth)
            let month = components.month
            let year = components.year

        courseCalendarData(year, month: month)
            
            
        }
    }
    func scrollCalendarToRight(){
        let maxYear = currentYear + 5
        let maxDate = calendar.dateWithYear(maxYear, month: 1, day: 1)
        
        if calendar.currentPage .compare(maxDate) == NSComparisonResult.OrderedDescending {
            print("NO")
        }
        else{
        let futureMonth = calendar.dateByAddingMonths(1, toDate: calendar.currentPage)
        self.lblMonthTitle.text = calendar.stringFromDate(futureMonth, format: "MMMM, YYYY")
        calendar.setCurrentPage(futureMonth, animated: true)
   
            let components = calendarDate.components([.Day , .Month , .Year], fromDate: futureMonth)
            let month = components.month
            let year = components.year
            courseCalendarData(year, month: month)

        }
        
    }

    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    
    func courseCalendarData(year : Int , month : Int){
        AFWrapper.serviceCallWitLoaderButNoTitleMessage(AFUrlRequestHamza.CourseCalendar(year: year, month: month), success: { (res) in
        
            
            if res["statusCode"].intValue == statusCode.success.rawValue{
            printLog(res)
            for i in 0 ..< res["data"]["items"].count {
                
                var date = res["data"]["items"][i]["date"].stringValue
                let link = res["data"]["items"][i]["courseLink"].stringValue
                
                
                
                self.dictCalendar["date"] = date
                self.dictCalendar["link"] = link

                
                
                self.arrDictCalendar.append(self.dictCalendar)
                
            }
            
            
            
            printLog(self.dictCalendar)
            
            for i in 0 ..< self.arrDictCalendar.count{
                self.arrEventDates.append(self.arrDictCalendar[i]["date"] as! String)
                
            }
            
            self.calendar.reloadData()
            }
            else {
                
                /* ---wwww
                Alert.showAlertMsgWithTitle("Error", msg: "No Event Found", btnActionTitle: "Ok", viewController: self, completionAction: { (success) in
                    printLog("")
                })
 */
            }
            
            
            
        }) { (NSERROR) in
                
                print("")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
