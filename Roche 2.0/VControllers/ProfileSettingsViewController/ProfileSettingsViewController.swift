//
//  ProfileSettingsViewController.swift
//  RocheDesignsOnly
//
//  Created by  Traffic MacBook Pro on 26/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import IQKeyboardManager
import SwiftyJSON

class ProfileSettingsViewController: HeaderViewController,ProfileSettingsTableViewCellDelegate , UITextFieldDelegate{
    
    @IBOutlet var tblViewProfile : UITableView!
    @IBOutlet var topConstraintFromHeader : NSLayoutConstraint?
    
    @IBOutlet var btnViewQR: buttonFontCustomClass!
    let arrProfile = ["firstName","lastName","email","password","confirmPassword","workNumber","mobileNumber","hospital","labManager","secondaryEmail","updateProfileButton"]
    
    
    var dataDict = [String:AnyObject]()
    var arrayTextField = [UITextField]()
    
    var arrProfileData = Array<myProfileModelClass>()
    var imageViewQR : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _headerView.heading = "Profile Settings"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        
        self.btnViewQR .addTarget(self, action: #selector(ProfileSettingsViewController.viewQR), forControlEvents: .TouchUpInside)
        // Do any additional setup after loading the view.
        
        tblViewProfile.hidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileSettingsViewController.tap(_:)))
        
        tblViewProfile.addGestureRecognizer(tapGesture)
        tblViewProfile.bounces = false
        getProfile()
        
        
        
        if imageViewQR == nil {
            
            let image = UserModel.sharedInstance.userQRimage
            imageViewQR = UIImageView(image: image!)
            imageViewQR.image = nil
            imageViewQR.tag = 1;
            imageViewQR.contentMode = .ScaleAspectFit
            imageViewQR.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.7)
            imageViewQR.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width , height: self.view.bounds.height)
            imageViewQR.userInteractionEnabled = true
            
            let uiImage  = UIImageView(image: image!)
            
            uiImage.contentMode = .ScaleAspectFit
            
            let xImg = (self.view.bounds.width - self.view.bounds.width / 2) / 2
            let yImg = (self.view.bounds.height - self.view.bounds.height/2) / 2
            uiImage.frame = CGRect(x: xImg, y: yImg, width: self.view.bounds.width/2 , height: self.view.bounds.height/2)
            
            imageViewQR.addSubview(uiImage)
            view.addSubview(imageViewQR)
            self.view.bringSubviewToFront(imageViewQR)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
            imageViewQR.addGestureRecognizer(tap)
            
            imageViewQR.hidden = true
            
            
        }
        
        
    }
   

    
    
    
    func tap(gesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(animated: Bool) {
        self.mm_drawerController.openDrawerGestureModeMask = .None
        
    }
    
    
    func getProfile() {
        
        AFWrapper.serviceCall(AFUrlRequestWaqas.GetMyProfile, success: { (res) in
            printLog(res)
            
            if res["status"].intValue == 1 {
                
                
                self.dataDict = res["data"].dictionaryObject!
                
                
                self.populateModelData(self.dataDict)
                self.tblViewProfile.hidden = false
                self.tblViewProfile.reloadData()
                
                
            }
            
        }) { (NSError) in
            printLog(NSError)
        }
    }
    
    
    
    func populateModelData(data : Dictionary<String,AnyObject>){
        
        var temp = ""
        
        // SW-- as per QA, salesRepEmail should be appear on both cases
        
//        if UserModel.sharedInstance.userType == userTypes.labSupervisor ||
//            UserModel.sharedInstance.userType == userTypes.labTechnician
//        {
//            temp = data["labManagerEmail"] as! String
//        }
//        else
//        {
//            
//        }
        
        temp = data["salesRepEmail"] as! String
        
        arrProfileData = [
            
            myProfileModelClass(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: (data["firstName"] as? String)!, placeholder: "First Name", leftImage: "gen-name-txtfield", rightImage: "", txtfieldValue: data["firstName"] as! String, lblTip: "", userInteraction: true),
            
            myProfileModelClass(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: data["lastName"] as! String, placeholder: "Last Name", leftImage: "gen-name-txtfield", rightImage: "", txtfieldValue: data["lastName"] as! String, lblTip: "", userInteraction: true),
            
            myProfileModelClass(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.EmailAddress, validationType: txtFieldTypeEnum.email, value: data["email"] as! String, placeholder: "Email", leftImage: "gen-email-txtfield", rightImage: "", txtfieldValue: data["email"] as! String, lblTip: "e.g example@example.com", userInteraction: false),
            
            myProfileModelClass(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: "", placeholder: "Password", leftImage: "gen-password-txtfield", rightImage: "", txtfieldValue: "", lblTip: "Tip: Choose a password with at least 8 characters (1 Uppercase & 1 Number)", userInteraction: true),
            
            myProfileModelClass(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: "", placeholder: "Confirm Password", leftImage: "gen-password-txtfield", rightImage: "", txtfieldValue: "", lblTip: "", userInteraction: true),
            
            myProfileModelClass(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.PhonePad, validationType: txtFieldTypeEnum.phone, value: data["workNumber"] as! String, placeholder: "Work Number", leftImage: "gen-work-txtfield", rightImage: "", txtfieldValue: data["workNumber"] as! String, lblTip: "e.g. +97123456789 OR 0097123456789", userInteraction: true),
            
            myProfileModelClass(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.PhonePad, validationType: txtFieldTypeEnum.phone, value: data["mobileNumber"] as! String, placeholder: "Mobile Number", leftImage: "gen-mobile-txtfield", rightImage: "", txtfieldValue:data["mobileNumber"] as! String, lblTip: "e.g. +97123456789 OR 0097123456789", userInteraction: true),
            
            myProfileModelClass(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: data["hospitalName"] as! String, placeholder: "Type Hospital Name", leftImage: "gen-hospital-txtfield", rightImage: "", txtfieldValue: data["hospitalName"] as! String, lblTip: "", userInteraction: false),
            
            myProfileModelClass(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: "", placeholder: "Select Job Title", leftImage: "gen-lab-txtfield", rightImage: "", txtfieldValue: getJobTitle(data["userType"] as! Int), lblTip: "", userInteraction: false),
            
            myProfileModelClass(cellType : registerPropertyFieldTypeEnum.kTxtField, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value:temp , placeholder: "Secondary Email", leftImage: "gen-email-txtfield", rightImage: "", txtfieldValue: temp , lblTip: "", userInteraction: false),
            
            myProfileModelClass(cellType : registerPropertyFieldTypeEnum.kButton, keyboardType: UIKeyboardType.Default, validationType: txtFieldTypeEnum.name, value: "", placeholder: "Update", leftImage: "", rightImage: "", txtfieldValue: "", lblTip: "", userInteraction: true)
            
            
        ]
        
        
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view .endEditing(true)
    }
    
    func viewQR(){
        print("QR")
        
        
        imageViewQR.hidden = false
        
        
        
        
    }
    
    func handleTap() {
        // handling code
        imageViewQR.hidden =  true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
    }
    //MARK: UITableViewDelegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if GlobalStaticMethods.isPadPro() {
            if indexPath.row > 1 && indexPath.row < 7 {
                if indexPath.row == 4 {
                    return 90
                }
                return   110 //DeviceUtil.size.width * 0.18841
                
            }else if indexPath.row == 9{
                
                return    110  //DeviceUtil.size.width * 0.18841
                
            }else if indexPath.row == 10 {
                
                return  160// DeviceUtil.size.width * 0.24155 // 100
                
            }
            
            
            
            return 80 //DeviceUtil.size.width * 0.13286
            
            
            
            
            
        }
            
        else if GlobalStaticMethods.isPad(){
            
            
            if indexPath.row > 1 && indexPath.row < 7 {
                if indexPath.row == 4 {
                    return 70
                }
                return   90 //DeviceUtil.size.width * 0.18841
                
            }else if indexPath.row == 9{
                
                return    90  //DeviceUtil.size.width * 0.18841
                
            }else if indexPath.row == 10 {
                
                return   120 //DeviceUtil.size.width * 0.24155 // 100
                
            }
            
            
            
            return 80 //DeviceUtil.size.width * 0.13286
            
            
            
            
        }else {
            
            if indexPath.row > 1 && indexPath.row < 7 {
                
                if indexPath.row == 4  || indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 4 || indexPath.row == 7 || indexPath.row == 8 {
                    return 50
                }
                return   78 //DeviceUtil.size.width * 0.18841
                
            }else if indexPath.row == 9{
                
                return    78  //DeviceUtil.size.width * 0.18841
                
            }else if indexPath.row == 10 {
                
                return   DeviceUtil.size.width * 0.24155 // 100
                
            }
            
            
            
            return 55 //DeviceUtil.size.width * 0.13286
            
        }
        
    

    }


    //MARK: UITableViewDataSource

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProfileData.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> ProfileSettingsTableViewCell {
        let count = arrProfileData.count
        
        
        let row = indexPath.row
        
        let cellData = arrProfileData[row]
        
        if indexPath.row < count-1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! ProfileSettingsTableViewCell
            
            cell.selectionStyle = .None
            //cell.contentView.backgroundColor = UIColor.clearColor()
            //--ww    cell.configureCellProfile(cell, forRowAtIndexPath: indexPath, data: dataDict)
            //--ww  cell.txtFieldProfile.delegate = self;
            cell.txtFieldProfile.tag = indexPath.row
            
            self.arrayTextField.append(cell.txtFieldProfile)
            
            
            cell.configureProfileCell(cell, forRowAtIndexPath: indexPath, Data: cellData)
            cell.delegate = self
            return cell
            
        }
        else{
            let cell = tableView.dequeueReusableCellWithIdentifier("buttonCell", forIndexPath: indexPath) as! ProfileSettingsTableViewCell
            cell.selectionStyle = .None
            
            cell.contentView.backgroundColor = UIColor.clearColor()
            //--ww cell.configureCellProfile(cell, forRowAtIndexPath: indexPath)
            //--ww cell.txtFieldProfile.delegate = self;
            //--ww  cell.txtFieldProfile.tag = indexPath.row
            cell.delegate = self
            return cell
        }
        
    }
    
    
    
    
    func btnPressedAction(buttonClass: ProfileSettingsTableViewCell) {
        
        
        validateAndSubmit()
        
        
    }
    
    
    
    
    // MARK: - Orientation
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraintFromHeader?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    
    
    /*
     
     func showPicker() -> Void{
     
     ActionSheetStringPicker.showPickerWithTitle("Choose Category", rows: ["Lab Manager", "Doctor", "Surgeon"], initialSelection: 1, doneBlock: {
     picker, value, index in
     
     print("value = \(value)")
     print("index = \(index)")
     print("picker = \(picker)")
     let index = 8
     let path = NSIndexPath(forRow: index, inSection: 0)
     
     self.tblViewProfile .scrollToRowAtIndexPath(path, atScrollPosition: .Middle, animated: true)
     
     return
     }, cancelBlock: { ActionStringCancelBlock in
     
     let index = 8
     let path = NSIndexPath(forRow: index, inSection: 0)
     
     self.tblViewProfile .scrollToRowAtIndexPath(path, atScrollPosition: .Middle, animated: true)
     
     return }, origin: self.view)
     
     
     }
     
     
     func textFieldDidBeginEditing(textField: UITextField) {
     print("TextField did begin editing method called")
     
     if textField.tag == 8{
     
     self.view .endEditing(true)
     textField.resignFirstResponder()
     showPicker()
     
     }
     
     
     
     let pointTable = textField.superview!.convertPoint(textField.frame.origin, toView: self.tblViewProfile)
     
     var contentOffset = self.tblViewProfile.contentOffset
     
     contentOffset.y = pointTable.y - (textField.frame.size.height)
     
     self.tblViewProfile .setContentOffset(contentOffset, animated: true)
     }
     func textFieldDidEndEditing(textField: UITextField) {
     print("TextField did end editing method called")
     
     
     let cell = textField.superview?.superview as! UITableViewCell
     let indexPath = self.tblViewProfile .indexPathForCell(cell)
     
     
     self.tblViewProfile .scrollToRowAtIndexPath(indexPath!, atScrollPosition: .Middle, animated: true)
     
     }
     func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
     print("TextField should begin editing method called")
     
     
     return true;
     }
     func textFieldShouldClear(textField: UITextField) -> Bool {
     print("TextField should clear method called")
     return true;
     }
     func textFieldShouldEndEditing(textField: UITextField) -> Bool {
     print("TextField should snd editing method called")
     return true;
     }
     func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
     print("While entering the characters this method gets called")
     return true;
     }
     func textFieldShouldReturn(textField: UITextField) -> Bool {
     print("TextField should return method called")
     
     //        
     //        let nextTag = textField.tag + 1
     //        
     //        
     //        if nextTag < 10 {
     //            
     //            let nextResponder = self.tblViewProfile .viewWithTag(nextTag) as! UITextField
     //            
     //            if nextTag == 8{
     //                
     //                self.view .endEditing(true)
     //                nextResponder.resignFirstResponder()
     //            }
     //            
     //            nextResponder.becomeFirstResponder()
     //            
     //            
     //        }else{
     //            
     //            
     //            
     //            textField.resignFirstResponder();
     //        }
     return true;
     }
     
     
     
     func textFieldShouldReturn(textField: UITextField) -> Bool {
     
     if textField.tag == 6 {
     
     textField.resignFirstResponder()
     
     
     }
     else{
     if textField.tag == 9  {
     textField.resignFirstResponder()
     }
     else{
     IQKeyboardManager.sharedManager().goNext()
     }
     }
     
     
     return true
     }
     
     
     func textFieldDidEndEditing(textField: UITextField) {
     print("TextField did end editing method called")
     
     
     let cell = textField.superview?.superview as! UITableViewCell
     let indexPath = self.tblViewProfile .indexPathForCell(cell)
     
     
     self.tblViewProfile .scrollToRowAtIndexPath(indexPath!, atScrollPosition: .Middle, animated: true)
     
     }
     
     */
    
    
    func getJobTitle(uType : Int) -> String
    {
        switch uType {
        case 1:
            return "Lab Manager"
        case 2:
            return "Lab Supervisor"
        case 3:
            return "Lab Technician"
        default:
            return ""
        }
        
    }
    
    
    
    enum managerEnum : Int {
        
        case  labManager = 1
        case labSupervisor = 2
        case  labTechnician = 3
    }
    
    enum cellList : Int {
        case firstName = 0
        case lastName
        case email
        case password
        case confirmPassword
        case workNumber
        case mobileNumber
        case hospital
        case labManager
        case secondaryEmail
        
    }
    
    
    func getTxtField(indexP : Int) -> UITextField {
        
        
        let indexPath = NSIndexPath(forRow: indexP, inSection: 0)
        let cell = self.tblViewProfile.cellForRowAtIndexPath(indexPath) as! ProfileSettingsTableViewCell
        
        return cell.txtFieldProfile
    }
    
    
    func validateAndSubmit(){
        
        let txtFirstName = arrProfileData[cellList.firstName.rawValue].txtfieldValue
        let txtLastName  = arrProfileData[cellList.lastName.rawValue].txtfieldValue
        let txtEmail     = arrProfileData[cellList.email.rawValue].txtfieldValue
        let txtPassword  = arrProfileData[cellList.password.rawValue].txtfieldValue
        let txtConfirmPassword = arrProfileData[cellList.confirmPassword.rawValue].txtfieldValue
        let txtWorkNumber  = arrProfileData[cellList.workNumber.rawValue].txtfieldValue
        let txtMobileNumber = arrProfileData[cellList.mobileNumber.rawValue].txtfieldValue
        
        
        
        
        
        var strMessage = ""
        
        var status = true
        
        var indexnNo : Int!
        
        
        if stringsClass.isEmptyString(txtFirstName) == ""{
            
            indexnNo = cellList.firstName.rawValue
            strMessage = "First Name can not be empty"
            status = false
            
        }else if GlobalStaticMethods.isLettersANDSpacesOnly(txtFirstName) == false {
            indexnNo = cellList.firstName.rawValue
            strMessage = "Letters only."
            status = false
            
            
        }else if stringsClass.isEmptyString(txtLastName) == ""{
            indexnNo = cellList.lastName.rawValue
            strMessage = "Last Name can not be empty"
            status =  false
            
            
        }else if GlobalStaticMethods.isLettersANDSpacesOnly(txtLastName) == false {
            
            indexnNo = cellList.lastName.rawValue
            strMessage = "Letters only."
            
            status =  false
            
            
        }else if stringsClass.isEmptyString(txtEmail) == ""{
            
            
            strMessage = "Email can not be empty"
            
            status =  false
            
            
        }else if GlobalStaticMethods.isValidEmail(txtEmail) == false {
            
            strMessage = "Invalid Email"
            status =  false
            
            
        }else if stringsClass.isEmptyString(txtPassword) != ""{
            
            
            //strMessage = "Password can not be empty"
            
            if txtPassword.characters.count < 8{
                
                
                strMessage = "Choose a password with at least 8 characters (1 number & 1 Uppercase)"
                status =  false
                
                
                
            }else if GlobalStaticMethods.isValidPassword(txtPassword) != true{
                
                strMessage = "Choose a password with at least 8 characters (1 number & 1 Uppercase)"
                status =  false
                
            }else if stringsClass.isEmptyString(txtConfirmPassword) == ""{
                
                
                strMessage = "Confirm your password"
                status =  false
                
                
                
                
            } else if txtPassword != txtConfirmPassword{
                
                
                strMessage = "Passwords does not match"
                status =  false
                
                
                
                
                
                
            }
            
            
        }else if stringsClass.isEmptyString(txtWorkNumber)  == ""{
            
            
            strMessage = "Enter work number"
            
            status =  false
            
            
            
            
        }else if GlobalStaticMethods.isPhoneNumber(txtWorkNumber) == false {
            
            strMessage = "Enter valid work number"
            
            status =  false
            
            
            
        }else if stringsClass.isEmptyString(txtMobileNumber) == ""{
            
            
            strMessage = "Enter mobile number"
            status =  false
            
            
            
        }else if GlobalStaticMethods.isPhoneNumber(txtMobileNumber) == false {
            
            strMessage = "Enter valid mobile number"
            status =  false
            
        }
        
        
        
        
        if status == false {
            
            Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: strMessage, btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                
                
                
            })
            
            
        }else{
            
            
            let params = [ "sessionId" : UserModel.sharedInstance.sessionID,
                           "firstName" : txtFirstName,
                           "lastName" : txtLastName,
                           "workNumber" : txtWorkNumber,
                           "mobileNumber" : txtMobileNumber,
                           "password" : txtPassword
                
            ]
            
        debugPrint("Parameters is : \(params)")
            
            
            AFWrapper.serviceCall(AFUrlRequestWaqas.UpdateProfile(param: params), success: { (res) in
                printLog(res)
                
                if res["status"].intValue == 1
                {
                    
                    self.SetData(res)
                    
                    Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: "Profile has been updated successfully!", btnActionTitle: "OK", viewController: self, completionAction: { (res) in
                        self.navigationController?.popViewControllerAnimated(true)
                    })
                    
                    
                    
                    
                }
                
                }, failure: { (NSError) in
                    printLog(NSError)
            })
            
            
        }
        
    }
    
    
    
    func SetData(jsonObject : JSON)
    {
        let result = jsonObject["data"].dictionary!
        
        let user = UserModel.sharedInstance

        user.userName = result["firstName"]!.stringValue.stringByAppendingFormat(" %@" ,result["lastName"]!.stringValue)
       
        NSNotificationCenter.defaultCenter().postNotificationName("reloadLeftMenu", object: nil)
        
        
        
        print(result["qrImage"]!.stringValue)
    }
    
}
