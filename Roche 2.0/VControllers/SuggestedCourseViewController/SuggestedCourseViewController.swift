//
//  SuggestedCourseViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 8/22/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import SwiftyJSON


class SuggestedCourseViewController: HeaderViewController, suggestedTableViewCellDelegate,AFWrapperDelegate{

    @IBOutlet weak var tableview : UITableView!
    @IBOutlet weak var viewCalender : UIView!
    @IBOutlet weak var viewResult : UIView!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    
    var dataDict = [String:JSON]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        _headerView.heading = "Suggested Courses"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 140
        self.serviceCall()
    
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func serviceCall()  {
       
        
        let user = UserModel.sharedInstance
        AFWrapper.delegate = self
        AFWrapper.serviceCall(AFUrlRequestNabeel.GetSuggestedCourse(sessionId: user.sessionID), success: { (res) in
            
            if (res.dictionary!["statusMessage"]?.string)! == "success"
            {
                
                self.dataDict = (res.dictionary!["data"]?.dictionary)!
                print("dataDict" , self.dataDict)
                self.tableview .reloadData()
                
            }else{
                
                
                
                
            }
            
        }) { (NSError) in
            printLog(NSError)
            
        }
        
    }
    func didTapOnOKayOnAFWrapperAlert(errorCode : Int) -> Void {
        
        printLog("Do your awesome work here....")
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        if self.dataDict["items"] != nil {
            
            self.tableview.hidden = false
            
            return (self.dataDict["items"]?.count)!
        }else{
            self.tableview.hidden = true
            return 0
        }
        
    }
    
    
   
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> SuggestedCourseTableViewCell {
        [self.tableview.separatorColor = UIColor .clearColor()]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! SuggestedCourseTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        if self.dataDict["items"]![indexPath.row]["courseType"].int! == 1{
            cell.lblTitle?.text = Constants.ONLINE_COURSE
            cell.imgCourse?.image = UIImage (named: Constants.ONLINE_COURSE_IMAGE)
            
        }
        
            
        else{
            cell.lblTitle?.text = Constants.INCLASS_COURSE
            cell.imgCourse?.image = UIImage (named: Constants.INCLASS_COURSE_IMAGE)
            
        }
        
        cell.lblHeading?.text = self.dataDict["items"]![indexPath.row]["title"].stringValue
        let fontSize:Int = Int((cell.lblDescription!.font?.pointSize)!)
        
        let attributedString = stringsClass.getAttributedStringForHTMLWithFont((self.dataDict["items"]![indexPath.row]["shortDescription"].stringValue ), textSize: fontSize  , fontName: "Tahoma")
        
        cell.lblDescription?.attributedText = attributedString

        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor.whiteColor()
            
            
        } else{
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
        
        
        if GlobalStaticMethods.isPad() {
            if  GlobalStaticMethods.isPadPro() {
                cell.btnHeightConstraint?.constant = 51
                
            }else{
                cell.btnHeightConstraint?.constant = 40
            }
        }
        
        cell.tag = indexPath.row
        cell.delegate = self
        return cell
        
        
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        let cellSize: CGFloat
        
        let deviceHeight = CGRectGetHeight(self.view.frame);
        
        
        if  constants.deviceType.IS_IPHONE {
            
            
            return UITableViewAutomaticDimension

        }
            
        else if GlobalStaticMethods.isPad() {
            if GlobalStaticMethods.isPadPro() {
                print("ipad pro changed orientation")
                
                
                
                          }
            
        }
        cellSize = CGFloat (deviceHeight * 0.35)
        return UITableViewAutomaticDimension
        
    }
    
    func suggestedBtnTappedDelegate(detailBTN: SuggestedCourseTableViewCell) {
       // navigation.goToViewController(constants.viewControllers.courseDetailVC, sideDrawer: self.mm_drawerController)
        
        let courseID = self.dataDict["items"]![detailBTN.tag]["id"].stringValue
        let courseType = self.dataDict["items"]![detailBTN.tag]["courseType"].intValue
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseDetailVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.courseDetailVC) as! courseDetailViewController
        vc.strCourseID = courseID
        vc.courseType = courseType
        print(detailBTN.btnDetail!.tag)
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        

        
        
    }
   
    
    
    
    
    //    func tableView(tableView: UITableView,
    //                   heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //
    //        return 158 //Whatever fits your need for that cell
    //    }
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
