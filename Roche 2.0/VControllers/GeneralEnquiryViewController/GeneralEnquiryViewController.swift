//
//  GeneralEnquiryViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/10/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit
import IQKeyboardManager

// protocol delegate
protocol GeneralEnquiryDelegate {
    func viewEditingBegan()
    //  optional func SwiftPagesVisiblePageNumber(currentIndex: Int) -> Void
}

class GeneralEnquiryViewController: UIViewController,UITextViewDelegate ,UIGestureRecognizerDelegate{
    @IBOutlet weak var txtView : UITextView!
    @IBOutlet weak var btnSend : UIButton!

    @IBOutlet weak var txtViewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var sendBtnHeightConstraint: NSLayoutConstraint!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    @IBOutlet weak var tapView: UIView!
    
     var delegate : GeneralEnquiryDelegate?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtView.textContainerInset =
            UIEdgeInsetsMake(8,8,8,8);
        // Do any additional setup after loading the view.
        txtView.delegate = self
        
      //  _headerView.heading = "Enquiry"
      //  _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        let tap = UITapGestureRecognizer(target: self, action: Selector("handleTap:"))
        // we use our delegate on ourself
        tap.delegate = self
        // allow for user interaction
        tapView.userInteractionEnabled = true
        // add tap as a gestureRecognizer to tapView
        tapView.addGestureRecognizer(tap)
       /*
        let maskPAth1 = UIBezierPath(roundedRect: self.txtView.bounds,
                                     byRoundingCorners: [.TopLeft , .BottomLeft, .TopRight, .BottomRight],
                                     cornerRadii:CGSizeMake(8.0, 8.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.txtView.bounds
        maskLayer1.path = maskPAth1.CGPath
        self.txtView.layer.mask = maskLayer1
 */
        
        
        txtView.text = "Type your enquiry here…"
        
        if #available(iOS 9.0, *) {
            topConstraint?.constant = (DeviceUtil.size.width * 0.10) + GlobalStaticMethods.getHeaderFooterHeight()
        }else{
        
        topConstraint?.constant = DeviceUtil.size.width * 0.10
        }
        
    }
    
 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        self.view.endEditing(true)
        
    }
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        self.txtView.endEditing(true)
        
    }
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "Type your enquiry here…" {
            textView.text = ""
            textView.textColor = UIColor(hexString:"113C8B")
        }
        
        
        IQKeyboardManager.sharedManager().enable = false
        
        delegate?.viewEditingBegan()
        
    }
    func textViewDidEndEditing(textView: UITextView) {
        
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldShowTextFieldPlaceholder = false
        IQKeyboardManager.sharedManager().shouldHidePreviousNext = true
        IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = ""
        IQKeyboardManager.sharedManager().toolbarTintColor = UIColor.clearColor()
        IQKeyboardManager.sharedManager().enableAutoToolbar = false

        if textView.text.isEmpty {
            textView.text = "Type your message here..."
            textView.textColor = UIColor(hexString:"b8c1d1")
        }
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
      // topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        txtViewHeightConstraints.constant = 300
                        sendBtnHeightConstraint.constant = 48
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    @IBAction func sendBtnClicked(sender: UIButton) {
        
        self.view.endEditing(true)
        
        if self.txtView.text == "" || self.txtView.text == "Type your message here..."{
            

            Alert.showAlertMsgWithTitle("Message", msg: "Please enter a comment", btnActionTitle: "Ok", viewController: self, completionAction: { (success) in

            })
            
        }
        else{
            
            let user = UserModel.sharedInstance

            AFWrapper.serviceCall(AFUrlRequestNabeel.GeneralInquiry(comment: self.txtView.text, sessionId: user.sessionID), success: { (res) in
                
                printLog(res)

                if res["statusCode"] == 1000{
                    self.txtView.text = "Type your message here..."
                    self.txtView.textColor = UIColor(hexString:"b8c1d1")

                    Alert.showAlertMsgWithTitle("Success", msg: "Successfully Submitted", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                        
                        
                    })
                    
                    
                }
                
                
                
                }, failure: { (NSError) in
                    printLog(NSError)
                    
                    
                    

            })
            
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
