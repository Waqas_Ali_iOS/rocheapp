//
//  PostNewMessageViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/3/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit

class PostNewMessageViewController: HeaderViewController,UIGestureRecognizerDelegate,UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {

     let imagePicker = UIImagePickerController()
    @IBOutlet weak var sendBtnheightConstraints: NSLayoutConstraint!
    @IBOutlet weak var TxtFieldView : UIView!
    @IBOutlet weak var txtViewMsg : UITextView!
    @IBOutlet weak var subjectView : UIView!

    @IBOutlet var txtSubject: BaseUITextField!
    
    @IBOutlet var btnBrowse: buttonFontCustomClass!
    @IBOutlet var btnSend : UIButton?
    
    @IBInspectable var borderColor : UIColor?
  

    @IBOutlet var topConstraint : NSLayoutConstraint?
    @IBOutlet var browseBtnHeightConstraint : NSLayoutConstraint?

    @IBOutlet weak var tapView: UIView!
    
    var imageData : NSData!
    var imageName : String!

    @IBOutlet weak var viewAssignment: UIView!
    
    @IBOutlet weak var btnAssignment: BaseUIButton!
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        
        _headerView.heading = "Post New Message"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        viewAssignment.hidden = true
        txtViewMsg.delegate = self
        
        self.btnBrowse .addTarget(self, action: #selector(PostNewMessageViewController.selectAttachment), forControlEvents: .TouchUpInside)
     self.btnSend! .addTarget(self, action: #selector(PostNewMessageViewController.doPostMsg), forControlEvents: .TouchUpInside)

      
        self.subjectView.layer.borderWidth = 1
        self.subjectView.layer.borderColor =   UIColor (red: 217.0/255.0, green:221.0/255.0, blue: 227/255.0, alpha: 1.0).CGColor
        
        
        
        _headerView.heading = "Post a New Thread"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        let tap = UITapGestureRecognizer(target: self, action: #selector(PostNewMessageViewController.handleTap))
        // we use our delegate on ourself
        tap.delegate = self
        // allow for user interaction
       //--ww  tapView.userInteractionEnabled = true
        // add tap as a gestureRecognizer to tapView
        self.view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        
        imagePicker.delegate = self
        
        imageData  = nil
        imageName = ""
        
        if GlobalStaticMethods.isPad() {
            if  GlobalStaticMethods.isPadPro() {
                browseBtnHeightConstraint?.constant = 51
                
            }else{
                browseBtnHeightConstraint?.constant = 40
            }
        }
        
        
        
        
    }
    func handleTap() {
        self.view.endEditing(true)
        
    }
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "Type your message here..." {
            textView.text = ""
            textView.textColor = UIColor(hexString:"113C8B")
        }
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type your message here..."
            textView.textColor = UIColor(hexString:"b8c1d1")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    
    func browse(){
        print("Browse")
    }
    func sendMessage(){

        let alertController  =  UIAlertController(title: "Success", message: "Message sent", preferredStyle: .Alert)
        
        
        let action = UIAlertAction(title: "OK", style: .Default) { (success) in
                self.navigationController?.popViewControllerAnimated(true)
        }
        
        alertController.addAction(action)
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
        
    }

    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            
             topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 20
            
            self.txtViewMsg.textContainerInset =
                UIEdgeInsetsMake(8,3,8,4) // TOP,LEFT,BOTTOM,RIGHT
            print("iphone changed orientation")
        }
        else{
          if GlobalStaticMethods.isPadPro() {
                
                    print("ipad pro changed orientation")
                    
                    topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 80

                    
                    if orientation.isLandscape {
                        sendBtnheightConstraints.constant = 60

                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else if GlobalStaticMethods.isPad(){
                    if orientation.isLandscape {
                        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight() + 70
                        sendBtnheightConstraints.constant = 43
                        self.txtViewMsg.textContainerInset =
                            UIEdgeInsetsMake(8,8,8,8)
                        
                      //  sendBtnheightConstraints.constant = 45

                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            
        }
        
    }
    


    
    func selectAttachment() {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        //    imagePicker.modalPresentationStyle = .CurrentContext
        //      presentViewController(imagePicker, animated: true, completion: nil)
        
        imagePicker.modalPresentationStyle = .Popover
        let presentationController = imagePicker.popoverPresentationController
        
        // You must set a sourceView or barButtonItem so that it can
        // draw a "bubble" with an arrow pointing at the right thing.
       
        
        
        presentationController?.sourceView =  btnBrowse
        
        self.presentViewController(imagePicker, animated: true) {}
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        imageData = nil
        imageName = ""
       dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        var newImage: UIImage
        
        if let possibleImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            newImage = possibleImage
        } else if let possibleImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            newImage = possibleImage
        } else {
            return
        }
        
        let imageUrl          = info[UIImagePickerControllerReferenceURL] as! NSURL
        imageName         = imageUrl.lastPathComponent
        //--ww   let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first as String!
        //--wwlet photoURL          = NSURL(fileURLWithPath: documentDirectory)
        //--ww let localPath         = photoURL.URLByAppendingPathComponent(imageName!)
        //--ww   let image             = info[UIImagePickerControllerOriginalImage]as! UIImage
        imageData      = UIImageJPEGRepresentation(newImage, 0.6)
        
        // do something interesting here!
        print(newImage.size)
        printLog(imageName)
        imageName = "Attachment"
        //--ww  printLog(data)
        viewAssignment.hidden = false
        btnAssignment .setTitle("Attachment", forState: .Normal)
        
        btnAssignment.userInteractionEnabled = false
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
      func doPostMsg() {
        
        
        
        if stringsClass.isEmptyString(txtViewMsg.text!) == "" || txtViewMsg.text! == "Type your message here..." {
            
            Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: "Message can not be blank!", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                
            })
            
        }else{
            
            var msgSub = txtSubject.text!
            if stringsClass.isEmptyString(msgSub) == ""{
            msgSub = "No Subject!"
            }
            
         
            var parameters = [String:AnyObject]()
            parameters = ["sessionId":UserModel.sharedInstance.sessionID,
                          "subject": msgSub,
                          "comment":txtViewMsg.text!,
                          "attachTitle":imageName,
                          "attachType":"jpg"
            ]
            printLog(parameters)
            
            
            
            
            AFWrapper.UploadImageWithParamteres(parameters, imgData: imageData, URL: "postNewMessage", success: { (res) in
                printLog(res)
                
                Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: "Message has been sent successfully!", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                    self.navigationController?.popViewControllerAnimated(true)
                })
                
               
                }, failure: { (NSError) in
                    printLog(NSError)
            })
            
            
        }
        
    }


}
