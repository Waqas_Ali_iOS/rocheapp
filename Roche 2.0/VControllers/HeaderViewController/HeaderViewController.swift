
//
//  HeaderViewController.swift
//  Skeleton
//
//  Created by Waqas Ali on 12/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//


import UIKit

public class HeaderViewController: UIViewController, HeaderViewDelegate {
    
    var _headerView:HeaderView!
    var _footerView:HeaderView!
    
    internal var viewsDictionary = Dictionary<String,AnyObject>()
    internal var constraints = [NSLayoutConstraint]()
    var headerViewHeight : CGFloat = 56
    
    override public func viewDidLoad() {
        super.viewDidLoad()

      //  self.loadWithNib("HeaderView", viewIndex: 0, owner: self)
        
        headerViewHeight = GlobalStaticMethods.getHeaderFooterHeight()
        
        self._headerView =  HeaderView(frame: CGRectMake(0, 0, self.view.frame.size.width, headerViewHeight))
        self.view.addSubview(self._headerView!)
        
        
        
        self._footerView = HeaderView(frame: CGRectMake(0, (self.view.frame.size.height - headerViewHeight), self.view.frame.size.width, headerViewHeight))
        
        self.view.addSubview(self._footerView!)
        
        
        
     
        _headerView.delegate = self
        _footerView.delegate = self

        configureHeaderViewConstraints()
        
        self .updateHeaderWithHeadingText("", rightBtnImageName: "", leftBtnImageName:"", backgroundImageName: "", bgColor: "#113c8b")
        
        self .updateFooterWithHeadingText("", rightBtnImageName: "", leftBtnImageName:"", backgroundImageName: "", bgColor: "#113c8b")
        
      _footerView.hidden = true
        
        
//        self._footerView.btnFooter = UIButton.init(type: .Custom)
//        
//        self._footerView.btnFooter  .frame = CGRectMake(0, 0,  self._footerView.frame.size.height , self._footerView.frame.size.height)
//        
//        self._footerView.btnFooter.backgroundColor = UIColor.clearColor()
//        self._footerView.addSubview(self._footerView.btnFooter!)
        
        self._footerView.btnFooter  .addTarget(self, action: #selector(HeaderViewController.didFooterBtnPressed), forControlEvents: .TouchUpInside)
        
        
        self._footerView.btnFooter  .addTarget(self, action: #selector(HeaderViewController.heighlightFooter), forControlEvents: .TouchDown)
        
        
        self._footerView.btnFooter  .addTarget(self, action: #selector(HeaderViewController.unHeighlightFooter), forControlEvents: .TouchCancel)
        
        self._footerView.btnFooter  .addTarget(self, action: #selector(HeaderViewController.unHeighlightFooter), forControlEvents: .TouchDragOutside)

        
        

        
      
    }
    
    public override func viewDidAppear(animated: Bool) {
        
    
    }

    
  
    

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadWithNib(nibName:String, viewIndex:Int, owner: AnyObject) -> Void {
        let headerNib : UIView = (NSBundle.mainBundle().loadNibNamed(nibName, owner: owner, options: nil) as NSArray).objectAtIndex(viewIndex) as! UIView;
        
        
        headerNib.frame = CGRectMake( 0, 0, self.view.frame.width, 56)
        
       self.view.addSubview(headerNib)
        
       
    }
    
    
    
    
    public func updateHeaderWithHeadingText(hText: String, rightBtnImageName: String, leftBtnImageName: String, backgroundImageName: String, bgColor: String  ) -> Void {
        
        
        _headerView.updateHeaderWithHeadingText(hText, rightBtnImageName: rightBtnImageName, leftBtnImageName: leftBtnImageName, backgroundImageName:backgroundImageName, bgColor: bgColor);
       
        
    }
    
    
    
    public func updateFooterWithHeadingText(hText: String, rightBtnImageName: String, leftBtnImageName: String, backgroundImageName: String, bgColor: String  ) -> Void {
        
        
        _footerView.updateHeaderWithHeadingText(hText, rightBtnImageName: rightBtnImageName, leftBtnImageName: leftBtnImageName, backgroundImageName:backgroundImageName, bgColor: bgColor);
        
        
    }
    
    
    

    func headerViewLeftBtnDidClick(headerView: HeaderView) {
        self.view.endEditing(true)
        
        
   
        if headerView == self._headerView {
                 print("leftButtonClicked")
        if(self._headerView.leftButtonImage != constants.imagesNames.LEFT_MENU_BACK_ICON){
        
       
            mm_drawerController .toggleDrawerSide(.Left, animated: true) { (success) in
                print("Allhamdulillah")
                
            }

        }
        else{
            self.navigationController!.popViewControllerAnimated(true)
        }
        }else{
        
        print("leftButtonClicked footer")
        
        }
    }
    
    
    func headerViewRightBtnDidClick(headerView: HeaderView) {
        print("rightButtonClicked")
       // self._headerView.lblHeading.text = "waqas ali"
        
        self._headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
    }
    
    
    func didFooterBtnPressed (sender : AnyObject) {
        
     
        self._footerView.viewContainer.backgroundColor =  UIColor(hexString: self._footerView.bgColorVar!)
        
    }
    
    
    func heighlightFooter(){

        
self._footerView.viewContainer.backgroundColor =  UIColor(hexString: self._footerView.bgColorHeighlightedFooter!)

    
    }
    
    func unHeighlightFooter(){
    
     self._footerView.viewContainer.backgroundColor =  UIColor(hexString: self._footerView.bgColorVar!)
    }
    
    
    func configureHeaderViewConstraints(){
        
        
        viewsDictionary["headerView"] = _headerView
        
        _headerView.translatesAutoresizingMaskIntoConstraints = false
        _footerView.translatesAutoresizingMaskIntoConstraints = false
        
        let strVConstraints = "V:|-0-[headerView(\(headerViewHeight))]"
        
        
        let strHConstraints = "H:|-0-[headerView]-0-|"
        
        
        viewsDictionary["footerView"] = _footerView
        
        _headerView.translatesAutoresizingMaskIntoConstraints = false
        
        let strVConstraintsfooterView = "V:[footerView(\(headerViewHeight))]-0-|"
        
        
        let strHConstraintsfooterView = "H:|-0-[footerView]-0-|"
        
        
        setConstraintsForHeader(strVConstraints)
        setConstraintsForHeader(strHConstraints)
        setConstraintsForHeader(strVConstraintsfooterView)
        setConstraintsForHeader(strHConstraintsfooterView)

        
        NSLayoutConstraint.activateConstraints(constraints)
        
    }
    
    func setConstraintsForHeader(format : String){
        
        let newConstraint = NSLayoutConstraint.constraintsWithVisualFormat(format, options:[], metrics: nil, views: viewsDictionary)
        
        constraints += newConstraint
        
    }
    

    public override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func navigatePageAfterLocalNotificatio(userInfo : NSNotification?)
    {
       //--ww printLog(userInfo!.valueForKey("userInfo"))
        
        printLog("Dont show alert")
       //--ww  let notiDetail = userInfo.valueForKey("userInfo")!  //as! Dictionary<String, String>
        let notiDetail = Singleton.sharedInstance.localNotificationObj
        let cid = notiDetail["courseId"]! as! String
         let cTitle = notiDetail["courseTitle"]! as! String
        
        let temp = NSUserDefaults.standardUserDefaults()
    
        if temp.objectForKey("ShowPOPUP") as! String == "Yes"
        {
           
            
            goToCourseDetailPage(cid)
            
        }
        else
        {
            printLog("show alert")
            
            Alert.showAlertMsgWithTitle(Constants.NOTIFICATION, msg: "2 hours left : \(cTitle)", otherBtnTitle: "Check", otherBtnAction: { (Void) in
                self.goToCourseDetailPage(cid)
                }, cancelBtnTitle: "No", cancelBtnAction: { (Void) in
                    
                }, viewController: self)
            if let wd = self.view.window {
                var vc = wd.rootViewController
                if(vc is UINavigationController){
                    vc = (vc as! UINavigationController).visibleViewController
                }
                
                if(vc is courseDetailViewController){
                   
                }else{
               
                    
                  
                
                }
            }
        }
        
      
      
    }
    
    
    
    func goToCourseDetailPage(ciD : String, cType : Int? = 2) {
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseDetailVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.courseDetailVC) as! courseDetailViewController
        vc.strCourseID = ciD
        vc.courseType = cType!
        printLog(vc.courseType)
       leftMenu.pushOrPopObj(vc, sideDrawer:self.mm_drawerController)
        
        leftMenu.pushOrPopWithAnimation(vc, sideDrawer: self.mm_drawerController, isAnimating: true)
//                let alert = UIAlertController(title: "Token", message: "\(ciD)", preferredStyle: UIAlertControllerStyle.Alert)
//                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
//                self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func goToMessageDetail(tID: Int)  {
        let sb = navigation.getStoryBoardForController(constants.viewControllers.replyMsgVC)?.storyboardObject
        let desiredVC = sb!.instantiateViewControllerWithIdentifier(constants.viewControllers.replyMsgVC) as! ReplyMessageViewController
        desiredVC.tID = tID
        
      //  leftMenu.pushOrPopObj(desiredVC, sideDrawer:self.mm_drawerController)
        
        leftMenu.pushOrPopWithAnimation(desiredVC, sideDrawer: self.mm_drawerController, isAnimating: true)
    }
    
    func setControllerAsPerNotification(){
        
        let notiDict = Singleton.sharedInstance.pushNotificationObj
        
        
        // Based on notification type move to certain page
        switch Singleton.sharedInstance.PNSType {
        case .Message:
            printLog("Message")
            goToMessageDetail(notiDict["tID"] as! Int)
        case .CourseSuggested:
            printLog("CourseSuggested")
            let cdi = notiDict["cID"] as! Int
            let ctp = notiDict["cT"] as! Int
            goToCourseDetailPage("\(cdi)", cType : ctp)
        case .EnrollmentApproval:
            printLog("EnrollmentApproval")
            sheduleNotification(notiDict)
            let cdi = notiDict["cID"] as! Int
            goToCourseDetailPage("\(cdi)", cType: 2)
        case .TimeChanged:
            printLog("TimeChanged")
            sheduleNotification(notiDict)
            let cdi = notiDict["cID"] as! Int
            goToCourseDetailPage("\(cdi)", cType: 2)
        }
        
    }
    



  func navigatePageAfterPushNotificatio()
    {
        
        let userDefault = NSUserDefaults.standardUserDefaults()
        
        // Check user come from push notification,
        
        if (userDefault.objectForKey("isComeFromPushNotification") != nil) && (userDefault.objectForKey("isComeFromPushNotification") as! String) == "YES"
        {
            
            userDefault.setObject("NO", forKey: "isComeFromPushNotification")
            userDefault.synchronize()
            
            
            
            if userDefault.objectForKey("ShowPOPUP") as! String == "Yes"
            {
                
              setControllerAsPerNotification()
                printLog("dont show alert")
                
            }
            else
            {
                printLog("show alert")
                
                   let notiDict = Singleton.sharedInstance.pushNotificationObj
                Alert.showAlertMsgWithTitle(Constants.NOTIFICATION, msg:notiDict["msg"] as! String, otherBtnTitle: "Check", otherBtnAction: { (Void) in
                      self.setControllerAsPerNotification()
                    }, cancelBtnTitle: "No", cancelBtnAction: { (Void) in
                        
                    }, viewController: self)
                
            }
        }
}
    
    func sheduleNotification(dict : Dictionary<String, AnyObject>){
        //--ww  let actualTimestamp = Double(1478178000)
        
        
//        Alert.showAlertMsgWithTitle("check karo", msg: "\(dict)", btnActionTitle: "ok", viewController: self) { (Void) in
//            
//        }
        
        let timeStamp : Double = dict["timestamp"] as! Double
        
        let reminderTimestamp = Double(timeStamp - 120 * 60)
        
        let currentTimestamp = Int(NSDate().timeIntervalSince1970)
        let fromCurrentTimestamp = Double(currentTimestamp - 120 * 60)
        let date = NSDate(timeIntervalSince1970: reminderTimestamp)
        
        //Get Current Date/Time
        //--ww   let currentDateTime = NSDate()
        //Check if reminderTime is Less than  current time
        //  if(currentDateTime.isGreaterThanDate(date)) {
        
        if currentTimestamp <=  Int(reminderTimestamp)  {
            
            
            
            let dayTimePeriodFormatter = NSDateFormatter()
            dayTimePeriodFormatter.dateFormat = "MMM dd YYYY hh:mm aa"
            
            let dateString = dayTimePeriodFormatter.stringFromDate(date)
            
            print( " _ts value is \(reminderTimestamp)")
            print( " _ts value is \(dateString)")
            
            let cIDInt = dict["cID"] as! Int
            let cIDString = "\(cIDInt)"
            
            
            let notification = ABNScheduler.notificationWithIdentifier(cIDString)
            
            printLog(notification)
            if notification == nil {
                
                let cTitle = dict["title"] as! String
                let userInfo = ["userId": UserModel.sharedInstance.userID,
                                "courseId": cIDString,
                                "courseTitle" : cTitle
                ]
                
                let note = ABNotification(alertBody: dict["reminderMsg"] as! String, identifier: cIDString)
                //--ww note.alertAction = "some text"
                note.repeatInterval = .None
                note.userInfo = userInfo as Dictionary<NSObject, AnyObject>
                //--ww  NSDate().nextMinutes(1)
                
                let identifier = note.schedule(fireDate:date)
                
                printLog(identifier)
            }else if notification?.fireDate != date{
                
                notification?.reschedule(fireDate: date)
            }
            printLog(ABNScheduler.scheduledNotifications())
            
            
        }
    }
 
    
 }
