//
//  CourseInProgressViewController.swift
//  
//
//  Created by Nabeel Siddiqui on 8/22/16.
//
//

import UIKit
import SwiftyJSON

class CourseInProgressViewController: HeaderViewController, CourseInProgressTableViewCellDelegate, AFWrapperDelegate {
    
    @IBOutlet weak var tableview : UITableView!
   
    @IBOutlet var topConstraint : NSLayoutConstraint?
    //var dataDict = [JSON]()
    var  arrData = [JSON]()
    var courseId : Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 140
         _headerView.heading = "Courses in Progress"
         _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        self.serviceCall()
        
        // self.viewCalender .removeFromSuperview()
        //self.viewResult .removeFromSuperview()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        self.mm_drawerController.openDrawerGestureModeMask = .None
        
    }
    
    
    func parseResponce(res : JSON)
    {
        
        if res["data"] != nil
        {
            if res["data"]["items"] != nil
            {
                
                arrData = res["data"]["items"].arrayValue
                self.tableview.reloadData()
                
            }
        }
    }

    func serviceCall()  {
        
        let user = UserModel.sharedInstance
        AFWrapper.delegate = self
        AFWrapper.serviceCall(AFUrlRequestNabeel.GetCourseInProgress(sessionId: user.sessionID), success: { (res) in
            
            
             self.parseResponce(res)
            
            
//            if (res.dictionary!["statusCode"])! == 1000
//            {
//                
//                self.dataDict = (res.dictionary!["data"]?.dictionary)!
//                print("dataDict" , self.dataDict)
//                self.tableview .reloadData()
//                
//            }else{
//                
//                let msg = res.dictionary!["statusMessage"]?.stringValue
//                
//                Alert.showAlertMsgWithTitle("Message", msg: msg!, btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
//                    
//                })
//                
//                
//            }
            
        }) { (NSError) in
            printLog(NSError)
            
        }
        
    }

    func didTapOnOKayOnAFWrapperAlert(errorCode: Int) {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        
        if  arrData.count != 0 {
            
            self.tableview.hidden = false
            
            return ( arrData.count)
        }else{
            self.tableview.hidden = true
            return 0
        }

        
        
        
//        if self.dataDict["items"] != nil {
//            
//            self.tableview.hidden = false
//            
//            return (self.dataDict["items"]?.count)!
//        }else{
//            self.tableview.hidden = true
//            return 0
//        }
        
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> CourseInProgressTableViewCell {
        [self.tableview.separatorColor = UIColor .clearColor()]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! CourseInProgressTableViewCell
        
        
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.delegate = self
        cell.lblHeading?.text = arrData[indexPath.row]["title"].stringValue
        cell.lblDescription?.text = arrData[indexPath.row]["shortDescription"].stringValue
        cell.btnBookMark?.tag = arrData[indexPath.row]["id"].int!
        cell.btnDetail?.tag = arrData[indexPath.row]["id"].int!
        cell.btnSuggest?.tag = arrData[indexPath.row]["id"].int!
        
        if arrData[indexPath.row]["courseType"].int == 1{
            cell.lblTitle?.text = Constants.ONLINE_COURSE
            cell.imgCourse?.image = UIImage (named: Constants.ONLINE_COURSE_IMAGE)
            
        }
        else{
            cell.imgCourse?.image = UIImage (named: Constants.INCLASS_COURSE_IMAGE)
            cell.lblTitle?.text = Constants.INCLASS_COURSE
            
        }
        
        if arrData[indexPath.row]["bookmarked"].int! == 1
            
        {
            if GlobalStaticMethods.isPad(){
                cell.btnBookMark!.setBackgroundImage(UIImage(imageName :"Gen-bookmark-img-sel" ), forState: .Normal)
            }
            else{
                cell.btnBookMark!.setImage(UIImage(named: "Gen-bookmark-img-sel"), forState: UIControlState.Normal)
            }
            
        }else{
            if GlobalStaticMethods.isPad(){
                cell.btnBookMark!.setBackgroundImage(UIImage(imageName :"Gen-BookMark-Btn" ), forState: .Normal)
            }
            else{
                cell.btnBookMark!.setImage(UIImage(named: "gen-bookmark"), forState: UIControlState.Normal)
            }
            
        }
        
        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor.whiteColor()
            
            
        } else{
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
        
        if GlobalStaticMethods.isPad() {
            if  GlobalStaticMethods.isPadPro() {
                cell.btnHeightConstraint?.constant = 51
                
            }else{
                cell.btnHeightConstraint?.constant = 40
            }
        }
        
        print(cell.btnDetail!.tag)
        cell.tag = indexPath.row
        
        return cell
        
        

        
        
        
        
       /*
        cell.lblHeading?.text = self.dataDict["items"]![indexPath.row]["title"].stringValue
        let fontSize:Int = Int((cell.lblDescription!.font?.pointSize)!)
        
        let attributedString = stringsClass.getAttributedStringForHTMLWithFont((self.dataDict["items"]![indexPath.row]["shortDescription"].stringValue ), textSize: fontSize  , fontName: "Tahoma")
        
        cell.lblDescription?.attributedText = attributedString
        
        
        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor.whiteColor()
            
            
        } else{
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
        
        if GlobalStaticMethods.isPad() {
            if  GlobalStaticMethods.isPadPro() {
                cell.btnHeightConstraint?.constant = 51
                
            }else{
                cell.btnHeightConstraint?.constant = 40
            }
        }
        
        
        cell.delegate = self
        return cell
        */
        
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        let cellSize: CGFloat
        
        let deviceHeight = CGRectGetHeight(self.view.frame);
        
        
        if  constants.deviceType.IS_IPHONE {
            
        return UITableViewAutomaticDimension

        }
            
        else if GlobalStaticMethods.isPad() {
            if GlobalStaticMethods.isPadPro() {
                print("ipad pro changed orientation")
                
               
            }
            
        }
        cellSize = CGFloat (deviceHeight * 0.35)
        return UITableViewAutomaticDimension
        
    }
    
    
    
   
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }
    
    func goToSuggestBtnDelegate(cell: CourseInProgressTableViewCell) {
        
        courseId = cell.btnSuggest?.tag
        print(courseId ,"courseID ")
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseSuggestVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.courseSuggestVC) as! CourseSuggestionViewController
        vc.courseID = cell.btnDetail!.tag
    
        print(cell.btnDetail!.tag)
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
   
    
    func CourseInProgressBtnTappedDelegate(detailBTN: CourseInProgressTableViewCell) {
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseDetailVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.courseDetailVC) as! courseDetailViewController
        vc.strCourseID = String(detailBTN.btnDetail!.tag)
        

        vc.courseType = arrData[detailBTN.tag]["courseType"].int!
        
    
        print(detailBTN.btnDetail!.tag)
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        //  navigation.goToViewController(constants.viewControllers.courseDetailVC, sideDrawer: self.mm_drawerController)
        
    }
    
    func UnbookMarkBtnTappedDelegate(unBookBTN: CourseInProgressTableViewCell) {
        
        courseId = unBookBTN.btnBookMark?.tag
        
        var user = UserModel.sharedInstance
        
        AFWrapper.serviceCall(AFUrlRequestNabeel.BookMarkCourse(sessionId:user.sessionID , courseId: courseId!), success: { (result) in
            
            
            
            if (result.dictionary!["statusCode"])! == 1000            {
                
                
                
                
                if GlobalStaticMethods.isPad(){
                    
                    
                    if unBookBTN.btnBookMark!.currentBackgroundImage!.isEqual(UIImage(imageName :"Gen-bookmark-img-sel")) {
                        //do something here
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.UNBOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        
                        unBookBTN.btnBookMark?.setBackgroundImage(UIImage(imageName :"gen-bookmark" ), forState: .Normal)
                        
                        self.arrData[unBookBTN.tag]["bookmarked"] = 0
                        
                    }
                    else
                    {
                        
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.BOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        
                        unBookBTN.btnBookMark?.setBackgroundImage(UIImage(imageName :"Gen-bookmark-img-sel" ), forState: .Normal)
                        self.arrData[unBookBTN.tag]["bookmarked"] = 1
                        
                    }
                    
                    
                    
                    
                    //self.arrData["bookmark"].Int = 1
                }
                    
                else{
                    
                    
                    if unBookBTN.btnBookMark!.currentImage!.isEqual(UIImage(imageName :"Gen-bookmark-img-sel")) {
                        //do something here
                        
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.UNBOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        unBookBTN.btnBookMark?.setImage(UIImage(imageName :"gen-bookmark" ), forState: .Normal)
                        
                        self.arrData[unBookBTN.tag]["bookmarked"] = 0
                        
                    }
                    else{
                        
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.BOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        unBookBTN.btnBookMark?.setImage(UIImage(imageName :"Gen-bookmark-img-sel" ), forState: .Normal)
                        
                        self.arrData[unBookBTN.tag]["bookmarked"] = 1
                        
                    }
                }
                
                
                
            }
                
            else{
                
                Alert.showAlertMsgWithTitle("Message", msg: "Some Error", btnActionTitle: "ok", viewController: self, completionAction: { (Void) in
                    
                    
                    
                    
                })
                
            }
            
        }) { (NSError) in
            
            
            
        }
        
        
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
