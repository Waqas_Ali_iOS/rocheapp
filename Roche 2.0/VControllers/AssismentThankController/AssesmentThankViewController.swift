//
//  AssesmentThankViewController.swift
//  
//
//  Created by Nabeel Siddiqui on 8/24/16.
//
//

import UIKit
import SwiftyJSON

class AssesmentThankViewController: HeaderViewController,NSURLSessionDownloadDelegate,AFWrapperDelegate {
    
    @IBOutlet weak var lblPassFailTitle: BaseUILabel!
    @IBOutlet weak var imgFace: UIImageView!
    
    @IBOutlet weak var lblMessage: BaseUILabel!
    @IBOutlet weak var lblThankYouMessage: BaseUILabel!
    
    @IBOutlet weak var btnNextQuestion: buttonFontCustomClass?
    
    
    var methodType : AFUrlRequestSharjeel.ListServiceType = AFUrlRequestSharjeel.ListServiceType.Assesment
    
    
    var methodTypeVariable = 0
    
    var task : NSURLSessionTask!
    
    
    var filePath = ""
    
    var percentageWritten:Float = 0.0
    var taskTotalBytesWritten = 0
    var taskTotalBytesExpectedToWrite = 0
    
    
    lazy var session : NSURLSession = {
        let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        config.allowsCellularAccess = false
        let session = NSURLSession(configuration: config, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        return session
    }()
    
    
    @IBOutlet weak var mainView: UIView!
    var dataDic : Dictionary <String,AnyObject> = [:]
    
    var strQuestionID = ""
    var strAnswerID = ""
    
    var strUrl = ""
    
    var courseId : Int = 0

    override func viewDidLoad() {
        
        
        if GlobalStaticMethods.isPhone()
        {
                self.lblPassFailTitle.font = UIFont.init(name: "Imago-Medium", size: 0.04106280193 * self.view.frame.size.width)
        }
        
        
        super.viewDidLoad()
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        _headerView.heading = "Assessment"
        
        
        switch methodType {
        case AFUrlRequestSharjeel.ListServiceType.Assesment:
             _headerView.heading = "Assessment"
        case AFUrlRequestSharjeel.ListServiceType.preCourseSurvay:
            _headerView.heading = "Pre Attend Survey"
        default:
            _headerView.heading = "Post Attend Survey"
            
        }

        strUrl = "http://rocheapp-1.qa.tphp.net/app_dev.php/user/coursecertificate/online-24082016"
        mainView.hidden = true
        
        // Do any additional setup after loading the view.
        callService(methodTypeVariable)
        

        switch methodTypeVariable {
        case AFUrlRequestSharjeel.ListServiceType.Assesment.rawValue:
             _headerView.heading = "Assessment"
            
            break
        case AFUrlRequestSharjeel.ListServiceType.preCourseSurvay.rawValue:
            _headerView.heading = "Pre Attend Survey"
            break
        default:
            _headerView.heading = "Post Attend Survey"
            break
        }
        
        
        _footerView.lblHeading.iPhoneFontSize = 50
        _footerView.leftButtonImage = "footer-curriculum"
        _footerView.rightButtonImage = "rightWhiteArrow"
        _footerView.bgColorVar = "#3e8ada"
        _footerView.bgColorHeighlightedFooter = "#113c8b"
        _footerView.heading = "Browse My Curriculum"
        _footerView.hidden = false
        _footerView.btnFooter.hidden = false
        
   
      
    }

    override func viewDidAppear(animated: Bool) {
     //   self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Custom navigation
    
    override func headerViewLeftBtnDidClick(headerView: HeaderView)
    {
       self.goback()
    }
    
    
    func goback()
    {
        // pass here valuses.
        navigation.PopToVC(constants.viewControllers.courseDetailVC, sideDrawer: self.mm_drawerController)
    }
    
    func viewCertificate()
    {
      //  self.DownloadWithProgressBar()
        
      //  DownloadVideo()
        
         openUrl()
    }
    
    
    func  openUrl()
    {
      //  strUrl = "http://rocheapp-1.qa.tphp.net/user_certificates/Online Cou_c131278e28b4703f6466a4bdf0fa863e.pdf"
        
        let urlStr : NSString = strUrl.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!

        let url = NSURL(string: urlStr as String)!
        UIApplication.sharedApplication().openURL(url)
    }
    

    //MARK: -  Web service Call and Responce parser
    func callService(mType : AFUrlRequestSharjeel.ListServiceType.RawValue)
    {
        
        
       
        
        AFWrapper.delegate = self
        AFWrapper.serviceCall(AFUrlRequestSharjeel.GetAssessmentResult(courseId: courseId, strQID: strQuestionID, strAID: strAnswerID, methodType: mType), success: { (res) in
            
            
            self.parseResponce(res)
            
        }) { (NSError) in
            printLog(NSError)
        }
        
    }
    
    
    func parseResponce(res : JSON)
    {
        if res["data"] != nil
        {
            dataDic = res["data"].dictionaryObject!
            
            
            if methodTypeVariable == 1
            {
                if (dataDic["passed"] as! Int) == 1
                {
                    lblPassFailTitle.text = "CONGRATULATIONS"
                    let img =  UIImage.init(named: "Gen-Congratulation-img")
                    imgFace.image = img
                    btnNextQuestion?.hidden = false
                }
                else
                {
                    lblPassFailTitle.text = "SORRY"
                    let img =  UIImage (imageName: "Gen-sory-smile-img")
                    imgFace.image = img
                    btnNextQuestion?.hidden = true
                }
                
                strUrl = (dataDic["certificate"] as? String)!
                
                lblMessage.text = dataDic["passedText"] as? String
                
                
                mainView.hidden = false
                
                if strUrl != ""
                {
                    self.btnNextQuestion!.addTarget(self, action: #selector(self.viewCertificate), forControlEvents: .TouchUpInside)
                }
                
            }
            else
            {
                mainView.hidden = false
                lblPassFailTitle.text = "THANK YOU"
                lblMessage.text = dataDic["passedText"] as? String
               
                
                
                let img =  UIImage.init(named: "Gen-Congratulation-img")
                imgFace.image = img
                
                lblThankYouMessage.hidden = true
            
               // rightArrow
                
               let image : UIImage = UIImage.init(named: "Gen-RightArrow-img")!
                
                self.btnNextQuestion?.setImage(image, forState: .Normal)
                self.btnNextQuestion?.setImage(image, forState: .Highlighted)
                
                self.btnNextQuestion?.setTitle("Go back to course detail", forState: .Normal)
                self.btnNextQuestion?.setTitle("Go back to course detail", forState: .Highlighted)
                
                
                self.btnNextQuestion!.addTarget(self, action: #selector(self.goback), forControlEvents: .TouchUpInside)
            }
 
        }
    }
    
    
    
    func DownloadWithProgressBar() {
        if self.task != nil {
            
        }
        
        let s = strUrl
        let url = NSURL(string:s)!
        let req = NSMutableURLRequest(URL:url)
        let task = self.session.downloadTaskWithRequest(req)
        self.task = task
        task.resume()
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten writ: Int64, totalBytesExpectedToWrite exp: Int64)
    {
        print("downloaded \(100*writ/exp)")
        taskTotalBytesWritten = Int(writ)
        taskTotalBytesExpectedToWrite = Int(exp)
        percentageWritten = Float(taskTotalBytesWritten) / Float(taskTotalBytesExpectedToWrite)
        
        print(String(format: "%.01f", percentageWritten*100) + "%")
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
        // unused in this example
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        print("completed: error: \(error)")
    }
    
    // this is the only required NSURLSessionDownloadDelegate method
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL location: NSURL)
    {
        
        let documentsDirectory = self.getDocumentDirectory()
        
        let fileName = String.init(format: "PDF-%@", "\(courseId)")
        
        filePath = String.init(format: "%@/%@", documentsDirectory,fileName)
        
        let fileManager = NSFileManager.defaultManager()
        if !fileManager.fileExistsAtPath(filePath) {
            
            let data = NSData.init(contentsOfURL: location)!
            data.writeToFile(filePath, atomically: true)
            
//            self.saveFileNameInPlist(fileName!)
            }
    }
    
    
    func DownloadVideo()
    {
        //download the file in a seperate thread.
        
        let documentsDirectory = self.getDocumentDirectory()
        
        let fileName = String.init(format: "PDF-%@", "\(courseId)")
        
        filePath = String.init(format: "%@/%@", documentsDirectory,fileName)
        
        let fileManager = NSFileManager.defaultManager()
        if !fileManager.fileExistsAtPath(filePath) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                
                print("Download Start")
                
                let url = NSURL.init(string:self.strUrl)
                let urlData = NSData.init(contentsOfURL: url!)
                
                
                
                
                if (( urlData ) != nil)
                {
                    //saving is done on main thread
                    dispatch_async(dispatch_get_main_queue(), {
                        urlData?.writeToFile(self.filePath, atomically: true)
                        
                       self.loadData()
                        print("Downloaded")
                    })
                }
            }
        } else {
            print("FILE AVAILABLE")
        }
    }
    
    
    
    func loadData() {
        
        let url = NSURL.init(string: filePath)
        
        let req = NSURLRequest(URL: url!)
        
        let webView = UIWebView(frame: CGRectMake(20,20,self.view.frame.size.width-40,self.view.frame.size.height-40))
        webView.loadRequest(req)
        self.view.addSubview(webView)
    }
    
    

    
    
    func getDocumentDirectory() -> String {
        return NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
    }
    
    
    func didTapOnOKayOnAFWrapperAlert(errorCode: Int) {
        if errorCode != 1001 {
            goback()
        }
      
    }
    
    
    override func didFooterBtnPressed(sender: AnyObject) {
        super.didFooterBtnPressed(sender)
        
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
         leftMenu.pushOrPop(constants.viewControllers.curriculumVC, sideDrawer: appdelegate.drawerController!)
        
    }
    
    /*
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
