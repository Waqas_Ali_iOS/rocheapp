//
//  MyCertificateViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 6/29/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class MyCertificateViewController: HeaderViewController, MyCertificateTableViewCellDelegate, AFWrapperDelegate {
    
    @IBOutlet weak var tblView : UITableView!
    @IBOutlet var lblHeading: BaseUILabel!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    
    
    var arrData = [AnyObject]()
    var totalCount : Int = 0
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        _headerView.heading = "My Certificates"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        
        tblView.hidden = true
        CallService()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    
    
    // MARK: - Web Service Methods
    
    func CallService()
    {
        /*
        Alamofire.request(AFUrlRequestSharjeel.GetMyCertificates).responseString { (response) in
            
            print(response)
            
        }
       */
       
        AFWrapper.delegate = self
        AFWrapper.serviceCall(AFUrlRequestSharjeel.GetMyCertificates, success: { (res) in
            
            printLog(res)
            
            
            if res["data"] != nil
            {
                self.arrData = res["data"]["items"].arrayObject!
                self.totalCount = res["data"]["totalCount"].int!
                
                self.tblView.reloadData()
                self.tblView.hidden = false
            }
            
            
            
            
            
            
        }) { (NSError) in
            printLog(NSError)
        }
       
    }
    
    func didTapOnOKayOnAFWrapperAlert(errorCode : Int) -> Void {
        
        printLog("Do your awesome work here....")
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: - Tableview DataSource and Delegates
    
    
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
      
       // return 6
        
        return arrData.count
        
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        [self.tblView.separatorColor = UIColor .clearColor()]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! MyCertificateTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None

        
        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor.whiteColor()
            
            
        } else{
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
        
        if GlobalStaticMethods.isPad() {
            if  GlobalStaticMethods.isPadPro() {
                cell.viewBTnHeightConstraint.constant = 51

            }else{
        cell.viewBTnHeightConstraint.constant = 40
            }
        }
        
         cell.delegate = self
        
        let tempDic = arrData[indexPath.row]
        
        cell.setupCell(tempDic as! Dictionary<String, AnyObject>,index: indexPath.row)
       
        
//        if arrData.count - 1
//        {
//            CallService()
//        }
        
        
        

        return cell
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        var numOfSections: Int = 0
        
        if arrData.count > 0{
            self.tblView.separatorStyle = .None
            numOfSections                = 1
            self.tblView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRectMake(0, 0, self.tblView.bounds.size.width, self.tblView.bounds.size.height))
            noDataLabel.text             = "No data available"
            noDataLabel.textColor        = UIColor.lightGrayColor()
            noDataLabel.textAlignment    = .Center
            self.tblView.backgroundView = noDataLabel
            self.tblView.separatorStyle = .None
        }
        
        return numOfSections
    }
    
    
     // MARK: - Handle Layout
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }

    
    func detailBtnPressed(myCertificateTableViewCell: MyCertificateTableViewCell)
    {
        
        
        let tempDic = arrData[myCertificateTableViewCell.btnDetails.tag]
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseDetailVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.courseDetailVC) as! courseDetailViewController
        vc.strCourseID = "\(tempDic["id"] as! Int)"

        
        vc.courseType = tempDic["courseType"] as! Int

        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
