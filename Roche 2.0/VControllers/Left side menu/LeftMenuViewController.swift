//
//  LeftMenuViewController.swift
//  RocheDesignsOnly
//
//  Created by  Traffic MacBook Pro on 26/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit


@objc protocol qrImageProtocol {
    
    optional func imageButtonTapped(leftMenu : LeftMenuViewController) -> Void
    
    
}
class LeftMenuViewController: UIViewController , UITableViewDelegate{
    
    
    
    var delegate : qrImageProtocol?
    let arrProfile = ["My Profile Settings","Logout","Invite a New Member","Home","Academy Courses","My Upcoming Events","My Curriculum","My Team","Enroll Requests","Courses in Progress","Messages","My Certificates","My Bookmarks","Suggested Courses","Contact Roche"]
    
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblEmail : UILabel!
    @IBOutlet var imgProfile : UIImageView!
    @IBOutlet var btnClose : UIButton!
    
    let single = Singleton.sharedInstance
    @IBOutlet var tblViewProfile : UITableView!
    @IBOutlet var btnViewQR: buttonFontCustomClass!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        lblName.text = "Sam Al Hussein"
//        lblEmail.text = "sam@mediclinic.ae"
        
        
        
        imgProfile?.image = UserModel.sharedInstance.userQRimage
        lblName.text = UserModel.sharedInstance.userName
        lblEmail.text = UserModel.sharedInstance.userEmail
        
        btnViewQR.addTarget(self, action: #selector(self.openQRImage), forControlEvents: .TouchUpInside)
        
        
        btnClose .addTarget(self, action: #selector(LeftMenuViewController.closeDrawer), forControlEvents: .TouchUpInside)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LeftMenuViewController.getNotification), name:"reloadLeftMenu", object: nil)
        
        
        
        // Do any additional setup after loading the view.
    }
    
    func openQRImage(){
        NSNotificationCenter.defaultCenter().postNotificationName("openImage", object: nil)
        
    }
    
    func getNotification(){
        
        print("Notification Received")
        
        imgProfile?.image = UserModel.sharedInstance.userQRimage
        lblName.text = UserModel.sharedInstance.userName
        lblEmail.text = UserModel.sharedInstance.userEmail
        
        tblViewProfile.reloadData()
        
    }
    
    func closeDrawer(){
        leftMenu.closeDrawer(self.mm_drawerController)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
        // Dispose of any resources that can be recreated.
    }
    //MARK: UITableViewDelegate
    
    
    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! leftMenuTableViewCell
        let count = indexPath.row
        
        
        if count < 3{
            cell.contentView.backgroundColor = UIColor(hexString: "#3862ae")
            cell.lblCell.textColor = UIColor(hexString: "#FFFFFF")
            let imgSel  = "\(cell.arrImages[indexPath.row] as String)-sel"
            cell.imgCell.image = UIImage(imageName: imgSel)
            
            
        }
        else{
            
            cell.contentView.backgroundColor = UIColor(hexString: "#113c8b")
            cell.lblCell.textColor = UIColor(hexString: "#FFFFFF")
            
            if GlobalStaticMethods.isPhone(){
            let imgSel  = "\(cell.arrImages[indexPath.row] as String)-sel"
            cell.imgCell.image = UIImage(imageName: imgSel)
            
            }else{
          let imageSel = UIImage(imageName : cell.arrImages[indexPath.row] as String)
          let templateImage = imageSel.imageWithRenderingMode(.AlwaysTemplate)
           cell.imgCell.image = templateImage
           cell.imgCell.tintColor = UIColor.whiteColor()
            }
            
        }
        
        
    }
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! leftMenuTableViewCell
        let count = indexPath.row
        if count < 3{
            
            cell.contentView.backgroundColor = UIColor(hexString: "#113c8b")
            cell.lblCell.textColor = UIColor(hexString: "#FFFFFF")
            let imgSel  = cell.arrImages[indexPath.row] as String
            cell.imgCell.image = UIImage(imageName: imgSel)
            
            
            
            
        }
        else{
            
            cell.contentView.backgroundColor = UIColor.whiteColor()
            cell.lblCell.textColor = UIColor(hexString: "#113c8b")
            let imgSel  = cell.arrImages[indexPath.row] as String
            cell.imgCell.image = UIImage(imageName: imgSel)
            
        }
        
        
        
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        //       let cell = tableView.cellForRowAtIndexPath(indexPath) as! leftMenuTableViewCell
        //
        ////
        //        let count = indexPath.row
        //       if count < 3 {
        //        cell.contentView.backgroundColor = UIColor.clearColor()
        //        cell.cellSeparatorLine.backgroundColor = UIColor(hexString: "#113c8b")
        ////
        //        
        ////
        //       }
        //        else{
        //        cell.contentView.backgroundColor = UIColor(hexString: "#113c8b")
        //        cell.cellSeparatorLine.backgroundColor = UIColor(hexString: "#3862ae")
        //        }
        
        
        switch indexPath.row {
        case 0:
            leftMenu.pushOrPop("ProfileSettingsViewController", sideDrawer:  self.mm_drawerController)
            
        //            leftMenu.pushFromLeftMenu(constants.viewControllers.profileSettingsVC, sideDrawer: self.mm_drawerController)
        case 1:
            print("Logout")
            
            Alert.showAlertMsgWithTitle("Confirmation", msg: "Do you really want to log out?", otherBtnTitle: "Yes", otherBtnAction: { (Void) in
                self.logMeOut()
                }, cancelBtnTitle: "No", cancelBtnAction: { (Void) in
                    
                }, viewController: self)
           
        case 2:
            leftMenu.pushFromLeftMenu(constants.viewControllers.inviteNewMemberVC, sideDrawer: self.mm_drawerController)
        case 3:
            leftMenu.pushFromLeftMenu(constants.viewControllers.homeContainerViewController, sideDrawer: self.mm_drawerController)
        case 4:
            print("Academy Courses")
            
          leftMenu.pushFromLeftMenu(constants.viewControllers.academyCourseVC, sideDrawer: self.mm_drawerController)
        case 5:
            leftMenu.pushFromLeftMenu(constants.viewControllers.upcomingEventVC, sideDrawer: self.mm_drawerController)
        case 6:
            
            print("Curriculum")
            
            leftMenu.pushFromLeftMenu(constants.viewControllers.curriculumVC, sideDrawer: self.mm_drawerController)
            
        //leftMenu.pushFromLeftMenu(constants.viewControllers.myTeamVC, sideDrawer: self.mm_drawerController)
        case 7:
            leftMenu.pushFromLeftMenu(constants.viewControllers.myTeamVC, sideDrawer: self.mm_drawerController)
        case 8:
            leftMenu.pushFromLeftMenu(constants.viewControllers.enrollRequest, sideDrawer: self.mm_drawerController)
        case 9:
            print("Course in progress")
            leftMenu.pushFromLeftMenu(constants.viewControllers.CourseInProgressVC, sideDrawer: self.mm_drawerController)
            
        //   leftMenu.pushFromLeftMenu(constants.viewControllers.profileSettingsVC, sideDrawer: self.mm_drawerController)
        case 10:
            leftMenu.pushFromLeftMenu(constants.viewControllers.messageVC, sideDrawer: self.mm_drawerController)
        case 11:
            leftMenu.pushFromLeftMenu(constants.viewControllers.myCertificateVC, sideDrawer: self.mm_drawerController)
        case 12:
            
            print("Bookmark")
            
            leftMenu.pushFromLeftMenu(constants.viewControllers.bookmarkVC, sideDrawer: self.mm_drawerController)
            
        //   leftMenu.pushFromLeftMenu(constants.viewControllers., sideDrawer: self.mm_drawerController)
        case 13:
            print("Suggested Course")
            
            leftMenu.pushFromLeftMenu(constants.viewControllers.suggestedVC, sideDrawer: self.mm_drawerController)
            
        // leftMenu.pushFromLeftMenu(constants.viewControllers.profileSettingsVC, sideDrawer: self.mm_drawerController)
        case 14:
            leftMenu.pushFromLeftMenu(constants.viewControllers.contactRocheVC, sideDrawer: self.mm_drawerController)
        default:
            break
        }
        
        //  cell.selected = false
        
        
    }
    
    func logMeOut() {
        
        
        AFWrapper.serviceCall(AFUrlRequestHamza.Logout, success : { (res) in
            
            
            
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setBool(false, forKey: Constants.AUTO_LOGIN)
            userDefaults.synchronize()
            UserModel.sharedInstance.userID = nil
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.drawerController!.closeDrawerAnimated(true, completion: nil)
            navigation.PopToVC(constants.viewControllers.loginViewController, sideDrawer: appDelegate.drawerController!)
            
            
        } )
        { (NSError) in
            printLog(NSError)
        }

        
        
        
   
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        
        return getRowHeight(indexPath.row)
        //   return (UIScreen.mainScreen().nativeBounds.height/2) * 0.08831521739
        
    }
    
    
    func getRowHeight(indexRow : Int) -> CGFloat {
        
        var hg : CGFloat = 0.0
        
        if GlobalStaticMethods.isPad(){
            let myDeviceType = DeviceUtil.deviceType
            
            
            switch myDeviceType {
            case DeviceUtil.Device.iPad:
                hg = 0.055 * DeviceUtil.size.height
            case DeviceUtil.Device.iPadPro:
                hg  = 0.074 * DeviceUtil.size.width
                
            default:
                hg  = 0.062476563 * DeviceUtil.size.width
                
            }

            
            
        
        }else{
            
            let myDeviceType = DeviceUtil.deviceType
            
            
            switch myDeviceType {
            case DeviceUtil.Device.iPhone5:
                hg = 0.068 * DeviceUtil.size.height
            case DeviceUtil.Device.iPhone6:
                hg = 0.068 * DeviceUtil.size.height
            case DeviceUtil.Device.iPhone6Plus:
                hg = 0.068 * DeviceUtil.size.height

            default:
                hg = 0.068 * DeviceUtil.size.height

            }
        
        }
        
        // hiding My Team option for some users
        if indexRow == 7 || indexRow == 2{
        if UserModel.sharedInstance.userType == userTypes.labSupervisor ||
            UserModel.sharedInstance.userType == userTypes.labTechnician{
            hg = 0
            
            }
        }
        
        if indexRow == 8 {
            if UserModel.sharedInstance.userType == userTypes.labSupervisor ||
                UserModel.sharedInstance.userType == userTypes.labTechnician{
                hg = 0
                
            }
        }
        
        let intHg = Int(hg)
        return CGFloat(intHg)
      
    }
    
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
        //        let cell = tableView.cellForRowAtIndexPath(indexPath) as! leftMenuTableViewCell
        //        //14
        //        let count = indexPath.row
        //      
        //        switch count {
        //            
        //        case 0,1,2:
        //            
        //                
        //            print("lol")
        ////           
        ////            cell.contentView.backgroundColor = UIColor(hexString: "#113c8b")
        ////            cell.cellSeparatorLine.backgroundColor = UIColor(hexString: "#3862ae")
        //
        //
        //            
        //        
        //        case 3,4,5,6,7,8,9,10,11, 12,13 :
        //            print("lol")
        //
        ////
        ////            cell.contentView.backgroundColor = UIColor.clearColor()
        ////            cell.cellSeparatorLine.backgroundColor = UIColor(hexString: "#113c8b")
        //            
        //        default:
        //        
        //            break;
        //            
        //        }
    }
    
    
    
    
    //MARK: UITableViewDataSource
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProfile.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> leftMenuTableViewCell {
        //  if indexPath.row < count-1 {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! leftMenuTableViewCell
        
        
        
        if indexPath.row == 7 {
            if single.currentUser == userTypes.labSupervisor || single.currentUser == userTypes.labTechnician{
                print("Not Working")
                cell.userInteractionEnabled = true
                cell.contentView.backgroundColor = UIColor.grayColor()
                
                cell.lblCell.textColor = UIColor.grayColor()
            }
            else{
                cell.userInteractionEnabled = true
                cell.lblCell.textColor = UIColor.blackColor()
            }
        }
        
        cell.contentView.backgroundColor = UIColor.clearColor()
        cell.configureCell(indexPath, dataArr: arrProfile)
        
        cell.selectionStyle = .None
        
        if indexPath.row  == arrProfile.count - 1 {
            
        cell.cellSeparatorLine.hidden = true
        }else{
        cell.cellSeparatorLine.hidden = false
        }
        
        
        
        return cell
        
        //        }
        //        return nil
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
