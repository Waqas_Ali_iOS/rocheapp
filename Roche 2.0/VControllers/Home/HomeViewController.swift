//
//  HomeViewController.swift
//  Roche
//
//  Created by MBP on 15/08/2016.
//
//

import UIKit
import SwiftyJSON
//import RKNotificationHub

class HomeViewController: HeaderViewController,DataSource,Delegate,qrImageProtocol {

    @IBOutlet weak var SContainerViewMinimumHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var SContainerPropHeightWithSliderConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblNoDataFound: BaseUILabel!

    @IBOutlet weak var ContainerViewHeightConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var ingViewProfilePic: UIImageView?
    @IBOutlet weak var imgViewSettings: UIImageView?
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var circularChartObject: circularRatingView?
    
    @IBOutlet weak var lblMyCoursesInProgressHeading: UILabel?
    @IBOutlet weak var lblOnlineCourseHeading: UILabel?
    @IBOutlet weak var lblOnlineCourseHeadingText: UILabel?
    @IBOutlet weak var lblInlineCourseHeading: UILabel?
    @IBOutlet weak var lblInlineCourseText: UILabel?
    
    @IBOutlet weak var imgCurriculamPass: UIImageView!
    
    @IBOutlet weak var btnCurriculamPass: UIButton!
    
    @IBOutlet weak var newUserView: UIView!
    @IBOutlet weak var lblUserName: BaseUILabel!
    
    @IBOutlet weak var btnGotoTutorial: buttonFontCustomClass!
    
    
    @IBOutlet weak var lblCurriculumOverViewHeading: UILabel?
    @IBOutlet weak var ViewChart: UIView?
    @IBOutlet weak var rightView: UIView?
    @IBOutlet weak var imgCurriculumDetail: UIImageView?
    @IBOutlet weak var lblPreRequisites: UILabel?
    @IBOutlet weak var lblPreRequisitesText: UILabel?
    @IBOutlet weak var lblElective: UILabel?
    @IBOutlet weak var lblElectiveText: UILabel?
    @IBOutlet weak var lblUpcomingNewsHeading: UILabel?
    @IBOutlet weak var viewCSlider: CSlider!
    @IBOutlet weak var sViewTopConstraint: NSLayoutConstraint?
    @IBOutlet weak var btnProfile : UIButton?

    @IBOutlet weak var lblCOPercentage: BaseUILabel?
    
    @IBOutlet weak var BrowsAllMyCourse: UIButton?
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var btnCurriculum: UIButton!
    weak var timer: NSTimer?
    
    var dicData = Dictionary <String, AnyObject>()
    
    var single = Singleton.sharedInstance
    
    var notificationHub : RKNotificationHub!
    
    var imageViewQR : UIImageView!
    
    
    @IBOutlet var btnCircularView : UIButton!
    
    
    
    @IBOutlet var btnViewQR: buttonFontCustomClass!

    // MARK: -  view load methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mm_drawerController.openDrawerGestureModeMask = .All
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_ICON
        _headerView.rightButtonImage = constants.imagesNames.RIGHT_MENU_STAR_ICON
        _headerView.heading = "Welcome"
        
        
        
        
        _footerView.lblHeading.iPhoneFontSize = 50
        _footerView.leftButtonImage = "education"
        _footerView.rightButtonImage = "rightWhiteArrow"
        _footerView.bgColorVar = "#3e8ada"
        _footerView.bgColorHeighlightedFooter = "#113c8b"
        _footerView.heading = "Browse All Academy Courses"
        
        _footerView.hidden = false
        _footerView.btnFooter.hidden = false
        
        imgCurriculamPass.hidden = true
        btnCurriculamPass.hidden = true
        
        self.btnViewQR .addTarget(self, action: #selector(self.viewQR), forControlEvents: .TouchUpInside)
        self.btnCircularView .addTarget(self, action: #selector(self.goTocourseInProgress), forControlEvents: .TouchUpInside)

        if imageViewQR == nil {
            
            let image = UserModel.sharedInstance.userQRimage
            imageViewQR = UIImageView(image: image!)
            imageViewQR.image = nil
            imageViewQR.tag = 1;
            imageViewQR.contentMode = .ScaleAspectFit
            imageViewQR.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.7)
            imageViewQR.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width , height: self.view.bounds.height)
            imageViewQR.userInteractionEnabled = true
            
            let uiImage  = UIImageView(image: image!)
            
            uiImage.contentMode = .ScaleAspectFit
            
            let xImg = (self.view.bounds.width - self.view.bounds.width / 2) / 2
            let yImg = (self.view.bounds.height - self.view.bounds.height/2) / 2
            uiImage.frame = CGRect(x: xImg, y: yImg, width: self.view.bounds.width/2 , height: self.view.bounds.height/2)
            
            imageViewQR.addSubview(uiImage)
            view.addSubview(imageViewQR)
            self.view.bringSubviewToFront(imageViewQR)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(ProfileSettingsViewController.handleTap))
            imageViewQR.addGestureRecognizer(tap)
            
            imageViewQR.hidden = true
            
            
        }

        
        
        notificationHub = RKNotificationHub(view: _headerView.btnRight)
        
        var xDiff : CGFloat = 0
        if GlobalStaticMethods.isPhone() {
            notificationHub.scaleCircleSizeBy(0.6)
        }else{
            if GlobalStaticMethods.isPadPro(){
                notificationHub.scaleCircleSizeBy(0.8)
                xDiff = 8
            }
            else
            {
                notificationHub.scaleCircleSizeBy(0.55)
            }
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.viewQR), name:"openImage", object: nil)

        
        notificationHub.setCircleColor(UIColor(hexString: "#3e8ada"), labelColor: UIColor.whiteColor())
        
       // _headerView.btnRight.backgroundColor = UIColor.redColor()
        _headerView.btnRight.imageView?.backgroundColor = UIColor.clearColor()
        
     // let xNoti = (_headerView.btnRight.frame.width  - (_headerView.btnRight.imageView?.image?.size.width)!) / 2 + (_headerView.btnRight.imageView?.image?.size.width)!/1.2
      let  xNoti = (_headerView.btnRight.frame.width / 2 ) + xDiff
        notificationHub.setCircleX(xNoti, y:_headerView.btnRight.frame.height/3.5)
        
        
        
        
        
        btnProfile?.exclusiveTouch = true
        btnCurriculum.exclusiveTouch = true
        
        
        bottomView.hidden = true
        
        
        
        sViewTopConstraint?.constant = headerViewHeight
       

        viewCSlider!.dataSource = self
        viewCSlider!.delegate = self
        
        self.performSelector(#selector(setupSlider), withObject: nil, afterDelay: 0.1)

        btnProfile?.addTarget(self, action: #selector(HomeViewController.openProfile), forControlEvents: .TouchUpInside)
        
        btnGotoTutorial?.addTarget(self, action: #selector(HomeViewController.gotoTutorial), forControlEvents: .TouchUpInside)
        
        switch DeviceUtil.deviceType
        {
        case .iPhone6Plus:
            circularChartObject?.lineWidth = 13
            break
        case .iPhone6:
            circularChartObject?.lineWidth = 12
            break
        case .iPhone5:
            circularChartObject?.lineWidth = 10
            break
        case .iPadPro:
            circularChartObject?.lineWidth = 15
            break
        default:
             circularChartObject?.lineWidth = 14
            break
        }
        
        newUserView.hidden = true

        
        lblElectiveText?.text = ""
        lblPreRequisitesText?.text = ""
        lblInlineCourseText?.text = ""
        lblOnlineCourseHeadingText?.text = ""
        lblCOPercentage?.text = ""
        circularChartObject?.rating = 0.0
        
        
      //  setUserData()
        
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HeaderViewController.navigatePageAfterLocalNotificatio(_:)), name: "NaviagteToCertainPage", object:nil)
        
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector( self.goToNotifiedPage), name: "NaviagteAfterPushNotification", object:nil)
        
     
           if single.isComeFromLocalNotification == false {
        if single.isInValidUserCheckRequired == false
        {
            /*if (single.pushNotificationObj != nil)
            {
                Alert.showAlertMsgWithTitle("Alert", msg: single.pushNotificationObj["alert"] as! String, otherBtnTitle: "Check Now", otherBtnAction: { (Void) in
                    
                   self.goToNotifiedPage()
                    
                    }, cancelBtnTitle: "Cancel", cancelBtnAction: { (Void) in}, viewController: self)
            }
            
            */
           
            
            }else
            {
            printLog(single.pushNotificationObj)
            
            if single.pushNotificationObj["uID"] as! Int == UserModel.sharedInstance.userID
                {
                self.goToNotifiedPage()
                    }
                }

           }else{
        
            if single.isInValidUserCheckRequired != false{
                if single.localNotificationObj["userId"] as! Int == UserModel.sharedInstance.userID{
                notificationHub.count = Int32(UserModel.sharedInstance.unreadMessageCount)
                
                self.navigatePageAfterLocalNotificatio(nil)
                }

            
            }
        
        }
    
        
        // Do any additional setup after loading the view.
        
        getUpCommingEvents()
    }
    
    
//    override func headerViewLeftBtnDidClick(headerView: HeaderView) {
//    
//        
//        
//        mm_drawerController .toggleDrawerSide(.Left, animated: true) { (success) in
//            
//            let sb = navigation.getStoryBoardForController(constants.viewControllers.leftmenuViewController)?.storyboardObject
//            let leftVC = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.leftmenuViewController) as! LeftMenuViewController
//            leftVC.delegate = self
//            
//            
//        }
//        
//    }
    
    
//    func imageButtonTapped(leftMenu: LeftMenuViewController) {
//   
//        
//        mm_drawerController.closeDrawerAnimated(true) { (success) in
//            self.viewQR()
//        }
//    }
    
    func handleTap() {
        // handling code
        imageViewQR.hidden =  true
    }
 
    func viewQR(){
        print("QR")

        let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        appDel.drawerController?.closeDrawerAnimated(true, completion: { (Success) in
            
        })
        self.imageViewQR.hidden = false
        
    }

    
    func goTocourseInProgress(){
    
        navigation.goToViewController(constants.viewControllers.CourseInProgressVC, sideDrawer: self.mm_drawerController)
    }
    
    func goToNotifiedPage(){
        notificationHub.count = Int32(UserModel.sharedInstance.unreadMessageCount)
    
     self.navigatePageAfterPushNotificatio()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        print("viewCSlider is =%d",viewCSlider.frame.size.width)
        
        
        setUserData()
        
        callService()
        
    }
    
    override func viewWillDisappear(animated: Bool) {
      //--ww   NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        
        self.mm_drawerController.openDrawerGestureModeMask = .All
        
        notificationHub.count = Int32(UserModel.sharedInstance.unreadMessageCount)

        print("viewCSlider is =%d",viewCSlider.frame.size.width)
        
    }
    
    // MARK: -  Setup Slider
    
    func setupSlider() {
        
        viewCSlider.setUp();
    }

    
    // MARK: -  CSlider Delegates Methods
    
    func numberOfItemsInSelectionList(numberOfRows: CSlider) -> NSInteger {
        
        
         if dicData["upcomingEvents"] != nil
         {
            let count =  (dicData["upcomingEvents"]!["items"]!!.count)! as Int
            return count
            
        }
    
        return 0
    }
    
    

    
    func DataForRowAtIndex(numberOfRows : CSlider, forIndex : Int) -> NSDictionary
    {
        var tempDictionary = Dictionary<String,AnyObject>()
        
        if dicData["upcomingEvents"] != nil
        {
            tempDictionary["nibName"] = "HomeScreenUpComingEventSlider"
            tempDictionary["Data"] = dicData["upcomingEvents"]!["items"]!![forIndex]
        }
        
        return tempDictionary
        
    }
    
     // MARK: -  CSlider detail button Method
    
    func  didTapOnDetailBtn(detailBTN: CSlider, Tag: Int)
    {
        
        var tempUpcomingEvents =  Dictionary<String,AnyObject>()
        tempUpcomingEvents = dicData["upcomingEvents"] as! Dictionary<String,AnyObject>
        
        var tempUpcomingEventsItem =  [AnyObject]()
        tempUpcomingEventsItem = tempUpcomingEvents["items"] as! Array<AnyObject>
        
        
        var tempDic = tempUpcomingEventsItem[Tag]  as! Dictionary<String,AnyObject>
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseDetailVC)!.storyboardObject
        let vc = sb.instantiateViewControllerWithIdentifier(constants.viewControllers.courseDetailVC) as! courseDetailViewController

        
        vc.strCourseID = "\(tempDic["id"]!)"
        vc.isCurriculum = false
        vc.courseType = tempDic["type"] as! Int
        
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    
    func didArrowBtnTap(obj: CSlider, Tag: Int)
    {
        if timer != nil
        {
            timer?.invalidate()
        }
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(self.gotoUpcomingEvents), userInfo:nil, repeats: false)
    }
    
    func gotoUpcomingEvents()
    {
         goToViewController(constants.viewControllers.upcomingEventVC)
    }
    
    
    
    
    // MARK: -  Header View delegates

    override func headerViewRightBtnDidClick(headerView: HeaderView) {
        print("LOL")
        
        goToViewController(constants.viewControllers.notificationVC)
    }
    
    
    // MARK: -  Navigation Methods
    
    func goToViewController(identifier : String){
        let sb = navigation.getStoryBoardForController(identifier)!.storyboardObject
        let homeVC = sb.instantiateViewControllerWithIdentifier(identifier)
        self.navigationController!.pushViewController(homeVC, animated: true)
        
    }
    
    // MARK: -  View buttons Methods

    @IBAction func btnBrowseAllMyCourseClick(sender: AnyObject)
    {
        
       // goToViewController(constants.viewControllers.academyCourseVC)
        
    }
    @IBAction func goToMyCurriculum(sender: AnyObject) {
        
        goToViewController(constants.viewControllers.curriculumVC)
    }
    
    func openProfile(){
        
        goToViewController(constants.viewControllers.profileSettingsVC)
    }
    
    
//    user.sessionID = result["sessionId"]!.stringValue
//    user.userID = result["userId"]!.intValue
//    user.userName = result["name"]!.stringValue
//    user.userEmail = result["email"]!.stringValue
//    user.userType = userTypes(rawValue: result["userType"]!.intValue)
//    
//    /*
//     let str = "iVBORw0KGgoAAAANSUhEUgAAADoAAAA6AQMAAADbddhrAAAABlBMVEUjTZr+/v45oR6dAAAA2UlEQVQokXXPsQrCMBCA4QuFZhGztiD2FdLNoaSvUnBwLXQt6ObiIwi+yk262VeIFOwDuFQonilKm9q6hG847r8AUahxSwgdkM+DVcosFFW4c7PnALCu/0HmagCzJz9/F35gWsfzN/oDgLhsXwsBa1TmY4/HTgWu84IJiP01USnHHpFXnHCmoYc4weLStDMjyCRu2ME0O0QbUb5mJtpBenEJYFpjRJz0hWoLACEp526BiGm3/cUYyKWjHHNhh6ISOqblABBA6uMkRMkOTwvImSa6WWhbCvwJvAFKFNfp4PfTLgAAAABJRU5ErkJggg=="
//     let imageData = NSData(base64EncodedString: str, options: [])
//     
//     user.userQRimage =  UIImage(data: imageData!)
//     
//     */
//    user.userQRimage = imageClass.convertBase64ToImage((result["qrImage"]?.stringValue)!)
//    
//    user.unreadMessageCount = result["unreadMessageCount"]!.intValue
    
    
    //MARK: -  Set UI, load user data
    func setUserData()
    {
        
        ingViewProfilePic?.image = UserModel.sharedInstance.userQRimage
        lblName.text = UserModel.sharedInstance.userName
        lblEmail.text = UserModel.sharedInstance.userEmail
        
        
        
        lblUserName.text = String(format: "Hi %@", UserModel.sharedInstance.userEmail)

    }
    
    
    
    
    //MARK: -  Web service Call and Responce parser
    func callService()
    {
        
        AFWrapper.serviceCall(AFUrlRequestSharjeel.GetDashboard, success: { (res) in
            
            if self.dicData.count > 0
            {
                self.dicData.removeAll()
            }
            
            
            self.parseResponce(res)
             
            }) { (NSError) in
                printLog(NSError)
        }
        
    }
    
    
    func parseResponce(res : JSON)
    {
        if res["data"] != nil
        {
            dicData = res["data"].dictionaryObject!
            
            lblElectiveText?.text = "\(dicData["passElectiveCourses"] as! Int)/\(dicData["totalElectiveCourses"] as! Int) Completed"
            lblPreRequisitesText?.text = "\(dicData["passPrerequisiteCourses"] as! Int)/\(dicData["totalPrerequisiteCourses"] as! Int) Completed"
            lblInlineCourseText?.text = "\(dicData["inClassCourse"] as! Int)"
            lblOnlineCourseHeadingText?.text = "\(dicData["onlineCourse"] as! Int)"
            
            if dicData["currPercentage"] as! Double > 0.0  {
               lblCOPercentage?.text = String.init(format: "%@%%", "\(dicData["currPercentage"] as! Double)")
            }
            else
            {
                lblCOPercentage?.text = String.init(format: "0%%")
            }
            
            
            
            
            if((dicData["inClassCourse"] as! Int > 0) || (dicData["onlineCourse"] as! Int > 0) || (dicData["currPercentage"] as! Int > 0))
            {
                //SW-- newUserView.hidden = true
            }
            else
            {
               //SW--  newUserView.hidden = false
            }
            
            
            circularChartObject?.rating = (dicData["currPercentage"] as! Double) / 100
            
            if (dicData["currPercentage"] as! Double) >= 100
            {
                imgCurriculamPass.hidden = false
                btnCurriculamPass.hidden = false
                
                self.btnCurriculamPass.addTarget(self, action: #selector(self.gotoMyCertificates), forControlEvents: .TouchUpInside)
            }
            
            
            if dicData["upcomingEvents"]!["totalCount"] as! Int == 0
            {
                
                if GlobalStaticMethods.isPhone()
                {
                    self.ContainerViewHeightConstraint!.constant = 225+128+67 + (DeviceUtil.size.height * 0.18788043)
                    
                    
                    
                    self.view.layoutIfNeeded()
                }
                lblNoDataFound.text = "No Upcoming Events"
                lblNoDataFound.hidden = false
                viewCSlider.hidden = true
            }
            else
            {
                
                if GlobalStaticMethods.isPhone()
                {
                    var x : CGFloat = 0.0
                    
                    
                    
                    let devicetype =  DeviceUtil.deviceType
                    
                    switch devicetype {
                    case .iPhone5:
                        x = -12.0
                        break
                    case .iPhone6:
                         x = 10.0
                        break
                    case .iPhone6Plus:
                        x = 30.0
                        break
                    default:
                        break
                    }
                    
                    self.ContainerViewHeightConstraint!.constant = 722 + x
                    

                    self.view.layoutIfNeeded()
                }
                lblNoDataFound.text = ""
                lblNoDataFound.hidden = true
                viewCSlider.hidden = false
            }
            
          //  viewCSlider.removeAllSubviews()

            viewCSlider.reloadSlider()
            
        }
        else
        {
            printLog("No data found show new user login")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func gotoTutorial()
    {
        let sb = navigation.getStoryBoardForController(constants.viewControllers.tutorialScreenVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.tutorialScreenVC) as! tutorialViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    override func didFooterBtnPressed(sender: AnyObject) {
        super.didFooterBtnPressed(sender)
        
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        
        leftMenu.pushOrPop(constants.viewControllers.academyCourseVC, sideDrawer: appdelegate.drawerController!)
        
    }
    
    func gotoMyCertificates()
    {
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.myCertificateVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.myCertificateVC) as! MyCertificateViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func getUpCommingEvents()
    {
        
        AFWrapper.serviceCallWithoutLoader(AFUrlRequestSharjeel.GetUpComingEvents, success: { (res) in
            
            self.parseUpcomingResponce(res)
            
        }) { (NSError) in
            printLog(NSError)
        }
        
    }
    func didTapOnOKayOnAFWrapperAlert(errorCode: Int) {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    func parseUpcomingResponce(res : JSON)
    {
        
        if res["data"] != nil
        {
            if res["data"]["items"] != nil
            {
                
            let dataArray = res["data"]["items"].arrayObject!
             let total = dataArray.count
                for i in 0..<total{
                
                printLog(dataArray[i]["id"] as? Int)
                    
                  sheduleLocalNotification((dataArray[i] as? Dictionary<String, AnyObject>)!)
                
                
                }
              
                
            }
        }
    }
    
    func sheduleLocalNotification(dict : Dictionary<String, AnyObject>){
        //--ww  let actualTimestamp = Double(1478178000)
        
        let timeStamp : Int = dict["timestamp"] as! Int
        
        let reminderTimestamp = Double(timeStamp - 120 * 60)
        
        let currentTimestamp = Int(NSDate().timeIntervalSince1970)
        let fromCurrentTimestamp = Double(currentTimestamp - 120 * 60)
        let date = NSDate(timeIntervalSince1970: reminderTimestamp)
        
        //Get Current Date/Time
     //--ww   let currentDateTime = NSDate()
       //Check if reminderTime is Less than  current time
     //  if(currentDateTime.isGreaterThanDate(date)) {
        
        if currentTimestamp <=  Int(reminderTimestamp) {
          
      
        
        let dayTimePeriodFormatter = NSDateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd YYYY hh:mm aa"
        
        let dateString = dayTimePeriodFormatter.stringFromDate(date)
        
        print( " _ts value is \(reminderTimestamp)")
        print( " _ts value is \(dateString)")
        
       let cIDInt = dict["id"] as! Int
       let cIDString = "\(cIDInt)"
        
        
        let notification = ABNScheduler.notificationWithIdentifier(cIDString)
        
        printLog(notification)
        if notification == nil {
            
        let cTitle = dict["title"] as! String
        let userInfo = ["userId": UserModel.sharedInstance.userID,
                        "courseId": cIDString,
                        "courseTitle" : cTitle
                            ]
            
        let note = ABNotification(alertBody: "2 hours left: \(cTitle)", identifier: cIDString)
       //--ww note.alertAction = "some text"
        note.repeatInterval = .None
        note.userInfo = userInfo as Dictionary<NSObject, AnyObject>
       //--ww  NSDate().nextMinutes(1)
        
        let identifier = note.schedule(fireDate:date)
        
        printLog(identifier)
        }else if notification?.fireDate != date{
        
            notification?.reschedule(fireDate: date)
        }
     printLog(ABNScheduler.scheduledNotifications())
        
        
        }
    }
}


