//
//  SplashViewController.swift
//  BaseProject
//
//  Created by  Traffic MacBook Pro on 15/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    let single = Singleton.sharedInstance

    override func viewDidLoad() {
        
        
        
        super.viewDidLoad()
        
       
        
        
     sheduleLocalNotification()
        
        getHospitals()

        self.performSelector(#selector(SplashViewController.setNextViewController), withObject: self, afterDelay: 2)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.mm_drawerController.openDrawerGestureModeMask = .None
    }
    
    func setNextViewController(){
        let userDefault = NSUserDefaults.standardUserDefaults()
        
        if userDefault.boolForKey("isTutorialSkipped") == false{
            userDefault.setBool(true, forKey: "isTutorialSkipped")
            userDefault.synchronize()
            self.openTutorial()
            
        }else{
            
            self.goToLogin()
        }
        
        
    }
    
    func openTutorial(){
        
        //       goToViewController(constants.viewControllers.tutorialScreenVC)
        let sb = navigation.getStoryBoardForController(constants.viewControllers.tutorialScreenVC)!.storyboardObject
        let homeVC = sb.instantiateViewControllerWithIdentifier(constants.viewControllers.tutorialScreenVC)
        
        self.navigationController?.pushViewController(homeVC, animated: true)
        // goToLogin()
        
    }
    func goToLogin(){
     
        let navCon = leftMenu.getMainViewController(self.mm_drawerController)
        
        navigation.setViewControllerArray(constants.viewControllers.loginViewController, navCon: navCon)
       
        
//        Alert.showAlertMsgWithTitle("message", msg: single.deviceToken, btnActionTitle: "ok", viewController: nil) { (Void) in
//            
//            
//            let navCon = leftMenu.getMainViewController(self.mm_drawerController)
//            
//            navigation.setViewControllerArray(constants.viewControllers.loginViewController, navCon: navCon)
//            
//            
//        }
        
        
       
        
        //            getEmails()
//        }

        
    }
    
    
    

    func getHospitals(){
        AFWrapper.serviceCallWithoutLoader(AFUrlRequestHamza.PredictiveHosp, success: { (res) in
            printLog(res)
            
            self.single.arrHospitalsName = []
            self.single.arrHospitals = []
            
            
            //            var arrDataName = [String]()
            //            var arrDataID = [Int]()
            
            if res["statusCode"].intValue == statusCode.success.rawValue{
                let data = res["data"].dictionary
                if data?.count > 0{
                    
                    self.single.arrHospitals = res["data"]["items"].arrayObject!
                    
    
                    
//-- SW
                    for i in 0 ..< res["data"]["items"].count{
                        
                        
                        
                        let strName = res["data"]["items"][i]["name"].stringValue
                        let strID = res["data"]["items"][i]["id"].intValue
                        var dict = Dictionary<Int,AnyObject>()
                        
                        dict[i] = ["id": strID , "name" : strName]
                        //dict[strID] = strName
                        
                        self.single.arrHospitalsName.append(strName)
                        
                        
                        
                    }
                }
                
            }
            //            cell.showPicker(arrDataName as! [String], arrForID:  arrDataID)
            
        }) { (NSError) in
            printLog(NSError)
        }
        
    }
    
    
    
    //
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    func sheduleLocalNotification(){
        
       
    
     /*
        
        let notification = ABNScheduler.notificationWithIdentifier("17")
        
        printLog(notification)
        
        let note = ABNotification(alertBody: "Message", identifier: "18")
        note.alertAction = "some text"
        note.repeatInterval = .Hourly
      
        
        
        let identifier = note.schedule(fireDate:NSDate().nextMinutes(1))
            
            printLog(identifier)
 
 */
        
       

 
        
        
    }
    
   
}
