//
//  AcademyCoursesViewController.swift
//  
//
//  Created by Nabeel Siddiqui on 8/22/16.
//
//

import UIKit
import SwiftyJSON


class AcademyCoursesViewController:  HeaderViewController, AcademyTableViewCellDelegate, AFWrapperDelegate {
    
    var refreshControl: UIRefreshControl!

    @IBOutlet weak var viewCalendearHeightConstraint: NSLayoutConstraint!
    @IBOutlet var btnFilter: buttonFontCustomClass!
    @IBOutlet var btnCalendar: buttonFontCustomClass!
    @IBOutlet weak var tableview : UITableView!
    @IBOutlet weak var viewCalender : UIView!
    @IBOutlet weak var viewResult : UIView!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    @IBOutlet var topViewConstraint : NSLayoutConstraint?
    
    var  arrData = [JSON]()
    var strBookMark = 0
    var courseId : Int!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
      //  refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        tableview.addSubview(refreshControl) // not required when using UITableViewController
        
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 440
        _headerView.heading = "Academy Courses"
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        
        self.viewCalenderHeight()
        
        
        self.btnFilter.addTarget(self, action: #selector(AcademyCoursesViewController.filter), forControlEvents: .TouchUpInside)
        self.btnCalendar.addTarget(self, action: #selector(AcademyCoursesViewController.goToCalendar), forControlEvents: .TouchUpInside)
        if GlobalStaticMethods.isPad() {
            if  GlobalStaticMethods.isPadPro() {
                self.topViewConstraint?.constant = 51
                
            }else{
                self.topViewConstraint?.constant = 40
            }
        }
        
      //  self.serviceCall()
        
    }
    
    func refresh(sender:AnyObject) {

    self.serviceCall()
       // self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.serviceCall()

        self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    
    
    func parseResponce(res : JSON)
    {
        
        if res["data"] != nil
        {
            if res["data"]["items"] != nil
            {
                
                arrData = res["data"]["items"].arrayValue
                self.tableview.reloadData()
                
            }
        }
    }
    func serviceCall()  {
        
        let user = UserModel.sharedInstance
        AFWrapper.delegate = self

        AFWrapper.serviceCall(AFUrlRequestNabeel.GetSearchAcademyCourse(sessionId: user.sessionID, keyword: "", sortBy: "", courseType:"" , offset: 0, limit:10 , year:"" , month: "", date:"" ), success: { (res) in
            
            
            self.parseResponce(res)
            
            
        }) { (NSError) in
            printLog(NSError)
            
            
            
            
            
            
        }
        
    }
    
    func didTapOnOKayOnAFWrapperAlert(errorCode : Int) -> Void {
        
        printLog("Do your awesome work here....")
        
        self.navigationController?.popViewControllerAnimated(true)
    }

    
    func goToCalendar(){
        
        leftMenu.pushOrPop("customCalendarViewController", sideDrawer: self.mm_drawerController)
        
    }
    func filter(){
        
        leftMenu.pushOrPop(constants.viewControllers.filterVC, sideDrawer: self.mm_drawerController)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableViewCell, numberOfRowsInSection section: Int) -> Int {
        if  arrData.count != 0 {
            
            self.tableview.hidden = false
            
            return ( arrData.count)
        }else{
            self.tableview.hidden = true
            return 0
        }
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> AcademyCourseTableViewCell {
        [self.tableview.separatorColor = UIColor .clearColor()]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! AcademyCourseTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.delegate = self
        cell.lblHeading?.text = arrData[indexPath.row]["title"].stringValue
        cell.lblDescription?.text = arrData[indexPath.row]["shortDescription"].stringValue
        cell.btnBookMark?.tag = arrData[indexPath.row]["id"].int!
        cell.btnDetail?.tag = arrData[indexPath.row]["id"].int!
        cell.btnSuggest?.tag = arrData[indexPath.row]["id"].int!
        
        
        
        if arrData[indexPath.row]["courseType"].int == 1{
            cell.lblTitle?.text = Constants.ONLINE_COURSE
            cell.imgCourse?.image = UIImage (named: Constants.ONLINE_COURSE_IMAGE)
            
        }
        else{
            cell.lblTitle?.text = Constants.INCLASS_COURSE
            cell.imgCourse?.image = UIImage (named: Constants.INCLASS_COURSE_IMAGE)

        }
        
        if arrData[indexPath.row]["bookmarked"].int! == 1
            
        {
            if GlobalStaticMethods.isPad(){
                cell.btnBookMark!.setBackgroundImage(UIImage(imageName :"Gen-bookmark-img-sel" ), forState: .Normal)
            }
            else{
                cell.btnBookMark!.setImage(UIImage(named: "Gen-bookmark-img-sel"), forState: UIControlState.Normal)
            }
            
        }else{
            if GlobalStaticMethods.isPad(){
                cell.btnBookMark!.setBackgroundImage(UIImage(imageName :"Gen-BookMark-Btn" ), forState: .Normal)
            }
            else{
                cell.btnBookMark!.setImage(UIImage(named: "gen-bookmark"), forState: UIControlState.Normal)
            }
            
        }
        
        if(indexPath.row % 2 == 0){
            
            cell.backgroundColor = UIColor.whiteColor()
            
            
        } else{
            cell.backgroundColor = UIColor (red: 236.0/255.0, green:239.0/255.0, blue: 244/255.0, alpha: 1.0)
        }
        
        if GlobalStaticMethods.isPad() {
            if  GlobalStaticMethods.isPadPro() {
                cell.btnHeightConstraint?.constant = 51
                
            }else{
                cell.btnHeightConstraint?.constant = 40
            }
        }
        
        print(cell.btnDetail!.tag)
        cell.tag = indexPath.row
        
        return cell
        
        
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if  constants.deviceType.IS_IPHONE {
            
            return UITableViewAutomaticDimension
            
        }
            
        else if GlobalStaticMethods.isPad() {
            if GlobalStaticMethods.isPadPro() {
                print("ipad pro changed orientation")
                
            }
            
        }
        return UITableViewAutomaticDimension
        
    }
    
    func viewCalenderHeight(){
        
        let deviceWidth = CGRectGetWidth(self.view.frame);
        if constants.deviceType.IS_IPHONE {
            viewCalendearHeightConstraint.constant = deviceWidth * 0.17
        }
        else if  constants.deviceType.IS_IPAD {
            viewCalendearHeightConstraint.constant = deviceWidth * 0.08
        }
        else{
            viewCalendearHeightConstraint.constant = deviceWidth * 0.08
            
        }
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    func AcademyBtnTappedDelegate(cell: AcademyCourseTableViewCell)
    {
        
        let single = Singleton.sharedInstance
            
        let buttonPosition = cell.convertPoint(CGPointZero, toView: self.tableview)
        let indexPath = self.tableview.indexPathForRowAtPoint(buttonPosition)
        
        let count = Int(indexPath!.row)
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseDetailVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.courseDetailVC) as! courseDetailViewController
        vc.strCourseID = "\(cell.btnDetail!.tag)"
        
        let dict = arrData[cell.tag]
        
        vc.courseType = dict["courseType"].int!
        printLog(vc.courseType)
        print(cell.btnDetail!.tag)
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        //        switch count {
        //        case 0:
        //            
        //            
        //            single.courseVariatio = courseDetailVariations.first
        //            
        //
        //        case 1:
        //            single.courseVariatio = courseDetailVariations.second
        //        case 2:
        //            single.courseVariatio = courseDetailVariations.fourth
        //        case 3:
        //            single.courseVariatio = courseDetailVariations.fifth
        //        case 4:
        //            single.courseVariatio = courseDetailVariations.six
        ////        case 5:
        ////            single.courseVariatio = courseDetailVariations.eight
        //        default:
        //            break
        //        }
        
        
        
    }
    
    func UnbookMarkBtnTappedDelegate(cell: AcademyCourseTableViewCell) {
        
        courseId = cell.btnBookMark?.tag
        
        var user = UserModel.sharedInstance
        
        AFWrapper.serviceCall(AFUrlRequestNabeel.BookMarkCourse(sessionId: user.sessionID, courseId: courseId!), success: { (result) in
            
            
            
            if (result.dictionary!["statusCode"])! == 1000            {
                
                
                
                
                if GlobalStaticMethods.isPad(){
                    
                    
                    if cell.btnBookMark!.currentBackgroundImage!.isEqual(UIImage(imageName :"Gen-bookmark-img-sel")) {
                        //do something here
                        
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.UNBOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        
                        cell.btnBookMark?.setBackgroundImage(UIImage(imageName :"gen-bookmark" ), forState: .Normal)
                        
                        self.arrData[cell.tag]["bookmarked"] = 0
                        
                    }
                    else
                    {
                        
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.BOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        
                        cell.btnBookMark?.setBackgroundImage(UIImage(imageName :"Gen-bookmark-img-sel" ), forState: .Normal)
                        self.arrData[cell.tag]["bookmarked"] = 1
                        
                    }
                    
                    
                    
                    
                    //self.arrData["bookmark"].Int = 1
                }
                    
                else{
                    
                    
                    if cell.btnBookMark!.currentImage!.isEqual(UIImage(imageName :"Gen-bookmark-img-sel")) {
                        //do something here
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.UNBOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        
                        cell.btnBookMark?.setImage(UIImage(imageName :"gen-bookmark" ), forState: .Normal)
                        
                        self.arrData[cell.tag]["bookmarked"] = 0
                        
                    }
                    else{
                        
                        Alert.showAlertMsgWithTitle("Message", msg: Constants.BOOKMARK_MSG , btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
                            
                        })
                        
                        cell.btnBookMark?.setImage(UIImage(imageName :"Gen-bookmark-img-sel" ), forState: .Normal)
                        
                        self.arrData[cell.tag]["bookmarked"] = 1
                        
                    }
                }
                
                
                
                
                
                
            }
                
            else{
                
                Alert.showAlertMsgWithTitle("Message", msg: "Some Error", btnActionTitle: "ok", viewController: self, completionAction: { (Void) in
                    
                    
                    
                    
                })
                
            }
            
        }) { (NSError) in
            
            
            
        }
        
        
        
    }
    
    
    func goToSuggestCourse(Cell: AcademyCourseTableViewCell) {
        courseId = Cell.btnSuggest?.tag
        print( courseId)
        let sb = navigation.getStoryBoardForController(constants.viewControllers.courseSuggestVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.courseSuggestVC) as! CourseSuggestionViewController
        vc.courseID = courseId
        
        print(Cell.btnSuggest!.tag)
        print(courseId)
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
