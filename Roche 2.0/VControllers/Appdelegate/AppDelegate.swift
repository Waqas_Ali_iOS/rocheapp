                                                            //
//  AppDelegate.swift
//  BaseProject
//
//  Created by  Traffic MacBook Pro on 15/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//
                                                            


import UIKit
import IQKeyboardManager
import Fabric
import Crashlytics

                                                            
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    let googleMapKey = "AIzaSyBJXyuX9y5lrTwa_8ip4zXkSP1dMm1P4xo"
    
    var drawerController: MMDrawerController?
    let single = Singleton.sharedInstance
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        let temp = NSUserDefaults.standardUserDefaults()
        
        
        temp .setObject("No", forKey: "ShowPOPUP");
        
        GMSServices.provideAPIKey(googleMapKey)

        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldShowTextFieldPlaceholder = false
        IQKeyboardManager.sharedManager().shouldHidePreviousNext = true
        IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = ""
        IQKeyboardManager.sharedManager().toolbarTintColor = UIColor.clearColor()
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        application.statusBarHidden = true
        
        Fabric.with([Crashlytics.self])
        
        //        let single = Singleton.sharedInstance
        //        single.projectEnum = RequestType.ApprovedVC
        //        
        //        
        //        print(single.projectEnum.rawValue)
        //        
        //        print(single.projectDataDictionary)
        //
       setupNavController()
        
        self.window?.makeKeyAndVisible()
        
        
        let notificationSettings = UIUserNotificationSettings(forTypes: [.Badge, .Sound, .Alert], categories: nil)
       UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
        
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        let pushNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        
        if let remoteNotification = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary {
            self.application(application, didReceiveRemoteNotification: remoteNotification as [NSObject : AnyObject])
        }
        
       // application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
        
       
         setUDID()
        
        return true
    }
    
    func setUDID() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        if (userDefaults.stringForKey("UDID") != nil){
            
            single.deviceToken = userDefaults.stringForKey("UDID")!
        }
    }
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        printLog("applicationDidEnterBackground")
        
        
//        var notification = UILocalNotification.init()
//        
//        notification.repeatInterval = .Minute
//        notification.alertBody = "My test."
//        notification.fireDate = NSDate.init(timeIntervalSinceReferenceDate: 1)
//        //[notification setFireDate:[NSDate dateWithTimeIntervalSinceNow:1]];
//        notification.timeZone = NSTimeZone.defaultTimeZone()
//  
//        
//        application.scheduledLocalNotifications = [notification]
        
        
        let temp = NSUserDefaults.standardUserDefaults()
        temp .setObject("Yes", forKey: "ShowPOPUP");
        
        
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        let temp = NSUserDefaults.standardUserDefaults()
        temp .setObject("No", forKey: "ShowPOPUP");
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
        
        
    }
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        print("DEVICE TOKEN = \(deviceToken)")
        
        
        let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        var tokenString = ""
        
        for i in 0..<deviceToken.length {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        userDefaults.setObject(tokenString, forKey: "UDID")
        
        userDefaults.synchronize()
       
         single.deviceToken = tokenString
    }
    
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject])
    {
        printLog(userInfo)
        
//        let alert = UIAlertController(title: "Token", message: "\(userInfo)", preferredStyle: UIAlertControllerStyle.Alert)
//        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
//        self.window?.rootViewController!.presentViewController(alert, animated: true, completion: nil)

        
     
        single.isComeFromLocalNotification = false
        let temp = userInfo as! Dictionary <String,AnyObject>
        let userDefault = NSUserDefaults.standardUserDefaults()
        
        userDefault.setObject("YES", forKey: "isComeFromPushNotification")
        userDefault.synchronize()
        single.pushNotificationObj = temp
        single.PNSType = Singleton.pushNotificationType(rawValue : (temp["nT"]?.integerValue)!)!
        
        printLog(single.PNSType.rawValue)
        
        // Means user is login and with in the app internal screens, direct navigate to the certain view controller
        
        if UserModel.sharedInstance.userID != nil
        {
            single.isInValidUserCheckRequired = false
            UserModel.sharedInstance.unreadMessageCount = UserModel.sharedInstance.unreadMessageCount + 1
          NSNotificationCenter.defaultCenter().postNotificationName("NaviagteAfterPushNotification", object: nil)
        }
        else
        {
            single.isInValidUserCheckRequired = true
            //this means user is not login, direct navigate to the login view cotroller
            printLog("user model")
        }
 
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        
        print("notification - tapped --- inside appdelegate" )
        print(notification.userInfo!["courseId"])
        // NSNotificationCenter.defaultCenter().postNotificationName("NaviagteToCertainPage", object: nil, userInfo: userInfo as [NSObject : AnyObject])
        
        let temp = notification.userInfo as! Dictionary <String,AnyObject>
       
     
        single.localNotificationObj = temp
        
        single.isComeFromLocalNotification = true
        if UserModel.sharedInstance.userID != nil
        {
                 UserModel.sharedInstance.unreadMessageCount = UserModel.sharedInstance.unreadMessageCount + 1
            single.isInValidUserCheckRequired = false
            NSNotificationCenter.defaultCenter().postNotificationName("NaviagteToCertainPage", object: nil, userInfo: notification.userInfo! as [NSObject : AnyObject])
        }
        else
        {
            single.isInValidUserCheckRequired = true
            //this means user is not login, direct navigate to the login view cotroller
            printLog("user model")
        }
  }
    
    func setupNavController(){
        
        let vcBase = "SplashViewController" //AttendenceListViewController
        
        
        //--wwlet sB = UIStoryboard(name: "waqas-ipad-en", bundle: nil)
        
        let sB = navigation.getStoryBoardForController(vcBase)?.storyboardObject
        let initialVC : UIViewController = (sB!.instantiateViewControllerWithIdentifier(vcBase))
        
        let navController = MMNavigationController(rootViewController: initialVC)
        navController.restorationIdentifier = vcBase
        navController.setNavigationBarHidden(true , animated: false)
        
        
        let leftSideNavController = createSideMenuViewControllerFromStoryBoard("LeftMenuViewController")
    //    let rightSideNavController = createSideMenuViewControllerFromStoryBoard("RightSideMenuViewController")
        
        //loooool
        //  drawerController = MMDrawerController(centerViewController: navController, leftDrawerViewController: leftSideNavController, rightDrawerViewController: nil)
        drawerController = MMDrawerController(centerViewController: navController, leftDrawerViewController: leftSideNavController, rightDrawerViewController: nil)
        
        drawerController?.showsShadow = true
        drawerController?.openDrawerGestureModeMask = .All
        drawerController?.closeDrawerGestureModeMask = .All
        //  drawerController?.centerHiddenInteractionMode = .Full
        //drawerController?.maximumLeftDrawerWidth = leftMenu .getMenuWidth()
        //  drawerController?.centerHiddenInteractionMode = .Full
        
        var dwWidth = UIScreen.mainScreen().bounds.width * 0.845410628
        
        if GlobalStaticMethods.isPad(){
        dwWidth =   DeviceUtil.size.height * 0.38251
        }
        
        drawerController?.maximumLeftDrawerWidth = dwWidth
        drawerController?.shouldStretchDrawer = false
        drawerController?.setDrawerVisualStateBlock({ (drawerController, drawerSide, percentVisible) in
            let block :  MMDrawerControllerDrawerVisualStateBlock
            block = MMExampleDrawerVisualStateManager .sharedManager().drawerVisualStateBlockForDrawerSide(drawerSide)
            //if block == true {
            
            block(drawerController, drawerSide, percentVisible)
            
            //}
            
        })
        
        self.window?.rootViewController = self.drawerController
        //        self.window ?.makeKeyAndVisible()
        
        
    }
    
    func createSideMenuViewControllerFromStoryBoard(vcIdentifier : String) -> UINavigationController {
        let sB = navigation.getStoryBoardForController(vcIdentifier)!.storyboardObject
         //--ww let sB = UIStoryboard(name: "waqas-ipad-en", bundle: nil)
        let sideDrawerViewController = sB.instantiateViewControllerWithIdentifier(vcIdentifier)
        
        let sideNavController = MMNavigationController(rootViewController: sideDrawerViewController)
        sideNavController .setNavigationBarHidden(true, animated: true)
        
        return sideNavController
        
    }
    
    func application(application: UIApplication,
                     supportedInterfaceOrientationsForWindow window: UIWindow?)
        -> UIInterfaceOrientationMask {
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
                return .Landscape
            } else {
                return .Portrait
            }
    }
    
   
    
}
                                                            

 extension UIButton {
    
public override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
    
// Ignore if button hidden
if self.hidden {
   return nil
  }
// If here, button visible so expand hit area
 let hitSize = CGFloat(56.0)
let buttonSize = self.frame.size
let widthToAdd = (hitSize - buttonSize.width > 0) ? hitSize - buttonSize.width : 0
let heightToAdd = (hitSize - buttonSize.height > 0) ? hitSize - buttonSize.height : 0
let largerFrame = CGRect(x: 0-(widthToAdd/2), y: 0-(heightToAdd/2), width: buttonSize.width+widthToAdd, height: buttonSize.height+heightToAdd)
return (CGRectContainsPoint(largerFrame, point)) ? self : nil
 }
}
extension NSDate {                                                                func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
   //Declare Variables
  var isGreater = false
//Compare Values
if self.compare(dateToCompare) == NSComparisonResult.OrderedDescending {
  isGreater = true
  }
//Return Result
 return isGreater
 }
}
                                                       