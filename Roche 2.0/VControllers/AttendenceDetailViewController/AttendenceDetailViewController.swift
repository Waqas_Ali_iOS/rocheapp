//
//  AttendenceDetailViewController.swift
//  Roche
//
//  Created by Hamza Khan on 18/08/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class AttendenceDetailViewController: HeaderViewController{

    
    @IBOutlet var btnAttendenceCheck : UIButton?
    @IBOutlet var btnAddExtra : UIButton?
    @IBOutlet var coursetypeImg : UIImageView!

    @IBOutlet var txtViewDescription : UITextView!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblLocation : UILabel!
    @IBOutlet var topConstraint : NSLayoutConstraint?
    @IBOutlet  var btnScanQRCode: UIButton?
    @IBOutlet  var upperView: UIView?

    
    var courseID = 0
    var locationID = 0
    
    
    var courseId :Int!
    var locationId : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        upperView!.hidden = false
        
        btnAttendenceCheck?.setTitle("Check Details", forState: .Normal)
        
        btnAddExtra?.setTitle("Add Extra Attendee", forState:.Normal)
        
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        _headerView.heading = "Attendance Detail"
        
        btnAttendenceCheck?.addTarget(self, action: #selector(AttendenceDetailViewController.gotocheck), forControlEvents: .TouchUpInside)
        btnAddExtra?.addTarget(self, action: #selector(AttendenceDetailViewController.gotoAddExtra), forControlEvents: .TouchUpInside)

        lblTitle.text = ""
        lblLocation.text = ""
        txtViewDescription.text = ""
        
        self.whiteView()
        
        serviceCall()
        // Do any additional setup after loading the view.
    }
    
    
   
    func whiteView()  {
        
        upperView!.backgroundColor = UIColor.whiteColor()
    }
    
    func gotocheck(){
        
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.attendenceCheckVC)?.storyboardObject
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.attendenceCheckVC) as! AttendenceCheckViewController
        
        vc.courseID = courseId
        
        vc.locID = locationId
           
        self.navigationController?.pushViewController(vc, animated: true)

        
        
        
      //  navigation.goToViewController(constants.viewControllers.attendenceCheckVC, sideDrawer: self.mm_drawerController)

        
    }
    func gotoAddExtra(){
        let sb = navigation.getStoryBoardForController(constants.viewControllers.addExtraVC)!.storyboardObject
        let vc = sb.instantiateViewControllerWithIdentifier(constants.viewControllers.addExtraVC) as! AddExtraAttendeeViewController
        vc.courseID = courseID
        vc.locationID = locationID
        
        
        self.navigationController?.pushViewController(vc, animated: true)

//        navigation.goToViewController(constants.viewControllers.addExtraVC, sideDrawer: self.mm_drawerController)
    }
    func goToViewController(identifier : String){
        let sb = navigation.getStoryBoardForController(identifier)!.storyboardObject
        let homeVC = sb.instantiateViewControllerWithIdentifier(identifier)
        self.navigationController!.pushViewController(homeVC, animated: true)
        
    }

    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    
    
    func serviceCall(){
        
        AFWrapper.serviceCall(AFUrlRequestNabeel.AttendanceDetail(locId: locationID, courseId: courseID), success: { (res) in
            printLog(res)
            let data =  res["data"].dictionaryValue
            let longDescription =  data["description"]!.stringValue
            let title = data["title"]!.stringValue
            self.courseId = data["courseId"]!.intValue
            self.locationId = data["locId"]!.intValue
            let locationName = data["locName"]!.stringValue
            
            self.lblLocation.text = locationName
            self.lblTitle.text = title
            
            
//            if courseType == courseTypeEnum.online.rawValue {
//                lblTitle?.text = "Online Course"
//                coursetypeImg.image = UIImage (named: Constants.ONLINE_COURSE_IMAGE)
//                
//                
//            }
//            else if courseType == courseTypeEnum.inClass.rawValue{
//                lblTitle?.text = "In-Class Course"
//                
//                coursetypeImg.image = UIImage (named: Constants.INCLASS_COURSE_IMAGE)
//                
//            }
            
            
            
            var size = 0
            if GlobalStaticMethods.isPad(){
//                size = 20
//            }
//            else if GlobalStaticMethods.isPad(){
                var temp = 0.02225 * DeviceUtil.size.width
                size = Int(temp)

            }
            else if GlobalStaticMethods.isPhone(){
                var temp = 0.0372318 * DeviceUtil.size.width
                
                if temp < 12
                {
                    temp = 12
                }
                
                size = Int(temp)
            }
            self.txtViewDescription.attributedText = stringsClass.getAttributedStringForHTML(longDescription, textSize: size)
            
            self.upperView?.hidden = true

            
            }) { (NSError) in
                
                printLog(NSError)
                self.navigationController?.popViewControllerAnimated(true)

        }
        
    }

    
    func didTapOnOKayOnAFWrapperAlert(errorCode: Int) {
        
        
        self.navigationController?.popViewControllerAnimated(true)
        
        
    }
    
    @IBAction func scanAction(sender: AnyObject) {
        
        
     
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.ScanQRCodeViewController)?.storyboardObject
        
        
        let vc = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.ScanQRCodeViewController) as! ScanQRCodeViewController
        vc.courseID = courseID
        vc.locID = locationID
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
            }
    
    
    
    
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
