//
//  AttendenceCheckViewController.swift
//  Roche
//
//  Created by Syed Sharjeel Ali on 8/23/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import SwiftyJSON


class AttendenceCheckViewController: HeaderViewController {
    

    //let  dataDic : NSMutableDictionary = ["title":"A","text":"1","image":"gen-work-txtfield"]
    
    
    
    @IBOutlet weak var lblTopHeading: BaseUILabel?
    
    @IBOutlet weak var lblLocationText: BaseUILabel?
    @IBOutlet  var upperView: UIView?

    @IBOutlet weak var lblAdressText: BaseUILabel?
    
    
    
    @IBOutlet weak var imgMapIcon: UIImageView?
    
    // Custom Cell object
    @IBOutlet weak var customCView: customColView?
    

    @IBOutlet weak var btnExportCV: buttonFontCustomClass?
    
    @IBOutlet weak var locationView: UIView?
    
    
    @IBOutlet weak var lblTopConstraint: NSLayoutConstraint?

    
    
    var courseID = 9
    var locID = 14
    var dataArr : [String :JSON]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        _headerView.heading = "Check Attendance"
        
        self.locationView?.layer.cornerRadius = 10
        self.locationView?.layer.borderWidth = 1
        self.locationView?
            .layer.borderColor = UIColor.lightGrayColor().CGColor

        // customCView?.arrCellData = [["title":"Instructor","text":"John","image":"gen-Instructor"],["title":"Attendies","text":"15","image":"gen-attendies"],["title":"Date","text":"02-02-2000","image":"gen-Date"],["title":"Course Type","text":"Online","image":"gen-courseType"]]
        
        
        
        if GlobalStaticMethods.isPadPro() {
            lblTopConstraint?.constant = 100
        }
        
//        lblLocationText?.text = ""
        lblTopHeading?.text = ""
        lblAdressText?.text = ""
        
        self.whiteView()
        self.serviceCall()

        // Do any additional setup after loading the view.
    }
    
    func whiteView()  {
        
        upperView!.backgroundColor = UIColor.whiteColor()
        upperView!.hidden = false
    }
    
    override func viewWillAppear(animated: Bool){
        self.locationView?.layer.cornerRadius = 10
        self.locationView?.layer.borderWidth = 1
        self.locationView?
            .layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func serviceCall(){
        
        AFWrapper.serviceCall(AFUrlRequestNabeel.AttendanceCheck(courseID: courseID, locId: locID), success: { (res) in
            printLog(res)

            if res["statusCode"].int! == 1000{
                
                
                self.dataArr = res["data"].dictionary
                
                print(self.dataArr)
                
                let name = self.dataArr["instructor"]!.string!
                let attendees = self.dataArr["attendies"]!.int!
                let courseType = 2
                let date = self.dataArr["date"]!.string!
                let address = self.dataArr["locName"]!.string!
                let title = self.dataArr["title"]!.string
                self.lblAdressText!.text = address
                self.lblTopHeading!.text = title
                var StrCourseType = ""
                if courseType == 2 {
                    StrCourseType = "In-Class"
                }
                else{
                    StrCourseType = "Online"
                }
                
                

                self.customCView?.arrCellData = [["title":"Instructor","text":name,"image":"gen-Instructor"],["title":"Attendees","text":"\(attendees)","image":"gen-attendies"],["title":"Date","text":date,"image":"gen-Date"],["title":"Course Type","text":StrCourseType,"image":"gen-courseType"]]
                
                self.customCView?.reloadData()
                self.upperView!.hidden = true
                
                
                self.locationView?.layer.cornerRadius = 10
                self.locationView?.layer.borderWidth = 1
                self.locationView?
                    .layer.borderColor = UIColor.lightGrayColor().CGColor

 
            }
            
            
            
            
        }) { (NSError) in
            printLog(NSError)
        }
        
        
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return (customCView?.arrCellData.count)!
    }
    
    // make a cell for each cell index path
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        var cell = CustomCollectionViewCell()
        cell = collectionView.dequeueReusableCellWithReuseIdentifier("CustomCell", forIndexPath: indexPath) as! CustomCollectionViewCell
        
        let dic : NSDictionary = customCView?.arrCellData[indexPath.row] as! NSDictionary
        
        //let dic : NSDictionary = [] as! NSDictionary

        
        cell.SetupCell(dic, indexpath: indexPath.row)
        
        if indexPath.row % 2 != 0  {
            cell.verticalLineView?.hidden = true;
        }
        else
        {
            cell.verticalLineView?.hidden = false;
        }
        
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    //Use for size
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
       
        
        let  width =  CGRectGetWidth((self.customCView?.frame)!) * 0.50;
        let height  = CGRectGetHeight((self.customCView?.frame)!) / 2
        
        
        return CGSizeMake(width, height);
        
    }
    //Use for interspacing
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }

    
    @IBAction func btnExportCV (Sender : UIButton){
        
        
        AFWrapper.serviceCall(AFUrlRequestNabeel.AttendExportCV(locId: locID, courseId: courseID), success: { (res) in
            printLog(res)
            
            if res["statusCode"].int! == 1000{

           Alert.showAlertMsgWithTitle("Message", msg: "CSV exported to your email", btnActionTitle: "OK", viewController: self, completionAction: { (Void) in
            
           })
            
            }
            
            
        }) { (NSError) in
            printLog(NSError)
        }
        
    }
    
    
    
}
