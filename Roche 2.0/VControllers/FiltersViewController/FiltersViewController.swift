//
//  FiltersViewController.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/3/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit

class FiltersViewController: HeaderViewController , UITextFieldDelegate{

    @IBOutlet var btnInClass: UIButton!
    @IBOutlet var btnOnline: UIButton!
    @IBOutlet var btnLatest: UIButton!
    @IBOutlet var btnAlphabetical: UIButton!
    @IBOutlet var txtKeyword: BaseUITextField!
    @IBOutlet var btnApplyFilter: buttonFontCustomClass!
    @IBOutlet var btnCourseType: UIButton!
    @IBOutlet var btnSort: UIButton!
    @IBOutlet var btnSearch: UIButton!
    @IBOutlet weak var alphabeticImage : UIImageView!
    @IBOutlet weak var latestAdded : UIImageView!
    @IBOutlet weak var onLine : UIImageView!
    @IBOutlet weak var inClass : UIImageView!
    @IBOutlet weak var keyWordView : UIView!

    var isHighLightedOnline:Bool = false
    var isHighLighted:Bool = false
    @IBOutlet var topConstraint : NSLayoutConstraint?
    var sortBy = 1
    var courseTypeOnline : String?
    var courseTypeInclass : String?


    override func viewDidLoad() {
        txtKeyword.delegate = self
        super.viewDidLoad()
        _headerView.leftButtonImage = constants.imagesNames.LEFT_MENU_BACK_ICON
        _headerView._bgImage = "filter-image"
        _headerView.bgColorVar = "#FFFFFF"
        _headerView.lblHeading.textColor = UIColor(hexString: "#113c8b")
        
        _headerView.rightButtonImage = "reset-image"
        
        _headerView.leftButtonImage = "header-blue-arrow"
       
        // Do any additional setup after loading the view.
        txtKeyword.text = ""
        btnApplyFilter.addTarget(self, action: #selector(FiltersViewController.gotosearch), forControlEvents: .TouchUpInside)
        
        self.setUpView()
        
    }
   
   
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        self.mm_drawerController.openDrawerGestureModeMask = .None

    }
    func setUpView()  {
        
        courseTypeInclass = ""
        courseTypeOnline = ""
        
        let color = UIColor(hexString: "#5777B3")
        let placeHolder = NSAttributedString(string:"Keyword", attributes: [NSForegroundColorAttributeName: color])
        txtKeyword.attributedPlaceholder = placeHolder
        
        
        self.keyWordView.layer.borderWidth = 1
        self.keyWordView.layer.borderColor =   UIColor (red: 17.0/255.0, green:60.0/255.0, blue: 139/255.0, alpha: 1.0).CGColor
        
        
        self.alphabeticImage.image = UIImage(named: "Gen-RoundBlue-img" )
        self.latestAdded.image = UIImage(named: "Gen-RoundBlueCircle-img" )
        
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        // Tap the bottom area, excute the fold animation

    self.txtKeyword.resignFirstResponder()
    
    }
    
    func gotosearch(){
        
        let sb = navigation.getStoryBoardForController(constants.viewControllers.PrivacyPolicyViewController)?.storyboardObject
        let newsVC = sb?.instantiateViewControllerWithIdentifier(constants.viewControllers.searchResultVC) as! SearchResultViewController
        newsVC.strKeyword = self.txtKeyword.text!
        newsVC.courseType = courseTypeOnline!  +  courseTypeInclass! ?? "0"
        newsVC.sortBy = sortBy
        print(newsVC.courseType)
        self.navigationController!.pushViewController(newsVC, animated: true)

        }
    

    @IBAction func alphabetical(sender: UIButton) {
       self.alphabeticImage.image = UIImage(named: "Gen-RoundBlue-img" )
        self.latestAdded.image = UIImage(named: "Gen-RoundBlueCircle-img" )
        sortBy = 1
    }
    
    @IBAction func latestAdded(sender: UIButton) {
        self.latestAdded.image = UIImage(named: "Gen-RoundBlue-img" )
        self.alphabeticImage.image = UIImage(named: "Gen-RoundBlueCircle-img" )
       sortBy = 2
    }
    
    @IBAction func online(sender: UIButton) {
        if isHighLightedOnline == false{
            self.onLine.image = UIImage(named: "Gen-SquareBlue-img" )
            courseTypeOnline = ""

            isHighLightedOnline = true
        }else{
            self.onLine.image = UIImage(named: "Gen-SquareBlueTick-img" )
            courseTypeOnline = "1"
            isHighLightedOnline = false
        }

    }
    
    @IBAction func inClass(sender: UIButton) {
        
        if isHighLighted == false{
            self.inClass.image = UIImage(named: "Gen-SquareBlue-img" )
            courseTypeInclass = ""

            isHighLighted = true
        }else{
            self.inClass.image = UIImage(named: "Gen-SquareBlueTick-img" )
            courseTypeInclass = "2"
            isHighLighted = false
        }

        
        
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        
        coordinator.animateAlongsideTransition({ (context : UIViewControllerTransitionCoordinatorContext!) -> Void in
            // print("LOOOOOL")
            
            self.updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
            
        }) { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            //  print("End")
            
        }
    }
    
    
    func updateLayoutForNewOrientation(orientation : UIInterfaceOrientation){
        topConstraint?.constant = GlobalStaticMethods.getHeaderFooterHeight()
        
        if GlobalStaticMethods.isPhone() {
            print("iphone changed orientation")
        }
        else{
            if GlobalStaticMethods.isPad() {
                if GlobalStaticMethods.isPadPro() {
                    print("ipad pro changed orientation")
                    
                    
                    
                    if orientation.isLandscape {
                        //   self.viewSupplementHeightConstraint.constant = 45
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                }
                else{
                    if orientation.isLandscape {
                        //    self.viewSupplementHeightConstraint.constant = 50
                    }
                    else{
                        //  self.viewSupplementHeightConstraint.constant = heightConstraintValueForView
                        
                    }
                    
                    print("ipad changed orientation")
                }
            }
        }
        
    }
    override func viewWillAppear(animated: Bool) {
        updateLayoutForNewOrientation(UIApplication.sharedApplication().statusBarOrientation)
        
    }
    
    override func headerViewLeftBtnDidClick(headerView: HeaderView) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func headerViewRightBtnDidClick(headerView : HeaderView){
        
        courseTypeInclass = ""
        courseTypeOnline = ""
        courseTypeOnline = ""
        courseTypeInclass = ""
        
        self.alphabeticImage.image = UIImage(named: "Gen-RoundBlue-img" )
        self.latestAdded.image = UIImage(named: "Gen-RoundBlueCircle-img" )
        
        
        self.onLine.image = UIImage(named: "Gen-SquareBlue-img" )
        self.inClass.image = UIImage(named: "Gen-SquareBlue-img" )
        
        self.txtKeyword.text = ""
        
    }
   

    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder();
        return true;
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
