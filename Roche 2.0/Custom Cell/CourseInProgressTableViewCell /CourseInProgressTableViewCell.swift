//
//  AcademyCourseTableViewCell.swift
//  
//
//  Created by Nabeel Siddiqui on 8/22/16.
//
//

import UIKit
@objc protocol CourseInProgressTableViewCellDelegate {
    
    optional func CourseInProgressBtnTappedDelegate(detailBTN : CourseInProgressTableViewCell) -> Void
    optional func UnbookMarkBtnTappedDelegate(unBookBTN : CourseInProgressTableViewCell) -> Void
    optional func goToSuggestBtnDelegate(cell : CourseInProgressTableViewCell) -> Void

    
}

class CourseInProgressTableViewCell: UITableViewCell {

    var delegate : CourseInProgressTableViewCellDelegate?    
    @IBOutlet var btnDetail: buttonFontCustomClass?
    @IBOutlet var lblDescription: UILabel?
    @IBOutlet var lblTitle: UILabel?
    @IBOutlet var lblHeading: UILabel?
    @IBOutlet var btnHeightConstraint: NSLayoutConstraint?
    @IBOutlet var btnBookMark: UIButton?
    @IBOutlet var btnSuggest: UIButton?
    @IBOutlet var imgCourse: UIImageView?


    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.btnDetail!.addTarget(self, action: #selector(CourseInProgressTableViewCell.detailBtnClicked), forControlEvents: .TouchUpInside)
        
        self.btnBookMark!.addTarget(self, action: #selector(CourseInProgressTableViewCell.unBookBtnClicked), forControlEvents: .TouchUpInside)
        
         self.btnSuggest!.addTarget(self, action: #selector(CourseInProgressTableViewCell.suggestBtnClicked), forControlEvents: .TouchUpInside)
        
        
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func unBookBtnClicked(sender: UIButton!) -> Void {
        
        delegate?.UnbookMarkBtnTappedDelegate?(self)
        
    }
    
    
    func detailBtnClicked(sender: UIButton!) -> Void {
        
        delegate?.CourseInProgressBtnTappedDelegate?(self)
        
    }

    
    func suggestBtnClicked(sender: UIButton!) -> Void {
        
        delegate?.goToSuggestBtnDelegate?(self)
        
    }
    
}
