//
//  ProfileSettingsTableViewCell.swift
//  RocheDesignsOnly
//
//  Created by  Traffic MacBook Pro on 26/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import IQKeyboardManager
@objc protocol ProfileSettingsTableViewCellDelegate {
    
    optional func didTypeOnPredictiveEmail(buttonClass : ProfileSettingsTableViewCell) -> Void
    optional func didTypeOnPredictiveHospital(buttonClass : ProfileSettingsTableViewCell) -> Void
    
    optional func btnPressedAction(buttonClass : ProfileSettingsTableViewCell) -> Void
    optional func didTapOnPickerBTN(buttonClass : ProfileSettingsTableViewCell) -> Void
    optional func didTapOnPickerDoneBTN(cell : ProfileSettingsTableViewCell) -> Void
    
    //    optional func headerViewLeftBtnDidClick(headerView : HeaderView) -> Void
    //    optional func headerViewRightBtnDidClick(headerView : HeaderView) -> Void
    //    optional func headerViewRightSecondBtnDidClick(headerView : HeaderView) -> Void
}


class ProfileSettingsTableViewCell: UITableViewCell,UITextFieldDelegate {
    
    
    
    var delegate: ProfileSettingsTableViewCellDelegate?
    
    
    
    @IBOutlet weak var textFieldContentHeightConstrint: NSLayoutConstraint?
    @IBOutlet var txtFieldProfile : BaseUITextField!
    @IBOutlet var lblDescription : BaseUILabel?
    @IBOutlet var btnUpdate : buttonFontCustomClass?
    var btnPicker : UIButton?
    
    
    var arrMainDataReference : registerPropertyModelStruct!
    var arrProfileDataReference : myProfileModelClass!
    
    var fromWhere : String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        if btnUpdate != nil{
            btnUpdate?.addTarget(nil, action: #selector(ProfileSettingsTableViewCell.btnPressedAction(_:)), forControlEvents: .TouchUpInside)
            btnUpdate?.mafConnectbutton = true
            
        }
        
        
        self.lblDescription?.iPhoneFontSize = 40
        self.lblDescription?.iPadFontSize = 35
        self.lblDescription?.iPadProFontSize = 35
        
        
        //        txtFieldProfile.delegate = self
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func configureCell(cell: ProfileSettingsTableViewCell, forRowAtIndexPath: NSIndexPath, Data : registerPropertyModelStruct) {
        
        fromWhere = "registerPropertyModelStruct"
        arrMainDataReference = Data
        
        if forRowAtIndexPath.row != 10 {
            lblDescription?.adjustsFontSizeToFitWidth = false
            

            txtFieldProfile.delegate = self
            txtFieldProfile.placeholder = arrMainDataReference.placeholder
          
            var name : String = arrMainDataReference.leftImage
            //txtFieldProfile.leftImage = arrMainDataReference.leftImage
            txtFieldProfile.keyboardType = arrMainDataReference.keyboardType
            txtFieldProfile.text = arrMainDataReference.txtfieldValue
            txtFieldProfile?.tag = forRowAtIndexPath.row
            self.lblDescription?.text = arrMainDataReference.descriptionLabel
            
            
            
            
            
//            if forRowAtIndexPath.row == 4 || forRowAtIndexPath.row == 0 || forRowAtIndexPath.row == 1 || forRowAtIndexPath.row == 7 || forRowAtIndexPath.row == 8 {
//                self.lblDescription?.removeFromSuperview()
//                
//            }
//            else{
//
//            }
            //            self.lblDescription?.backgroundColor = UIColor.redColor()
            
            
            if forRowAtIndexPath.row == 7 || forRowAtIndexPath.row == 9
            {
                txtFieldProfile.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
            }
            
            
            if forRowAtIndexPath.row == 8  {
                btnPicker = UIButton()
                btnPicker?.backgroundColor = UIColor .clearColor()
                btnPicker?.frame = txtFieldProfile.frame
                
                
                
                
                btnPicker?.addTarget(self, action: #selector(ProfileSettingsTableViewCell.pickerBtnPressed), forControlEvents: UIControlEvents .TouchUpInside)
                cell.contentView .addSubview(btnPicker!)
                btnPicker?.center = CGPointMake(self.center.x, txtFieldProfile.center.y)
                cell.contentView.bringSubviewToFront(btnPicker!)
                txtFieldProfile.rightImage = "gen-downArrow"
                
                cell.txtFieldProfile.userInteractionEnabled = false
                
                btnPicker?.tag = forRowAtIndexPath.row
                
                
                self.performSelector(#selector(setButtonFrame), withObject: nil, afterDelay: 0.1)
            }
            else
            {
                txtFieldProfile.rightImage = ""
            }
            
            if forRowAtIndexPath.row == 3 || forRowAtIndexPath.row == 4{
                
                txtFieldProfile.secureTextEntry = true
                
            }else{
                
                txtFieldProfile.secureTextEntry = false
                
            }
            
            cell.txtFieldProfile.userInteractionEnabled = true
            self.setTextFieldImage(txtFieldProfile,imageName: name)
            
            if forRowAtIndexPath.row == 3
            {
                if  lblDescription?.font.pointSize == 12 {
                   lblDescription?.font = UIFont.init(name: (lblDescription?.fontName)!, size: 10)
                }
            }
            
            
        }
        
    }
    
    func setButtonFrame() {

        btnPicker?.frame = CGRect(x: self.txtFieldProfile.frame.origin.x, y: self.txtFieldProfile.frame.origin.y, width: self.txtFieldProfile.frame.size.width, height: self.txtFieldProfile.frame.size.height + 5)
        btnPicker?.center = CGPointMake(self.center.x, txtFieldProfile.center.y + 15)

    }
    
    
    func configureProfileCell(cell: ProfileSettingsTableViewCell, forRowAtIndexPath: NSIndexPath, Data : myProfileModelClass) {
        
        fromWhere = "arrProfileDataReference"
        arrProfileDataReference = Data
        
        if forRowAtIndexPath.row != 10 {
            txtFieldProfile.delegate = self
            txtFieldProfile.placeholder = arrProfileDataReference.placeholder
            var name : String = arrProfileDataReference.leftImage
            
            self.setTextFieldImage(txtFieldProfile,imageName: name)
            
            txtFieldProfile.keyboardType = arrProfileDataReference.keyboardType
            txtFieldProfile.text = arrProfileDataReference.txtfieldValue
            txtFieldProfile?.tag = forRowAtIndexPath.row
            txtFieldProfile.userInteractionEnabled = arrProfileDataReference.userInteraction
            lblDescription?.text = arrProfileDataReference.lblTip
            
            
            if arrProfileDataReference.userInteraction == false{
            self.contentView.alpha = 0.5
            
            }else{
            self.contentView.alpha = 1
            }
            
            
            if forRowAtIndexPath.row == 3 || forRowAtIndexPath.row == 4{
                
                txtFieldProfile.secureTextEntry = true
                
            }else{
                
                txtFieldProfile.secureTextEntry = false
                
            }
            
            
            
        }
        
    }
    
    
    
    func configureCellProfile(cell: ProfileSettingsTableViewCell, forRowAtIndexPath: NSIndexPath, data : Dictionary<String,AnyObject>){
        
        var name : String = ""
        
        switch forRowAtIndexPath.row {
        case 0:
            txtFieldProfile.placeholder = "First Name"
            name =  "gen-name-txtfield"
          //SW--  txtFieldProfile.leftImage = "gen-name-txtfield"
            txtFieldProfile.text = data["firstName"] as? String
            lblDescription?.text = ""
            // lblDescription?.removeFromSuperview()
            
            
            txtFieldProfile.keyboardType = .Default
            txtFieldProfile.secureTextEntry = false
            
            txtFieldProfile.userInteractionEnabled = true
            
        case 1:
            txtFieldProfile.placeholder = "Last Name"
            name =  "gen-name-txtfield"
            //SW-- txtFieldProfile.leftImage = "gen-name-txtfield"
            txtFieldProfile.text = data["lastName"] as? String
            lblDescription?.text = ""
            //  lblDescription?.removeFromSuperview()
            
            //  self.backgroundColor = UIColor.brownColor()
            
            txtFieldProfile.keyboardType = .Default
            txtFieldProfile.secureTextEntry = false
            txtFieldProfile.userInteractionEnabled = true
            
        case 2:
            txtFieldProfile.placeholder = "Work Email"
            name =  "gen-email-txtfield"
            //SW-- txtFieldProfile.leftImage = "gen-email-txtfield"
            txtFieldProfile.text = data["email"] as? String
            lblDescription?.text = "e.g john@mediclinic.ae"
            txtFieldProfile.keyboardType = .EmailAddress
            txtFieldProfile.secureTextEntry = false
            
            txtFieldProfile.userInteractionEnabled = false
            
            //--ww   lblDescription?.backgroundColor = UIColor.redColor()
            //--ww   txtFieldProfile.backgroundColor = UIColor.grayColor()
            //--ww self.backgroundColor = UIColor.greenColor()
            
            
        case 3:
            txtFieldProfile.placeholder = "Password"
            name =  "gen-password-txtfield"
            //SW-- txtFieldProfile.leftImage = "gen-password-txtfield"
            lblDescription?.text = "Tip:  Choose a password with at least 6 characters and 1number "
            
            txtFieldProfile.keyboardType = .Default
            txtFieldProfile.secureTextEntry = true
            
            txtFieldProfile.userInteractionEnabled = true
            
        case 4:
            txtFieldProfile.placeholder = "Confirm Password"
            name = "gen-password-txtfield"
            //txtFieldProfile.leftImage = "gen-password-txtfield"
            lblDescription?.text = "Tip:  Choose a password with at least 6 characters and 1number "
            txtFieldProfile.keyboardType = .Default
            txtFieldProfile.secureTextEntry = true
            
            txtFieldProfile.userInteractionEnabled = true
            
        case 5:
            txtFieldProfile.placeholder = "Work Number"
            name = "gen-work-txtfield"
           // txtFieldProfile.leftImage = "gen-work-txtfield"
            lblDescription?.text = "e.g. +971 501234123 - include all codes"
            txtFieldProfile.keyboardType = .NumbersAndPunctuation
            txtFieldProfile.secureTextEntry = false
            
            txtFieldProfile.text = data["workNumber"] as? String
            
            txtFieldProfile.userInteractionEnabled = true
        case 6:
            txtFieldProfile.placeholder = "Mobile Number"
            name = "gen-mobile-txtfield"
            //txtFieldProfile.leftImage = "gen-mobile-txtfield"
            lblDescription?.text = "e.g. +971 501234123 - include all codes"
            txtFieldProfile.keyboardType = .NumbersAndPunctuation
            txtFieldProfile.secureTextEntry = false
            
            txtFieldProfile.text = data["mobileNumber"] as? String
            
            txtFieldProfile.userInteractionEnabled = true
            
        case 7:
            txtFieldProfile.placeholder = "Hospital"
            name = "gen-hospital-txtfield"
            //txtFieldProfile.leftImage = "gen-hospital-txtfield"
            lblDescription?.text = ""
            //  lblDescription?.removeFromSuperview()
            txtFieldProfile.keyboardType = .Default
            txtFieldProfile.secureTextEntry = false
            
            txtFieldProfile.text = data["hospitalName"] as? String
            
            txtFieldProfile.userInteractionEnabled = false
            
        case 8:
            txtFieldProfile.placeholder = "Lab Manager"
            name = "gen-lab-txtfield"
           // txtFieldProfile.leftImage = "gen-lab-txtfield"
            //--ww   txtFieldProfile.rightImage = "gen-downArrow"
            lblDescription?.text = ""
            // lblDescription?.removeFromSuperview()
            txtFieldProfile.keyboardType = .Default
            txtFieldProfile.secureTextEntry = false
            
            txtFieldProfile.userInteractionEnabled = false
            
        case 9:
            
            txtFieldProfile.placeholder = "Roche Rep Email"
            name = ""
            txtFieldProfile.leftImage = ""
            lblDescription?.text = "e.g john@roche.ae"
            txtFieldProfile.keyboardType = .EmailAddress
            txtFieldProfile.secureTextEntry = false
            txtFieldProfile.text = data["salesRepEmail"] as? String
            
            txtFieldProfile.userInteractionEnabled = false
            
        default:
            break
        }
        
        self.setTextFieldImage(txtFieldProfile,imageName: name)
        
        
    }
    
    
    
    func pickerBtnPressed(){
        
        
        delegate?.didTapOnPickerBTN!(self)
        
    }
    
    func btnPressedAction(sender: UIButton!) -> Void {
        
        delegate?.btnPressedAction!(self)
        
    }
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        //        delegate?.didTypeOnPredictiveEmail!(self)
        
    }
    
    
    func textFieldDidChange(textField: UITextField) {
        //your code
        
        if textField.tag == 9
        {
            delegate?.didTypeOnPredictiveEmail!(self)
        }
        else if textField.tag == 7 {
            delegate?.didTypeOnPredictiveHospital!(self)
        }

    }

    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 9 || textField.tag == 7{
            
            
            
            
            
            
        }
        
       
        
        
        if fromWhere == "arrProfileDataReference" {
            
            var txtAfterUpdate:NSString = textField.text! as NSString
            txtAfterUpdate = txtAfterUpdate.stringByReplacingCharactersInRange(range, withString: string)
         
            printLog(txtAfterUpdate)
            arrProfileDataReference.value = txtAfterUpdate as String
            arrProfileDataReference.txtfieldValue = txtAfterUpdate as String
            
        }
        
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if fromWhere == "registerPropertyModelStruct" {
            
            if txtFieldProfile.tag == 7 || txtFieldProfile.tag == 8 {
                arrMainDataReference.txtfieldValue = textField.text!

            }
            else {
            arrMainDataReference.txtfieldValue = textField.text!
            arrMainDataReference.value = textField.text!
            }
            
        }else{
           arrProfileDataReference.value = textField.text!
           arrProfileDataReference.txtfieldValue = textField.text!
            
        }
        
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField.tag == 7 {
            
            textField.resignFirstResponder()
            
            
        }
        else{
            if textField.tag == 9  {
                textField.resignFirstResponder()
            }
            else{
                IQKeyboardManager.sharedManager().goNext()
            }
        }
        
        
        return true
    }
    
    
    func showPicker(arr : Array<String> , arrForID : Array<Int>) -> Void{
        
        
        self.superview?.endEditing(true)
        
        ActionSheetStringPicker.showPickerWithTitle("Choose Category", rows: arr, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            if arr.count > 0{
                
                
                self.txtFieldProfile.text = arr[value]
                
                
                
                var tempIntger : Int = 1
                
                if arr.count == 2
                {
                    tempIntger = tempIntger + 1
                }
                
                
                if self.txtFieldProfile.tag ==  8 {
                    
                    self.arrMainDataReference.txtfieldValue = arr[value]
                    
                    
                    self.arrMainDataReference.value = String(value+tempIntger)
                    
                    
                    self.delegate?.didTapOnPickerDoneBTN!(self)
                    
                    
                    
                }
                else  if self.txtFieldProfile.tag == 7{
                    
                    self.arrMainDataReference.txtfieldValue = arr[value]
                    
                    self.arrMainDataReference.value = String(arrForID[value])
                    
                }
                    
                else if self.txtFieldProfile.tag == 9 {
                    self.arrMainDataReference.value = arr[value]
                    self.arrMainDataReference.txtfieldValue = arr[value]
                    
                }
                
                
                
                
                
            }
            
            
            
            
            
            return
            }, cancelBlock: { ActionStringCancelBlock in
                
                
                
                
                
                return }, origin: txtFieldProfile)
        
        
    }
    
    
    
    func setTextFieldImage(txtField: UITextField , imageName : String  )
    {
        
        
        var bgView = UIView.init()
        var imageView = UIImageView(image: UIImage(named: imageName))
        
        if GlobalStaticMethods.isPad()
        {
            bgView = UIView.init(frame: CGRectMake(0, 0, 0.04882 * DeviceUtil.size.width, CGRectGetHeight(txtField.frame)))
        }
        else
        {
            bgView = UIView.init(frame: CGRectMake(0, 0, 0.084541 * DeviceUtil.size.width, CGRectGetHeight(txtField.frame)))
        }
        
        
        
        imageView.contentMode = UIViewContentMode.Center
        
        
        imageView.frame = CGRectMake(0, 0, imageView.image!.size.width , imageView.image!.size.height)
        imageView.contentMode = .Left

       // bgView.backgroundColor = UIColor.redColor()
        
        bgView .addSubview(imageView)
        
        imageView.center = CGPointMake(bgView.center.x/2 , bgView.center.y)
        
        txtField.leftView = bgView
        
        txtField.leftViewMode = UITextFieldViewMode.Always
    }
    
    
}


