//
//  DetailTableViewCell.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/10/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit
@objc protocol DetailTableViewCellDelegate {
    
    optional func PhoneNoTappedDelegate(cell : DetailTableViewCell) -> Void
    
}

enum SelectedDetail {
    case Phone
    case Email
}

class DetailTableViewCell: UITableViewCell {
    var delegate : DetailTableViewCellDelegate!

    @IBOutlet var btnEmail: UIButton!
    @IBOutlet var btnNumber: UIButton!
    @IBOutlet var lblTitle: BaseUILabel!
    
    
    var seletedItem : SelectedDetail = .Phone

    override func awakeFromNib() {
        super.awakeFromNib()
        
       // self.btnEmail!.addTarget(self, action: #selector(DetailTableViewCell.detailBtnClicked), forControlEvents: .TouchUpInside)
        
        
        self.btnNumber!.addTarget(self, action: #selector(DetailTableViewCell.PhoneNoTapped), forControlEvents: .TouchUpInside)
        
        self.btnEmail!.addTarget(self, action: #selector(DetailTableViewCell.EmailTapped), forControlEvents: .TouchUpInside)

        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func PhoneNoTapped(sender: UIButton!) -> Void {
        
        seletedItem = .Phone
        delegate.PhoneNoTappedDelegate?(self)
    }
    func EmailTapped(sender: UIButton!) -> Void {
        
        seletedItem = .Email
        delegate.PhoneNoTappedDelegate?(self)
    }
   
    
    
    func setupCell(dic: Dictionary<String,AnyObject>)
    {
        
        
        
        
        
        printLog(dic)
        
    }

}
