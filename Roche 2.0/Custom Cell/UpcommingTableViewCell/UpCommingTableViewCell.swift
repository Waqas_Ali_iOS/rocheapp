//
//  UpCommingTableViewCell.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 8/23/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit


@objc protocol upcommingTableViewCellDelegate {
    
    optional func upCommingBtnTappedDelegate(detailBTN : UpCommingTableViewCell) -> Void
    
    
}
class UpCommingTableViewCell: UITableViewCell {

    var delegate : upcommingTableViewCellDelegate?
    
    
    @IBOutlet var lblTimeDate: BaseUILabel?
    
    @IBOutlet var btnDetail: buttonFontCustomClass?
    @IBOutlet var lblLocation: BaseUILabel?
    @IBOutlet var btnSetLocation: buttonFontCustomClass!
    @IBOutlet var lblDescription: UILabel?
    @IBOutlet var lblTitle: UILabel?
    @IBOutlet var lblHeading: UILabel?
    @IBOutlet weak var upperView : UIView!
    @IBOutlet weak var lowerView : UIView!
    @IBOutlet weak var mapBtn : UIButton!
    @IBOutlet weak var lblTitleImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        

            
        
        self.btnDetail!.addTarget(self, action: #selector(UpCommingTableViewCell.detailBtnClicked), forControlEvents: .TouchUpInside)
        
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    func detailBtnClicked(sender: UIButton!) -> Void
    {
        
        printLog(sender.tag)
        
        delegate?.upCommingBtnTappedDelegate?(self)
        
    }

}
