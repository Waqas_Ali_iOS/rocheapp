//
//  AcademyCourseTableViewCell.swift
//  
//
//  Created by Nabeel Siddiqui on 8/22/16.
//
//

import UIKit
@objc protocol suggestedTableViewCellDelegate {
    
    optional func suggestedBtnTappedDelegate(detailBTN : SuggestedCourseTableViewCell) -> Void

    
}
class SuggestedCourseTableViewCell: UITableViewCell {

    var delegate : suggestedTableViewCellDelegate?

    @IBOutlet var btnHeightConstraint: NSLayoutConstraint?

    @IBOutlet var btnDetail: buttonFontCustomClass?
    @IBOutlet var lblDescription: UILabel?
    @IBOutlet var lblTitle: UILabel?
    @IBOutlet var lblHeading: UILabel?
    @IBOutlet var imgCourse: UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.btnDetail!.addTarget(self, action: #selector(SuggestedCourseTableViewCell.detailBtnClicked), forControlEvents: .TouchUpInside)
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
   
    
    func detailBtnClicked(sender: UIButton!) -> Void {
        
        delegate?.suggestedBtnTappedDelegate?(self)
        
    }
///
}
