//
//  CustomCollectionViewCell.swift
//  Roche
//
//  Created by Syed Sharjeel Ali on 8/17/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imgViewUsertype: UIImageView?
    @IBOutlet weak var lblUserdesignationTitle: UILabel?
    @IBOutlet weak var lblUserDesignationTitleText: UILabel?
    @IBOutlet weak var verticalLineView: UIView?
    @IBOutlet weak var horizontalLineView: UIView?
    
    
    func SetupCell(dic : NSDictionary, indexpath : NSInteger)
    {
        
        
        if indexpath == 0 || indexpath == 1  {
            horizontalLineView?.hidden = true
        }
        else
        {
            horizontalLineView?.hidden = false
        }
        
        if GlobalStaticMethods.isPhone(){
            
            if indexpath % 2 != 0  {
                verticalLineView?.hidden = true;
            }
            else
            {
                 verticalLineView?.hidden = false;
            }
            
        }else{
            
            if indexpath == 2 || indexpath == 5{
                verticalLineView?.hidden = true;
            }else{
                verticalLineView?.hidden = false;
            }
            
        }
        
        
       
        
        imgViewUsertype?.image = UIImage.init(imageName: (dic.objectForKey("image") as? String)!)
        
        lblUserdesignationTitle?.text = dic.objectForKey("title") as? String
        lblUserDesignationTitleText?.text = dic.objectForKey("text") as? String
        
    }
    
}
