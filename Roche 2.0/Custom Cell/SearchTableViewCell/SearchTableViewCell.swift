//
//  SearchTableViewCell.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 8/22/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
@objc protocol SearchTableViewCellDelegate {
    
    optional func SearchBtnTappedDelegate(detailBTN : SearchTableViewCell) -> Void
    optional func UnbookMarkBtnTappedDelegate(Cell : SearchTableViewCell) -> Void
    optional func goToSuggestCourse(Cell : SearchTableViewCell) -> Void

}

class SearchTableViewCell: UITableViewCell {

    var delegate : SearchTableViewCellDelegate?

    @IBOutlet var lblTimeDate: BaseUILabel?
    
    @IBOutlet var btnDetail: buttonFontCustomClass?
    @IBOutlet var lblDescription: UILabel?
    @IBOutlet var lblTitle: UILabel?
    @IBOutlet var lblHeading: UILabel?
    @IBOutlet weak var upperView : UIView!
    @IBOutlet weak var lowerView : UIView!
    @IBOutlet var btnHeightConstraint: NSLayoutConstraint?
    @IBOutlet var btnBookMark: UIButton?
    @IBOutlet var btnSuggest: UIButton?
    @IBOutlet var imgCourse: UIImageView?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.btnDetail!.addTarget(self, action: #selector(SearchTableViewCell.detailBtnClicked), forControlEvents: .TouchUpInside)
        self.btnBookMark!.addTarget(self, action: #selector(SearchTableViewCell.unBookBtnClicked), forControlEvents: .TouchUpInside)

        self.btnSuggest!.addTarget(self, action: #selector(SearchTableViewCell.goToSuggestScreen), forControlEvents: .TouchUpInside)

        
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func unBookBtnClicked(sender: UIButton!) -> Void {
        
        delegate?.UnbookMarkBtnTappedDelegate?(self)
        
    }
    func goToSuggestScreen(sender: UIButton!) -> Void {
        
        delegate?.goToSuggestCourse?(self)
        
    }
    func detailBtnClicked(sender: UIButton!) -> Void {
        
        delegate?.SearchBtnTappedDelegate?(self)
        
    }


}
