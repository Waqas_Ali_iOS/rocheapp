//
//  AttendenceListTableViewCell.swift
//  Roche
//
//  Created by  Traffic MacBook Pro on 22/08/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

@objc protocol attendenceListTableViewCellDelegate {
    
    optional func detailBtnTappedDelegate(detailBTN : AttendenceListTableViewCell) -> Void
    
    
}


class AttendenceListTableViewCell: UITableViewCell {
    var delegate : attendenceListTableViewCellDelegate?
    var cellData : attendanceModelClass!

    
    @IBOutlet var lblTimeDate: BaseUILabel?
    
    @IBOutlet var btnDetail: buttonFontCustomClass?
    @IBOutlet var lblLocation: BaseUILabel?
    @IBOutlet var lblDescription: UILabel?
    @IBOutlet var lblTitle: UILabel?
    @IBOutlet var lblHeading: UILabel?
    @IBOutlet weak var upperView : UIView!
    @IBOutlet weak var lowerView : UIView!
    @IBOutlet weak var mapBtn : UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.btnDetail!.addTarget(self, action: #selector(AttendenceTableViewCell.detailBtnClicked), forControlEvents: .TouchUpInside)

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(index : Int , data : attendanceModelClass){
        
        
        cellData = data
        let title = cellData.title
        let courseType = cellData.courseType
        let courseID = cellData.courseID
        
        let location = cellData.location
        let timedate = cellData.timeDate
        
        
        
        
        lblHeading?.text = title
        lblLocation?.text = location
        lblTimeDate?.text = timedate
        
        
        if courseType == courseTypeEnum.online.rawValue {
            lblTitle?.text = "Online Course"
        }
        else if courseType == courseTypeEnum.inClass.rawValue{
            lblTitle?.text = "In-Class Course"
        }
        
    }

    func detailBtnClicked(sender: UIButton!) -> Void {
        
        delegate?.detailBtnTappedDelegate?(self)
        
    }


}
