//
//  AcademyCourseTableViewCell.swift
//  
//
//  Created by Nabeel Siddiqui on 8/22/16.
//
//

import UIKit
@objc protocol AcademyTableViewCellDelegate {
    
    optional func AcademyBtnTappedDelegate(cell : AcademyCourseTableViewCell) -> Void
    optional func UnbookMarkBtnTappedDelegate(cell : AcademyCourseTableViewCell) -> Void
    optional func goToSuggestCourse(Cell : AcademyCourseTableViewCell) -> Void

}

class AcademyCourseTableViewCell: UITableViewCell {

    var delegate : AcademyTableViewCellDelegate?

    @IBOutlet var lblTimeDate: BaseUILabel?
    
    @IBOutlet var btnDetail: buttonFontCustomClass?
    @IBOutlet var lblDescription: UILabel?
    @IBOutlet var lblTitle: UILabel?
    @IBOutlet var lblHeading: UILabel?
    @IBOutlet var btnHeightConstraint: NSLayoutConstraint?
    @IBOutlet var btnBookMark: UIButton?
    @IBOutlet var btnSuggest: UIButton?
    @IBOutlet var imgCourse: UIImageView?


    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.btnDetail!.addTarget(self, action: #selector(AcademyCourseTableViewCell.detailBtnClicked), forControlEvents: .TouchUpInside)
        
        self.btnBookMark!.addTarget(self, action: #selector(AcademyCourseTableViewCell.unBookBtnClicked), forControlEvents: .TouchUpInside)
        self.btnSuggest!.addTarget(self, action: #selector(AcademyCourseTableViewCell.goToSuggestScreen), forControlEvents: .TouchUpInside)

        // Initialization code
    }
    
    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func detailBtnClicked(sender: UIButton!) -> Void {
        
        delegate?.AcademyBtnTappedDelegate?(self)
        
    }

    
    func goToSuggestScreen(sender: UIButton!) -> Void {
        
        delegate?.goToSuggestCourse?(self)
        
    }
    
    func unBookBtnClicked(sender: UIButton!) -> Void {
        
        delegate?.UnbookMarkBtnTappedDelegate?(self)
        
    }
    
}
