//
//  EnrollRequestTableViewCell.swift
//  Roche
//
//  Created by TRA-MBP02 on 10/30/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
@objc protocol EnrollRequestTableViewCellDelegate {
    
    optional func ApproveOrReject(sender : UIButton, forIndex : Int) -> Void
    
}
class EnrollRequestTableViewCell: UITableViewCell {

    @IBOutlet var lblCourseTitle: BaseUILabel?
    @IBOutlet var lblNameDesignation: BaseUILabel?
    @IBOutlet var btnAccept: buttonFontCustomClass?
    @IBOutlet var btnReject: buttonFontCustomClass?
    var delegate : EnrollRequestTableViewCellDelegate?
    var  row : Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.btnAccept!.addTarget(self, action: #selector(EnrollRequestTableViewCell.ApproveOrRejectBtnTap), forControlEvents: .TouchUpInside)
        self.btnReject!.addTarget(self, action: #selector(EnrollRequestTableViewCell.ApproveOrRejectBtnTap), forControlEvents: .TouchUpInside)

        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func ApproveOrRejectBtnTap(sender: UIButton!) -> Void {
        
        delegate?.ApproveOrReject!(sender, forIndex: row)
        
    }

    
    
}
