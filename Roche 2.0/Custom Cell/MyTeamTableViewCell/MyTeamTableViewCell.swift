//
//  MyTeamTableViewCell.swift
//  Roche
//
//  Created by Syed Sharjeel Ali on 10/4/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class MyTeamTableViewCell: UITableViewCell {
    

    
    @IBOutlet var lblName: BaseUILabel?
    @IBOutlet var lblDesignation: BaseUILabel?
    @IBOutlet var lblemail: BaseUILabel?
    @IBOutlet var lblCurrPercentage: BaseUILabel?


    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        lblName?.text = "N/A"
        lblemail?.text = "N/A"
        lblDesignation?.text = "N/A"
        lblCurrPercentage?.text = "N/A"
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupCell(dic : Dictionary <String,AnyObject>)
    {
        lblName?.text = dic["name"] as? String
        lblemail?.text = dic["email"] as? String
        lblDesignation?.text = dic["designation"] as? String
        lblCurrPercentage?.text = String(format: "%.2f%% Curriculum Completed", (dic["currPercentage"] as? Double)!)
    }

}
