//
//  NotificationTableViewCell.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/13/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet weak var viewSuggest : UIView!
    @IBOutlet weak var viewHeading : UIView!
    @IBOutlet weak var imgIdentity : UIImageView!
    
    // -- Reminder Cell 
    
    @IBOutlet var remLblTitle: UILabel!
    @IBOutlet var remLblDescription: UILabel!
    @IBOutlet var remLblTime: UILabel!
    @IBOutlet weak var reImgIdentity : UIImageView!

    //--- Message Cell
    
    @IBOutlet var mesLblTitle: UILabel!
    @IBOutlet var mesLblDescription: UILabel!
    @IBOutlet var mesLblTime: UILabel!
    @IBOutlet var mesLblMessage: UILabel!
    @IBOutlet weak var mesImgIdentity : UIImageView!
    var row = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
