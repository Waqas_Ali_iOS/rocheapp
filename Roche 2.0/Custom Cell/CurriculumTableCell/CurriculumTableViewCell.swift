//
//  CurriculumTableViewCell.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 8/15/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit


@objc protocol CurriculumTableViewCellDelegate {
    
    optional func detailBtnTappedDelegate(detailBTN : CurriculumTableViewCell) -> Void
    
    
    optional func bookMarkBtnTappedDelegate(detailBTN : CurriculumTableViewCell) -> Void
    
    optional func SuggestCourseTappedDelegate(detailBTN : CurriculumTableViewCell) -> Void
    
    
}
class CurriculumTableViewCell: UITableViewCell {

    
    
    var delegate: CurriculumTableViewCellDelegate?
    
    
    @IBOutlet weak var imgCourseType: UIImageView!
    @IBOutlet weak var lblCourseType: BaseUILabel?
    @IBOutlet weak var btnBookMark: UIButton?
    @IBOutlet weak var btnShare: UIButton?
    
    
    @IBOutlet weak var lblCTitle: BaseUILabel?
    
    @IBOutlet weak var lblCourseDesc: BaseUILabel?
    
    
    
    
    
    

    @IBOutlet weak var upperView : UIView!
    @IBOutlet weak var lowerView : UIView!
    @IBOutlet weak var mapBtn : UIButton!
    
    @IBOutlet weak var btnCourseStatus: BaseUIButton?
    @IBOutlet weak var btnCourseDetail : UIButton?
    
    
    @IBOutlet weak var btnCourseStatusiPad: buttonFontCustomClass?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code


    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func detailBtnClicked(sender: UIButton!) -> Void {
        
            delegate?.detailBtnTappedDelegate?(self)
        
    }
    
    
    func bookMarkBtnTappedDelegate(sender: UIButton!) -> Void {
        
        delegate?.bookMarkBtnTappedDelegate?(self)
        
    }
    
    
    func SuggestCourseTappedDelegate(sender: UIButton!) -> Void {
        
        delegate?.SuggestCourseTappedDelegate?(self)
        
    }
    
    
    
    func setupCell(dic : CurriculumModel  , Index : Int)
    {
        //imgCourseType
        //lblCourseType
        //btnBookMark
        //btnShare
        
        if (dic.courseType ) == AFUrlRequestSharjeel.courseType.Online.rawValue
        {
            imgCourseType!.image = UIImage.init(imageName: "certificate-online-img")
            lblCourseType?.text = "Online Course"
        }
        else
        {
            imgCourseType!.image = UIImage.init(imageName: "certificate-inClass-img")
            lblCourseType?.text = "In-Class Course"
        }
        
        
        var strBtnTitle = ""
        
        var imageName = ""
        
        
        switch dic.courseStatus {
        case CourseStatus.NotEnrolled.rawValue:
            strBtnTitle = "Not Enrolled"
            imageName = "gen-enrolled"
            break
        case CourseStatus.ApprovalPending.rawValue:
            strBtnTitle = "Approval Pending"
            imageName = "gen-enrolled"
            break
        case CourseStatus.Enrolled.rawValue:
            strBtnTitle = "Enrolled"
            imageName = "gen-enrolled"
            break
        case CourseStatus.Passed.rawValue:
            strBtnTitle = "Passed"
            imageName = "curriculum-pass-img"
            break
        default:
            break
        }
        
        if GlobalStaticMethods.isPad()
        {
            self.btnCourseStatusiPad?.setTitle(strBtnTitle, forState: .Normal)
            self.btnCourseStatusiPad?.setImage(UIImage.init(imageName: imageName), forState: .Normal)
        }
        else
        {
            self.btnCourseStatus?.setTitle(strBtnTitle, forState: .Normal)
            self.btnCourseStatus?.setImage(UIImage.init(imageName: imageName), forState: .Normal)
        }
        
        
        
        
        
        
        if dic.bookmarked == 1
            
        {
            if GlobalStaticMethods.isPad(){
                self.btnBookMark!.setBackgroundImage(UIImage(imageName :"Gen-bookmark-img-sel" ), forState: .Normal)
            }
            else{
                self.btnBookMark!.setImage(UIImage(named: "Gen-bookmark-img-sel"), forState: UIControlState.Normal)
            }
            
        }else{
            if GlobalStaticMethods.isPad(){
                self.btnBookMark!.setBackgroundImage(UIImage(imageName :"Gen-BookMark-Btn" ), forState: .Normal)
            }
            else{
                self.btnBookMark!.setImage(UIImage(named: "gen-bookmark"), forState: UIControlState.Normal)
            }
            
        }
        
        
        lblCTitle?.text = dic.title
        lblCourseDesc?.text = dic.shortDescription
        
        btnCourseDetail?.tag = Index;
        btnBookMark?.tag = Index;
        btnShare?.tag = Index;
        
        
        btnCourseDetail?.addTarget(self, action: #selector(CurriculumTableViewCell.detailBtnClicked), forControlEvents: .TouchUpInside)
        
        btnBookMark?.addTarget(self, action: #selector(CurriculumTableViewCell.bookMarkBtnTappedDelegate(_:)), forControlEvents: .TouchUpInside)
        
        btnShare?.addTarget(self, action: #selector(CurriculumTableViewCell.SuggestCourseTappedDelegate(_:)), forControlEvents: .TouchUpInside)
        
        
        
        
//        @IBOutlet weak var btnCourseStatus: BaseUIButton!
//        @IBOutlet weak var btnCourseDetail : UIButton?
//        @IBOutlet weak var btnBookMark: UIButton!
//        @IBOutlet weak var btnShare: UIButton!
        
        
        
    }
    
    



    enum CourseStatus : Int {
        case NotEnrolled = 1,
        ApprovalPending,
        Enrolled,
        Passed
    }


}
