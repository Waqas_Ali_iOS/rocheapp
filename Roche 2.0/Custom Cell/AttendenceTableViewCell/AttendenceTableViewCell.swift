//
//  AttendenceTableViewCell.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/11/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit


@objc protocol attendenceTableViewCellDelegate {
    
    optional func detailBtnTappedDelegate(cell : AttendenceTableViewCell) -> Void
    
    
}

class AttendenceTableViewCell: UITableViewCell {
    
    
    var delegate : attendenceTableViewCellDelegate?
    
    
    var cellData : attendanceModelClass!
    
    @IBOutlet var lblTimeDate: BaseUILabel?
    
    @IBOutlet var btnDetail: buttonFontCustomClass?
    @IBOutlet var lblLocation: BaseUILabel?
    @IBOutlet var btnSetLocation: buttonFontCustomClass!
    @IBOutlet var lblDescription: UILabel?
    @IBOutlet var lblTitle: UILabel?
    @IBOutlet var lblHeading: UILabel?
    @IBOutlet weak var upperView : UIView!
    @IBOutlet weak var lowerView : UIView!
    @IBOutlet weak var mapBtn : UIButton!
    @IBOutlet var btnHeightConstraint: NSLayoutConstraint?
    @IBOutlet var coursetypeImg : UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    
        self.btnSetLocation!.addTarget(self, action: #selector(AttendenceTableViewCell.detailBtnClicked), forControlEvents: .TouchUpInside)
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func configCell(index : Int , data : attendanceModelClass){
        
        
        cellData = data
        let title = cellData.title
        let description = cellData.shortDescription
        let courseType = cellData.courseType
        let courseID = cellData.courseID
        
        
        
        lblHeading?.text = title
        lblDescription?.text = description
        
        
        if courseType == courseTypeEnum.online.rawValue {
            lblTitle?.text = "Online Course"
            coursetypeImg.image = UIImage (named: Constants.ONLINE_COURSE_IMAGE)
            
            
        }
        else if courseType == courseTypeEnum.inClass.rawValue{
            lblTitle?.text = "In-Class Course"
            
            coursetypeImg.image = UIImage (named: Constants.INCLASS_COURSE_IMAGE)

        }
        
    }
    
    
    func detailBtnClicked(sender: UIButton!) -> Void {
        
        delegate?.detailBtnTappedDelegate?(self)
        
    }
}
