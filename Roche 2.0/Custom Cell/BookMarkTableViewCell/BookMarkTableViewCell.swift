//
//  BookMarkTableViewCell.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 8/22/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
@objc protocol bookMarkTableViewCellDelegate {
    
    optional func bookMarkBtnTappedDelegate(detailBTN : BookMarkTableViewCell) -> Void
    optional func UnbookMarkBtnTappedDelegate(unBookBTN : BookMarkTableViewCell) -> Void
    
}


class BookMarkTableViewCell: UITableViewCell {

    var delegate : bookMarkTableViewCellDelegate?

    @IBOutlet var btnHeightConstraint: NSLayoutConstraint?

    @IBOutlet var btnDetail: buttonFontCustomClass?
    @IBOutlet var lblDescription: UILabel?
    @IBOutlet var lblTitle: UILabel?
    @IBOutlet var lblHeading: UILabel?
    @IBOutlet var BtnUnbook: UIButton?
    @IBOutlet var imgCourse: UIImageView?


    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.btnDetail!.addTarget(self, action: #selector(BookMarkTableViewCell.detailBtnClicked), forControlEvents: .TouchUpInside)
        self.BtnUnbook!.addTarget(self, action: #selector(BookMarkTableViewCell.unBookBtnClicked), forControlEvents: .TouchUpInside)

        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func detailBtnClicked(sender: UIButton!) -> Void {
        
        delegate?.bookMarkBtnTappedDelegate?(self)
        
    }

    func unBookBtnClicked(sender: UIButton!) -> Void {
        
        delegate?.UnbookMarkBtnTappedDelegate?(self)
        
    }
    
}
