//
//  ReplyMessageTableViewCell.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 8/15/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit



class ReplyMessageTableViewCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet var txtView : UITextView!
    @IBOutlet var btnAssignment : UIButton!
    
   

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        txtView.text = "Type your message here!"
        txtView.delegate = self
        
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
 
    
    func textViewDidBeginEditing(textView: UITextView) {
        
        if textView.text == "Type your message here!"
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(textView: UITextView)  {
        if textView.text == ""
        {
            textView.text = "Type your message here!"
        }
    }


}
