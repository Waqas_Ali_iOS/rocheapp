//
//  MessageTableViewCell.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 7/12/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit
import SwiftyJSON

@objc protocol MessageTableViewCellDelegate {
    
    optional func btnAssignmentDidClick(cell : MessageTableViewCell, indexNo: Int ) -> Void
    
}

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var lblHeading : UILabel!
    @IBOutlet weak var viewSeperator : UIView!

    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblTimeDate : UILabel!
    @IBOutlet weak var imgTimeDate : UIImageView!

    @IBOutlet weak var btnAssignment : UIButton!
    @IBOutlet weak var ViewAssignment : UIView!

    
    @IBOutlet weak var viewAssignmentHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewAssignmentBottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewAssignmentTopConstraint: NSLayoutConstraint!
    
     var delegate : MessageTableViewCellDelegate?
    
    var indexPathNo : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnAssignment.addTarget(self, action: #selector(MessageTableViewCell.btnAssignmentTapped), forControlEvents: .TouchUpInside)
        
    }

    
    func btnAssignmentTapped(){
        
        delegate?.btnAssignmentDidClick!(self, indexNo: indexPathNo!)
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data : Dictionary<String, AnyObject>) {
        
       
        
        lblHeading.text = data["title"] as? String
       lblDescription.text = data["shortDescription"] as? String
        lblTimeDate.text = data["timeDate"] as? String
        
        btnAssignment.setTitle(data["attachTitle"] as? String, forState: .Normal)
        
        
        if data["read"]?.integerValue == 1 {
        self.backgroundColor = UIColor (hexString: "#a9acb2")
            
            self.backgroundColor = UIColor (hexString: "#eceff4")
            lblHeading.textColor = UIColor (hexString: "#7b7b7b")
            lblDescription.textColor = UIColor (hexString: "#7b7b7b")
            lblTimeDate.textColor = UIColor (hexString: "#a9acb2")
            viewSeperator.backgroundColor = UIColor (hexString: "#7b7b7b")
            
            
            imgTimeDate.image = imgTimeDate.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            imgTimeDate.tintColor = UIColor (hexString: "#a9acb2")
            btnAssignment.setTitleColor(UIColor (hexString: "#a9acb2"), forState: UIControlState.Normal)
            btnAssignment.enabled = false
            
            
            let image = UIImage(named: "Message-Attachment-icon")?.imageWithRenderingMode(.AlwaysTemplate)
            btnAssignment.setImage(image, forState: .Normal)
            btnAssignment.tintColor = UIColor (hexString: "#a9acb2")
            
           
           
        }else{
        self.backgroundColor = UIColor.whiteColor()
            
            
            
            
            
            lblHeading.textColor = UIColor (hexString: "#3E8ADA")
            lblDescription.textColor = UIColor (hexString: "#3E8ADA")
            lblTimeDate.textColor = UIColor (hexString: "#3E8ADA")
            viewSeperator.backgroundColor = UIColor (hexString: "#007AFF")
            
            
            imgTimeDate.image = imgTimeDate.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            imgTimeDate.tintColor = UIColor (hexString: "#3E8ADA")
            btnAssignment.setTitleColor(UIColor (hexString: "#3E8ADA"), forState: UIControlState.Normal)
            btnAssignment.enabled = false
            
            
            let image = UIImage(named: "Message-Attachment-icon")?.imageWithRenderingMode(.AlwaysTemplate)
            btnAssignment.setImage(image, forState: .Normal)
            btnAssignment.tintColor = UIColor (hexString: "#3E8ADA")
        }
        
        if GlobalStaticMethods.isPhone() {
        
        if btnAssignment.currentTitle == "" {
            
            viewAssignmentHeightConstraint.constant = 0
            
            viewAssignmentTopConstraint.constant = 0
            viewAssignmentBottomConstraint.constant = 0
            viewAssignmentTopConstraint.priority = 250
            
        }else{
            viewAssignmentHeightConstraint.constant = 24
            viewAssignmentTopConstraint.constant = 8
            viewAssignmentBottomConstraint.constant = 9
             viewAssignmentTopConstraint.priority = 750
            
        }
        }else{
            if btnAssignment.currentTitle == "" {
                
              ViewAssignment.hidden = true
                
            }else{
               ViewAssignment.hidden = false
                
            }
        
        
        }
        
    }
    
    func setDataReplyMessage(data : JSON) {
        
//        lblHeading.text = data["sender"] as? String
//        lblDescription.text = data["description"] as? String
//        lblTimeDate.text = data["timeDate"] as? String
        
        
        
        lblHeading.text = data["sender"].stringValue
        lblDescription.text = data["description"].stringValue
        lblTimeDate.text = data["timeDate"].stringValue

        
        btnAssignment.setTitle(data["attachTitle"].stringValue, forState: .Normal)
        
        if btnAssignment.currentTitle == "" {
        ViewAssignment.hidden  = true
        }else{
         ViewAssignment.hidden  = false
        
        }
    
    }

}
