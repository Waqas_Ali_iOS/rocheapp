//
//  MyCertificateTableViewCell.swift
//  Roche
//
//  Created by Nabeel Siddiqui on 6/29/16.
//  Copyright © 2016 Nabeel Siddiqui. All rights reserved.
//

import UIKit



@objc protocol MyCertificateTableViewCellDelegate {
    
    optional func detailBtnPressed(myCertificateTableViewCell : MyCertificateTableViewCell) -> Void
    
}

class MyCertificateTableViewCell: UITableViewCell {

    @IBOutlet var btnViewCertificate: buttonFontCustomClass!
    @IBOutlet var viewBTnHeightConstraint: NSLayoutConstraint!
    @IBOutlet var imgCourseType: CustomImageView!
    @IBOutlet var lblCourseType: BaseUILabel!
    @IBOutlet var btnDetails: buttonFontCustomClass!
    @IBOutlet var lblTitle: BaseUILabel!
    
    var strCerificateUrl = ""
    
    
    var delegate : MyCertificateTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    func setupCell(dic: Dictionary<String,AnyObject>, index : Int)
    {
//        lblName?.text = dic["name"] as? String
//        lblemail?.text = dic["email"] as? String
//        lblDesignation?.text = dic["designation"] as? String
        
        lblTitle?.text = dic["title"] as? String
        strCerificateUrl = dic["certificate"] as! String
        
        if (dic["courseType"] as! Int) == AFUrlRequestSharjeel.courseType.Online.rawValue
        {
            
            imgCourseType?.image = UIImage.init(named:"certificate-online-img")
            lblCourseType?.text = "Online Course"
        }
        else
        {
            imgCourseType?.image = UIImage.init(named: "certificate-inClass-img")
            lblCourseType?.text = "In-Class Course"
        }
        
        
        btnDetails.tag = index
        
        btnViewCertificate.addTarget(self, action: #selector(self.viewCertificate), forControlEvents: .TouchUpInside)
        
        btnDetails.addTarget(self, action: #selector(self.callDelegate), forControlEvents: .TouchUpInside)
        
        
        btnViewCertificate.backgroundColor = UIColor.init(hexString: "#B6BCC6")
        
        printLog(dic)
    }
    
    
    func viewCertificate()
    {
        
        let urlStr : NSString = strCerificateUrl.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        
        let url = NSURL(string: urlStr as String)!
        UIApplication.sharedApplication().openURL(url)
    }
    
    func callDelegate()
    {
        delegate?.detailBtnPressed!(self)
    }

}
