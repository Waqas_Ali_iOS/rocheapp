//
//  leftMenuTableViewCell.swift
//  RocheDesignsOnly
//
//  Created by  Traffic MacBook Pro on 26/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class leftMenuTableViewCell: UITableViewCell {

    
    @IBOutlet var imgCell : UIImageView!
    @IBOutlet var lblCell : UILabel!
    @IBOutlet var cellSeparatorLine : UIView!
    
    @IBOutlet weak var SeperatorHeightConstraint: NSLayoutConstraint?
  //  @IBOutlet var btnCell : UIButton!
    
    var arrImages = ["leftmenu-profile","leftmenu-logout","leftmenu-invite","leftmenu-home","leftmenu-academyCourses","leftmenu-upcomingEvents","leftmenu-curriculum","leftmenu-team","leftmenu-enrollrequest","leftmenu-progress","leftmenu-messages","leftmenu-certificates","leftmenu-bookmarks","leftmenu-suggested","leftmenu-contact"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgCell.backgroundColor = UIColor.clearColor()
        imgCell.contentMode = .Center
       }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func configureCell(forRowAtIndexPath: NSIndexPath, dataArr: Array<String>) {
        print("asdasd")
        let count = forRowAtIndexPath.row
        SeperatorHeightConstraint?.constant = 0
        
        if count < 3 {
            self.lblCell.textColor = UIColor.whiteColor()
            
            self.contentView.backgroundColor = UIColor(hexString: "#113c8b")
            cellSeparatorLine.backgroundColor = UIColor(hexString: "#3862ae")
            
            
            
        }
        else{
            self.lblCell.textColor = UIColor(hexString: "#113c8b")
            

            self.contentView.backgroundColor = UIColor.clearColor()
            cellSeparatorLine.backgroundColor = UIColor(hexString: "#113c8b")
        }
        
        
        lblCell.text = dataArr[forRowAtIndexPath.row]
        let strImage = arrImages[forRowAtIndexPath.row]

        let imageCell = UIImage(imageName: strImage)
    
        imgCell.image = imageCell
        
        
        SeperatorHeightConstraint?.constant = 1
        cellSeparatorLine.hidden = true
        
        
        
    }
}
