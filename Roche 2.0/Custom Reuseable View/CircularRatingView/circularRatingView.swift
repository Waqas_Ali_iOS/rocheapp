//
//  circularRatingView.swift
//  BaseProject
//
//  Created by  Traffic MacBook Pro on 18/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import QuartzCore


@IBDesignable
public class circularRatingView: UIView {

    var backgroundCircle : CAShapeLayer!
    var ratingCircle : CAShapeLayer!
    var innerCircle : CAShapeLayer!
    
   
    @IBInspectable var borderColorForBackground : String?
    @IBInspectable var ratingCircleColor : String? /// UIColor(white: 0.5, alpha: 0.3)

     var rating : Double = 0.5{
        didSet {
            
            
                self.updateRatings()
                
                   }
    }
    
    var lineWidth : Double = 13{
        didSet {
            updateRatings()
        }
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        
        
        if (backgroundCircle == nil) {
            backgroundCircle = CAShapeLayer()
            layer.addSublayer(backgroundCircle)
            
            
            let rect = CGRectInset(bounds, CGFloat(lineWidth / 2.0), CGFloat(lineWidth / 2.0))
            let path = UIBezierPath(ovalInRect: rect)
            backgroundCircle.path = path.CGPath
            backgroundCircle.fillColor = nil
            backgroundCircle.lineWidth = CGFloat(lineWidth)
            backgroundCircle.strokeColor = UIColor(hexString: borderColorForBackground ?? "#FFFFFF").CGColor
            
        }
        
        backgroundCircle.frame = layer.bounds
        if (ratingCircle == nil){
            ratingCircle = CAShapeLayer()
            
            let innerPath = CGRectInset(bounds, CGFloat(lineWidth / 2.0), CGFloat(lineWidth / 2.0))
            let path = UIBezierPath(ovalInRect: innerPath)
            ratingCircle.path = path.CGPath
            ratingCircle.fillColor = nil
            ratingCircle.lineWidth = CGFloat(lineWidth)
            ratingCircle.strokeColor = UIColor(hexString: ratingCircleColor ?? "#000000").CGColor
            ratingCircle.anchorPoint = CGPointMake(0.5, 0.5)
            ratingCircle.transform = CATransform3DRotate(ratingCircle.transform, CGFloat(-M_PI / 2), 0, 0, 1)
            layer.addSublayer(ratingCircle)
           // updateRatings()

            
        }
        
        
       
        
        ratingCircle.frame = layer.bounds
        
       // UIView.animateWithDuration(5, delay: 2, options: .TransitionNone, animations: {
            self.updateRatings()

         //   }) { (success) in
           //     print(success)
       // }
        
        
        
        
        
        
        
    }
    
     func updateRatings(){
        

        if  (ratingCircle != nil) {
            
            
           // UIView.animateWithDuration(5, delay: 10, options: .TransitionNone, animations: {

                self.ratingCircle.strokeEnd = CGFloat(self.rating)
                self.layoutIfNeeded()
            //}) { (success) in
              //  print(success)
            //}

            
        }
        
    }
    
//    public func updateRatingFromVC(ratingFromVC: Double ) -> Void {
//        
//        rating  = ratingFromVC
//      
//        
//        
//    }

    
    
    
}
