//
//  HeaderView.swift
//  Skeleton
//
//  Created by Waqas Ali on 12/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit


@objc protocol HeaderViewDelegate {
   
    optional func headerViewLeftBtnDidClick(headerView : HeaderView) -> Void
    optional func headerViewRightBtnDidClick(headerView : HeaderView) -> Void
    optional func headerViewRightSecondBtnDidClick(headerView : HeaderView) -> Void
}

public class HeaderView: UIView {
    
     var delegate: HeaderViewDelegate?
    @IBOutlet var leadingConstraintBtnLeft : NSLayoutConstraint?
    @IBOutlet var trailingConstraintBtnRight : NSLayoutConstraint?

    @IBOutlet weak var btnRightLeadingConstraintToImage: NSLayoutConstraint? // 0
    @IBOutlet weak var btnRightLeadingConstraintToHeading: NSLayoutConstraint? // 18
   
    @IBOutlet weak var lblHeading: BaseUILabel!
    
    @IBOutlet var imgHeading: UIImageView!

    @IBOutlet var btnLeft: UIButton!
    
    @IBOutlet var btnRight: UIButton!
    
    @IBOutlet var viewContainer: UIView!
    
   
    @IBOutlet weak var btnFooter: UIButton!
    
    var bgColorHeighlightedFooter : String?
    
    
    private var _leftButtonImage : String? = nil
    var leftButtonImage : String?{
//        get {
//            return _leftButtonImage
//        }
        didSet
        {
            
            if (leftButtonImage != "" && (leftButtonImage  == _leftButtonImage))
            {return}
            //--
            _leftButtonImage = leftButtonImage
            //--
            
            if (_leftButtonImage != "")
            {
                self.btnLeft.hidden = false
              //  let imge = UIImage(imageName: <#T##String#>)
                printLog(_leftButtonImage)

                self.btnLeft.setImage(UIImage(imageName:_leftButtonImage! ), forState: .Normal)
                
            }
            else{
                self.btnLeft.hidden = true
            }
        }
    }
  
    
    private var _rightButtonImage : String? = nil
    
    var rightButtonImage : String? {
               didSet {
                
                if (rightButtonImage != "" && (rightButtonImage  == _rightButtonImage))
                {return}
                //--
                _rightButtonImage = rightButtonImage
                //--
                
                if (_rightButtonImage != "")
                {
                    
                    printLog(_rightButtonImage)
                    self.btnRight.hidden = false
                    self.btnRight.setImage(UIImage(imageName:_rightButtonImage! ), forState: .Normal)
                    
                }
                else{
                    self.btnRight.hidden = true
                }

        }
    }
    
    
    private var _heading : String? = nil
    var heading : String?{
     
        didSet {
            _heading = heading;
            if(_heading != nil){
            self.lblHeading.hidden = false
            self.lblHeading.text = heading
            }
            
        }
    }
  
    var _bgImage : String?{
        
        didSet {
            let hImage : UIImage! = UIImage(named: _bgImage!)
            
            imgHeading.hidden = false
            imgHeading.image = hImage
              self.lblHeading.hidden = true
            
        }
        
    }
    private var _bgColorVar : String?
    var bgColorVar : String?{
        
        didSet {
            _bgColorVar = bgColorVar
            if ((bgColorVar) != nil)
            {
                self.viewContainer.backgroundColor = UIColor(hexString: bgColorVar!)
                
            }

        }
    }
    
    var _isBackgroundColored : Bool?
    
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    public override func layoutSubviews() {
        
        if GlobalStaticMethods.isPad() {
        if  GlobalStaticMethods.isPadPro() {
            
          
         leadingConstraintBtnLeft?.constant = 15
         trailingConstraintBtnRight?.constant = -35
         btnRightLeadingConstraintToHeading?.constant = 45
         btnRightLeadingConstraintToImage?.constant = 63
        }else{
           // leadingConstraintBtnLeft?.constant = 10
           // trailingConstraintBtnRight?.constant = 10
           // leadingConstraintBtnLeft?.constant = 10
            trailingConstraintBtnRight?.constant = -25
            btnRightLeadingConstraintToHeading?.constant = 35
            btnRightLeadingConstraintToImage?.constant = 53
            
        }
        }else{
        
            trailingConstraintBtnRight?.constant = 7
            btnRightLeadingConstraintToHeading?.constant = 3
            btnRightLeadingConstraintToImage?.constant = 21
        
        }

    }
    
    var view:UIView!;
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib ()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       // loadViewFromNib ()
    }
    func loadViewFromNib() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "HeaderView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.addSubview(view);
        
        btnLeft.addTarget(self, action: #selector(HeaderView.leftbuttonClicked(_:)), forControlEvents: .TouchUpInside)
        
        btnRight.addTarget(self, action: #selector(HeaderView.rightbuttonClicked(_:)), forControlEvents: .TouchUpInside)
        
       
        
    }

    
    func leftbuttonClicked(sender: UIButton!) -> Void {
        
         delegate?.headerViewLeftBtnDidClick?(self)
        
    }
    
    func rightbuttonClicked(sender: UIButton!) -> Void {
          delegate?.headerViewRightBtnDidClick?(self)
    }
  
    
    public func updateHeaderWithHeadingText(hText: String, rightBtnImageName: String, leftBtnImageName: String, backgroundImageName: String  ) -> Void {
}
    
    public func updateHeaderWithHeadingText(hText: String, rightBtnImageName: String, leftBtnImageName: String, backgroundImageName: String, bgColor: String  ) -> Void {
        
         rightButtonImage  = rightBtnImageName
         leftButtonImage    = leftBtnImageName
         bgColorVar = bgColor
         heading = hText
        _bgImage  = backgroundImageName;
        
        
    }
    

 
  
}
