//
//  BaseUITextView.swift
//  SampleSwift
//
//  Created by  Traffic MacBook Pro on 5/17/16.
//  Copyright © 2016 Traffic MacBook Pro. All rights reserved.
//

import UIKit

class BaseUITextView: UITextView {
    @IBInspectable var iPhoneFontSize : CGFloat = 50.0
    @IBInspectable var iPadFontSize : CGFloat = 50.0
    @IBInspectable var iPadProFontSize : CGFloat = 50.0
    //    @IBInspectable var imageForIpadPro : String?
    @IBInspectable var fontName : String = "MarselisPro"
    let defaultFontVal: CGFloat = 50.0

    
    
    
    override func awakeFromNib() {
        self .customizeFontSizes()
    }
    
    func customizeFontSizes(){
        
        let myfont = self.font
        
        if GlobalStaticMethods.isPhone(){
            self.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "MarselisPro", psdFontSize:iPhoneFontSize  ?? defaultFontVal)
            
        }
        else{
            if GlobalStaticMethods.isPadPro(){
                self.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "MarselisPro", psdFontSize:iPadProFontSize ?? defaultFontVal)

            }
            else {
                self.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "MarselisPro", psdFontSize:iPadFontSize ?? defaultFontVal)

            }
            
        }
        
        
    }
    

}
