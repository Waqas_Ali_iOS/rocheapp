//
//  BaseUILabel.swift
//  SampleSwift
//
//  Created by  Traffic MacBook Pro on 5/17/16.
//  Copyright © 2016 Traffic MacBook Pro. All rights reserved.
//

import UIKit

class BaseUILabel: UILabel {
    
    @IBInspectable var iPhoneFontSize : CGFloat = 50.0{
        didSet{
        self .customizeFontSizes()
        
        }
    
    
    }
    @IBInspectable var iPadFontSize : CGFloat = 50.0
    @IBInspectable var iPadProFontSize : CGFloat = 50.0
    //    @IBInspectable var imageForIpadPro : String?
    @IBInspectable var fontName : String = ""
    let defaultFontVal : CGFloat = 50.0
    
    
    
    
    override func awakeFromNib() {
        self .customizeFontSizes()
    }
    
    func customizeFontSizes(){
//        if iPhoneFontSize < 47{
//            iPhoneFontSize = 50
//        }
//        if iPadFontSize < 50{
//            iPadFontSize = 53
//        }
//        if iPadProFontSize < 50{
//            iPadProFontSize = 53
//        }

        let myfont = self.font
        if GlobalStaticMethods.isPhone(){
            self.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "Tahoma", psdFontSize:iPhoneFontSize ?? defaultFontVal)
            
        }
        else{
            if GlobalStaticMethods.isPadPro(){
                self.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "Tahoma", psdFontSize:iPadProFontSize ?? defaultFontVal)
                
            }
            else {
                self.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "Tahoma", psdFontSize:iPadFontSize ?? defaultFontVal)
                
            }
            
        }
        
        
        
    }
    
}
