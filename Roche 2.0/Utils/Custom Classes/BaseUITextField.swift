 //
 //  BaseUITextField.swift
 //  SampleSwift
 //
 //  Created by  Traffic MacBook Pro on 5/17/16.
 //  Copyright © 2016 Traffic MacBook Pro. All rights reserved.
 //
 
 import UIKit
 
 class BaseUITextField: UITextField {
    @IBInspectable var underLineTextfield : Bool = false
    @IBInspectable var customizeFont: Bool = false
    
    
    
    @IBInspectable var iPhoneFontSize : CGFloat = 50.0
    @IBInspectable var iPadFontSize : CGFloat = 50.0
    @IBInspectable var iPadProFontSize : CGFloat = 50.0
    //    @IBInspectable var imageForIpadPro : String?
    @IBInspectable var fontName : String = ""
    @IBInspectable var bottomBorderHeight : CGFloat = 1
    @IBInspectable var bottomBorderColor : String = ""
    
    @IBInspectable var ShowAcitvityIndicator :  Bool = false
    
    @IBInspectable var spaceLeftImage : CGFloat = 10.0
    
    
    @IBInspectable var leftImage : String! = ""{
        didSet{
            
            
            setImage()
        }
    }
    @IBInspectable var rightImage : String! = ""{
        didSet{
            setImage()
        }
    }
    
    var bottomBorder : UIView!
    let defaultFontVal: CGFloat = 50.0
    
    var imageView = UIImageView.init()
    
    var activityIndicator = UIActivityIndicatorView.init()
    
    //    override func drawRect(rect: CGRect) {
    //
    //
    //    }
    
    override func awakeFromNib() {
        
        
        //      self.setImage()
        
        if customizeFont == true {
            self .customizeFontSizes()
        }
        
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        if underLineTextfield == true{
            
            self.underlineUpdatedMethod()
        }
        
    }
    
    
    func customizeFontSizes(){
        
        let myfont = self.font
        
        
        if GlobalStaticMethods.isPhone(){
            self.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "MarselisPro", psdFontSize:iPhoneFontSize ?? defaultFontVal)
            
        }
        else{
            if GlobalStaticMethods.isPadPro(){
                self.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "MarselisPro", psdFontSize:iPadProFontSize ?? defaultFontVal)
                
            }
            else {
                self.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "MarselisPro", psdFontSize:iPadFontSize ?? defaultFontVal)
                
            }
            
        }
        
        
    }
    
    
    func underlineUpdatedMethod(){
        
        if bottomBorder == nil {
            if bottomBorderColor == "" {
                bottomBorderColor = "#FFFFFF"
            }
            if bottomBorderHeight < 1 {
                bottomBorderHeight = 1
            }
            
            
            bottomBorder =  UIView(frame: CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: bottomBorderHeight))
            bottomBorder.backgroundColor = UIColor(hexString: bottomBorderColor ?? "#FFFFFF")
            
            self.addSubview(bottomBorder)
        }
        
        bottomBorder.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: bottomBorderHeight)
        
        
        
    }
    func createUnderLine(){
        
        self.borderStyle = UITextBorderStyle.None
        
        var deviceVal : CGFloat = 2
        if constants.deviceType.IS_IPHONE_5{
            deviceVal = 10
        }
        else if constants.deviceType.IS_IPHONE_6{
            deviceVal = 4
        }
        else if constants.deviceType.IS_IPHONE_6P{
            deviceVal = 2
        }
        
        
        let bottomLine = CALayer()
        let yPosition = self.frame.height - deviceVal
        bottomLine.frame = CGRectMake(0.0, yPosition, self.frame.width, 1)
        bottomLine.backgroundColor = UIColor.whiteColor().CGColor
        self.layer.borderColor = UIColor.clearColor().CGColor
        self.layer.backgroundColor = UIColor.clearColor().CGColor
        self.layer.borderWidth = 0.0
        self.layer.addSublayer(bottomLine)
        self.layer.masksToBounds = true // the most important line of code
        
    }
    
    func setImage()
    {
        
        
        if leftImage != ""
        {
            /* --ww not neccessary for now
             
             var bgView = UIView.init()
             
             if GlobalStaticMethods.isPad()
             {
             bgView = UIView.init(frame: CGRectMake(0, 0, CGRectGetWidth(self.frame) * 0.05, CGRectGetHeight(self.frame)))
             }
             else
             {
             bgView = UIView.init(frame: CGRectMake(0, 0, CGRectGetWidth(self.frame) * 0.075, CGRectGetHeight(self.frame)))
             }
             
             
             */
            
            
            
            
            imageView = UIImageView(image: UIImage(named: leftImage))
            imageView.contentMode = UIViewContentMode.ScaleAspectFit
            
            //--ww  imageView.frame = CGRectMake(0,(CGRectGetHeight(self.frame) -  CGRectGetHeight(self.frame)*0.80)/2, CGRectGetWidth(self.frame)*0.05, CGRectGetHeight(self.frame)*0.80)
            
            imageView.frame = CGRectMake(0, 0, imageView.image!.size.width + spaceLeftImage, imageView.image!.size.height)
            
            imageView.contentMode = .Left
            
           // printLog("Image width is  \(imageView.frame.size.width)")
            
            
            //--ww  bgView .addSubview(imageView)
            
            //--ww  imageView.center = bgView.center;
            
            self.leftView = imageView
            
            self.leftViewMode = UITextFieldViewMode.Always
            
        }
        if ShowAcitvityIndicator
        {
            var bgView = UIView.init()
            if GlobalStaticMethods.isPad()
            {
                bgView = UIView.init(frame: CGRectMake(0, 0, CGRectGetWidth(self.frame) * 0.05, CGRectGetHeight(self.frame)))
            }
            else
            {
                bgView = UIView.init(frame: CGRectMake(0, 0, CGRectGetWidth(self.frame) * 0.075, CGRectGetHeight(self.frame)))
            }
            activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .Gray)
            
            bgView .addSubview(activityIndicator)
            
            activityIndicator.center = bgView.center;
            activityIndicator.startAnimating()
            
            self.rightView = bgView
            self.rightViewMode = UITextFieldViewMode.Always
        }
        else if (rightImage != nil) && (!ShowAcitvityIndicator) && rightImage != ""
        {
            var bgView = UIView.init()
            if GlobalStaticMethods.isPad()
            {
                bgView = UIView.init(frame: CGRectMake(0, 0, CGRectGetWidth(self.frame) * 0.05, CGRectGetHeight(self.frame)))
            }
            else
            {
                bgView = UIView.init(frame: CGRectMake(0, 0, CGRectGetWidth(self.frame) * 0.075, CGRectGetHeight(self.frame)))
            }
            
            
            
            imageView = UIImageView(image: UIImage(named:  rightImage))
            
            
            imageView.contentMode = UIViewContentMode.Right
            imageView.frame = CGRectMake(CGRectGetWidth(self.frame) * 0.11-CGRectGetWidth(self.frame)*0.05,(CGRectGetHeight(self.frame) -  CGRectGetHeight(self.frame)*0.80)/2, CGRectGetWidth(self.frame)*0.04, CGRectGetHeight(self.frame)*0.80)
            imageView.contentMode = .Center
            
            bgView .addSubview(imageView)
            
            imageView.center = bgView.center;
            
            self.rightView = bgView
            self.rightViewMode = UITextFieldViewMode.Always
        }
        else
        {
            self.rightView = nil
        }
        
        
    }
    
    
    //  self.borderStyle = .None
    //        self.backgroundColor = UIColor.clearColor()
    //        let border = CALayer()
    //        let width = CGFloat(2.0)
    //        border.borderColor = UIColor.darkGrayColor().CGColor
    //       // border.borderWidth = 3.0
    //        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
    //        
    //        border.borderWidth = width
    //        self.layer.addSublayer(border)
    //        self.layer.masksToBounds = true
    
    
    
    
 }
