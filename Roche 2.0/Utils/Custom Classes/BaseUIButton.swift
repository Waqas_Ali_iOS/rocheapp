//
//  CustomButton.swift
//  Buttons
//
//  Created by Waqas Ali on 01/08/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//


import UIKit


public class BaseUIButton: UIButton {
    
    // MARK: Public interface
    @IBInspectable var isRounded : Bool = false
    @IBInspectable var cornerRadius : CGFloat = 0
    @IBInspectable var imageRightTitleLeft : Bool = false
    
    @IBInspectable var borderWidth : CGFloat = 2
    @IBInspectable var borderColor : String = "#000000"
    @IBInspectable var highlightedColor : String = "cc"
    @IBInspectable var unhighlightedColor : String = "cc"
    @IBInspectable var revertFontColorOnTap : Bool = false
    @IBInspectable var fontHighlightedColor : String = "cc"
    @IBInspectable var fontUnHighlightedColor : String = "cc"
    @IBInspectable var spacingImageText : CGFloat = 5.0
    @IBInspectable var imageForIpadPro : String?
    @IBInspectable var titleTopInset : CGFloat = 0.0
    
    
    
    
    // MARK: Overrides
    
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.exclusiveTouch = true
        
        if(isRounded == true){
            layoutRoundBorderLayer()
        }
        
        self.addTarget(self, action: #selector(BaseUIButton.highlightButton), forControlEvents: .TouchDown)
        self.addTarget(self, action: #selector(BaseUIButton.unhighlightButton), forControlEvents: .TouchUpInside)
        self.addTarget(self, action: #selector(BaseUIButton.unhighlightButton), forControlEvents: .TouchDragOutside)
        self.addTarget(self, action: #selector(BaseUIButton.unhighlightButton), forControlEvents: .TouchCancel)
        
        self.unhighlightButton()
        
        
        
        self.titleLabel?.font = UIFont(name: self.titleLabel!.font.fontName, size: UIView.convertFontSizeToRatio(self.titleLabel!.font.pointSize, fontStyle:nil , sizedForIPad:true));
        
         self.titleLabel?.layoutIfNeeded()
        self.titleLabel?.setNeedsLayout()
        self.titleLabel?.setNeedsDisplay()
    
        
        self.setImageForIpadPro()
        //--wwself .setTitleAndImageSpacing(spacingImageText)
        
        
    }
    
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        if(isRounded == true){
            layoutRoundBorderLayer()
        }
        //--ww self.titleLabel?.font = UIFont(name: self.titleLabel!.font.fontName, size: UIView.convertFontSizeToRatio(self.titleLabel!.font.pointSize, fontStyle:nil , sizedForIPad:true));
         self .setTitleAndImageSpacing(spacingImageText)
    }
    
    // MARK: Private
    
    private func layoutRoundBorderLayer() {
        
        
        
        let layer:CALayer = self.layer
        layer.borderColor = UIColor(hexString: borderColor ?? "#ffffff").CGColor
        layer.cornerRadius = self.frame.size.height * 0.5
        if(cornerRadius > 0){
            layer.cornerRadius = cornerRadius
        }
        layer.masksToBounds = true
        layer.borderWidth = 2
    }
    
    func highlightButton(){
        if highlightedColor == "cc"{
            self.backgroundColor = UIColor.clearColor()
            self.setTitleColor(UIColor(hexString: fontHighlightedColor ?? "#ffffff"), forState: .Highlighted)

            
        }
        else{
            self.backgroundColor = UIColor(hexString: highlightedColor)
            
            if(revertFontColorOnTap == true ){
                
                self.setTitleColor(UIColor(hexString: unhighlightedColor), forState: .Normal)
            }else{
                if(fontHighlightedColor != "cc"){
                    self.setTitleColor(UIColor(hexString: fontHighlightedColor), forState: .Normal)
                    
                }
                
                
            }
            
        }
        
    }
    func unhighlightButton(){
        
        
        if unhighlightedColor == "cc"{
            self.backgroundColor = UIColor.clearColor()
            
        }
        else{
            self.backgroundColor = UIColor(hexString: unhighlightedColor)
            if(revertFontColorOnTap == true){
                
                self.setTitleColor(UIColor(hexString: highlightedColor), forState: .Normal)
            }else{
                if(fontUnHighlightedColor != "cc"){
                    self.setTitleColor(UIColor(hexString: fontUnHighlightedColor), forState: .Normal)
                    
                }else{
                    
                    self.setTitleColor(UIColor(hexString: "#FFFFFF"), forState: .Normal)
                    
                }
                
                
            }
        }
    }
    
    
    
    func setImageForIpadPro(){
        
        
        if imageForIpadPro != nil {
            
            // let str = "\(imageForIpadPro!)-sel"
            
            self.setImage(UIImage(imageName: imageForIpadPro!), forState: .Normal)
            
            if(self.imageView?.highlightedImage != nil){
                
                let str = "\(imageForIpadPro!)-sel"
                
                self.setImage(UIImage(imageName: str), forState:.Highlighted)
            }
            
        }
    }
    
    
    func setTitleAndImageSpacing(spacing : CGFloat){
        
        
        let insetAmount :CGFloat = UIView.convertToRatio(spacing)
        
        // UIEdgeInsetsMake(<#T##top: CGFloat##CGFloat#>, <#T##left: CGFloat##CGFloat#>, <#T##bottom: CGFloat##CGFloat#>, <#T##right: CGFloat##CGFloat#>)
        
        if(imageRightTitleLeft == true){
            
            self.titleEdgeInsets = UIEdgeInsetsMake(titleTopInset, -(self.imageView!.frame.size.width + insetAmount), 0, self.imageView!.frame.size.width);
            self.imageEdgeInsets = UIEdgeInsetsMake(0, self.titleLabel!.frame.size.width, 0, -(self.titleLabel!.frame.size.width + insetAmount));
            
        }else{
            
            self.imageEdgeInsets = UIEdgeInsetsMake(0, -insetAmount, 0, -insetAmount)
            
            self.titleEdgeInsets = UIEdgeInsetsMake(titleTopInset, insetAmount, 0, -insetAmount);
        }
      //  self.contentEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, insetAmount);
      self.contentEdgeInsets = UIEdgeInsetsMake(0, -insetAmount, 0, 0);
    }
    
    
    
}