//
//  CustomUIButtonVertical.swift
//  Buttons
//
//  Created by Waqas Ali on 02/08/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//

import UIKit

public class CustomUIButtonVertical: BaseUIButton {
    
    // MARK: Public interface
    
    @IBInspectable var titleBottomImageTop : Bool = false
    @IBInspectable var verticalSpacing : CGFloat = 0
 
    
    
    
    
    // MARK: Overrides
    
    
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        self.exclusiveTouch = true
        
        self .setTitleAndImageAtTopBottom(titleBottomImageTop)
        
    }
    
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        
        
    }
    
    func setTitleAndImageAtTopBottom(isInverse : Bool){
        
        
     self.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
      self.contentVerticalAlignment = UIControlContentVerticalAlignment.Top
        let buttonFrame = self.frame.size
        let imageFrame = self.imageView!.frame.size
        let titleFrame = self.titleLabel!.frame.size
        
        let actualHeight = (buttonFrame.height - (imageFrame.height
            + titleFrame.height )) / 2
        if(verticalSpacing >  actualHeight){
            verticalSpacing = (buttonFrame.height - (titleFrame.height + imageFrame.height)) / 2.5

        
        }
        
        let vSpace = UIView.convertToRatio(verticalSpacing)
        
        let getHeight = (buttonFrame.height - (titleFrame.height + imageFrame.height)) / 2.0
        
        let complex =  getHeight + imageFrame.height + vSpace
        
        if(isInverse == true){
            
           
    self.titleEdgeInsets = UIEdgeInsetsMake (complex,
                                                    titleFrame.width + self.imageEdgeInsets.left > buttonFrame.width ? -imageFrame.width  +  (buttonFrame.width - titleFrame.width) / 2 : (buttonFrame.width - titleFrame.width) / 2 - imageFrame.width,
                                                    0,0)
           
          
self.imageEdgeInsets = UIEdgeInsetsMake (
    (buttonFrame.height - (titleFrame.height + imageFrame.height)) / 2 - vSpace,
                                                    (buttonFrame.width - imageFrame.width) / 2,
                                                    0,0)
        }else{
            
            self.titleEdgeInsets = UIEdgeInsetsMake ( (buttonFrame.height - (titleFrame.height + imageFrame.height)) / 2 - vSpace,
                                                     titleFrame.width + self.imageEdgeInsets.left > buttonFrame.width ? -imageFrame.width  +  (buttonFrame.width - titleFrame.width) / 2 : (buttonFrame.width - titleFrame.width) / 2 - imageFrame.width,
                                                     0,0)
            
            
            self.imageEdgeInsets = UIEdgeInsetsMake (
               complex,
                (buttonFrame.width - imageFrame.width) / 2,
                0,0)
            
        }
        
      self.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
}

