//
//  buttonFontCustomClass.swift
//  SampleSwift
//
//  Created by  Traffic MacBook Pro on 5/17/16.
//  Copyright © 2016 Traffic MacBook Pro. All rights reserved.
//

import UIKit

//@IBDesignable
class buttonFontCustomClass: UIButton {
    
    
    @IBInspectable var mafConnectbutton : Bool = false
    
    
    let defaultFontVal: CGFloat = 50.0
    let defaultBorderVal: CGFloat = 2.0
    @IBInspectable var fontColor : String = "#FFFFFF"{
        didSet{
            updateColor()
        }
    }
    
    @IBInspectable var textAndImageSpace : CGFloat = 0{
        didSet{
            self.centerButtonTitleAndImage(CGFloat(textAndImageSpace))
        }
    }
    
    
    
    @IBInspectable var fontChanges : Bool = false
    @IBInspectable var spacingImageText : Bool = false
    
    @IBInspectable var iPhoneFontSize : CGFloat = 50.0
    @IBInspectable var iPadFontSize : CGFloat = 50.0
    @IBInspectable var iPadProFontSize : CGFloat = 50.0
    @IBInspectable var imageForIpadPro : String?
    @IBInspectable var fontName : String = "MarselisPro"
    @IBInspectable var borderWidth : CGFloat = 2
    @IBInspectable var borderColor : String = "#FFFFFF"{
        didSet{
            updateColor()
        }
    }
    @IBInspectable var highlightedColor : String = "#FFFFFF"
    @IBInspectable var highlightedFontColor : String = "#FFFFFF"

    @IBInspectable var unhighlightedColor : String = "#FFFFFF"
    @IBInspectable var highlightImage : String?
    
    
    /*
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    
   internal override func layoutSubviews() {
        super.layoutSubviews()

    
        if mafConnectbutton == true{
            self.buttonForMAFConnect()
        }

    }
    override func awakeFromNib() {
        self.exclusiveTouch = true
        self.addTarget(self, action: #selector(buttonFontCustomClass.highlightButton), forControlEvents: .TouchDown)
        self.addTarget(self, action: #selector(buttonFontCustomClass.unhighlightButton), forControlEvents: .TouchUpInside)
        self.addTarget(self, action: #selector(buttonFontCustomClass.unhighlightButton), forControlEvents: .TouchDragOutside)
        self.addTarget(self, action: #selector(buttonFontCustomClass.unhighlightButton), forControlEvents: .TouchCancel)
        self.backgroundColor = UIColor(hexString: unhighlightedColor)

                self .imagePro()
        if highlightImage != nil {
   
            let str = "\(highlightImage!)"
            
            self.setImage(UIImage(imageName: str), forState: .Highlighted)
            
        }

        
        
//        if mafConnectbutton == true{
//            self.buttonForMAFConnect()
//        }
        
        if fontChanges == true{
            self .customizeFontSizes()
        }
        
        
        
        
      
        if spacingImageText == true{
            var spaceX : CGFloat
            if textAndImageSpace == 0 {
            
            spaceX = 0.053142 * constants.deviceType.SCREEN_WIDTH
            }else{
            spaceX  = textAndImageSpace
            }
            self.centerButtonTitleAndImage(spaceX)
        }
 
        
    }
    
    
    func highlightButton(){
        if highlightedColor == "cc"{
            self.backgroundColor = UIColor.clearColor()
            self.setTitleColor(UIColor(hexString: highlightedFontColor ?? "#ffffff"), forState: .Highlighted)
            
        }
        else{
            self.backgroundColor = UIColor(hexString: highlightedColor)
            self.setTitleColor(UIColor(hexString: highlightedFontColor ?? "#ffffff"), forState: .Highlighted)

            
        }
        
        }
    func unhighlightButton(){
        if unhighlightedColor == "cc"{
            self.backgroundColor = UIColor.clearColor()
             self.setTitleColor(UIColor(hexString: fontColor ?? "#ffffff"), forState: .Normal)
            
        }
        else{
            self.backgroundColor = UIColor(hexString: unhighlightedColor)
            self.setTitleColor(UIColor(hexString: fontColor ?? "#ffffff"), forState: .Normal)

            
        }
//        if highlightImage != nil {
//            self.setImage(UIImage(imageName: highlightImage!), forState: .Normal)
//            
//        }

    }
    func customizeFontSizes(){
        
        let myfont = self.titleLabel?.font
        
        self.titleLabel?.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "MarselisPro", psdFontSize:self.iPhoneFontSize ?? defaultFontVal )
        
        
        
        
        if GlobalStaticMethods.isPhone(){
            self.titleLabel?.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "MarselisPro", psdFontSize:self.iPhoneFontSize )
            
        }
        else{
            if GlobalStaticMethods.isPadPro(){
                self.titleLabel?.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "MarselisPro", psdFontSize:self.iPadProFontSize )
                
            }
            else {
                self.titleLabel?.font = myfont!.setGeneralFontSizeUsingRatio(self.fontName ?? "MarselisPro", psdFontSize:self.iPadFontSize )
                
            }
            
        }
        
        
        
    }
    
    func imagePro(){
        
        
        if imageForIpadPro != nil {
            
           // let str = "\(imageForIpadPro!)-sel"
            
            self.setImage(UIImage(imageName: imageForIpadPro!), forState: .Normal)
            
        }

//        guard self.imageForIpadPro.characters.count < 0 || self.imageForIpadPro == "" else{
//            let myImagePro = self.imageView?.image
//            
//            return   self .setImage(myImagePro, forState: .Normal)
//            
//        }
    }
    
    
    func buttonForMAFConnect(){
        
        
        self.layer.cornerRadius = frame.size.height * 0.5 //0.06038 * UIScreen.mainScreen().bounds.size.width;
        self.layer.borderWidth = borderWidth
      //  self.layer.backgroundColor = UIColor.clearColor().CGColor
       //--ww self.titleLabel?.textColor = UIColor(hexString: borderColor)
        updateColor()
        //
        //self.setTitleAndImageAtCorner(20)
      
        
    }
    
    func updateColor(){
 
        self.setTitleColor(UIColor(hexString: fontColor ?? "#ffffff"), forState: .Normal)
        
        self.layer.borderColor = UIColor(hexString: borderColor ?? "#ffffff").CGColor

    }
}
