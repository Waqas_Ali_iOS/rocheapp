//
//  ButtonImageTitleAtCorner.swift
//  Skeleton
//
//  Created by Waqas Ali on 03/07/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

extension UIButton{
    
    func setTitleAndImageAtCorner(spacing : CGFloat){
        
        
        let insetAmount :CGFloat = spacing
        
      //  UIEdgeInsetsMake(<#T##top: CGFloat##CGFloat#>, <#T##left: CGFloat##CGFloat#>, <#T##bottom: CGFloat##CGFloat#>, <#T##right: CGFloat##CGFloat#>)
        
       // self.imageEdgeInsets = UIEdgeInsetsMake(0, (self.frame.width - self.imageView!.frame.size.width - insetAmount), 0, 0)
        
    self.imageEdgeInsets = UIEdgeInsetsMake(0, (self.frame.width - 10 - insetAmount), 0, 0)
        
        self.titleEdgeInsets = UIEdgeInsetsMake(0, -(self.frame.width - self.titleLabel!.frame.size.width - insetAmount), 0, 0)
        self.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
}