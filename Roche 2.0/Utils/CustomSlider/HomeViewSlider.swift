//
//  HomeViewSlider.swift
//  Roche
//
//  Created by Syed Sharjeel Ali on 8/24/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit

class HomeViewSlider: UIView {
    
    
    var CTag : Int = 0

    @IBOutlet weak var lblHeadingText: BaseUILabel?
    @IBOutlet weak var lblDescText: BaseUILabel?
    
    
    @IBOutlet weak var imgCalenderIcon: UIImageView?
    
    @IBOutlet weak var imgMapIcon: UIImageView?
    
    @IBOutlet weak var lblDateAndTime: BaseUILabel?
    
    
    @IBOutlet weak var lblLocation: BaseUILabel?
    
    
    @IBOutlet weak var btnDetail: buttonFontCustomClass?
 
    @IBOutlet var btnTes: UIButton!
    @IBOutlet weak var btnArrowClick: buttonFontCustomClass!
    
    @IBAction func btnDetailClick(sender: AnyObject)
    {
        
    }
    
    
    /*
     
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
