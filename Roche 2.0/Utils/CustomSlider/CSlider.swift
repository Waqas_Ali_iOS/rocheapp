//
//  CSlider.swift
//  customSlider
//
//  Created by Syed Sharjeel Ali on 8/15/16.
//  Copyright © 2016 Syed Sharjeel Ali. All rights reserved.
//

import UIKit

@objc protocol DataSource {
    
    func numberOfItemsInSelectionList(numberOfRows : CSlider) -> NSInteger
    func DataForRowAtIndex(numberOfRows : CSlider, forIndex : Int) -> NSDictionary
}

@objc protocol Delegate {
    
    optional func didTapOnDetailBtn(detailBTN : CSlider, Tag : Int) -> Void
    
    optional func didTapOnArrowBtn(obj : CSlider, Tag : Int) -> Void

    
    optional func didArrowBtnTap(obj : CSlider, Tag : Int) -> Void

}

struct Tag {
    static let forLabel = 100000
    static let forImage = 200000
    static let forButton = 300000
}



class CSlider: UIView,UIScrollViewDelegate {
    
    // MARK: Slider Variable Data Source Variable
    
    var count : NSInteger!
    var dataSource: DataSource!
    
    var delegate: Delegate!
    
    // MARK: Slider Variable UI source
    
    
    var kevScrollView = UIScrollView()
    
    var kevPageControl = UIPageControl()
    
    
    var viewsArray : NSMutableArray! = []
    
    var viewsDictionary = Dictionary<String,AnyObject>()
    
    var mainViewsContraints = [NSLayoutConstraint]()
    
    var scrollWidth : CGFloat! = 0.0
        {
        didSet{
        }
    }
    
    
    var scrollHeight : CGFloat! = 0.0
        {
        didSet{
            //   tutorialImplementation()
            
        }
    }
    
    
    var total : Int = 0
    
    
    var currentPage : Int = 0
    
    
    var landscapeOrient : Bool = false
    
    
    var view = HomeViewSlider()
    
    
    var sliderViewContraints : NSMutableArray = []
    
    
    var ViewTag : Int = 0
    
    
    
    //    override init(frame: CGRect)
    //    {
    //        super.init(frame: frame)
    //    }
    
    
    func reloadSlider()
    {
      // self.removeAllSubviews()
        
        self.removeAllSubviews()
        
        count  = 0
        kevScrollView = UIScrollView()
        kevPageControl = UIPageControl()
        viewsArray = []
        viewsDictionary = Dictionary<String,AnyObject>()
        mainViewsContraints = [NSLayoutConstraint]()
        
        total = 0
        currentPage = 0
        landscapeOrient = false
        view = HomeViewSlider()
        sliderViewContraints = []
        
        
        
        setUp()
    }
    
    func setUp()
    {
        
        kevScrollView.bounces = false;
    
        count = dataSource?.numberOfItemsInSelectionList(self);
        if count > 0
        {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CSlider.setConstraintsWithWidth), name: UIApplicationDidChangeStatusBarOrientationNotification, object: nil)
            
            self.setConstraintsWithWidth()
            
            
    
            
            self.scrollWidth = self.bounds.width * 1
            self.scrollHeight = self.bounds.height
            
            
            //ScrollView
            kevScrollView.frame = CGRectMake(0, 0, scrollWidth, scrollHeight)
            kevScrollView.backgroundColor = UIColor.clearColor()
            kevScrollView.pagingEnabled = true
            kevScrollView.delegate = self
            kevScrollView.translatesAutoresizingMaskIntoConstraints = false
            kevScrollView.showsVerticalScrollIndicator = false;
            kevScrollView.showsHorizontalScrollIndicator = false;
            
            self.addSubview(kevScrollView)
            
            
            if  count == 1 {
                kevPageControl.hidden = true
            }
            else
            {
                kevPageControl.hidden = false
            }
            
            //PageControl
            kevPageControl.currentPage = 0
            kevPageControl.numberOfPages = count
            kevPageControl.backgroundColor = UIColor.clearColor()
            kevPageControl.pageIndicatorTintColor = UIColor.init(red: (184/255.0), green: (187/255.0), blue: (192/255.0), alpha: 1.0)
            kevPageControl.currentPageIndicatorTintColor = UIColor.init(red: (105/255.0), green: (105/255.0), blue: (113/255.0), alpha: 1.0)
            kevPageControl.translatesAutoresizingMaskIntoConstraints = false
            
            self.addSubview(kevPageControl)
            
            
            //adding to views dictionary
            self.viewsDictionary["kevScroll"] = kevScrollView
            self.viewsDictionary["kevPage"] = kevPageControl
            //self.viewsDictionary["skipButton"] = kevSkipButton
            
            
            setConstraintsForMainViews()
            
            
            
            for i in 0...count! - 1
            {
                // Get Dictionary from user
                let dic : NSDictionary = (dataSource?.DataForRowAtIndex((self), forIndex: i))!
                
                
                
                let xibName : NSString = (dic.valueForKey("nibName"))! as! NSString
                
                
                
                // load xib from name provided by user
                if GlobalStaticMethods.isPhone() {
                    view = NSBundle.mainBundle().loadNibNamed(xibName as String, owner: self, options: nil).last as! HomeViewSlider ;
                }
                else
                {
                    view = NSBundle.mainBundle().loadNibNamed(xibName as String, owner: self, options: nil).first as! HomeViewSlider ;
                }
                
                
                
                self.setData(dic.valueForKey("Data") as! NSDictionary, atIndex: i)
                
                
                
                view.translatesAutoresizingMaskIntoConstraints = false
                kevScrollView.addSubview(view);
                
                
                
                let str = "view\(i)"
                //let str = "imgView"
                //       let dictImgView = Dictionary(dictionaryLiteral: (str, imgView ))
                
                
            
                
                viewsDictionary[str]  = view
                let scrollwidth = CGRectGetWidth(self.kevScrollView.frame)
                let scrollHeight = CGRectGetHeight(self.kevScrollView.frame)
                
                
                print("view width is %f",scrollwidth)
                
                
                let red_constraint_H = NSLayoutConstraint.constraintsWithVisualFormat("H:[view\(i)(\(scrollwidth))]", options: [], metrics: nil, views: viewsDictionary)
                
                red_constraint_H.first?.identifier = "Horizontal Contraint of slider inner view"
                
                let red_constraint_V = NSLayoutConstraint.constraintsWithVisualFormat("V:[view\(i)(\(scrollHeight))]", options: [], metrics: nil, views: viewsDictionary)
                
                red_constraint_V.last?.identifier = "Vertical Contraint of slider inner view"
                
                viewsArray!.addObject(view)
                
                
                // Get Constraint into array
                let arr = [red_constraint_H,red_constraint_V]
                sliderViewContraints.addObject(arr)
                view .addConstraints(red_constraint_H)
                view.addConstraints(red_constraint_V)
                
                

            }
            
            setVerticalAndHorizontalSpacingOfViewFromSlider()
            
            kevScrollView.contentSize = CGSizeMake( CGFloat(count) * scrollWidth!*1,scrollHeight!)
      
        }
        
    }
    
    func setVerticalAndHorizontalSpacingOfViewFromSlider(){
        
        var strContraintSet = NSMutableString.init(string: "H:|-0-")
        
        
        var dictView : Dictionary<String,AnyObject> = Dictionary()
        
        
        
        for i in 0...viewsArray!.count - 1 {
            
            let key = "v\(i)"
            
            
            dictView[key] = "\(viewsArray![i])"
            
            dictView .updateValue( (viewsArray![i]), forKey: key)
            
            strContraintSet = NSMutableString(string: "\(strContraintSet)[\(key)]-0-")
            
            if i == viewsArray!.count - 1{
                
                strContraintSet = NSMutableString(string: "\(strContraintSet)|")
                
            }
            
            
        }
        
        
        let metrics: [String : AnyObject] = ["vSpacing": 0, "hSpacing": 0]
        self .layoutIfNeeded()
        
        for i in 0...viewsArray!.count - 1 {
            
            let constraints_POS_V = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[v\(i)]", options: [], metrics: nil, views: dictView)
            self.addConstraints(constraints_POS_V)
            
        }
        
        self .updateConstraints()
        self .layoutIfNeeded()
        
        let constraint_POS_H = NSLayoutConstraint.constraintsWithVisualFormat(strContraintSet as String, options: [], metrics: metrics, views: dictView)
        
        self.addConstraints(constraint_POS_H)
    }
    
    
    func setConstraintsForMainViews() {
        
        // adding constraints for scrollView
        //Vertical Constraints
        //Vertical Constraints
        let strVConstriant = "V:|-0-[kevScroll]-0-|"
        //HorizontalConstraints
        var strHConstriant = "H:|-0-[kevScroll]-0-|"
        addConstraintForMain(strVConstriant)
        addConstraintForMain(strHConstriant)
        
        
        if GlobalStaticMethods.isPhone()
        {
            var device = DeviceUtil.deviceType
            
            switch device {
            case .iPhone5:
                addConstraintForMain("V:[kevScroll]-(-65)-[kevPage(20)]-0-|")
                break
            case .iPhone6:
                addConstraintForMain("V:[kevScroll]-(-35)-[kevPage(20)]-0-|")
                break
            case .iPhone6Plus:
                addConstraintForMain("V:[kevScroll]-(-45)-[kevPage(20)]-0-|")
                break
            default:
                addConstraintForMain("V:[kevScroll]-(-45)-[kevPage(20)]-0-|")
            }
            
            
            
        }
        else{
            if GlobalStaticMethods.isPadPro()
            {
                addConstraintForMain("V:[kevScroll]-(-40)-[kevPage(40)]-20-|")
            }
            else
            {
                addConstraintForMain("V:[kevScroll]-(-30)-[kevPage(30)]-25-|")
            }
        }
        
        
        
        //adding constranits for page control
        //Horizontal Constriants
        strHConstriant = "V:[kevScroll]-(<=1)-[kevPage]" // horizontal center of the scroll View
        
        addPageConstraint(strHConstriant)
        
        NSLayoutConstraint.activateConstraints(mainViewsContraints)
        
    }
    
    func addConstraintForMain(format: String){
        
        
        let newContraint = NSLayoutConstraint.constraintsWithVisualFormat(format, options: [], metrics: nil, views: self.viewsDictionary)
        
        self.mainViewsContraints += newContraint
        
    }
    
    
    func addPageConstraint(format:String){
        let newContraint = NSLayoutConstraint.constraintsWithVisualFormat(format, options: .AlignAllCenterX, metrics: nil, views: self.viewsDictionary)
        // let newConstraint = NSLayoutConstraint.constraint
        self.mainViewsContraints += newContraint
        
    }
    
    
    
    
    
    
    
    func setConstraintsWithWidth(){
        
        
        
       // self.layoutIfNeeded()
        
        self.scrollWidth = self.bounds.width * 1
        self.scrollHeight = self.bounds.height
        
        
        
        for (var i = 0 ; i < sliderViewContraints.count - 1 ; i += 1) {
            
            let constraintArr = sliderViewContraints[i].firstObject!
            
            
            
            
            let constant1 : NSLayoutConstraint = constraintArr!.objectAtIndex(0) as! NSLayoutConstraint
            
            constant1.identifier = "Horizontal Contraint of slider inner view"
            
            constant1.constant = self.scrollWidth
            
            //   sliderImageViewContraints[i].firstObject = constant1
            
            let constraintArr2 = sliderViewContraints[i].lastObject!
            
            
            let constant2 : NSLayoutConstraint = constraintArr2!.objectAtIndex(0) as! NSLayoutConstraint
            
            
            constant2.constant = self.scrollHeight
            constant2.identifier = "Vertical Contraint of slider inner view"
            
            
            self.updateConstraints()
            
            
            self.layoutIfNeeded()
            
            //  constant1.firstItem.constant = self.scrollWidth
            
        }
        gotoPage(false)
    }
    
    // Mark methods for page.
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView)
    {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width * 1)
        kevPageControl.currentPage = Int(pageNumber)
        currentPage = kevPageControl.currentPage;
    }
    
    
    func gotoPage(animated: Bool){
        
        let width = Int( CGRectGetWidth(self.frame)*1)
        let xVal = CGFloat(width  * currentPage)
        
         self.kevScrollView.scrollRectToVisible(CGRectMake(xVal, 0,CGRectGetWidth(self.frame)*1, CGRectGetHeight(self.frame)), animated: animated)
        //Below method is better then above, inOrder to scroll At certain Position
        self.kevScrollView.setContentOffset(CGPoint(x: xVal, y: 0), animated: animated)
    }
    
    // Mark method for set View data
    
    func setData(dic: NSDictionary, atIndex index:Int)
    {
        
      
        ViewTag = index
        
        
        view.btnDetail?.tag = index
        
        
        view.btnDetail?.addTarget(self, action: #selector(btnDetailMethod), forControlEvents:.TouchUpInside)
        
        
        if GlobalStaticMethods.isPhone()
        {
            view.btnTes?.addTarget(self, action: #selector(callBtnDelegateArrow), forControlEvents:.TouchUpInside)
            
        
        }
        else
        {
            view.btnArrowClick?.addTarget(self, action: #selector(callBtnDelegateArrow), forControlEvents:.TouchUpInside)
        }
        

        
        
        if dic["title"] != nil
        {
            
            view.lblHeadingText?.text = dic["title"] as? String
            view.lblDescText?.text = dic["shortDescription"] as? String
            view.lblLocation?.text = dic["timeDate"] as? String
            view.lblDateAndTime?.text = dic["location"] as? String
        }
        
        
        
        
    }
    
    
    func btnDetailMethod(sender: UIButton!) -> Void
    {
        callDelegate(sender.tag)
    }
    
    
    
    
    
    func callDelegate(index: Int)
    {
        delegate.didTapOnDetailBtn!(self, Tag: index)
    }
    
    
    func callBtnDelegateArrow(index: Int)
    {
        delegate.didArrowBtnTap!(self, Tag: ViewTag)
    }
    
    
    
    
    
}
