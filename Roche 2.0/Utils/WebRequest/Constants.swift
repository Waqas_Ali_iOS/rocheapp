
//
//  Constants.swift
//  WebserviceTest
//
//  Created by Waqas Ali on 15/06/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//
import UIKit


struct Constants {
    
    static let publicKeyBase64ForEncryption = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3ti2g8HeVGjdcOLtr6L+KWcR4sz/im28eQkWlWOrOOedQdV7an7L5AqAEm5G4Srbr7suniMpV7vJCtnCXFa6g6fRTP6TisEwJnbkfvDGaWD+ldv22OBlLayY/22r+aJXsz8YEYND9hkSbU075UOg/VHHI+XdKawzGe6ke4zqJ6wIDAQAB"
    
    static let tagPublicKey  =  "ae.maf.cobu.Sample_Public"
    
    //static let baseURL  = "http://maf3.trafficdemos.net/api/app/v1/"
    
    static let baseURL    = "https://mafosbdev.maf.ae/"
    
    static let baseURLClient = "http://roche-redev-phase2.trafficdemos.net/app_dev.php/api/app/v1/"
    
    
    static let baseURLDemo = "http://roche-redev-phase2.trafficdemos.net/app_dev.php/api/app/v1/"
    static let baseURLLocal = "http://roche-re2.zeeshan.tphp.net/app_dev.php/api/app/v1/"
    static let baseURLLocalNaeemBhai = "http://rocheapp-1.qa.tphp.net/app_dev.php/api/app/v1/"
    
    static let baseURLLocalAfterClientChanges =  "http://rocheredev.trafficdemos.net/app_dev.php/api/app/v1/"
    
    
    static let baseURLForAppStore =  "http://www.rdmeacademy.com/api/app/v1/"
    
    static let baseDEVURLForAppStore =  "http://dev.rdmeacademy.com/api/app/v1/"
    
    static let universalBaseURL = Constants.baseURLForAppStore
        
    static let dotNetURL  = "http://mafconnect.mvc.trafficdemos.com/api/home/"
    
    static let MESSAGE = "Message"
    static let NOTIFICATION = "Notification"
    
    static let AUTO_LOGIN = "autoLogin"
    
    static let BOOKMARK_MSG = "This course added to your bookmarks"
    static let UNBOOKMARK_MSG = "This course removed from your bookmarks"

    static let ONLINE_COURSE = "Online Course"
    static let INCLASS_COURSE = "In-Class Course"
    
    
    static let ONLINE_COURSE_IMAGE = "onlineCourse"
    static let INCLASS_COURSE_IMAGE = "InlineCourse"

}


