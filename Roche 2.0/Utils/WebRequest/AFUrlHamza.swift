//
//  AlamofireRequestFormatter.swift
//  WebserviceTest
//
//  Created by Waqas Ali on 21/07/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//

import Foundation
import Alamofire

public enum AFUrlRequestHamza: URLRequestConvertible {
    
    static let baseURLPath = Constants.universalBaseURL
    
    //    static let user = "MafERPConnect"
    //    static let password = "MafERPConnect123"
    //    static let credentialData = "\(user):\(password)".dataUsingEncoding(NSUTF8StringEncoding)!
    //    static let base64Credentials = credentialData.base64EncodedStringWithOptions([])
    //    static let authenticationToken = "Basic \(base64Credentials)"
    
    
    
    static let authenticationToken = "Basic xxx"
    
    
    
    
    case InviteNewMembers(userEmail :String)
    
    case EnrollCourseOnline(courseID :String)
    case EnrollCourseInClass(courseID :String, eventID :String)
    
    case PredictiveEmail(userType : String, keyword : String)
    
    case PredictiveHosp
    
    case ForgetPassword(email : String)
    
    case Register( dict : Dictionary<String, AnyObject>)
    
    case CourseDetail(courseID : Int)
    
    case CourseCalendar(year : Int , month : Int)
    
    case AddExtraAttendees(courseId : Int , locId : Int, name : String, email : String , designation : String, hospName : String)
    
    case Logout
    
    
    
    //(registerAs : String , firstName : String , lastName : String , email : String , password: String , dob : String , mobileNumber : String , country : String , city : String , city : String , userType : String , deviceType : String , deviceToken : String , facebookID : String)
    
    //    case LoginUser(userName: String, password: String)
    //    case GetNewsFeed
    
    
    
    
    public var URLRequest: NSMutableURLRequest {
        var encoding = Alamofire.ParameterEncoding.URL
        let AdditionalInfo = ["appName": "PropertySales",
                              "appVersion" : "1.0",
                              "KVMap" :[
                                "key" : "Platform",
                                "value" : "Simulator"
            ]]
        
        
        
        
        
        
        
        let result: (path: String, method: Alamofire.Method, parameters: [String: AnyObject]) = {
            switch self {
                
                
            case .AddExtraAttendees(let courseID , let locID , let name , let email , let designation , let hospName):
                
                let params = ["sessionId" : UserModel.sharedInstance.sessionID, "locId" : locID , "courseId" : courseID , "name": name, "email": email , "designation": designation , "hospName":  hospName]
                printLog(params)
                
                return ("addExtraAttendees", .POST, params as! [String : AnyObject] )
                
                
            case .InviteNewMembers(let userEmail):
                
                let params = [ "sessionId" : UserModel.sharedInstance.sessionID , "email": userEmail]
                
                printLog(params)
                
                return ("inviteNewMember", .POST, params )
                
                
            case .EnrollCourseOnline(let courseID ):
                
                let params = [ "sessionId" : UserModel.sharedInstance.sessionID , "courseId": courseID]
                
                printLog(params)
                
                return ("enrollCourse", .POST, params )
                
            case .EnrollCourseInClass(let courseID , let eventID ):
                
                let params = [ "sessionId" : UserModel.sharedInstance.sessionID , "courseId": courseID , "eventId" : eventID]
                
                printLog(params)
                
                return ("enrollCourse", .POST, params )
                
            case .PredictiveHosp:
                
                //   let params = [ "sessionId" : "26okvqjhmght7idd99nli1g4f6" , "courseId": ""]
                
                //     printLog(params)
                
                return ("getHospitals", .POST, [:] as [String:AnyObject])
                
                
                
            case .PredictiveEmail(let userType , let keyword):
                
                let params = [ "userType": userType , "keyword": keyword]
                
                //     printLog(params)
                
                return ("predictiveEmail", .POST, params)
                
            case .ForgetPassword(let email):
                let params = [ "email": email]
                
                //     printLog(params)
                
                return ("forgotPassword", .POST, params )
                
                
            case .Register(let dict):
                
                return("signUp", .POST , dict)
                
                
            case .Logout:
                
                let params = ["sessionId" : UserModel.sharedInstance.sessionID]
                
                return ("logout", .POST, params)
                
                
            case .CourseDetail(let courseID):
                
                let params = ["sessionId" : UserModel.sharedInstance.sessionID , "courseId": courseID]
                
                return("courseDetail", .POST , params as! [String : AnyObject])
                
                
            case .CourseCalendar(let year , let month):
                
                let params = ["sessionId" : UserModel.sharedInstance.sessionID , "year": year, "month": month]
                
                return("courseCalendar", .POST , params as! [String : AnyObject])
                
                
            }
            
        }()
        
        
        
        let URL = NSURL(string: AFUrlRequestNabeel.baseURLPath)!
        let URLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(result.path))
        URLRequest.HTTPMethod = result.method.rawValue
        URLRequest.setValue(AFUrlRequestNabeel.authenticationToken, forHTTPHeaderField: "Authorization")
        URLRequest.timeoutInterval = NSTimeInterval(10 * 1000)
        // var encoding = Alamofire.ParameterEncoding.URL
        // url encoding set in the top for allowing different parameter encoding for deifferent webervices like json or url or custom
        
        
        return encoding.encode(URLRequest, parameters: result.parameters).0
    }
    
}


