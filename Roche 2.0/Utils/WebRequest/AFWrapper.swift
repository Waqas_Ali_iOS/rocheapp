//
//  AlamofireRequestHelper.swift
//  WebserviceTest
//
//  Created by Waqas Ali on 21/07/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

@objc protocol AFWrapperDelegate {
    
    optional func didTapOnOKayOnAFWrapperAlert(errorCode : Int) -> Void
    
}




class AFWrapper: NSObject, NVActivityIndicatorViewable {
    
    static var delegate : AFWrapperDelegate?

    class func requestGETURL(strURL: String, success:(AnyObject) -> Void, failure:(NSError) -> Void) {
        Alamofire.request(.GET, strURL).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson  = responseObject.result.value!
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    class func requestPOSTURL(strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:(AnyObject) -> Void, failure:(NSError) -> Void){
        Alamofire.request(.POST, strURL, parameters: params, encoding: ParameterEncoding.JSON, headers: headers).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = responseObject.result.value!
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    
    class func serviceCall(urlEnum: URLRequestConvertible, success:(JSON) -> Void, failure:(NSError) -> Void) {
        
        let size = CGSize(width: 50, height:50)

        startActivityAnimatingNew(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
        
        Alamofire.request(urlEnum).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            stopActivityAnimatingNew()
            
            if responseObject.result.isSuccess {
                let resJson  = JSON(responseObject.result.value!)
                
                if resJson["status"].intValue == 0 {
                                        
                    Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: resJson["statusMessage"].stringValue, btnActionTitle: "OK", viewController: nil, completionAction: { (Void) in
                        printLog("is this working?")
                        
                        if resJson["statusCode"].intValue == 1001 {
                        
                            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                            appDelegate.drawerController!.closeDrawerAnimated(true, completion: nil)
                            navigation.PopToVC(constants.viewControllers.loginViewController, sideDrawer: appDelegate.drawerController!)
                        }else{
                        
                         self.delegate?.didTapOnOKayOnAFWrapperAlert!(resJson["statusCode"].intValue)
                        }
                    })
                }else{
                    success(resJson)
                    
                    
                }
               
            }
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error!
                if let err = error as? NSURLError where err == .NotConnectedToInternet {
                    // no internet connection
                    
                    printLog("hey bro! you r not connected to internet")
                    Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: "Internet connection appears to be offline.", btnActionTitle: "OK", viewController: nil, completionAction: { (Void) in
                        failure(error)
                    })
                    
                } else {
                    // "Server not responding, please try again later."
                    Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: error.localizedDescription, btnActionTitle: "OK", viewController: nil, completionAction: { (Void) in
                        failure(error)
                    })
                }
                
                //--ww  failure(error)
            }
        }
    }
    
    
    class func serviceCallWitLoaderButNoTitleMessage(urlEnum: URLRequestConvertible, success:(JSON) -> Void, failure:(NSError) -> Void) {
        
        
        
                let size = CGSize(width: 50, height:50)
        
                startActivityAnimatingNew(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
        
        
        
        Alamofire.request(urlEnum).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            stopActivityAnimatingNew()
            
            if responseObject.result.isSuccess {
                let resJson  = JSON(responseObject.result.value!)
                
                //                if resJson["status"].intValue == 0 {
                //
                ////                    Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: resJson["statusMessage"].stringValue, btnActionTitle: "OK", viewController: nil, completionAction: { (Void) in
                ////                        printLog("is this working?")
                //
                //                        if resJson["statusCode"].intValue == 1001 {
                //
                //                            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                //                            appDelegate.drawerController!.closeDrawerAnimated(true, completion: nil)
                //                            navigation.PopToVC(constants.viewControllers.loginViewController, sideDrawer: appDelegate.drawerController!)
                //                        }
                //
                //                  //  })
                //                }else{
                success(resJson)
                
                
                // }
                
            }
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    class func serviceCallWithoutLoader(urlEnum: URLRequestConvertible, success:(JSON) -> Void, failure:(NSError) -> Void) {
        
        
        
//        let size = CGSize(width: 50, height:50)
        
//        startActivityAnimatingNew(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
        
        
        
        Alamofire.request(urlEnum).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
//            stopActivityAnimatingNew()
            
            if responseObject.result.isSuccess {
                let resJson  = JSON(responseObject.result.value!)
                
//                if resJson["status"].intValue == 0 {
//                    
////                    Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: resJson["statusMessage"].stringValue, btnActionTitle: "OK", viewController: nil, completionAction: { (Void) in
////                        printLog("is this working?")
//                    
//                        if resJson["statusCode"].intValue == 1001 {
//                            
//                            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//                            appDelegate.drawerController!.closeDrawerAnimated(true, completion: nil)
//                            navigation.PopToVC(constants.viewControllers.loginViewController, sideDrawer: appDelegate.drawerController!)
//                        }
//                        
//                  //  })
//                }else{
                    success(resJson)
                    
                    
               // }
                
            }
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error!
                failure(error)
            }
        }
    }

    
    
    
    class func UploadImageWithParamteres(param: [String:AnyObject], imgData : NSData?, URL : String,  success:(JSON) -> Void, failure:(NSError) -> Void) {
        
        /*var parameters = [String:AnyObject]()//define parameter according to your requirement
        parameters = ["sessionId":"Sample",
                      "threadId":"Sample",
                      "comment":"Sample",
                      "attachTitle":"Sample",
                      "attachType":"Sample"
        ]
        */
        
        let size = CGSize(width: 50, height:50)
       startActivityAnimatingNew(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
        
        let URL = AFUrlRequestWaqas.baseURLPath + URL
    
        
        Alamofire.upload(.POST, URL, multipartFormData: {
            multipartFormData in
            
          //--ww  if  let imageData = UIImageJPEGRepresentation(image!, 0.6) {
            if imgData != nil {
                multipartFormData.appendBodyPart(data: imgData!, name: "attachment", fileName: "Attachment.png", mimeType: "image/png")
            }
         //--ww   }
            
            for (key, value) in param {
                multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
            }
            
            }, encodingCompletion: {
                encodingResult in
                
                switch encodingResult {
                    
                case .Success(let upload, _, _):
                    printLog("success")
                    
                    upload.responseJSON { response in
                        printLog(response.request)  // original URL request
                        printLog(response.response) // URL response
                        printLog(response.data)     // server data
                        printLog(response.result)   // result of response serialization
                        stopActivityAnimatingNew()
                       if response.result.isSuccess {
                        if let resP = response.result.value {
                           let resJson = JSON(resP)
                             printLog("JSON: \(resJson)")
                            if resJson["status"].intValue == 0 {
                                
                                Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: resJson["statusMessage"].stringValue, btnActionTitle: "OK", viewController: nil, completionAction: { (Void) in
                                    printLog("is this working?")
                                    
                                    if resJson["statusCode"].intValue == 1001 {
                                        
                                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                        appDelegate.drawerController!.closeDrawerAnimated(true, completion: nil)
                                        navigation.PopToVC(constants.viewControllers.loginViewController, sideDrawer: appDelegate.drawerController!)
                                    }
                                    
                                })
                            }else{
                                success(resJson)
                                
                                
                            }
                            
                        }
                    }
                        if response.result.isFailure {
                            let error : NSError = response.result.error!
                            failure(error)
                        }
                }
                    
                case .Failure(let encodingError):
                    printLog(encodingError)
                 
                
            
                 
                }
        })
    }

    
    
    
    class func startActivityAnimatingNew(size: CGSize? = nil, message: String? = nil, type: NVActivityIndicatorType? = nil, color: UIColor? = nil, padding: CGFloat? = nil) {
        let activityContainer: UIView = UIView(frame: UIScreen.mainScreen().bounds)
        
        activityContainer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
       activityContainer.restorationIdentifier = "Loader"
        
        let actualSize = size ?? NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE
        let activityIndicatorView = NVActivityIndicatorView(
            frame: CGRectMake(0, 0, actualSize.width, actualSize.height),
            type: type,
            color: color,
            padding: padding)
        
        activityIndicatorView.center = activityContainer.center
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.startAnimation()
        activityContainer.addSubview(activityIndicatorView)
        
        let width = activityContainer.frame.size.width / 3
        if let message = message where !message.isEmpty {
            let label = UILabel(frame: CGRectMake(0, 0, width, 30))
            label.center = CGPointMake(
                activityIndicatorView.center.x,
                activityIndicatorView.center.y + actualSize.height)
            label.textAlignment = .Center
            label.text = message
            label.font = UIFont.boldSystemFontOfSize(20)
            label.textColor = activityIndicatorView.color
            activityContainer.addSubview(label)
        }
        
        UIApplication.sharedApplication().keyWindow!.addSubview(activityContainer)
    }

    
    class func stopActivityAnimatingNew() {
        for item in UIApplication.sharedApplication().keyWindow!.subviews
            where item.restorationIdentifier == "Loader" {
                item.removeFromSuperview()
        }
    }
}
