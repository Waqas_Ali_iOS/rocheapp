//
//  AlamofireRequestFormatter.swift
//  WebserviceTest
//
//  Created by Waqas Ali on 21/07/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//

import Foundation
import Alamofire

public enum AFUrlRequestNabeel: URLRequestConvertible {
    
    static let baseURLPath = Constants.universalBaseURL
    
    static let user = "MafERPConnect"
    static let password = "MafERPConnect123"
    static let credentialData = "\(user):\(password)".dataUsingEncoding(NSUTF8StringEncoding)!
    static let base64Credentials = credentialData.base64EncodedStringWithOptions([])
    static let authenticationToken = "Basic \(base64Credentials)"
    //static let authenticationToken = "Basic xxx"
    
  
    
   
 
   // case LoginUser(userName: String, password: String)
  //  case GetNewsFeed
    case PrivacyPolicy(type : String)
    case ContactDetail(sessionId : String)
    case GeneralInquiry(comment : String , sessionId :String)
    case GetBookmarkCourses(sessionId : String)
    case BookMarkCourse(sessionId : String , courseId :Int)
    case GetSuggestedCourse(sessionId : String)
    case GetCourseInProgress(sessionId : String)
    
    case GetAcademyCourse (sessionID : String )
    case GetSearchAcademyCourse(sessionId : String, keyword : String, sortBy: String,courseType : String,offset : Int , limit : Int, year : String , month : String , date  :String )
    
    case CourseSuggestion(sessionID : String,  courseID : Int,  email : String)

    case AttendanceScan(locId : Int, courseId : Int , userId : String)
    
    case AttendanceDetail(locId : Int, courseId : Int)

    case AttendanceSelectLocation(courseId : Int)

    case AttendanceCheck(courseID : Int , locId : Int)
    
    case Attendance(offset: Int , limit : Int)
    
    case AttendExportCV(locId : Int, courseId : Int)
    
    case Notification()
    case EnrollRequest()
    case EnrollrequestStatus (userID : String , status : String)

    public var URLRequest: NSMutableURLRequest {
        var encoding = Alamofire.ParameterEncoding.URL
        let AdditionalInfo = ["appName": "PropertySales",
                              "appVersion" : "1.0",
                              "KVMap" :[
                                "key" : "Platform",
                                "value" : "Simulator"
            ]]
        
        
        
      
        
        let result: (path: String, method: Alamofire.Method, parameters: [String: AnyObject]) = {
            switch self {
                


               
            case .PrivacyPolicy (let type):
                let params  = ["slug" : type ]
            // encoding = Alamofire.ParameterEncoding.JSON

                printLog(params)
                return ("getPage", .GET, params)
                

            case .ContactDetail (let sessionId):
                
                let params  = ["sessionId" : sessionId ]
                // encoding = Alamofire.ParameterEncoding.JSON
                
                printLog(params)
                return ("contactDetails", .POST, params)
                
            case .GeneralInquiry(let Comment, let Session):
                
                let params  = ["comment" : Comment , "sessionId" : Session]
                // encoding = Alamofire.ParameterEncoding.JSON
                
                printLog(params)
                return ("contactEnquiry", .POST, params)
                
                
            case GetBookmarkCourses(let SessionId):

                let params  = [ "sessionId" : SessionId]
                // encoding = Alamofire.ParameterEncoding.JSON
                
                printLog(params)
                return ("getBookmarkedCourses", .POST, params)
               
               
            case BookMarkCourse (let SessionId , let CourseId):
                
                let params  = [ "sessionId" : SessionId , "courseId" : CourseId]
                // encoding = Alamofire.ParameterEncoding.JSON
                
                printLog(params)
                return ("bookmarkCourse", .POST, params as! [String : AnyObject])
                
                
            case GetSuggestedCourse(let SessionId):
                
                let params  = [ "sessionId" : SessionId]
                // encoding = Alamofire.ParameterEncoding.JSON
                
                printLog(params)
                return ("getSuggestedCourses", .POST, params)
                
                
            case GetCourseInProgress(let SessionId):
                
                let params  = [ "sessionId" : SessionId]
                // encoding = Alamofire.ParameterEncoding.JSON
                
                printLog(params)
                return ("getCoursesInProgress", .POST, params)
                
             
                
                case GetSearchAcademyCourse (let SessionId, let Keyword, let
                    SortBy, let CourseType , let OffSet , let Limit, let Year , let Month , let Date):
                
                    let params  = [ "sessionId" : SessionId , "keyword" : Keyword, "sortBy" : SortBy, "courseType" : CourseType, "offset" : OffSet , "limit" : Limit , "year" : Year, "month" : Month, "date" : Date]
                    // encoding = Alamofire.ParameterEncoding.JSON
                    
                    printLog(params)
                    return ("getAcademyCourses", .POST, params as! [String : AnyObject])

                
            case GetAcademyCourse(let sessionId):
                
                let params  = ["sessionId" : sessionId]
                // encoding = Alamofire.ParameterEncoding.JSON
                
                printLog(params)
                return ("getAcademyCourses", .POST, params )
                
               
                
            case .CourseSuggestion(let sessionID, let courseID, let email):
                
                
                let params  = ["sessionId" : sessionID, "courseId" : courseID, "email" : email]
                // encoding = Alamofire.ParameterEncoding.JSON
                
                printLog(params)
                return ("suggestCourse", .POST, params as! [String : AnyObject] )
                
                
                
                
                
            case .AttendExportCV(let locID , let courseID):
                
                let params = ["sessionId" : UserModel.sharedInstance.sessionID, "locId" : locID , "courseId" : courseID]
                
                print(params)
                return ("attenExportCV", .POST, params as! [String : AnyObject])
                
                
                
                
            case .AttendanceCheck(let courseID , let locID):
                
                let params = ["sessionId" : UserModel.sharedInstance.sessionID, "courseId" : courseID , "locId" : locID]
                
                print(params)
                return ("attendanceCheck", .POST, params as! [String : AnyObject])
                
                
                
            case .AttendanceSelectLocation(let courseID):
                
                let params = ["sessionId" : UserModel.sharedInstance.sessionID , "courseId" : courseID]
                
                
                return ("attendanceSelectLocation", .POST, params as! [String : AnyObject])
                //
                
                
            case .AttendanceDetail(let locID , let courseID):
                
                let params = ["sessionId" : UserModel.sharedInstance.sessionID, "locId" : locID , "courseId" : courseID]
                
                printLog(UserModel.sharedInstance.sessionID)
                return ("attendanceDetail", .POST, params as! [String : AnyObject])
                
            case  .Attendance(let offset , let limit):
                
                
                let params = ["sessionId" : UserModel.sharedInstance.sessionID, "offset" : offset , "limit" : limit]
                
                
                return ("attendance", .POST, params as! [String : AnyObject])

                
                
            case .AttendanceScan(let locId , let courseId  , let userId ):
                
                let params = ["sessionId" : UserModel.sharedInstance.sessionID, "locId" : locId , "courseId" : courseId , "userId":userId ]
                
                printLog(params)
                return ("attendanceScan", .POST, params as! [String : AnyObject])

            case .Notification():
  
                let params = ["sessionId" : UserModel.sharedInstance.sessionID]
                
                printLog(params)
                return ("notification", .POST, params )
                
                
                
                
            case .EnrollRequest:
                
                let params = ["sessionId" : UserModel.sharedInstance.sessionID]
                
                printLog(params)
                return ("enrollRequests", .POST, params )

                
                
                
            case .EnrollrequestStatus(let userID , let Status):
             
                let params = ["sessionId" : UserModel.sharedInstance.sessionID , "enrollmentId" : userID , "status" : Status]
                
                printLog(params)
                return ("setEnrollRequestStatus", .POST, params )
               
                
                
                
                
                
            }
        }()
        
        let URL = NSURL(string: AFUrlRequestNabeel.baseURLPath)!
        let URLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(result.path))
        URLRequest.HTTPMethod = result.method.rawValue
        URLRequest.setValue(AFUrlRequestNabeel.authenticationToken, forHTTPHeaderField: "Authorization")
        URLRequest.timeoutInterval = NSTimeInterval(10 * 1000)
        // var encoding = Alamofire.ParameterEncoding.URL
        // url encoding set in the top for allowing different parameter encoding for deifferent webervices like json or url or custom
        
        
        return encoding.encode(URLRequest, parameters: result.parameters).0
    }
    
}


