//
//  AlamofireRequestFormatter.swift
//  WebserviceTest
//
//  Created by Waqas Ali on 21/07/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//

import Foundation
import Alamofire

public enum AFUrlRequestWaqas: URLRequestConvertible {
    
    static let baseURLPath = Constants.universalBaseURL
     static let authenticationToken = "Basic xxx"
    
  
    
   
 
    case LoginUser(userName: String, password: String)
    case GetNewsFeed
    
    case GetMessages
    case GetMessageDetail(tid : Int)
    
    case GetMyProfile
    
    case UpdateProfile(param : Dictionary<String,AnyObject>)
    
    
    
    
    
    
    
    
    public var URLRequest: NSMutableURLRequest {
        var encoding = Alamofire.ParameterEncoding.URL
 
         let result: (path: String, method: Alamofire.Method, parameters: [String: AnyObject]) = {
            switch self {
                
            case .LoginUser(let userName, let password):
                
          
                let params = [ "email" : userName , "password" : password,
                               "deviceType" : 1, "deviceToken" : Singleton.sharedInstance.deviceToken]
                
              
                //--ww encoding = Alamofire.ParameterEncoding.JSON
                
                printLog(params)
                return ("login", .POST, params as! [String : AnyObject])
                
            case .GetNewsFeed:
                
                let params  = [
                    "requestType" : "getNewsFeed",
                    "userToken " : " ",
                ]
                
                encoding = Alamofire.ParameterEncoding.JSON
                
                return ("internal/MafConnect/EBM/MafConnectWorklist", .POST, params )
                
            case .GetMessages:
                let params  = [
                    "sessionId" : UserModel.sharedInstance.sessionID,
                    "offset" : 0,
                    "limit" : 1000
                    ]

                return ("getMessages", .POST, params as! [String : AnyObject] )
                
                
            case .GetMessageDetail(let threadID):
                let params  = [
                    "sessionId" : UserModel.sharedInstance.sessionID,
                    "offset" : 0,
                    "limit" : 1000,
                    "threadId" : threadID
                ]
                
                return ("getMessageDetail", .POST, params as! [String : AnyObject] )
                
                
                
            case .GetMyProfile:
                let params  = [
                    "sessionId" : UserModel.sharedInstance.sessionID]
                
                return ("myProfile", .POST, params)
   
                
                
                
            case .UpdateProfile(let params):
              
                return ("updateProfile", .POST, params)
                
                
            }
        }()
        
        let URL = NSURL(string: AFUrlRequestWaqas.baseURLPath)!
        let URLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(result.path))
        URLRequest.HTTPMethod = result.method.rawValue
        URLRequest.setValue(AFUrlRequestWaqas.authenticationToken, forHTTPHeaderField: "Authorization")
        URLRequest.timeoutInterval = NSTimeInterval(10 * 1000)
        // var encoding = Alamofire.ParameterEncoding.URL
        // url encoding set in the top for allowing different parameter encoding for deifferent webervices like json or url or custom
        
        printLog(URLRequest)
        printLog(result.parameters)
        
        return encoding.encode(URLRequest, parameters: result.parameters).0
    }
    
}



