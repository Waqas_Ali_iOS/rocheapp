//
//  AlamofireRequestFormatter.swift
//  WebserviceTest
//
//  Created by Waqas Ali on 21/07/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//

import Foundation
import Alamofire

public enum AFUrlRequestSharjeel: URLRequestConvertible {
    
    
    enum CurriculumType {
        
        case PreRequisites ,
        Electives
    }
    
    enum courseType : Int {
        
        case Online = 1,
        Inclass
    }
    
    
    enum ListServiceType : Int {
        case Assesment = 1,
        preCourseSurvay,
        postCourseSurvay
    }
    
    
    
    
    static let baseURLPath = Constants.universalBaseURL
    
    static let user = "MafERPConnect"
    static let password = "MafERPConnect123"
    static let credentialData = "\(user):\(password)".dataUsingEncoding(NSUTF8StringEncoding)!
    static let base64Credentials = credentialData.base64EncodedStringWithOptions([])
   // static let authenticationToken = "Basic \(base64Credentials)"
   static let authenticationToken = "Basic xxx"
    
  
    
   
 
    case LoginUser(userName: String, password: String)
    case GetNewsFeed
    
    case GetMyCertificates
    
    case GetDashboard
    
    case GetUpComingEvents
    
    case GetMyTeam(offset: Int, limit: Int)
    
    case GetMyCurriculum(showCourse: Int, offset: Int?, limit: Int?)
    
    case GetAssessmentList(courseId: Int, methodType: ListServiceType.RawValue)

    case GetAssessmentResult(courseId: Int, strQID : String, strAID : String, methodType: Int)
    
    case ResetPassword(strPassword: String)
    
    
    
    
    
    public var URLRequest: NSMutableURLRequest {
        var encoding = Alamofire.ParameterEncoding.URL
        let AdditionalInfo = ["appName": "PropertySales",
                              "appVersion" : "1.0",
                              "KVMap" :[
                                "key" : "Platform",
                                "value" : "Simulator"
            ]]
        
        
        
        
        
      
        
        let result: (path: String, method: Alamofire.Method, parameters: [String: AnyObject]) = {
            switch self {
                
            case .LoginUser(let userName, let password):
                
          
                let params = [ "userID" : userName , "passwordEnc" : password, "AdditionalInfo" : AdditionalInfo ]
                 encoding = Alamofire.ParameterEncoding.JSON
                
                printLog(params)
                return ("framework/login/authPost", .POST, params as! [String : AnyObject])
                
            case .GetNewsFeed:
                
                let params  = [
                    "requestType" : "getNewsFeed",
                    "userToken " : " ",
                ]
                
                encoding = Alamofire.ParameterEncoding.JSON
                
                return ("internal/MafConnect/EBM/MafConnectWorklist", .POST, params )
                
            case .GetMyCertificates:

                let params  = [
                    "sessionId" : UserModel.sharedInstance.sessionID,
                    ]

                return ("myCertificates", .POST, params )
                
                
            case .GetDashboard:
                let params  = [
                    "sessionId" : UserModel.sharedInstance.sessionID,
                ]
                
                return ("dashboard", .POST, params )
                
            case .GetUpComingEvents:
                let params  = [
                    "sessionId" :  UserModel.sharedInstance.sessionID,
                ]
                
                return ("upcomingEvents", .POST, params )
                
            case .GetMyTeam(let offset, let limit):
                let params  = [
                    "sessionId" :  UserModel.sharedInstance.sessionID,
                    "offset" : offset,
                    "limit" : limit,
                ]
                
                return ("myTeam", .POST, params as! [String : AnyObject] )
            
            case .GetMyCurriculum(let showCourse, let offset, let limit):
                let params  = [
                    "sessionId" :  UserModel.sharedInstance.sessionID,
                    "offset" : showCourse,
                    "offset" : "\(offset)" ?? "",
                    "offset" : "\(limit)" ?? "",
                    ]
                
                return ("myCurriculum", .POST, params as! [String : AnyObject] )
                
                
            case .GetAssessmentList(let courseID, let methodType):
                let params  = [
                    "sessionId" : UserModel.sharedInstance.sessionID,
                    "courseId" : courseID,
                ]
                
                var str = ""
                switch methodType {
                case ListServiceType.Assesment.rawValue:
                    str = "assessmentList"
                case ListServiceType.preCourseSurvay.rawValue:
                    str = "preCourseSurveyList"
                default:
                     str = "postCourseSurveyList"
                }
                
                
                return (str, .POST, params as! [String : AnyObject] )
                
            case .GetAssessmentResult(let courseId, let strQID , let strAID , let methodType):
                let params  = [
                    "sessionId" : UserModel.sharedInstance.sessionID,
                    "courseId" : courseId,
                    "questions" : strQID,
                    "answers" : strAID
                    ]
                
                 var str = ""
                
                switch methodType {
                case ListServiceType.Assesment.rawValue:
                    str = "assessmentResult"
                case ListServiceType.preCourseSurvay.rawValue:
                    str = "preCourseSurveyResult"
                default:
                    str = "postCourseSurveyResult"
                }
                
                return (str, .POST, params as! [String : AnyObject] )
                
                
            case .ResetPassword(let strPass):
                let params  = [
                     "sessionId" : UserModel.sharedInstance.sessionID,
                     "password" : strPass,
                     
                ]
                return ("resetPassword", .POST, params )
                
            }
            
        }()
        
        let URL = NSURL(string: AFUrlRequestSharjeel.baseURLPath)!
        let URLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(result.path))
        URLRequest.HTTPMethod = result.method.rawValue
        URLRequest.setValue(AFUrlRequestSharjeel.authenticationToken, forHTTPHeaderField: "Authorization")
        URLRequest.timeoutInterval = NSTimeInterval(10 * 1000)
        // var encoding = Alamofire.ParameterEncoding.URL
        // url encoding set in the top for allowing different parameter encoding for deifferent webervices like json or url or custom
        
        printLog(URLRequest)
        printLog(result.parameters)
        
        return encoding.encode(URLRequest, parameters: result.parameters).0
    }
    
}



