//
//  STable.swift
//  createSuggestionTable
//
//  Created by Syed Sharjeel Ali on 10/2/16.
//  Copyright © 2016 Syed Sharjeel Ali. All rights reserved.
//

import UIKit

@objc protocol STCustomDelegate
{
    func numberOfItemsInSelectionList(cSlider : STable, selectedObject : AnyObject)
    
}


class STable: UIView,UITableViewDataSource,UITableViewDelegate {

    var delegate: STCustomDelegate!
    
    
    var forView = UIView() // forView, Display Suggestion table View for certain view i.e TextField, TextView, UIview.
    var inView = UIView() // inView, Display Suggestion table View in centain view i.e ViewController, UIview.
    
    var borderColer : UIColor? // Border color of ST
    var bgColor : UIColor? // Background color of ST
    
    var cellSelectedColor : UIColor? // Color of ST, on selction

    var lblTextSize : CGFloat = 0.0 // Font size of label text
    
    var arrselectedValues = [Int]()
    
    var suggestionTblView = UITableView()
    
    var  arrSugesstionData = Array<AnyObject>() // Array for ST,
        {
        didSet{
               CreatTableForSuggestion()
        }
    }
    
    
    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        //self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        //self.commonInit()
    }
    
    
    func CreatTableForSuggestion()
    {
        
        if arrSugesstionData.count > 0
        {
            
           if (CGRectIsEmpty(suggestionTblView.frame))
           {
            let txtFieldframe : CGRect = forView.frame;
            
            let yPosition = txtFieldframe.origin.y + txtFieldframe.size.height + 20;
            
            
            var ratio : CGFloat = 1.1 // for iPad
            
            //            if GlobalStaticMethods.isPad() {
            //                ratio = 1.05
            //            }
            
            let tblView = UITableView(frame: CGRectMake(txtFieldframe.origin.x, yPosition , txtFieldframe.size.width * ratio , 44 * 4), style: .Plain)
            
            tblView.backgroundColor = UIColor.lightGrayColor()
            tblView.center = CGPointMake(forView.center.x, tblView.center.y)

            if #available(iOS 9, *) {
                tblView.cellLayoutMarginsFollowReadableWidth = false
            }
            
            
            
            tblView.layer.borderColor = borderColer?.CGColor
            tblView.layer.borderWidth = 1.0
            
            tblView.layer.cornerRadius = 10
            
            
            
            tblView.dataSource = self
            tblView.delegate = self
            
            tblView.layoutMargins = UIEdgeInsetsZero
            tblView.separatorInset = UIEdgeInsetsZero
            
            tblView.backgroundColor = bgColor
            
            suggestionTblView = tblView;
            
            suggestionTblView.hidden = true
            
            self.inView.addSubview(suggestionTblView)
            
            
            
            inView.translatesAutoresizingMaskIntoConstraints = false
            forView.translatesAutoresizingMaskIntoConstraints = false
            
            self.inView.addConstraint(
                NSLayoutConstraint(
                    item: suggestionTblView,
                    attribute: .Top,
                    relatedBy: .Equal,
                    toItem: forView,
                    attribute: .Bottom,
                    multiplier: 1.0,
                    constant: 20
                ))
            

            self.suggestionTblView.layoutIfNeeded()
            
            }
        }
        
        if suggestionTblView.hidden == true
        {
            suggestionTblView.hidden = false
        }
        suggestionTblView.reloadData()
        
    }
    
    
    // MARK: - TableView Datasource and delegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrSugesstionData.count
        //return arrSugesstionData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell()
        
        cell.backgroundColor = UIColor.clearColor()
        
        cell.textLabel?.text = arrSugesstionData[indexPath.row] as? String
        
        cell.textLabel?.textColor = UIColor.whiteColor()
        
        cell.textLabel?.font =  UIFont(name: "MarselisPro", size: lblTextSize)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        
        // On Selection of cell calls slider delegates Methods
       
        delegate.numberOfItemsInSelectionList(self, selectedObject: arrSugesstionData[indexPath.row])
        
        suggestionTblView.hidden = true
    }
    
    
    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        
        let selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        // On Selection of cell change slider delegates Methods
        selectedCell.contentView.backgroundColor = cellSelectedColor
    }
    
    
    func UpdateFrame()
    {
//        let txtFieldframe : CGRect = forView.frame;
//        
//        let yPosition = txtFieldframe.origin.y + txtFieldframe.size.height + 20;
//        
//        var ratio : CGFloat = 1.1 // For iPad, this might needs to change
//
//        suggestionTblView.frame = CGRectMake(txtFieldframe.origin.x, yPosition , txtFieldframe.size.width * ratio , 44 * 4)
//        
//        suggestionTblView.center = CGPointMake(forView.center.x, suggestionTblView.center.y)
        
    }

}
