//
//  Keywords.swift
//  Skeleton
//
//  Created by  Traffic MacBook Pro on 5/25/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//


let LanguageChosen = "chosenLanguage"
let arabicLanguage = "ar"



struct constants {
    
    static let LanguageChosen = "chosenLanguage"
    static let arabicLanguage = "ar"

    struct serverConstants {
        static let SERVER_PATH =  "http://maf.khaliq.tphp.net/app_dev.php/api/app/v1/"
        static let GOOGLE_API_PATH =      "http://maps.googleapis.com/maps/api/geocode/json"
        static let DOMAIN_PATH     =    "http://maf.trafficdemos.net/"
        static let IMAGE_THUMB_PATH   = ""

    }
    
    struct encryption {
       static let ENCRYPTION_ENABLED     = false
    }
    
    struct deviceType {
        

        static let SCREEN_WIDTH = UIScreen.mainScreen().bounds.size.width
        static let SCREEN_HEIGHT = UIScreen.mainScreen().bounds.size.height
        static let SCREEN_MAX_LENGTH = max(SCREEN_WIDTH, SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH = min(SCREEN_WIDTH, SCREEN_HEIGHT)

       
       
      
        
        static let IS_IPAD =  UIDevice.currentDevice().userInterfaceIdiom == .Pad
        static let IS_IPHONE =  UIDevice.currentDevice().userInterfaceIdiom == .Phone
        static let IS_IPHONE_4 = SCREEN_HEIGHT  == 480
        static let IS_IPHONE_5 = SCREEN_HEIGHT  == 568
        static let IS_IPHONE_6 = SCREEN_HEIGHT  == 667
       static let IS_IPHONE_6P = SCREEN_HEIGHT  == 736

    
    }
    
    struct navigationKeywords {
        
               
        
    }
    
    struct basicKeywords {
        static let LanguageChosen = "chosenLanguage"
        static let arabicLanguage = "ar"
        static let englishLanguage = "en"
    }
    
    
    struct imagesNames {
        static let LEFT_MENU_ICON = "leftmenu-icon"
        static let RIGHT_MENU_STAR_ICON = "rightmenu-notification"
        static let RIGHT_NEW_MESSAGE = "rightmenu-plus"
        static let LEFT_MENU_SETTING_ICON = "gen-right-menu-setting"
        static let LEFT_MENU_BACK_ICON = "leftmenu-backicon"
        static let FOOTER_CURRICULUM_LEFT_IMAGE = "footer-curriculum"
        static let FOOTER_ACADEMY_LEFT_IMAGE = "footer-academy"
        static let FOOTER_FORWARD_RIGHT_IMAGE = "footer-forward-arrow"
        static let HEADER_LOGOUT_RIGHT_IMAGE = "leftmenu-logout"
       
    }
    
    struct viewControllers {
        static let splashViewController = "SplashViewController"
        static let loginViewController = "LoginViewController"
        static let homeContainerViewController = "HomeViewController"
        static let leftmenuViewController = "LeftMenuViewController"
        static let settingsViewController = "SettingsViewController"
        static let filterSearchViewController = "FilterSearchViewController"
        static let HomeApprovalsViewController = "HomeApprovalsViewController"
        static let HomeUpdatesViewController = "HomeUpdatesViewController"
        static let headerViewController = "HeaderViewController"
        static let AnnouncementsListingViewController = "AnnouncementsListingViewController"
        static let PrivacyPolicyViewController = "PrivacyViewController"
        static let TermsConditionViewController = "TermsConditionViewController"
        static let RequestViewController = "RequestViewController"
        static let profileViewController = "profileViewController"
        static let FavouritesViewController = "FavouritesViewController"
        static let RequestDetailViewController = "RequestDetailViewController"
        static let forgetPasswordVC = "ForgetPasswordViewController"
        static let tutorialScreenVC = "TutorialViewController"
        static let travelPolicyVC = "TravelPolicyViewController"
        static let registerVC = "RegisterViewController"
        static let profileSettingsVC = "ProfileSettingsViewController"
        static let inviteNewMemberVC = "InviteNewMemberViewController"
        static let upcomingEventVC = "UpcomingEventViewController"
        static let myTeamVC = "MyTeamViewController"
        static let enrollRequest = "EnrollRequestViewController"
        static let messageVC = "MessageViewController"
        static let myCertificateVC = "MyCertificateViewController"
        static let contactRocheVC = "ContactRocheViewController"
        static let replyMsgVC = "ReplyMessageViewController"
static let notificationVC  = "NotificationViewController"
        static let curriculumVC = "MyCurriculumViewController"
        static let courseDetailVC = "courseDetailViewController"
static let attendenceVC = "AttendenceViewController"
        static let attendenceListVC = "AttendenceListViewController"
//        static let attendeceCheckVC  = "AttendeceCheckViewController"
static let addExtraVC  = "AddExtraAttendeeViewController"
        static let attendenceDetailVC  = "AttendenceDetailViewController"
        static let PostNewMessageVC = "PostNewMessageViewController"
        static let bookmarkVC = "BookMarkViewController"
        
        static let CourseInProgressVC = "CourseInProgressViewController"
        static let suggestedVC = "SuggestedCourseViewController"
        static let seminarVC   = "SeminarViewController"
        static let academyCourseVC   = "AcademyCoursesViewController"
        static let filterVC   = "FiltersViewController"
        static let searchResultVC  = "SearchResultViewController"
        static let attendenceCheckVC  = "AttendenceCheckViewController"
        static let courseSuggestVC  = "CourseSuggestionViewController"
        static let curriculumTableViewController = "CurriculumTableViewController"

        static let ScanQRCodeViewController = "ScanQRCodeViewController"
        
        static let assesmentThankViewController = "AssesmentThankViewController"
        
        
        static let locationViewController = "locationViewController"
        
        
        static let GeneralEnquiryViewController = "GeneralEnquiryViewController"

    }
    
}



