//
//  File.swift
//  Skeleton
//
//  Created by  Traffic MacBook Pro on 5/25/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import Foundation
import UIKit

class customAlert {
     func showAlertMsgWithTitle( title : String , msg : String , btnActionTitle : String , viewController : UIViewController ) -> UIAlertController{
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
       let alertAction = UIAlertAction(title: btnActionTitle, style: .Default, handler: nil)
        
        
        alertController .addAction(alertAction)
        
        
        return alertController
       // viewController .presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    func showAlertMsgWithTitle2Button( title : String , msg : String , btnActionTitle : String , btnActionTitle2 : String ,viewController : UIViewController ){
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        let alertAction = UIAlertAction(title: btnActionTitle, style: .Default, handler: nil)
        let alertAction2 = UIAlertAction(title: btnActionTitle, style: .Default, handler: nil)

        
        alertController .addAction(alertAction)
        alertController.addAction(alertAction2)
        viewController .presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    func showAlertMsgWithTitleWithCompletionBlock( title : String , msg : String , btnActionTitle : String , completion: (Void) ,viewController : UIViewController ){
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        let alertAction = UIAlertAction(title: btnActionTitle, style: .Default, handler: nil)
        let alertAction2 = UIAlertAction(title: btnActionTitle, style: .Default, handler: nil)
        
        
        alertController .addAction(alertAction)
        alertController.addAction(alertAction2)
        viewController.presentViewController(alertController, animated: true) { () -> Void in
            completion
        }
        
    }

    func showAlertWithActionsCompletionHandler(title : String , msg : String , btnActionTitle : String , btnActionTitle2 : String ,action1Completion: (Void) , action2Completion :(Void) ,viewController : UIViewController){
        
        
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        let alertAction = UIAlertAction(title: btnActionTitle, style: .Default) { (alert1) -> Void in
            action1Completion
        }
        
        let alertAction2 = UIAlertAction(title: btnActionTitle2, style: .Default) { (alert2) -> Void in
            action2Completion
        }

        
        alertController .addAction(alertAction)
        alertController.addAction(alertAction2)
        viewController.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    
}


