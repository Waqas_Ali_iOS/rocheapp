//
//  UserModel.swift
//  Roche
//
//  Created by Waqas Ali on 03/10/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import Foundation

class UserModel {
   static let sharedInstance = UserModel()
    
    var userID : Int!
    var userName : String!
    var userEmail : String!
    var userType : userTypes!
    var userQRimage : UIImage!
    var unreadMessageCount : Int!
    var sessionID : String!


}
