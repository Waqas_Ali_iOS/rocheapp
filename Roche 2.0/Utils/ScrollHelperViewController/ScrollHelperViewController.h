//
//  ScrollHelperViewController.h
//  scrollViewTest
//
//  Created by Mohammed Noman Siddiqui on 12/16/15.
//  Copyright © 2015 Traffic JLT. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "HeaderViewController.h"

@interface ScrollHelperViewController : UIViewController <UIScrollViewDelegate>
@property (weak, nonatomic) UIScrollView *baseScrollView;
//@property (weak, nonatomic) UIView *contentView;
@property (weak, nonatomic) UIView *topView;
//@property (weak, nonatomic) UIView *descView;
//@property (weak, nonatomic) UILabel *lblDesc;
//@property (weak, nonatomic) UIView *statusBarView;
@property (weak, nonatomic) UIView *viewBottomBar;
@property (weak, nonatomic) UIView *viewHeaderBar;

-(void)setUpDelegateAndData;
@end