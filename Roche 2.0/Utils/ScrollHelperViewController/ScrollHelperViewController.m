//
//  ScrollHelperViewController.m
//  scrollViewTest
//
//  Created by Mohammed Noman Siddiqui on 12/16/15.
//  Copyright © 2015 Traffic JLT. All rights reserved.
//

#import "ScrollHelperViewController.h"

@interface ScrollHelperViewController (){
    
    CGFloat pastScrollViewYOffset;
    
    
    UIScrollView *SviewObj;
    
}

@property (nonatomic) CGFloat previousScrollViewYOffset;
@end
static CGFloat kParallaxDeltaFactor = 0.5f;
@implementation ScrollHelperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
#define kDefaultHeaderFrame CGRectMake(0, 0, self.topView.frame.size.width, self.topView.frame.size.height)
    
#define kDefaultFooterFrame CGRectMake(self.viewBottomBar.frame.origin.x, self.viewBottomBar.frame.origin.y, self.viewBottomBar.frame.size.width, self.viewBottomBar.frame.size.height)
    
#define kDefaultFooter self.view.frame.size.height - self.viewBottomBar.frame.size.height
    
    
    
    
    

    
    
    //-- //NSLog(@"footer view Y = %f", kDefaultFooter);
    //self.scrollView.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UISCrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    SviewObj = scrollView;
    
    // pass the current offset of the UITableView so that the ParallaxHeaderView layouts the subViews.
    if(self.topView){
        [self layoutHeaderViewForScrollViewOffset:scrollView.contentOffset];
    }
    CGRect frame = self.viewHeaderBar.frame;
    CGRect frameBottomBar = self.viewBottomBar.frame;
    
    CGFloat size = frame.size.height - 0; //20;
    CGFloat sizeScroll = frameBottomBar.size.height;
    //CGFloat framePercentageHidden = ((20 - frame.origin.y) / (frame.size.height - 1));
    CGFloat scrollOffset = scrollView.contentOffset.y *kParallaxDeltaFactor;
    //scrollView.contentOffset.y;
    CGFloat scrollDiff = scrollOffset - self.previousScrollViewYOffset;
    CGFloat scrollHeight = scrollView.frame.size.height;
    CGFloat scrollContentSizeHeight = scrollView.contentSize.height + scrollView.contentInset.bottom;
    
    if (scrollOffset <= -scrollView.contentInset.top) {
        frame.origin.y = 0; //20
        frameBottomBar.origin.y = kDefaultFooter;
        // //NSLog(@"$----first");
    } else if ((scrollOffset + scrollHeight) >= scrollContentSizeHeight) {
        frame.origin.y = -size;
        frameBottomBar.origin.y = kDefaultFooter+ sizeScroll;
        // //NSLog(@"second----$");
    } else {
        //20
        frame.origin.y = MIN(0, MAX(-size, frame.origin.y - scrollDiff));
        frameBottomBar.origin.y = MAX(kDefaultFooter, MAX(-sizeScroll, frameBottomBar.origin.y + scrollDiff));
        
        if(frameBottomBar.origin.y > self.view.frame.size.height){
            frameBottomBar.origin.y = self.view.frame.size.height;
        }
        ////NSLog(@"third");
    }
    
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    
    //  //NSLog(@"bottomEdge = %f scrollView.contentSize.height = %f", bottomEdge,  scrollView.contentSize.height);
    if (bottomEdge >= (scrollView.contentSize.height - frameBottomBar.size.height )) {
        // we are at the end
          NSLog(@"we are at the end scroll helper");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MyScrollUpdatedNotification" object:self];
        
        if (scrollView.contentOffset.y < pastScrollViewYOffset) {
            //  //NSLog(@"down");
        } else if (scrollView.contentOffset.y > pastScrollViewYOffset) {
            ////NSLog(@"up");
            CGFloat scrollDiffForBottom = (scrollView.contentOffset.y - pastScrollViewYOffset) * 1.5;
            frameBottomBar.origin.y =   MAX(kDefaultFooter, (frameBottomBar.origin.y - scrollDiffForBottom));
            
            // frameBottomBar.origin.y = MAX( (frameBottomBar.origin.y + scrollDiff), kDefaultFooter);
        }
        
        
        
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MyScrollUpdatedNotificationWhenNotOnTop" object:self];
    }
    
    
    
    [self.viewHeaderBar setFrame:frame];
    [self.viewBottomBar setFrame:frameBottomBar];
    
    self.previousScrollViewYOffset = scrollOffset;
    pastScrollViewYOffset = scrollView.contentOffset.y;
}


- (void)layoutHeaderViewForScrollViewOffset:(CGPoint)offset
{
    CGRect frame = self.topView.frame;
    
    ////NSLog(@"headerView size height = %f", kDefaultHeaderFrame.size.height);
    
    if (offset.y > 0)
    {
        frame.origin.y = MAX(offset.y *kParallaxDeltaFactor, 0);
        self.topView.frame = frame;
        
    }
    else
    {
        CGFloat delta = 0.0f;
        CGRect rect = kDefaultHeaderFrame;
        delta = fabs(MIN(0.0f, offset.y));
        rect.origin.y -= delta;
        rect.size.height += delta;
        if(rect.size.height <= kDefaultHeaderFrame.size.height){
            self.topView.frame = rect;
        }
    }
}

//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    // [self stoppedScrolling];
//}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    //    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    //
    //    //NSLog(@"bottomEdge = %f scrollView.contentSize.height = %f", bottomEdge,  scrollView.contentSize.height);
    //    if (bottomEdge >= scrollView.contentSize.height) {
    //        // we are at the end
    //            //NSLog(@"we are at the end");
    //    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        // [self stoppedScrolling];
    }
}

- (void)stoppedScrolling
{
    CGRect frame = self.viewHeaderBar.frame;
    if (frame.origin.y < 0) {
        //[self animateNavBarTo:-(frame.size.height - 0)];
    }
    
    //     CGRect frameScroll = self.scrollView.frame;
    //
    //    if (frameScroll.origin.y < 64) {
    //        [self animateNavBarTo:-(frameScroll.size.height - 65)];
    //    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    SviewObj = scrollView;
}

- (void)updateBarButtonItems:(CGFloat)alpha
{
    [self.navigationItem.leftBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem* item, NSUInteger i, BOOL *stop) {
        item.customView.alpha = alpha;
    }];
    [self.navigationItem.rightBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem* item, NSUInteger i, BOOL *stop) {
        item.customView.alpha = alpha;
    }];
    self.navigationItem.titleView.alpha = alpha;
    self.navigationController.navigationBar.tintColor = [self.navigationController.navigationBar.tintColor colorWithAlphaComponent:alpha];
}

- (void)animateNavBarTo:(CGFloat)y
{
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.viewHeaderBar.frame;
        //  CGFloat alpha = (frame.origin.y >= y ? 0 : 1);
        frame.origin.y = y;
        [self.viewHeaderBar setFrame:frame];
        // [self updateBarButtonItems:alpha];
    }];
}


-(void)setUpDelegateAndData
{
    //NSLog(@"width of the view = %f", self.view.frame.size.width);
    // //NSLog(@"width of the Scroll view = %f", self.scrollView.frame.size.width);
    // //NSLog(@"width of the Content view = %f", self.contentView.frame.size.width);
    //NSLog(@"width of the image view = %f", self.topView.frame.size.width);
    // //NSLog(@"width of the decription view = %f", self.descView.frame.size.width);
    
    //self.scrollView.delegate = self;
    
}



-(void)dealloc{
    // self.scrollView = nil;
    // self.contentView = nil;
    self.topView = nil;
    // self.descView = nil;
    // self.lblDesc = nil;
    // self.statusBarView = nil;
    self.viewBottomBar = nil;
    self.viewHeaderBar = nil;
    //NSLog(@"dealloc called ScrollHelperViewController");
}

@end
