//
//  FontCategorySwift.swift
//  SampleSwift
//
//  Created by  Traffic MacBook Pro on 5/17/16.
//  Copyright © 2016 Traffic MacBook Pro. All rights reserved.
//

import UIKit
extension UIFont{
    
    
    func setGeneralFontSizeUsingRatio(name : String , psdFontSize : CGFloat) ->UIFont{
        
        var increaseInfont : Int = 0
        
        var ratio : CGFloat
        
        //iphone
        
        if GlobalStaticMethods.isPhone() {
            ratio = psdFontSize/1242
            
        }
        else{
            if constants.deviceType.SCREEN_HEIGHT > 1024 {
                ratio = psdFontSize/2732

            }
            else{
                
                let scale = UIScreen.mainScreen().scale
                
                if scale == 1 {
                    increaseInfont = 3
                }
                
                
                ratio = psdFontSize/2048 //(1024 * scale)
            }
        }
        //ipad pro
        
        //ipad
        
        
        var screenSize : CGFloat
        
        let screenRect : CGRect = UIScreen.mainScreen().bounds
        
        let screenWidth: CGFloat = screenRect.size.width
        
        let screenHeight: CGFloat = screenRect.size.height
        
        
        if screenWidth<screenHeight {
            screenSize = screenWidth;
        }
        else{
            screenSize = screenHeight;
            
        }
        
        var fontsize = (screenSize * ratio) + CGFloat(increaseInfont)
        
        if fontsize < 12 {
            fontsize = 12
        }
        
        
        debugPrint(self.fontName)
        
        
        var font = ""
        
        if (self.fontName == "Tahoma-Bold")
        {
            font = "Imago-Medium"
        }else if self.fontName == "Tahoma" {
            
            font = "Imago-Book"
        }
        else
        {
            font = "Imago-Book"
        }
        
        
        return UIFont(name: font, size: fontsize)!
        
        
        
    }
}