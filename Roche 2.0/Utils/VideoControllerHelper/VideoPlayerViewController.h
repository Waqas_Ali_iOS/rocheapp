//
//  VideoPlayerViewController.h
//  VideoStreamTest
//
//  Created by Traffic-Shaheer on 24/02/2016.
//  Copyright © 2016 Traffic JLT. All rights reserved.
//

#import <AVKit/AVKit.h>

@interface VideoPlayerViewController : AVPlayerViewController


@property(nonatomic, weak) NSString *videoUrlString;
@property(nonatomic, weak) NSString *videoServerId;
@property(nonatomic, weak)   NSURLSessionDownloadTask *downloadTask;

-(void)canellAllOperations;
@end
