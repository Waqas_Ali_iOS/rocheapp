//
//  VideoPlayerViewController.m
//  VideoStreamTest
//
//  Created by Traffic-Shaheer on 24/02/2016.
//  Copyright © 2016 Traffic JLT. All rights reserved.
//

#import "VideoPlayerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>



@interface VideoPlayerViewController ()<NSURLConnectionDataDelegate, AVAssetResourceLoaderDelegate>
{

    
    BOOL isSeekInProgress;
    CMTime chaseTime;
    AVPlayerStatus playerCurrentItemStatus; // your player.currentItem.status
    
  
}

@property (nonatomic, strong) AVURLAsset *vidAsset;
@property (nonatomic, strong) AVPlayerItem *playerItem;
@end

@implementation VideoPlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
  self.vidAsset = [AVURLAsset URLAssetWithURL:[self getRemoteVideoURL] options:nil];
    
         // Init Player Item
         self.playerItem = [AVPlayerItem playerItemWithAsset:self.vidAsset];
     //NSLog(@"%@", [single getProjectPrefix]);

   //--wwNSMutableDictionary * vdData = [DeviceUtil readPlistFileForData];
    
    
    
    
         self.player = [[AVPlayer alloc] initWithPlayerItem:self.playerItem];
   
    self.showsPlaybackControls = YES;
  //  NSURL *videoURL = [NSURL URLWithString:@"https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"];
   // self.player = [AVPlayer playerWithURL:videoURL];
    
    //self.player = player;
    [self.player play];
    
    
}


-(void)hideThisController{

[self dismissViewControllerAnimated:YES completion:^{
   // [Singleton showMessage:NETWORK_ERROR withTitle:NETWORK_ERROR withController:self withCompletion:nil];
   
}];
}



-(void)viewDidAppear:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

-(BOOL)preferStatusBarHidden {
    return YES;
}
    
  
    
-(void)viewDidDisappear:(BOOL)animated {

    self.player = nil;
    [_downloadTask cancel];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






- (NSURL *)getRemoteVideoURL
    {
        NSString *urlString = [self URLEncodeStringFromString :self.videoUrlString];
        return [NSURL URLWithString:urlString];
    }


- (NSString * )URLEncodeStringFromString:(NSString *)string
{
    return [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
    
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    //return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
    return UIInterfaceOrientationMaskAll;
}

-(void)canellAllOperations{
    self.player = nil;
    [_downloadTask cancel];

}

/*
- (void)stopPlayingAndSeekSmoothlyToTime:(CMTime)newChaseTime
{
    [self.player pause];
    
    if (CMTIME_COMPARE_INLINE(newChaseTime, !=, self->chaseTime))
    {
        self->chaseTime = newChaseTime;
        
        if (!self->isSeekInProgress)
            [self trySeekToChaseTime];
    }
}

- (void)trySeekToChaseTime
{
    if (playerCurrentItemStatus == AVPlayerItemStatusUnknown)
    {
        // wait until item becomes ready (KVO player.currentItem.status)
    }
    else if (playerCurrentItemStatus == AVPlayerItemStatusReadyToPlay)
    {
        [self actuallySeekToTime];
    }
}

- (void)actuallySeekToTime
{
    self->isSeekInProgress = YES;
    CMTime seekTimeInProgress = self->chaseTime;
    [self.player seekToTime:seekTimeInProgress toleranceBefore:kCMTimeZero
              toleranceAfter:kCMTimeZero completionHandler:
     ^(BOOL isFinished)
     {
         if (CMTIME_COMPARE_INLINE(seekTimeInProgress, ==, self->chaseTime))
             self->isSeekInProgress = NO;
         else
             [self trySeekToChaseTime];
     }];
}
 */

-(void)dealloc
{
    
    //NSLog(@"dealloc::%@", NSStringFromClass([self class]));
}
@end
